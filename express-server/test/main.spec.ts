
process.env.NODE_ENV = 'test';
import { app } from '../src/index';

import chai from 'chai';
import chaiHttp from 'chai-http';
import {} from 'mocha';

import mongoose from 'mongoose';
import dayjs from 'dayjs';
import util from 'util';

const expect = chai.expect;  
chai.use( chaiHttp );

import { AdminModel, IAdminModel } from './../src/models/admin';
import { BranchModel, IBranchModel } from './../src/models/branch';
import { EmployeeGroupModel, IEmployeeGroupModel, Escreens } from './../src/models/employeeGroup';
import { EmployeeModel, IEmployeeModel } from './../src/models/employee';
import { ICarTypeModel, CarTypeModel } from './../src/models/carType';
import { ICarGroupModel, CarGroupModel } from './../src/models/carGroup';
import { CarModel , ICarModel, EgearType, Estatus } from './../src/models/car';
import { BookingModel } from './../src/models/booking';
import { IClientModel, ClientModel } from './../src/models/client';


import { $enum } from "ts-enum-util";
import { ICarModelModel, CarModelModel } from '../src/models/carModel';
import { IBookingModel } from '../src/models/booking';

before(function() { 
    this.timeout(10000);
    return new Promise( async (resolve) => {

        await EmployeeModel.find({}).remove().exec();
        await EmployeeGroupModel.find({}).remove().exec();
        await AdminModel.find({}).remove().exec();
        await BranchModel.find({}).remove().exec();
        await CarTypeModel.find({}).remove().exec();
        await CarModelModel.find({}).remove().exec();

        await ClientModel.find({}).remove().exec();

        return resolve();

    });
});

interface Itokens {
    admin: string;
    employee: string;
    client: string;
};
let tokens: Itokens = {
    admin: null,
    employee: null,
    client: null,
};

let admin: IAdminModel|any = {
    _id : null,
    email : 'ahmed@gmail.com',
    password: '123456789',
};

let employee: IEmployeeModel|any = {
    _id : null,
    employeeGroup_id : null ,
    name: 'Ahmed',
    email : 'ahmed@gmail.com',
    password: '123456789'
};

let employeeGroup: IEmployeeGroupModel|any = {
    _id : null,
    branches : [] ,
    nameEn: 'Main Employee Group',
    nameAr: 'جروب الموظفين الرئيسي',
    screens : []
};

let car: ICarModel|any = {
    _id : null,
    branch_id: null,
    carGroup_id: null,
    plateNumber: '123-ABC',
    gearType: EgearType.automatic,
    status: Estatus.available,
};

let branch: IBranchModel|any = {
    _id : null,
    nameEn :'Main Branch',
    nameAr : 'الفرع الرئيسي',
    addressEn : 'address En',
    addressAr : 'العنوان',
    overviewEn : '<h1>hello</h1>',
    overviewAr : '<h1>اهلا</h1>',
    phone : '01205770515',
    location : [29.5 , 30.2],
    bookingStartCode : 123,
    waitingHoursForConfirm : 3,
    city_id : '5b81731242fb825b140975e2'
};

let carGroup: ICarGroupModel|any = {
    _id : null,
    carType_id : null,
    carModel_id : null,
    branch_id : null,
    year : 2018,

    allowedKm : 1000,
    plusKmPrice : 250,

    pricePerDay : 250,
    discountPerDay : 10,

}

let carType: ICarTypeModel|any = {
    _id : null,
    nameEn : 'hynda',
    nameAr : 'هايوندا'
};

let carModel: ICarModelModel|any = {
    _id : null,
    carType_id : null,
    nameEn : 'hynda',
    nameAr : 'هايوندا'
};

let client: IClientModel|any = {
    _id: null,
    name : 'Ahmed',
    email : 'ahmed@gmail.com',
    phone : '01205770515',
    password : '123456789',

    nationality: mongoose.Types.ObjectId(),
        
    iqama: '515154115151',
    iqamaExpiryDate: new Date(),
    
    drivingLicence: '515154115151',
    drivingLicenceExpiryDate: new Date(),
    
    birthday: new Date(),
    
    address: 'string',
    job: 'string',
};

let booking: IBookingModel|any = {
    _id : null,
    client_id : null,
    carGroup_id : null,
    pickUpBranch_id : null,
    returnBranch_id : null,
    pickUpDate : new Date(2018,7,2)  ,
    returnDate : new Date(2018,7,12),
    daysCount : 5,
    pricePerDay : 250,
    discountPerDay : 100,
    iqama : client.iqama,
    bookingStartCode_branch : '123',
    waitingDaysForConfirm_branch : 3
};

describe('Admin' , () => {
    it('Signup Admin' , done => {
        chai.request(app)
        .post('/api/admin/signup')
        .send({
            ...admin
        })
        .end( (_err, res) => {
            expect(res.status).equal(200);
            expect(res.body.token).to.be.a('string');
            expect(res.body.admin._id).to.be.a('string');
            
            tokens.admin = res.body.token;
            admin._id = res.body.admin._id;

            return done();
        });
    });
    it('Signin Admin' , done => {
        chai
        .request(app)
        .post('/api/admin/signin')
        .send({
            email : admin.email,
            password : admin.password,
        })
        .end( (_err, res) => {
            expect(res.status).equal(200);
            expect(res.body.token).to.be.a('string');
            expect(res.body.admin._id).to.be.a('string');

            return done();
        });

    });
    it('Admin Refresh Token' , done => {
        chai
        .request(app)
        .get('/api/admin/refresh-token')
        .send({
            email : admin.email,
            password : admin.password,
        })
        .set('token' , tokens.admin)
        .end( (_err, res) => {
            expect(res.status).equal(200);
            expect(res.body.token).to.be.a('string');
            return done();
        });

    });
    it('Add Employee Group' , done => {
        chai
        .request(app)
        .post('/api/admin/employee-group/add')
        .send({
            ...employeeGroup
        })
        .set('token' , tokens.admin)
        .end( (_err, res) => {
            expect(res.status).equal(200);
            expect(res.body._id).to.be.a('string');

            employeeGroup._id = res.body._id;
            employee.employeeGroup_id = res.body._id;

            return done();
        });

    });
    it('Add Employee' , done => {
        chai
        .request(app)
        .post('/api/admin/employee/add')
        .send({
            ...employee
        })
        .set('token' , tokens.admin)
        .end( (_err, res) => {
            expect(res.status).equal(200);
            expect(res.body._id).to.be.a('string');

            employee._id = res.body._id;

            return done();
        });

    });
    it('Edit Employee Group' , done => {
        chai.request(app)
        .put('/api/admin/employee-group/edit')
        .send({
            employeeGroup_id : employeeGroup._id,
            screens : $enum( Escreens ).getValues() 
        })
        .set('token' , tokens.admin)
        .end( (_err, res) => {
            expect(res.status).equal(200);
            return done();
        });

    });
    it('Edit Employee' , done => {
        chai.request(app)
        .put('/api/admin/employee/edit')
        .send({
            employee_id : employee._id,
            name : 'Ali'
        })
        .set('token' , tokens.admin)
        .end( (_err, res) => {
            expect(res.status).equal(200);
            return done();
        });

    });
    it('Edit Employee Password' , done => {
        chai.request(app)
        .put('/api/admin/employee/edit-password')
        .send({
            employee_id : employee._id,
            password : '123456789'
        })
        .set('token' , tokens.admin)
        .end( (_err, res) => {
            expect(res.status).equal(200);
            return done();
        });

    });
    it('Delete Employee' , done => {
        chai.request(app)
        .del('/api/admin/employee/delete')
        .query({
            employee_id : employee._id,
        })
        .set('token' , tokens.admin)
        .end( (_err, res) => {
            expect(res.status).equal(200);
            return done();
        });
    });
    it('Restore Employee' , done => {
        chai.request(app)
        .post('/api/admin/employee/restore')
        .send({
            employee_id : employee._id,
        })
        .set('token' , tokens.admin)
        .end( (_err, res) => {
            expect(res.status).equal(200);
            return done();
        });
    });
    it('Delete Employee Group' , done => {
        chai.request(app)
        .del('/api/admin/employee-group/delete')
        .query({
            employeeGroup_id : employeeGroup._id,
        })
        .set('token' , tokens.admin)
        .end( (_err, res) => {
            expect(res.status).equal(200);
            return done();
        });
    });
    it('Restore Employee Group' , done => {
        chai.request(app)
        .post('/api/admin/employee-group/restore')
        .send({
            employeeGroup_id : employeeGroup._id,
        })
        .set('token' , tokens.admin)
        .end( (_err, res) => {
            expect(res.status).equal(200);
            return done();
        });
    });
});

describe('Employee' , () => {
    describe('This' , () => {
        it('Signin Employee' , done => {
            chai.request(app)
            .post('/api/employee/signin')
            .send({
                ...employee
            })
            .end( (_err, res) => {
                expect(res.status).equal(200);
                expect(res.body.token).to.be.a('string');
                expect(res.body.employee._id).to.be.a('string');
                
                tokens.employee = res.body.token;
                employee._id = res.body.employee._id;

                return done();
            });
        });
        it('Employee Refresh Token' , done => {
            chai
            .request(app)
            .get('/api/employee/refresh-token')
            .send({
                email : employee.email,
                password : employee.password,
            })
            .set('token' , tokens.employee)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                expect(res.body.token).to.be.a('string');
                return done();
            });

        });
        it('Edit This Employee' , done => {
            chai.request(app)
            .put('/api/employee/edit-this-employee')
            .send({
                name : 'Mhmd',
            })
            .set('token' , tokens.employee)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                return done();
            });
        });
        it('Edit This Employee' , done => {
            chai.request(app)
            .put('/api/employee/edit-this-employee-password')
            .send({
                password : '123456789'
            })
            .set('token' , tokens.employee)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                return done();
            });
        });
    });
    describe('Branch' , () => {
        it('Add Branch' , done => {
            chai.request(app)
            .post('/api/employee/branch/add')
            .send({
                ...branch
            })
            .set('token' , tokens.employee)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                expect(res.body._id).to.be.a('string');
                branch._id = res.body._id;
                return done();
            });
        });
        it('Add Branch To Employee Group' , done => {
            chai.request(app)
            .put('/api/admin/employee-group/edit')
            .send({
                employeeGroup_id : employeeGroup._id,
                branches : [ branch._id]
            })
            .set('token' , tokens.admin)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                return done();
            });
    
        });
        it('Edit Branch' , done => {
            chai.request(app)
            .put('/api/employee/branch/edit')
            .send({
                branch_id : branch._id,
                nameEn : 'name En'
            })
            .set('token' , tokens.employee)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                return done();
            });
        });
        it('Delete Branch' , done => {
            chai.request(app)
            .del('/api/employee/branch/delete')
            .query({
                branch_id : branch._id,
            })
            .set('token' , tokens.employee)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                return done();
            });
        });
        it('Restore Branch' , done => {
            chai.request(app)
            .post('/api/employee/branch/restore')
            .send({
                branch_id : branch._id,
            })
            .set('token' , tokens.employee)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                return done();
            });
        });
    });
    describe('Car Type' , () => {
        it('Add Car Type' , done => {
            chai.request(app)
            .post('/api/employee/car-type/add')
            .send({
                ...carType
            })
            .set('token' , tokens.employee)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                carType._id = res.body._id;
                return done();
            });
        });
    });
    describe('Car Model' , () => {
        it('Add Car Model' , done => {
            carModel.carType_id = carType._id;
            chai.request(app)
            .post('/api/employee/car-model/add')
            .send({
                ...carModel
            })
            .set('token' , tokens.employee)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                expect(res.body._id).to.be.a('string');
                carModel._id = res.body._id;
                return done();
            });
        });
    });
    describe('Car Group' , () => {
        it('Add Car Group' , done => {
            carGroup.branch_id = branch._id;
            carGroup.carType_id = carType._id;
            carGroup.carModel_id = carModel._id;
            chai.request(app)
            .post('/api/employee/car-group/add')
            .send({
                ...carGroup
            })
            .set('token' , tokens.employee)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                expect(res.body._id).to.be.a('string');
                carGroup._id = res.body._id;
                return done();
            });
        });
        it('Edit Car Group' , done => {
            chai.request(app)
            .put('/api/employee/car-group/edit')
            .send({
                carGroup_id : carGroup._id,
                discountPerDay : '100'
            })
            .set('token' , tokens.employee)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                return done();
            });
        });
        it('Delete Car Group' , done => {
            chai.request(app)
            .del('/api/employee/car-group/delete')
            .query({
                carGroup_id : carGroup._id,
            })
            .set('token' , tokens.employee)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                return done();
            });
        });
        it('Restore Car Group' , done => {
            chai.request(app)
            .post('/api/employee/car-group/restore')
            .send({
                carGroup_id : carGroup._id,
            })
            .set('token' , tokens.employee)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                return done();
            });
        });
    });
    describe('Car' , () => {
        it('Add Car' , done => {
            car.carGroup_id = carGroup._id;
            chai.request(app)
            .post('/api/employee/car/add')
            .send({
                ...car
            })
            .set('token' , tokens.employee)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                expect(res.body._id).to.be.a('string');
                car._id = res.body._id;
                return done();
            });
        });
        it('Edit Car' , done => {
            chai.request(app)
            .put('/api/employee/car/edit')
            .send({
                car_id : car._id,
                status : Estatus.accident
            })
            .set('token' , tokens.employee)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                return done();
            });
        });
        it('Delete Car' , done => {
            chai.request(app)
            .del('/api/employee/car/delete')
            .query({
                car_id : car._id,
            })
            .set('token' , tokens.employee)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                return done();
            });
        });
        it('Restore Car' , done => {
            chai.request(app)
            .post('/api/employee/car/restore')
            .send({
                car_id : car._id,
            })
            .set('token' , tokens.employee)
            .end( (_err, res) => {
                expect(res.status).equal(200);
                return done();
            });
        });
    });
});

describe('Client' , () => {
    it('Signup Client' , done => {
        chai.request(app)
        .post('/api/client/signup')
        .send({
            ...client
        })
        .end( (_err, res) => {
            expect(res.status).equal(200);
            expect(res.body.token).to.be.a('string');
            expect(res.body.client._id).to.be.a('string');
            
            tokens.client = res.body.token;
            client._id = res.body.client._id;

            return done();
        });
    });
    it('Signin Client' , done => {
        chai
        .request(app)
        .post('/api/client/signin')
        .send({
            emailOrPhone : client.email,
            password : client.password,
        })
        .end( (_err, res) => {
            expect(res.status).equal(200);
            expect(res.body.token).to.be.a('string');
            expect(res.body.client._id).to.be.a('string');

            return done();
        });

    });
    it('Client Refresh Token' , done => {
        chai
        .request(app)
        .get('/api/client/refresh-token')
        .send({
            email : client.email,
            password : client.password,
        })
        .set('token' , tokens.client)
        .end( (_err, res) => {
            expect(res.status).equal(200);
            expect(res.body.token).to.be.a('string');
            return done();
        });

    });
    it('Edit Client' , done => {
        chai
        .request(app)
        .put('/api/client/edit')
        .send({
           ...client
        })
        .set('token' , tokens.client)
        .end( (_err, res) => {
            expect(res.status).equal(200);
            return done();
        });

    });
    it('Edit Client Password' , done => {
        chai
        .request(app)
        .put('/api/client/edit-password')
        .send({
            password: '1234567788',
        })
        .set('token' , tokens.client)
        .end( (_err, res) => {
            expect(res.status).equal(200);
            return done();
        });

    });

});

describe('Main' , () => {

    // 1
    it('Client Booking a Car' , done => {
        
        booking.client_id = client._id;
        booking.carGroup_id = carGroup._id;
        booking.pickUpBranch_id = branch._id;
        booking.returnBranch_id = branch._id;

        chai.request(app)
        .post('/api/client/add-booking')
        .send({
            ...booking
        })
        .set('token' , tokens.client)
        .end( (_err, res) => {
            expect(res.status).equal(200);

            booking._id = res.body._id; 

            return done();
        });
    });
    it('Employee set Car In Booking' , done => {
        chai.request(app)
        .post('/api/employee/booking/add-car')
        .send({
            booking_id : booking._id,
            car_id : car._id,
        })
        .set('token' , tokens.employee )
        .end( (_err, res) => {
            expect(res.status).equal(200);
            return done();
        });
    });
    it('Employee Approve Booking' , done => {
        chai.request(app)
        .post('/api/employee/booking/approve')
        .send({
            booking_id : booking._id,
        })
        .set('token' , tokens.employee )
        .end( (_err, res) => {
            expect(res.status).equal(200);
            return done();
        });
    });
    it('Employee Active Booking' , done => {
        chai.request(app)
        .post('/api/employee/booking/active')
        .send({
            booking_id : booking._id,
        })
        .set('token' , tokens.employee )
        .end( (_err, res) => {
            expect(res.status).equal(200);
            return done();
        });
    });

    // 2
    it('Client Booking a Car 2' , done => {
        
        booking.client_id = client._id;
        booking.carGroup_id = carGroup._id;
        booking.pickUpBranch_id = branch._id;
        booking.returnBranch_id = branch._id;

        chai.request(app)
        .post('/api/client/add-booking')
        .send({
            ...booking,
            waitingDaysForConfirm_branch : 0,
            pickUpDate : new Date(2018,1,17),
            returnDate : new Date(2018,1,22),
        })
        .set('token' , tokens.client)
        .end( (_err, res) => {
            expect(res.status).equal(200);
            booking._id = res.body._id; 
            return done();
        });
    });
    it('Employee set Car In Booking 2' , done => {
        chai.request(app)
        .post('/api/employee/booking/add-car')
        .send({
            booking_id : booking._id,
            car_id : car._id,
        })
        .set('token' , tokens.employee )
        .end( (_err, res) => {
            expect(res.status).equal(200);
            return done();
        });
    });
    it('Employee Approve Booking 2' , done => {
        chai.request(app)
        .post('/api/employee/booking/approve')
        .send({
            booking_id : booking._id,
        })
        .set('token' , tokens.employee )
        .end( (_err, res) => {
            expect(res.status).equal(200);
            return done();
        });
    });
    it('Employee Active Booking' , done => {
        chai.request(app)
        .post('/api/employee/booking/active')
        .send({
            booking_id : booking._id,
        })
        .set('token' , tokens.employee )
        .end( (_err, res) => {
            expect(res.status).equal(200);
            return done();
        });
    });

    it('View CarGroup' , done => {
        chai.request(app)
        .get('/api/employee/view/car-group')
        .query({
            carGroup_id : carGroup._id,
        })
        .set('token' , tokens.employee )
        .end( (_err, res) => {

            // console.log(JSON.stringify(res.body , null , 2 ));

            return done();
        });
    });

    it('View CarGroups' , done => {
        chai.request(app)
        .get('/api/employee/view/car-groups')
        .query({
            page : 1,
            limit : 16,
            sortBy : 'createdAt'
        })
        .set('token' , tokens.employee )
        .end( (_err, res) => {

            // console.log(JSON.stringify(res.body , null , 2 ));
            return done();
        });
    });

    it('View Car' , done => {
        chai.request(app)
        .get('/api/employee/view/car')
        .query({
            car_id : car._id,
        })
        .set('token' , tokens.employee )
        .end( (_err, res) => {

            // console.log(JSON.stringify(res.body , null , 2 ));
            return done();
        });
    });

    it('View Cars' , done => {
        chai.request(app)
        .get('/api/employee/view/cars')
        .query({
            page : 1,
            limit : 16,
            sortBy : 'createdAt'
        })
        .set('token' , tokens.employee )
        .end( (_err, res) => {

            // console.log(JSON.stringify(res.body , null , 2 ));

            return done();

        });
    });


    it('View Booking' , done => {
        chai.request(app)
        .get('/api/employee/view/booking')
        .query({
            booking_id : booking._id,
        })
        .set('token' , tokens.employee )
        .end( (_err, res) => {

            return done();
        });
    });

});
