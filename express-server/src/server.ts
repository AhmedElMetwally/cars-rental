import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import morgan from 'morgan';
import cors from 'cors';
import http from 'http';
import socketIo from 'socket.io';
import dotenv from 'dotenv';
import expressValidator from 'express-validator';

import { SocketIoEmployee } from './socket.io/socketIoEmployee';
import { ClientRouter } from './routes/client';
import { AdminRouter } from './routes/admin';
import { EmployeeRouter } from './routes/employee';
import { expressValidatorConfig } from './helpers/expressValidator';
import { log , isLog } from './helpers/log';
import { GuestRouter } from './routes/guest';
import express , { Request, Response } from 'express';

dotenv.config({ path: '.env' });

export class Server {

  private app: express.Application;
  private HttpServer: any;

  constructor() {
    this.init();
  };

  private init() {
    
    // create express new app 
    this.app = express();


    // http server
    this.HttpServer = http.createServer(this.app);


    // init socket.io
    const socketIoEmployee = new SocketIoEmployee();
    const socketIoServer = socketIo(this.HttpServer);


    // middleware
    this.app.set('etag' , false); 
    this.app.set('x-powered-by' , false); 
    this.app.use(cors());
    // hide http logs on hide logs 
    if ( isLog() ){
      this.app.use(morgan('dev'));
    }; 
    this.app.use(bodyParser.urlencoded({ extended : false }));
    this.app.use(bodyParser.json());
    this.app.use(expressValidator( expressValidatorConfig ) );

    // api routes
    this.app.use('/api/client' , ClientRouter );
    this.app.use('/api/admin' , AdminRouter );
    this.app.use('/api/employee', EmployeeRouter );
    this.app.use('/api/guest', GuestRouter );


    if(process.env.NODE_ENV === 'development'){
      this.app.use('/images' , express.static( process.env.IMAGES_SRC));
    };

    // error message on fail Request
    this.app.use('*' , (_req: Request , res: Response): void => {
      res.status(404).send('Not Found');
    });

    
    // listen 
    // mongoose
    mongoose.connect( process.env.MONGO_URL , {useNewUrlParser: true , autoReconnect: true} )
    .then( () => log(`  Mongoose Work [${process.env.MONGO_URL}]`) )
    .catch(err => {
      throw err;
    })
    
    // http
    this.HttpServer.listen(  process.env.HTTP_PORT  ,  
      () => log(`  HTTP Server Work`)
    );
    // socket.io
    socketIoEmployee.init( socketIoServer , 
      ( ) => log(`  Socket.io Server Work [/employee]`)
    );


    // on SIGINT close mongoose
    process.on('SIGINT', () => {  
      mongoose.connection.close(() => {
        process.exit();
      }); 
    }); 


  };

  public getApp(): express.Application {
    return this.app;
  };

};


