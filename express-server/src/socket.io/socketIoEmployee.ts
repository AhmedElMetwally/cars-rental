import socketIo from 'socket.io';
import { log } from '../helpers/log';
import { redisClient } from '../helpers/redis';
import { EmployeeTimerModel } from '../models/employeeTimer';
import { jwt } from '../helpers/jwt';
import { dateHelper } from '../helpers/dateHelper' 

export class SocketIoEmployee {

    constructor(){};
    
    // init socket.io server
    public init( socketIoServer: socketIo.Server , cb: any ): void {
        
        // clear redis
        this.clearAllOnlineEmployees_socketId();
        this.clearAllOnlineEmployees_openIn();

        // use /employee namespace
        const io: socketIo.Namespace = socketIoServer
        .of('/employee')
        .use( this.employeeGuard );

        // on new connect
        io.on('connection' , async ( socket: socketIo.Socket|any ) => {
         
            this.onConnect( socket.employee._id , socket.client.id );
            log(`employee connect to socket.io [${socket.employee._id}] [${dateHelper.getThisDay()}]`);

            // on disConnect
            socket.on('disconnect' , async () => {
                this.onDisconnect( socket.employee._id , socket.client.id );
                log(`employee disconnect from socket.io [${socket.employee._id}] [${dateHelper.getThisDay()}]`)
            });

        });

        cb();
    }; 

    private employeeGuard = jwt.allaw_socket_io({
        roles : ['employee'],
        dataField : 'employee',
        query_name_of_token : 'token',
        set_id_to_socket_from : '_id'
    });
    private clearAllOnlineEmployees_socketId = (): void => {
        redisClient.keys('socketsIds_employee_id_*' , ( _err , result ) => {
            
            result.forEach( v => {
                redisClient.del( v );
            })

        })
    };
    private clearAllOnlineEmployees_openIn = (): void => {
        redisClient.del('onlines_employees' , () => {});
    };
    private onConnect = (employee_id: string , socketId: string) => {
        // set employee openIn if not exist
        // push this socket.id in this employee list 
        redisClient
        .multi()
        .hsetnx('onlines_employees' , employee_id , new Date().getTime().toString() )
        .lpush(`socketsIds_employee_id_${employee_id}` , socketId )
        .exec(() => {});
    };
    private onDisconnect = (employee_id: string , socketId: string) => {
        // get employee openIn
        // delete socket.id from employee socket.io list
        // get length of this employee socket.io ( if connect from many pages )
        redisClient
        .multi()
        .hget('onlines_employees' , employee_id )
        .lrem(`socketsIds_employee_id_${employee_id}` , 1 , socketId )
        .llen(`socketsIds_employee_id_${employee_id}`)
        .exec((err , res ) => {

            // if any error stop here
            if( err ) return;
            if( ! res[0] ) return;
            if( res[2] !== 0 ) return; 
            
            // this is last connect
            // save diff (timeMs)
            // then delete this employee first connect date


            // get diff between first connect and now
            const timeMs = new Date().getTime() - res[0]
            const date = dateHelper.getThisDay();
            // save if this first connect in this day
            // update if this not fitst connect in this day
            EmployeeTimerModel.updateOne({
                employee_id,
                date,
            } , {
                employee_id,
                date,
                $inc : {
                    timeMs
                },
            } , {
                upsert : true
            }).exec(() => {});

            // delete this employee first connect date
            redisClient.hdel('onlines_employees' , employee_id , () => {});

        });
    };

    // use in express
    public getAllOnlineEmployees_ids = (): Promise<string[]> => {
        return new Promise( ( resolve ) => {
            redisClient.hkeys('onlines_employees' , ( err , result ) => {
                return resolve( result );
            });
        });
    };
    public isEmployeeOnline = (employee_id: string): Promise<Number> => {
        return new Promise( ( resolve ) => {
            redisClient.hget('onlines_employees' ,  employee_id.toString()  , ( err , result  ) => {
                if( err ) return resolve( 0 );
                if( result == null ) return resolve( 0 );
                return resolve( 1 );;
            });
        });
    };


};




