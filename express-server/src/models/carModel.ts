import { ICarTypeModel } from './carType';
import { mongooseSchemaOptions } from './mongooseSchemaOptions';
import mongoose from 'mongoose';

const CarModelSchema: mongoose.Schema = new mongoose.Schema({
    
    carType_id : {
        type : mongoose.SchemaTypes.ObjectId,
        ref : 'tb_carType',
        required : 1,
    },

    nameAr : {
        type      : String,
        required  : 1,
        unique    : 1,
        trim      : 1,
    },

    nameEn : {
        type      : String,
        required  : 1,
        unique    : 1,
        trim      : 1,
    },
    
    // 0 not deleted
    // 1 deleted
    deleted : {
        type : Number,
        default : 0,
    },
    
} , mongooseSchemaOptions );

export const CarModelModel = mongoose.model<ICarModelModel>( 'tb_carModel', CarModelSchema); 

export interface ICarModelModel extends mongoose.Document {
    
    carType_id: string; // ref to CarTypeModel
    
    nameAr: string;
    nameEn: string;
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};


export const RegExpCarModelModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    nameEn : /^[\w-\s]+$/,
    nameAr : /^[\u0600-\u06FF\w-\s]+$/,

    deleted : /^[0-1]$/, 
};


