import { IBookingModel } from './booking';
import { IGuestModel } from './guest';
import { IClientModel } from './client';
import { mongooseSchemaOptions } from './mongooseSchemaOptions';
import mongoose from 'mongoose';

const OnlinePaymentSchema: mongoose.Schema = new mongoose.Schema({
    
    client : {
        type : mongoose.SchemaTypes.ObjectId,
        ref : 'tb_client',
        required : 1
    },
    
    guest : {
        type : mongoose.SchemaTypes.ObjectId,
        ref : 'tb_guest',
        required : 1
    },

    booking : {
        type : mongoose.SchemaTypes.ObjectId,
        ref : 'tb_booking',
        required : 1
    } ,

    bookingCode : {
        type : String,
        required : 1,
        unique : 1 ,
    },

    iqama : {
        type : String,
        trim : 1,
    },

    daysCount : {
        type : Number,
        required : 1
    },
    pricePerDay : {
        type : Number,
        required : 1
    },
    discountPerDay : {
        type : Number,
        required : 1
    },
    total : {
        type : Number,
        required : 1
    },
    
    // 0 not deleted
    // 1 deleted
    deleted : {
        type : Number,
        default : 0,
    }

}, mongooseSchemaOptions );

export const OnlinePaymentModel = mongoose.model<IOnlinePaymentModel>('tb_onlinePayment', OnlinePaymentSchema); 


export interface IOnlinePaymentModel extends mongoose.Document {
    
    client: string|null|IClientModel;
    guest: string|null|IGuestModel;
    booking: string|null|IBookingModel;

    bookingCode: string;
    iqama: string;

    daysCount: number;
    pricePerDay: number;
    discountPerDay: number;
    total: number;

    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export const RegExpOnlinePaymentModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    
    bookingCode : /^[0-9]{3}-[0-9]{1,9}$/, // 999-999999999
    iqama : /^[0-9]{1,30}$/, // 30 length of Number
    
    daysCount : /^[1-9]([0-9]{0,3})$/, // 9999
    pricePerDay : /^[1-9]([0-9]{0,3})$/, // 9999
    discountPerDay : /^[1-9]([0-9]{0,3})$/, // 9999
    total : /^[1-9]([0-9]{0,5})$/, // 999999

    deleted : /^[0-1]$/, 
};

