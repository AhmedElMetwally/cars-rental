import { mongooseSchemaOptions } from './mongooseSchemaOptions';
import mongoose from 'mongoose';
import path from 'path';

const citySchema: mongoose.Schema = new mongoose.Schema({
    
    nameEn : {
        type: String,
        required : 1,
        unique : 1,
        trim : 1,
    },

    nameAr : {
        type: String,
        required : 1,
        unique : 1,
        trim : 1,
    },

}, mongooseSchemaOptions );

export const CityModel = mongoose.model<ICityModel>('tb_city', citySchema); 
export interface ICityModel extends mongoose.Document {
    
    nameEn: string;
    nameAr: string;

    createdAt?: Date;
    updatedAt?: Date;

};
export const RegExpCityModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    nameEn : /^[\w-\s]+$/,
    nameAr : /^[\u0600-\u06FF\w-\s]+$/,

    deleted : /^[0-1]$/, 
};


const data = require(
    path.resolve(`./${process.env.DEFAULT_APP_DATA}/cities-of-saudi-arabia.json`)
);
const addDefaultData = async () => {
    if(await CityModel.countDocuments() === 0) {
        CityModel.insertMany(
            data
        )
        .then( res => {
        })
        .catch( err => {
        })
    }
};
addDefaultData();


