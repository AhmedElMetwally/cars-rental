import { IEmployeeGroupModel } from './employeeGroup';
import mongoose from 'mongoose';
import { mongooseSchemaOptions } from './mongooseSchemaOptions';

const employeeSchema: mongoose.Schema = new mongoose.Schema({
    
    employeeGroup : {
        type : mongoose.Schema.Types.ObjectId,
        ref  : 'tb_employeeGroup'
    },

    name  : {
        type : String,
        required : 1,
        trim : 1
    },

    email : {
        type : String,
        required : 1,
        unique : 1,
        trim : 1,
        lowercase : 1
    },

    password : {
        type : String,
        required : 1,
        trim : 1
    },

    // [employee]
    roles : [{
        type : String,
        required : 1,
    }],

    // 0 not deleted
    // 1 deleted
    deleted : {
        type : Number,
        default : 0
    }

}, mongooseSchemaOptions );

employeeSchema.index({
    'name'  : 'text', 
    'email' : 'text',
});

export const EmployeeModel = mongoose.model<IEmployeeModel>('tb_employee', employeeSchema); 


export interface IEmployeeModel extends mongoose.Document {
    
    employeeGroup: string|IEmployeeGroupModel|any;

    name: string;
    email: string;

    password: string;

    roles: [string];
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

    isOnline?: Number;

};

export const RegExpEmployeeModel = {
    _id: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    name: /^[\u0600-\u06FF\w-\s]{1,40}$/,
    email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,

    password: /^.{8,30}$/ ,

    deleted: /^[0-1]$/, 
};
