import { IOnlinePaymentModel } from './onlinePayment';
import { IGuestModel } from './guest';
import { IClientModel } from './client';
import { ICarGroupModel } from './carGroup';
import { ICarModel } from './car';
import { IBranchModel } from './branch';
import { mongooseSchemaOptions } from './mongooseSchemaOptions';
import mongoose from 'mongoose';

const bookingSchema: mongoose.Schema = new mongoose.Schema({

    client : {
        type : mongoose.SchemaTypes.ObjectId,
        ref : 'tb_client',
        required : 1
    },
    
    guest : {
        type : mongoose.SchemaTypes.ObjectId,
        ref : 'tb_guest',
        required : 1
    },

    car : {
        type : mongoose.SchemaTypes.ObjectId,
        ref : 'tb_car',
        required : 1
    } ,

    carGroup : {
        type : mongoose.SchemaTypes.ObjectId,
        ref : 'tb_carGroup',
        required : 1
    },

    pickUpBranch : {
        type: mongoose.Schema.Types.ObjectId,
        ref : 'tb_branch',
        required : 1
    },

    returnBranch : {
        type: mongoose.Schema.Types.ObjectId,
        ref : 'tb_branch',
        required : 1
    },

    onlinePayment : {
        type : mongoose.SchemaTypes.ObjectId,
        ref : 'tb_onlinePayment',
        required : 1
    },

    // 999-999999999
    bookingCode : {
        type : String,
        required : 1,
        unique : 1
    },

    iqama : {
        type : String,
        trim : 1,
    },

    pickUpDate : {
        type: Date,
        required : 1
    },

    returnDate : {
        type: Date,
        required : 1
    },

    // date.now + waiting_days_for_confirm from branch
    expiredAt : {
        type: Date,
        required : 1
    },

    // enum
    bookingType : {
        type : Number,
        required : 1
    },

    daysCount : {
        type : Number,
        required : 1
    },
    pricePerDay : {
        type : Number,
        required : 1
    },
    discountPerDay : {
        type : Number,
        required : 1
    },
    total : {
        type : Number,
        required : 1
    },
    
    // 0 not deleted
    // 1 deleted
    deleted : {
        type : Number,
        default : 0,
    }

}, mongooseSchemaOptions );

bookingSchema.index({
    'bookingCode' : 'text', 
    'iqama' : 'text',
    'daysCount' : 'text',
    'pricePerDay' : 'text',
    'discountPerDay' : 'text' ,
    'total' : 'text' 
});

export const BookingModel = mongoose.model<IBookingModel>('tb_booking', bookingSchema); 

export interface IBookingModel extends mongoose.Document {
    
    client: string|null|IClientModel;
    guest: string|null|IGuestModel;
    car: string|null|ICarModel;
    onlinePayment: string|null|IOnlinePaymentModel;

    carGroup: string|ICarGroupModel;
    pickUpBranch: string|IBranchModel;
    returnBranch: string|IBranchModel;
    
    bookingCode: string;
    iqama: string;
    
    pickUpDate: Date;
    returnDate: Date;
    expiredAt: Date;
    
    bookingType: EbookingType;

    daysCount: number;
    pricePerDay: number;
    discountPerDay: number;
    total: number;
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export enum EbookingType {
    'request',
    'cancelled',
    'waiting_to_confirm', // then expired or active
    'active',
    'expired',
    'done',
    // if returnDate Done and type is active
    // this booking Done
};


export const RegExpBookingModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    
    bookingCode : /^[0-9]{1,12}$/, // 999-999999999
    iqama : /^[0-9]{1,30}$/, // 30 length of Number
    

    // pickUpDate : /^(20(1|2|3)[0-9])-(0[1-9]|(1|2)[0-2])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])$/,
    
    pickUpDate : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,
    returnDate : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,
    expiredAt : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,
    
    bookingType : /^[0-5]$/,
    
    daysCount : /^[1-9]([0-9]{0,3})$/, // 9999
    pricePerDay : /^[1-9]([0-9]{0,3})$/, // 9999
    discountPerDay : /^[0-9]([0-9]{0,3})$/, // 9999
    total : /^[1-9]([0-9]{0,5})$/, // 999999

    deleted : /^[0-1]$/, 
};
