import { mongooseSchemaOptions } from './mongooseSchemaOptions';
import mongoose from 'mongoose';


const GuestSchema: mongoose.Schema = new mongoose.Schema({
    
    name : {
        type : String,
        required : 1,
        trim : 1
    },

    phone : {
        type : String,
        unique : 1,
        required : 1,
        trim : 1
    },

    iqama : {
        type : String,
        trim : 1,
        unique : 1
    },

    iqamaExpiryDate : {
        type : Date,
        trim : 1
    },

    drivingLicence : {
        type : String,
        trim : 1
    },

    drivingLicenceExpiryDate : {
        type : Date,
        trim : 1
    },

    birthday : {
        type : Date,
        trim : 1
    },
    
    // 0 not deleted
    // 1 deleted
    deleted : {
        type : Number,
        default : 0,
    }

}, mongooseSchemaOptions);

GuestSchema.index({
    'name' : 'text', 
    'phone' : 'text',
    'job' : 'text',
    'iqama' : 'text' ,
    'drivingLicence' : 'text' 
});

export const GuestModel = mongoose.model<IGuestModel>('tb_guest', GuestSchema);

export interface IGuestModel extends mongoose.Document {
    
    name: string;
    phone: string;
    iqama: string;
    iqamaExpiryDate: Date;
    drivingLicence: string;
    drivingLicenceExpiryDate: Date;
    birthday: Date;
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export const RegExpGuestModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    name : /^[\u0600-\u06FF\w-\s]+$/,

    phone : /^[0-9\-\+]{9,15}$/,

    iqama : /^[0-9]{1,30}$/, // 30 length of Number
    iqamaExpiryDate : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,

    drivingLicence : /^[0-9]{1,30}$/, // 30 length of Number
    drivingLicenceExpiryDate : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,

    birthday : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,

    deleted : /^[0-1]$/, 
};

