import { ICarTypeModel } from './carType';
import { ICarModelModel } from './carModel';
import { IBranchModel } from './branch';
import { mongooseSchemaOptions } from './mongooseSchemaOptions';
import { ICarModel } from './car';
import mongoose from 'mongoose';


const CarGroupSchema: mongoose.Schema = new mongoose.Schema({

    // type name in ar and en
    carType : {
        type : mongoose.SchemaTypes.ObjectId,
        required : 1,
        ref : 'tb_carType'
    },

    // model name in ar and en
    carModel : {
        type : mongoose.SchemaTypes.ObjectId,
        required : 1,
        ref : 'tb_carModel'
    },

    // app data for his branch
    branch : {
        type : mongoose.SchemaTypes.ObjectId,
        required : 1,
        ref : 'tb_branch'
    },

    // year model
    year : {
        type : Number,
        required : 1,
    },

    // price for one day
    pricePerDay : {
        type : Number,
        required : 1,
    },

    allowedKm : {
        type : Number,
        required : 1,
    },

    plusKmPrice : {
        type : Number,
        required : 1,
    },

    discountPerDay : {
        type : Number,
        required : 1,
    },

    images : [{
        type : String,
    }],

    views : {
        type : Number,
        default : 0,
    },

    // 0 not deleted
    // 1 deleted
    deleted : {
        type : Number,
        default : 0,
    },
    
}, mongooseSchemaOptions );

export const CarGroupModel = mongoose.model<ICarGroupModel>('tb_carGroup', CarGroupSchema); 

export interface ICarGroupModel extends mongoose.Document {
    
    carType: string|ICarTypeModel;
    carModel: string|ICarModelModel;
    branch: string|IBranchModel;

    year: number;
    allowedKm: number;
    plusKmPrice: number;

    pricePerDay: number;
    discountPerDay: number;

    views: number;

    carsCount?: number; // set in [GET] viewCarGroup

    images: string[];
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export const RegExpCarGroupModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    year : /^[\d]{4}$/,
    allowedKm : /^[1-9]([0-9]{0,5})$/, // 999999 ,
    plusKmPrice : /^([0-9]([0-9]{0,5})|[0-9]([0-9]{0,5}).[1-9]([0-9]{0,5}))$/, // 2999 2999.2999

    pricePerDay : /^[1-9]([0-9]{0,3})$/, // 9999
    discountPerDay : /^[0-9]([0-9]{0,3})$/, // 9999

    views : /^\d+$/, // any Number

    deleted : /^[0-1]$/, 
};


