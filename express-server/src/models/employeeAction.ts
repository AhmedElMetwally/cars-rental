import { Escreens } from './employeeGroup';
import { mongooseSchemaOptions } from './mongooseSchemaOptions';
import { IBranchModel } from './branch';
import { IBookingModel } from './booking';
import { IClientModel } from './client';
import { ICarModel } from './car';
import { ICarGroupModel } from './carGroup';
import { ICarTypeModel } from './carType';
import { ICarModelModel } from './carModel';

import mongoose from 'mongoose';

const EmployeeActionSchema: mongoose.Schema = new mongoose.Schema({
    
    client : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'tb_client',
        required : 1
    },

    car : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'tb_car',
        required : 1
    },

    carGroup : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'tb_carGroup',
        required : 1
    },

    carType : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'tb_carType',
        required : 1
    },

    carModel : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'tb_carModel',
        required : 1
    },

    booking : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'tb_booking',
        required : 1
    },

    branch : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'tb_branch',
        required : 1
    },

    employee_id : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'tb_employee',
        required : 1
    },
    
    screen : {
        type : Number,
        required : 1,
    },

    body : {
        type: JSON,
        required : 1
    },

    query : {
        type: JSON,
        required : 1
    },

    itemName : {
        type : Number,
        required : 1,
    },

    deleted : {
        type : Number,
        default : 0,
    }

}, mongooseSchemaOptions );

export const EmployeeActionModel = mongoose.model<IEmployeeActionModel>('tb_employeeAction', EmployeeActionSchema); 

export interface IEmployeeActionModel extends mongoose.Document {
    
    client: string|null|IClientModel;
    car: string|null|ICarModel;
    carGroup: string|null|ICarGroupModel;
    carType: string|null|ICarTypeModel;
    carModel: string|null|ICarModelModel;
    booking: string|null|IBookingModel;
    branch: string|null|IBranchModel;

    employee_id: string;
    
    screen: Escreens;

    body: JSON;
    query: JSON;

    itemName: EitemName;

    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export enum EitemName {
    client,
    car,
    carGroup,
    carType,
    carModel,
    booking,
    branch,
};

export const RegExpEmployeeActionModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    screen : /^[0-9]{1,3}$/, // ref to screens in employeeGroup 

    itemName:/^[0-7]$/,

    deleted : /^[0-1]$/, 
};

