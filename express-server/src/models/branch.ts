import { ICityModel } from './city';
import { redisClient } from '../helpers/redis';
import { mongooseSchemaOptions } from './mongooseSchemaOptions';
import mongoose from 'mongoose';

const BranchSchema: mongoose.Schema = new mongoose.Schema({
    
    city : {
        type : mongoose.SchemaTypes.ObjectId,
        ref : 'tb_city',
        required : 1
    },

    // branch name in en
    nameEn : {
        type: String,
        required : 1,
        trim : 1,
    },

    // branch name in ar
    nameAr : {
        type: String,
        required : 1,
        trim : 1,
    },

    // branch address in en
    addressEn : {
        type: String,
        required : 1,
        trim : 1,
    },

    // branch address in ar
    addressAr : {
        type: String,
        required : 1,
        trim : 1,
    },

    // html code for en
    overviewEn : {
        type: String,
        required : 1,
    },

    // html code for ar
    overviewAr : {
        type: String,
        required : 1,
    },

    // branch phone
    phone : {
        type: String,
        required : 1,
        trim : 1,
        unique : 1
    },

    // [ longitude, latitude ]
    location : { 
        type: [Number , Number ], 
        index: '2dsphere'
    },

    // booking code [xxx-12345] [bookingStartCode-12345]
    bookingStartCode : {
        type : Number,
        required : 1,
    },

    // how many days to active booking
    waitingHoursForConfirm : {
        type : Number,
        required : 1,
    },

    images : [{
        type : String,
    }],

    // 0 not deleted
    // 1 deleted
    deleted : {
        type : Number,
        default : 0,
    }

}, mongooseSchemaOptions );

BranchSchema.index({
    nameEn : 'text', 
    nameAr : 'text', 
    addressEn : 'text',
    addressAr : 'text',
    bookingStartCode : 'text',
    phone : 'text'
});

export const BranchModel = mongoose.model<IBranchModel>('tb_branch', BranchSchema); 


export interface IBranchModel extends mongoose.Document {
    
    city: string|ICityModel;

    nameEn: string;
    nameAr: string;

    addressEn: string;
    addressAr: string;

    overviewEn: string;
    overviewAr: string;

    phone: string;
    location: [number , number];
    bookingStartCode: number;
    waitingHoursForConfirm: number;
    
    images: string[];
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export const RegExpBranchModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    nameEn : /^[\w-\s]+$/,
    nameAr : /^[\u0600-\u06FF\w-\s]+$/,

    addressEn : /^[\w-\s]+$/,
    addressAr : /^[\u0600-\u06FF\w-\s]+$/,

    overviewEn: /.*/,
    overviewAr: /.*/,

    phone : /^[0-9\-\+]{9,15}$/,
    location : /^((([1-9])|([1-9]([0-9]+)))|-?\d*(\.\d+)?)$/, // [ item , item ]
    bookingStartCode : /^[0-9]{3}$/, // 999 
    waitingHoursForConfirm : /^[0-9]([0-9]{0,2})$/, 

    deleted : /^[0-1]$/, 
};


export const branchBookingCounter = (): Promise<string> => {
    return new Promise( async (resolve) => {
        redisClient.incr(`branchBookingCounter` , (_err: any , number: number) => {
            if(number < 999) {
                resolve(number.toString().padStart(3, '0') );
            } else {
                resolve(number.toString());
            };
        });
    });
};
