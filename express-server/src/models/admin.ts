import { mongooseSchemaOptions } from './mongooseSchemaOptions';
import mongoose from 'mongoose';

const adminSchema: mongoose.Schema = new mongoose.Schema({
    
    email : {
        type: String,
        required : 1,
        unique : 1,
        trim : 1,
        lowercase : 1
    },

    password : {
        type: String,
        required : 1,
    },

    // [admin]
    roles : [{
        type : String,
        required : 1,
    }],

}, mongooseSchemaOptions);

export const AdminModel = mongoose.model<IAdminModel>('tb_admin', adminSchema); 

export interface IAdminModel extends mongoose.Document {
    
    email: string;
    password: string;

    roles: [string];

    createdAt?: Date;
    updatedAt?: Date;

};

export const RegExpAdminModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    email : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    password : /^.{8,30}$/ ,
};

const addDefaultData = async () => {
    if(await AdminModel.countDocuments() === 0) {
        AdminModel.create(
            {
                email: 'ahmed@gmail.com',
                password: '123456789',
                roles : ['admin']
            }
        )
        .then( res => {
        })
        .catch( err => {
        })
    }
};
addDefaultData();
