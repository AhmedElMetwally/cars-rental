import { mongooseSchemaOptions } from './mongooseSchemaOptions';
import mongoose from 'mongoose';
import path from 'path';

const CarTypeSchema: mongoose.Schema = new mongoose.Schema({
    
    nameAr : {
        type      : String,
        required  : 1,
        unique    : 1,
        trim      : 1,
    },

    nameEn : {
        type      : String,
        required  : 1,
        unique    : 1,
        trim      : 1,
    },
    
    // 0 not deleted
    // 1 deleted
    deleted : {
        type : Number,
        default : 0,
    },
    
} , mongooseSchemaOptions );

export const CarTypeModel = mongoose.model<ICarTypeModel>( 'tb_carType', CarTypeSchema); 

export interface ICarTypeModel extends mongoose.Document {
    
    nameAr: string;
    nameEn: string;
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export const RegExpCarTypeModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    nameEn : /^[\w-\s]+$/,
    nameAr : /^[\u0600-\u06FF\w-\s]+$/,

    deleted : /^[0-1]$/, 
};



const data = require(
    path.resolve(`./${process.env.DEFAULT_APP_DATA}/car-types.json`)
);
const addDefaultData = async () => {
    if(await CarTypeModel.countDocuments() === 0) {
        CarTypeModel.insertMany(
            data
        )
        .then( res => {
        })
        .catch( err => {
        })
    }
};
addDefaultData();


