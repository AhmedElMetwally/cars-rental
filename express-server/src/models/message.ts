import { mongooseSchemaOptions } from './mongooseSchemaOptions';
import mongoose from 'mongoose';

const messageSchema: mongoose.Schema = new mongoose.Schema({
    

    name : {
        type : String,
        required : 1,
        trim : 1
    },

    email : {
        type : String,
        required : 1,
        lowercase : 1,
        trim : 1,
    },

    message : {
        type : String,
        required : 1,
        trim : 1
    },

    isView : {
        type : Number,
        default : 0,
    },

    // 0 not deleted
    // 1 deleted
    deleted : {
        type : Number,
        default : 0,
    }

}, mongooseSchemaOptions);

messageSchema.index({
    'name' : 'text', 
    'email' : 'text',
    'message' : 'text',
});

export const MessageModel = mongoose.model<IMessageModel>('tb_message', messageSchema);

export interface IMessageModel extends mongoose.Document {

    name: string;
    email: string;
    message: string;
    
    isView: number;
    
    deleted: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export const RegExpMessageModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    name : /^[\u0600-\u06FF\w-\s]+$/,

    email : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    phone : /^[0-9\-\+]{9,15}$/,

    message : /^(.*){1,500}$/,
    
};

