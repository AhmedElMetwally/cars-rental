import { ICarGroupModel } from './carGroup';
import { mongooseSchemaOptions } from './mongooseSchemaOptions';
import mongoose from 'mongoose';

const CarSchema: mongoose.Schema = new mongoose.Schema({

    carGroup : {
        type : mongoose.SchemaTypes.ObjectId,
        required : 1,
        ref : 'tb_carGroup'
    },

    // 123-ABC
    plateNumber : {
        type : String,
        required : 1,
        trim : 1,
        unique : 1,
        lowercase : 1
    },

    // enum
    gearType : {
        type : Number,
        required : 1,
    },

    // enum
    status : {
        type : Number,
        required : 1,
    },

    // 0 not deleted
    // 1 deleted
    deleted : {
        type : Number,
        default : 0,
    },
    
}, mongooseSchemaOptions );

CarSchema.index({
    'plateNumber' : 'text', 
});

export const CarModel = mongoose.model<ICarModel>('tb_car', CarSchema); 

export interface ICarModel extends mongoose.Document {
    
    carGroup: string|ICarGroupModel;

    plateNumber: string;
    gearType: EgearType;
    status: Estatus;
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export enum EgearType {
    'manual',
    'automatic'
};

export enum Estatus {
    'hiring',
    'accident',
    'sold',
    'steal',
    'available',
    'not_available'
};


export const RegExpCarModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    plateNumber : /^[0-9]{4}-[\u0600-\u06FF]{1}\s[\u0600-\u06FF]{1}\s[\u0600-\u06FF]{1}$/,
    gearType : /^[0-1]$/, 
    status : /^[0-9]$/, 

    deleted : /^[0-1]$/, 
};