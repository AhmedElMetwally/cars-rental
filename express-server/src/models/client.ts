import { IBookingModel } from './booking';
import { INationalityModel } from './nationality';
import { mongooseSchemaOptions } from './mongooseSchemaOptions';
import mongoose from 'mongoose';

const clientSchema: mongoose.Schema = new mongoose.Schema({
    
    // nationality in en and ar 
    nationality : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'tb_nationality',
    },

    name : {
        type : String,
        required : 1,
        trim : 1
    },

    email : {
        type : String,
        unique : 1,
        required : 1,
        lowercase : 1,
        trim : 1,
    },

    phone : {
        type : String,
        unique : 1,
        required : 1,
        trim : 1
    },

    password : {
        type : String,
        required : 1 ,
        trim : 1
    },

    iqama : {
        type : String,
        trim : 1,
        index: true, 
        unique: true, 
        sparse: true
    },

    iqamaExpiryDate : {
        type : Date,
        trim : 1
    },

    drivingLicence : {
        type : String,
        trim : 1
    },

    drivingLicenceExpiryDate : {
        type : Date,
        trim : 1
    },

    birthday : {
        type : Date,
        trim : 1
    },
    
    address : {
        type : String,
        trim : 1
    },

    job : {
        type : String,
        trim : 1
    },

    image : {
        type : String,
        lowercase : 1
    },

    // enum
    signupFrom : {
        type : Number,
        required : 1
    },

    // [client]
    roles : [{
        type : String,
        required : 1,
    }],

    // 0 not deleted
    // 1 deleted
    deleted : {
        type : Number,
        default : 0,
    }

}, mongooseSchemaOptions);

clientSchema.index({
    'name' : 'text', 
    'email' : 'text',
    'phone' : 'text',
    'job' : 'text',
    'iqama' : 'text' ,
    'drivingLicence' : 'text' ,
    'address' : 'text' ,
});

export const ClientModel = mongoose.model<IClientModel>('tb_client', clientSchema);

export interface IClientModel extends mongoose.Document {
    
    nationality: string|INationalityModel;

    name: string;
    
    email: string;
    phone: string;
    
    password: string;
    
    iqama: string;
    iqamaExpiryDate: Date;
    
    drivingLicence: string;
    drivingLicenceExpiryDate: Date;
    
    birthday: Date;
    
    address: string;
    job: string;
    signupFrom: EsignupFrom;
    
    roles: [string];

    image?: string;
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export enum EsignupFrom {
    'normal_signup',
    'booking_before_and_signup',
}


export const RegExpClientModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    name : /^[\u0600-\u06FF\w-\s]+$/,

    email : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    phone : /^[0-9\-\+]{9,15}$/,

    password : /^.{8,30}$/ ,

    iqama : /^[0-9]{1,30}$/, // 30 length of Number
    iqamaExpiryDate : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,

    drivingLicence : /^[0-9]{1,30}$/, // 30 length of Number
    drivingLicenceExpiryDate : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,

    birthday : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,

    address : /^[\u0600-\u06FF\w-\s]+$/,
    job : /^[\u0600-\u06FF\w-\s]+$/,
    signupFrom : /^[0-1]$/, 


    deleted : /^[0-1]$/, 
};

