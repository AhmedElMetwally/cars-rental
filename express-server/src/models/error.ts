import mongoose from 'mongoose';

// mongoose Schema
const errorSchema: mongoose.Schema = new mongoose.Schema({
    status : String ,
    url : String,
    error : [{
        name : String,
        message: String
    }]
}, { timestamps: true });


export const ErrorModel = mongoose.model('error', errorSchema); 

