import { mongooseSchemaOptions } from './mongooseSchemaOptions';
import mongoose from 'mongoose';

const EmployeeOnlineTimerSchema: mongoose.Schema = new mongoose.Schema({
    
    employee_id : {
        type : mongoose.Schema.Types.ObjectId,
        ref  : 'tb_employee',
    }, 
    
    date : {
        type : String , 
        required : 1,
    }, 
    
    timeMs : {
        type : Number,
        required : 1
    }

} , mongooseSchemaOptions );


export const EmployeeTimerModel = mongoose.model<IEmployeeTimerModel>('tb_employeeOnlineTimer', EmployeeOnlineTimerSchema); 

export interface IEmployeeTimerModel extends mongoose.Document {
    
    employee_id: string;
    date: string;
    timeMs: number;

    createdAt?: Date;
    updatedAt?: Date;

};
export interface IEmployeeStatistics {
    date: string;
    timeMs: number;
};
export const RegExpEmployeeTimerModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    year : /^20[1-2][0-9]$/,
    month : /^([1-9]|1[0-2])$/,
};


