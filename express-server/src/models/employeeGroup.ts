import { mongooseSchemaOptions } from './mongooseSchemaOptions';
import mongoose from 'mongoose';
import { IBranchModel } from './branch';

const EmployeeGroupSchema: mongoose.Schema = new mongoose.Schema({
    
    branches : [{
        type : mongoose.Schema.Types.ObjectId,
        ref : 'tb_branch'
    }],

    nameEn : {
        type : String,
        unique : 1,
        trim : 1
    },

    nameAr : {
        type : String,
        unique : 1,
        trim : 1
    },

    // enum
    screens : [{
        type : Number,
    }],

    deleted : {
        type : Number,
        default : 0,
    }

}, mongooseSchemaOptions );

EmployeeGroupSchema.index({
    'nameEn' : 'text', 
    'nameAr' : 'text', 
});

export const EmployeeGroupModel = mongoose.model<IEmployeeGroupModel>('tb_employeeGroup', EmployeeGroupSchema); 

export interface IEmployeeGroupModel extends mongoose.Document {
    
    branches: string[]|IBranchModel[];
    
    screens: Escreens[];
   
    nameEn: string;
    nameAr: string;
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export enum Escreens {

    edit_client,
    edit_client_password,
    delete_client,
    restore_client,
    edit_client_image,
  
    add_car,
    edit_car,
    delete_car,
    restore_car,
  
    add_car_type,
    edit_car_type,
    delete_car_type,
    restore_car_type,
  
    add_car_model,
    edit_car_model,
    delete_car_model,
    restore_car_model,
  
    add_car_group,
    edit_car_group,
    delete_car_group,
    restore_car_group,
    edit_car_group_image,
  
    add_branch,
    edit_branch,
    delete_branch,
    restore_branch,
    edit_branch_image,

    edit_booking,
    cancel_booking,
    approve_booking,
    active_booking,
    delete_booking,
    restore_booking,
  
};

export const RegExpEmployeeGroupModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    nameEn : /^[\w-\s]+$/,
    nameAr : /^[\u0600-\u06FF\w-\s]+$/,

    screens : /^[0-9]{1,3}$/, // [num , ...etc]

    deleted : /^[0-1]$/, 
};
