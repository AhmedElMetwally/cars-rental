export const mongooseSchemaOptions = {
    validateBeforeSave: false,
    versionKey : false,
    timestamps : {
        createdAt : "createdAt",
        updatedAt : 'updatedAt'
    }
};
