import { mongooseSchemaOptions } from './mongooseSchemaOptions';
import mongoose from 'mongoose';
import path from 'path';

const nationalitySchema: mongoose.Schema = new mongoose.Schema({
    
    nameEn : {
        type: String,
        required : 1,
        unique : 1,
        trim : 1,
    },

    nameAr : {
        type: String,
        required : 1,
        unique : 1,
        trim : 1,
    },

}, mongooseSchemaOptions );

export const NationalityModel = mongoose.model<INationalityModel>('tb_nationality', nationalitySchema); 

export interface INationalityModel extends mongoose.Document {
    
    nameEn: string;
    nameAr: string;

    createdAt?: Date;
    updatedAt?: Date;

};

export const RegExpNationalityModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    nameEn : /^[\w-\s]+$/,
    nameAr : /^[\u0600-\u06FF\w-\s]+$/,

    deleted : /^[0-1]$/, 
};


const data = require(
    path.resolve(`./${process.env.DEFAULT_APP_DATA}/nationalities.json`)
);
const addDefaultData = async () => {
    if(await NationalityModel.countDocuments() === 0) {
        NationalityModel.insertMany(
            data
        )
        .then( res => {
        })
        .catch( err => {
        })
    }
};
addDefaultData();


