export enum mainErrorEnum {

    // main
    token_invalid,
    not_have_access,
    edit_date_not_found,
    datebase,
    admin_not_found,
    client_not_found,
    employee_not_found,
    employeeGroup_not_found,
    carGroup_not_found,
    branch_not_found,
    car_not_found,
    image_not_found,
    carType_not_found,
    carTypes_not_found,
    carModel_not_found,
    booking_not_found,
    booking_not_found_or_car_not_found,
    employeeGroup_nameEn_used_before ,
    employeeGroup_nameAr_used_before ,
    carType_nameEn_used_before ,
    carType_nameAr_used_before ,
    carModel_nameEn_used_before ,
    carModel_nameAr_used_before ,
    iqama_used_before,
    you_cant_booking_this_car_Group_in_this_booking_date,
    you_cant_booking_this_car_in_this_booking_date,
    you_cant_edit_booking_date,
    you_cant_edit_booking_expiredAt,
    you_cant_active_booking,
    this_car_is_booking_in_this_car_group,
    check_your_data,
    
    // view
    page_required ,
    page_invalid ,
    limit_required ,
    limit_invalid ,
    sortBy_required ,
    sortBy_invalid ,
    searchQuery_required ,
    searchQuery_invalid ,

    // item_id
    _id_required,
    _id_invalid,
    admin_id_required,
    admin_id_invalid,
    booking_id_required,
    booking_id_invalid,
    branch_id_required,
    branch_id_invalid, 
    car_id_required,
    car_id_invalid,
    carGroup_id_required,
    carGroup_id_invalid,
    carModel_id_required,
    carModel_id_invalid,
    carType_id_required,
    carType_id_invalid,
    client_id_required,
    client_id_invalid,
    employee_id_required,
    employee_id_invalid,
    employeeAction_id_required,
    employeeAction_id_invalid,
    employeeGroup_id_required,
    employeeGroup_id_invalid,
    guest_id_required,
    guest_id_invalid,
    nationality_id_required,
    nationality_id_invalid,
    onlinePayment_id_required,
    onlinePayment_id_invalid,
    
    // fileds
    name_required ,
    name_invalid ,
    nameEn_required ,
    nameEn_invalid ,
    nameAr_required ,
    nameAr_invalid ,

    email_required ,
    email_invalid ,
    email_used_before ,

    phone_required,
    phone_invalid,
    phone_used_before ,

    password_required ,
    password_invalid ,

    iqama_required,
    iqama_invalid,

    iqamaExpiryDate_required,
    iqamaExpiryDate_invalid,

    drivingLicence_required,
    drivingLicence_invalid,

    drivingLicenceExpiryDate_required,
    drivingLicenceExpiryDate_invalid,

    birthday_required,
    birthday_invalid,
    
    address_required,
    address_invalid,
    addressEn_required,
    addressEn_invalid,
    addressAr_required,
    addressAr_invalid,

    job_required,
    job_invalid,

    image_required,
    image_invalid,

    emailOrPhone_required ,   
    emailOrPhone_invalid ,  

    plateNumber_required,
    plateNumber_invalid,
    plateNumber_used_before,

    allowedKm_required,
    allowedKm_invalid,

    gearType_required,
    gearType_invalid,

    status_required,
    status_invalid,

    plusKmPrice_required,
    plusKmPrice_invalid,

    type_required,
    type_invalid,

    model_required,
    model_invalid,

    price_required,
    price_invalid,

    year_required,
    year_invalid,

    overviewEn_required,
    overviewEn_invalid,

    overviewAr_required,
    overviewAr_invalid,

    location_required,
    location_invalid,

    screens_required,
    screens_invalid,

    branches_required,
    branches_invalid,

    month_required,
    month_invalid,

    bookingCode_required,
    bookingCode_invalid,
    bookingCode_used_before,
    
    daysCount_required,
    daysCount_invalid,

    pricePerDay_required,
    pricePerDay_invalid,

    discountPerDay_required,
    discountPerDay_invalid,

    total_required,
    total_invalid,

    bookingStartCode_required,
    bookingStartCode_invalid,
    
    waitingHoursForConfirm_required,
    waitingHoursForConfirm_invalid,

    actionType_required,
    actionType_invalid,

    pickUpBranch_id_required,
    pickUpBranch_id_invalid,
    
    returnBranch_id_required,
    returnBranch_id_invalid,

    pickUpDate_required,
    pickUpDate_invalid ,

    returnDate_required,
    returnDate_invalid,

    expiredAt_required,
    expiredAt_invalid,

    bookingType_required,
    bookingType_invalid,

    item_name_required,
    item_name_invalid,

    views_required,
    views_invalid,

    city_id_required,
    city_id_invalid,

    guest_not_found,

    message_required,
    message_invalid,

};
export enum mainErrorEnumEn {

    // main
    "Token Is Invalid",
    "Not Have Access",
    "Edit Date Not Found",
    "Datebase",
    "Admin Not Found",
    "Client Not Found",
    "Employee Not Found",
    "Employee Group Not Found",
    "Car Group Not Found",
    "branch Not Found",
    "Car Not Found",
    "Image Not Found",
    "Car Type Not Found",
    "Car Types Not Found",
    "Car Model Not Found",
    "Booking Not Found",
    "booking Not Found Or Car Not Found",
    "Employee Group English Name Used Before" ,
    "Employee Group Arabic Name Used Before" ,
    "Car Type English Name Used Before" ,
    "Car Type Arabic Name Used Before" ,
    "Car Model English Name Used Before" ,
    "Car Model Arabic Name Used Before" ,
    "Iqama Used Before",
    "You Can`t Booking This Car Group  In This Booking Date",
    "You Can`t Booking This Car In This Booking Date",
    "You Can`t Edit Booking Date",
    "You Can`t Edit Booking ExpiredAt",
    "You Can`t Active Booking",
    "This car is Booking In This Car Group",
    "Check Your Data",
    
    // view
    "Page Is Required" ,
    "Page Is Invalid" ,
    "Limit Is Required" ,
    "Limit Is Invalid" ,
    "SortBy Is Required" ,
    "SortBy Is Invalid" ,
    "SearchQuery Is Required" ,
    "SearchQuery Is Invalid" ,

    // item Id
    "Id Is Required",
    "Id Is Invalid",
    "Admin Id Is Required",
    "Admin Id Is Invalid",
    "Booking Id Is Required",
    "Booking Id Is Invalid",
    "Branch Id Is Required",
    "Branch Id Is Invalid", 
    "Car Id Is Required",
    "Car Id Is Invalid",
    "Car Group Id Is Required",
    "Car Group Id Is Invalid",
    "Car Model Id Is Required",
    "Car Model Id Is Invalid",
    "Car Type Id Is Required",
    "Car Type Id Is Invalid",
    "Client Id Is Required",
    "Client Id Is Invalid",
    "Employee Id Is Required",
    "Employee Id Is Invalid",
    "Employee Action Id Is Required",
    "Employee Action Id Is Invalid",
    "Employee Group Id Is Required",
    "Employee Group Id Is Invalid",
    "Guest Id Is Required",
    "Guest Id Is Invalid",
    "Nationality Id Is Required",
    "Nationality Id Is Invalid",
    "Online Payment Id Is Required",
    "Online Payment Id Is Invalid",
    
    // fileds
    "Name Is Required" ,
    "Name Is Invalid" ,
    "English Name Is Required" ,
    "English Name Is Invalid" ,
    "Arabic Name Is Required" ,
    "Arabic Name Is Invalid" ,

    "Email Is Required" ,
    "email Is Invalid" ,
    "Email Used Before" ,

    "Phone Is Required",
    "Phone Is Invalid",
    "Phone Used Before" ,

    "Password Is Required" ,
    "Password Is Invalid" ,

    "Iqama Is Required",
    "Iqama Is Invalid",

    "Iqama Expiry Date Is Required",
    "Iqama Expiry Date Is Invalid",

    "Driving Licence Is Required",
    "Driving Licence Is Invalid",

    "Driving Licence Expiry Date Is Required",
    "Driving Licence Expiry Date Is Invalid",

    "Birthday Is Required",
    "Birthday Is Invalid",
    
    "Address Is Required",
    "Address Is Invalid",
    "English Address Is Required",
    "English Address Is Invalid",
    "Arabic Address Is Required",
    "Arabic Address Is Invalid",

    "Job Is Required",
    "Job Is Invalid",

    "Image Is Required",
    "Image Is Invalid",

    "Email Or Phone Is Required" ,   
    "Email Or Phone Is Invalid" ,  

    "Plate Number Is Required",
    "Plate Number Is Invalid",
    "Plate Number Used Before",

    "Allowed Km Is Required",
    "Allowed Km Is Invalid",

    "Gear Type Is Required",
    "Gear Type Is Invalid",

    "Status Is Required",
    "Status Is Invalid",

    "Plus Km Price Is Required",
    "Plus Km Price Is Invalid",

    "Type Is Required",
    "Type Is Invalid",

    "Model Is Required",
    "Model Is Invalid",

    "Price Is Required",
    "Price Is Invalid",

    "Year Is Required",
    "Year Is Invalid",

    "English Overview Is Required",
    "English Overview Is Invalid",

    "Arabic Overview Is Required",
    "Arabic Overview Is Invalid",

    "Location Is Required",
    "Location Is Invalid",

    "Screens Is Required",
    "Screens Is Invalid",

    "Branches Is Required",
    "Branches Is Invalid",

    "Month Is Required",
    "Month Is Invalid",

    "Booking Code Is Required",
    "Booking Code Is Invalid",
    "Booking Code Used Before",
    
    "Days Count Is Required",
    "Days Count Is Invalid",

    "Price Per Day Is Required",
    "Price Per Day Is Invalid",

    "Discount Per Day Is Required",
    "Discount Per Day Is Invalid",

    "Total Is Required",
    "Total Is Invalid",

    "Booking Start Code Is Required",
    "Booking Start Code Is Invalid",
    
    "Waiting Hours For Confirm Is Required",
    "Waiting Hours For Confirm Is Invalid",

    "Action Type Is Required",
    "Action Type Is Invalid",

    "PickUp Branch Id Is Required",
    "PickUp Branch Id Is Invalid",
    
    "Return Branch Id Is Required",
    "Return Branch Id Is Invalid",

    "PickUp Date Is Required",
    "PickUp Date Is Invalid" ,

    "Return Date Is Required",
    "Return Date Is Invalid",

    "ExpiredAt Is Required",
    "ExpiredAt Is Invalid",

    "Booking Type Is Required",
    "Booking Type Is Invalid",

    "Item Name Is Required",
    "Item Name Is Invalid",

    "Views Is Required",
    "Views Is Invalid",

    "City Id Is Required",
    "City Id Is Invalid",

    "Guest Not Found",

    "Message Is Required",
    "Message Is Invalid",

};
export enum mainErrorEnumAr {

    // main
    "الرمز غير صالح",
    "ليس لديك الصلاحية",
    "بيانات التعديل غير متوفرة",
    "الداتابيز",
    "المدير غير موجود",
    "العميل غير موجود",
    "الموظف غير موجود",
    "جروب الموظفين غير موجود",
    "جروب السيارات غير موجود",
    "الفرع غير موجود",
    "السيارة غير موجودة",
    "الصورة غير موجودة",
    "نوع السيارة غير موجود",
    "انواع السيارات غير موجودة",
    "موديل السيارة غير موجود",
    "الحجز غير موجود",
    "الحجز غير موجود او السيارة غير موجود",
    "الاسم بالانجليزي لجروب الموظفين مستخدم من قبل" ,
    "الاسم بالعربية لجروب الموظفين مستخدم من قبل" ,
    "الاسم بالانجليزية لنوع السيارة مستخدم من قبل" ,
    "الاسم بالعربية لنوع السيارة مستخدم من قبل" ,
    "الاسم بالانجليزية لموديل السيارة مستخدم من قبل" ,
    "الاسم بالعربية لموديل السيارة مستخدم من قبل" ,
    "رقم الاقامة مستخدم من قبل",
    "لا تستطيع حجز جروب السيارة في هذا توقيت هذا الحجز",
    "لا تستطيع حجز السيارة في هذا توقيت هذا الحجز",
    "لا تستطيع تعديل تاريخ الحجز",
    "لا يمكنك تعديل تاريخ انتهاء صلاحية الحجز",
    "لا يمكنك تفعيل الحجز",
    "هذة السيارة محجوزة تبع جروب السيارات",
    "تاكد من البيانات",
    
    // view
    "الصفحة مطلوبة",
    "الصفحة غير صالحة" ,
    "عدد العناصر مطلوب",
    "عدد العناصر غير صالحة" ,
    "ترتيب بواسطة مطلوب" ,
    "ترتيب بواسطة غير صالح" ,
    "كلمة البحث مطلوبة" ,
    "كلمة البحث غير صالحة" ,

    // item_id
    "الكود مطلوب",
    "الكود غير صالح",
    "كود المدير مطلوب",
    "كود المدير غير صالح",
    "كود الحجز مطلوب",
    "كود الحجز غير صالح",
    "كود الفرع مطلوب",
    "كود الفرع غير صالح", 
    "كود السيارة مطلوب",
    "كود السيارة غير صالح",
    "كود جروب السيارات مطلوب",
    "كود جروب السيارات غير صالح",
    "كود موديل السيارة مطلوب",
    "كود موديل السيارة غير صالح",
    "كود نوع السيارة مطلوب",
    "كود نوع السيارة غير صالح",
    "كود العميل مطلوب",
    "كود العميل غير صالح",
    "كود الموظف مطلوب",
    "كود الموظف غير صالح",
    "كود فعل الموظف مطلوب",
    "كود فعل الموظف غير صالح",
    "كود جروب الموظفين مطلوب",
    "كود جروب الموظفين غير صالح",
    "كود الزائر مطلوب",
    "كود الزائر غير صالح",
    "كود الجنسية مطلوب",
    "كود الجنسية غير صالح",
    "كود الدفع الالكتروني مطلوب",
    "كود الدفع الالكتروني غير صالح",
    
    // fileds
    "الاسم مطلوب" ,
    "الاسم غير صالح" ,
    "الاسم بالانجيلزي مطلوب" ,
    "الاسم بالانجيلزي غير صالح" ,
    "الاسم بالعربية مطلوب" ,
    "الاسم بالعربية غير صالح" ,

    "الايميل مطلوب" ,
    "الايميل غير صالح" ,
    "الايميل مستخدم من قبل" ,

    "رقم الهاتف مطلوب",
    "رقم الهاتف غير صالح",
    "رقم الهاتف مستخدم من قبل" ,

    "كلمة المرور مطلوبة" ,
    "كلمة المرور غير صالحة" ,

    "رقم الاقامة مطلوب",
    "رقم الاقامة غير صالح",

    "تاريخ انتهاء الاقامة مطلوب",
    "تاريخ انتهاء الاقامة غير صالح",

    "رخصة القيادة مطلوبة",
    "رخصة القيادة غير صالحة",

    "تاريخ انتهاء رخصة القيادة مطلوب",
    "تاريخ انتهاء رخصة القيادة غير صالح",

    "تاريه الميلاد مطلوب",
    "تاريخ الميلاد غير صالح",
    
    "العنوان مطلوب",
    "العنوان غير صالح",
    "العنوان بالانجليزية مطلوب",
    "العنوان بالانجليزية غير صالح ",
    "العنوان بالعربية مطلوب",
    "العنوان بالعربية غير صالح ",

    "الوظفية مطلوبة",
    "الوظيفة غير صالحة",

    "الصورة مطلوبة",
    "الصورة غير صالحة",

    "الهاتف او الايميل مطلوب" ,   
    "الهاتف او الايميل غير صالح" ,  

    "لوحة السيارة مطلوبة",
    "لوحة السيارة غير صالحة",
    "لوحة السيارة مستخدمة من قبل",

    "عدد الكيلومتر المسموح بيها مطلوبة",
    "عدد الكيلومتر المسموح بيها غير صالح",

    "نوع المحرك مطلوب",
    "نوع المحرك غير صالح",

    "الحالة مطلوبة",
    "الحالة غير صالحة",

    "سعر الكيلومتر الزيادة مطلوب",
    "سعر الكيلومتر الزيادة غير صالح",

    "النوع مطلوب",
    "النوع غير صالح",

    "الموديل مطلوب",
    "الموديل غير صالح",

    "السعر مطلوب",
    "السعر غير صالح",

    "السنة مطلوبة",
    "السنة غير صالحة",

    "نظرة عامة بالانجليزية مطلوبة",
    "نظرة عامة بالانجليزية غير صالحة",

    "نظرة عامة بالعربية مطلوبة",
    "نظرة عامة بالعربية غير صالحة",

    "المكان مطلوب",
    "المكان غير صالح",

    "الشاشات مطلوبة",
    "الشاشات غير صالحة",

    "الفروع مطلوبة",
    "الفروع غير صالحة",

    "الشهر مطلوب",
    "الشهر غير صالح",

    "كود الـحجز مطلوب",
    "كود الـحجز غير صالح",
    "كود الحجز مستخدم من قبل",
    
    "عدد الايام مطلوب",
    "عدد الايام غير صالح",

    "السعر في اليوم مطلوب",
    "السعر في اليوم غير صالح",

    "الخصم في اليوم مطلوب",
    "الخصم في اليوم غير صالح",

    "المجموع مطلوب",
    "المجموع غير صالح",

    "بداية كود الحجز مطلوبة",
    "بداية كود الحجز غير صالحة",
    
    "عدد ساعات انتظار التاكيد مطلوبة",
    "عدد ساعات انتظار التاكيد غير صالحة",

    "نوع الفعل مطلوب",
    "نوع الفعل غير صالح",

    "كود فرع اخذ السيارة مطلوب",
    "كود فرع اخذ السيارة غير صالح",
    
    "كود فرع اعادة السيارة مطلوب",
    "كود فرع اعادة السيارة غير صالح",

    "بداية تاريخ الحجز مطلوبة",
    "بداية تاريه الحجز غير صالحة" ,

    "تاريخ الاعادة مطلوب",
    "تاريه الاعادة غير صالح",

    "تاريخ انتهاء الصلاحية مطلوب",
    "تاريخ انتهاء الصلاحية غير صالح",

    "نوع الحجز مطلوب",
    "نوع الحجز غير صالح",

    "اسم العنصر مطلوب",
    "اسم العنصر غير صالح",

    "عدد المشاهدات مطلوب",
    "عدد المشاهدات غير صالح",

    "كود المدينة مطلوب",
    "كود المدينة غير صالح",

    "الزائر غير موجود",

    "الرسالة مطلوبة",
    "الرسالة غير صالحة",
};
