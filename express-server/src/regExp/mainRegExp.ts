import { RegExpMessageModel } from './../models/message';
import { RegExpCityModel } from '../models/city';
import { RegExpOnlinePaymentModel } from '../models/onlinePayment';
import { RegExpGuestModel } from '../models/guest';
import { RegExpNationalityModel } from '../models/nationality';
import { RegExpBookingModel } from '../models/booking';
import { RegExpClientModel } from '../models/client';
import { RegExpCarTypeModel } from '../models/carType';
import { RegExpCarModelModel } from '../models/carModel';
import { RegExpCarModel } from '../models/car';
import { RegExpBranchModel } from '../models/branch';
import { RegExpEmployeeGroupModel } from '../models/employeeGroup';
import { RegExpEmployeeActionModel } from '../models/employeeAction';
import { RegExpAdminModel } from '../models/admin';
import { RegExpEmployeeModel } from '../models/employee';
import { RegExpEmployeeTimerModel } from '../models/employeeTimer';
import { RegExpCarGroupModel } from '../models/carGroup';

export const mainRegExp = {
    branch : RegExpBranchModel,
    employeeGroup : RegExpEmployeeGroupModel,
    employeeAction : RegExpEmployeeActionModel,
    admin : RegExpAdminModel,
    employee : RegExpEmployeeModel,
    employeeTimer : RegExpEmployeeTimerModel ,
    carGroup : RegExpCarGroupModel,
    car : RegExpCarModel,
    carModel : RegExpCarModelModel,
    carType : RegExpCarTypeModel,
    client : RegExpClientModel,
    booking : RegExpBookingModel,
    nationality : RegExpNationalityModel,
    guest : RegExpGuestModel,
    onlinePayment : RegExpOnlinePaymentModel,
    city : RegExpCityModel,
    message : RegExpMessageModel,
    view : {
        page   : /^(([1-9])|([1-9][0-9])){1,4}$/  ,
        limit  : /^(([1-9])|([1-9][0-9])){1,3}$/ ,
        sortBy : /^((\-\w+)|\w+)$/  , 
        searchQuery : /^.{1,250}$/
    },

};




 