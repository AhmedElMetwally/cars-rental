import { createClient , RedisClient }  from 'redis';

import { log } from './log';


export const redisClient: RedisClient = createClient(process.env.REDIS_URL)
.on('connect' , () => {
    log('  Redis Work')
})
.on('error' , ( err ) => {
    throw err;
});

