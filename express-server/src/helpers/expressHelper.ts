import { InextError } from '../types/InextError';
import { Request, Response, NextFunction } from 'express';
import { mainErrorEnum } from './../enums/error.enum';

export class ExpressHelper {
    constructor() {};
    public expressErrorHandler = async (nextError: InextError ,req: Request , res: Response , next: NextFunction ) =>  {
        
        if(nextError.title === 'jwt-express-roles') {
            // error from jwt
            // i will create my error here

            return res.status(401).json({ 
                errorEnum : mainErrorEnum.token_invalid
            });

        } else {
            // sendCustomError 

            return res.status( nextError.status ).json({
                errorEnum : nextError.errorEnum
            });
            
        };

    };
    public sendCustomError(errorEnum: number , next: NextFunction) {
        
        const nextError: InextError = {
            status : 400,
            errorEnum
        };

        return next(nextError);

    };
};

export const expressHelper: ExpressHelper = new ExpressHelper();