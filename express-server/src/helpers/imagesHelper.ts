import path from 'path';
import uniqid from 'uniqid';
import fs from 'fs-extra';
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');

class ImageHelper {
    constructor(){};

    public uploadImage(image: any): Promise<string> {
        return new Promise( async (resolve , reject) => {
            await this.createImagesTmpFolder();

            const src = `${uniqid()}.jpg`;
            image.mv(
            path.resolve(`./${process.env.IMAGES_TMP_SRC}/${src}`)
            , async (err: any) => {
                if(err) {
                    reject(err)
                } else {
                    await this.useImageMin();
                    await this.clearImagesTmp();
                    resolve(src)
                };
            });
            
        });
    };
    public deleteImage(imageSrc: string): void {
        fs.unlink(path.resolve(`./${process.env.IMAGES_SRC}/${imageSrc}`), _err  => {});
    };
    private useImageMin(): Promise<any> {
        return new Promise( (resolve) => {
            imagemin([
                path.resolve(`./${process.env.IMAGES_TMP_SRC}/*.jpg`)
            ], path.resolve(`./${process.env.IMAGES_SRC}`) ,
            {
                plugins: [
                    imageminJpegtran(),
                ]
            }).then(() => {
                resolve();
            })
            .catch(() => {
                resolve();
            });
        });
    };
    private clearImagesTmp(): Promise<any> {
        return new Promise( (resolve) => {
            return fs.emptyDir(path.resolve(`./${process.env.IMAGES_TMP_SRC}`))
            .then(() => {
                resolve();
            })
            .catch(() => {
                resolve();
            });
        });
    };
    private createImagesTmpFolder(): Promise<any> {
        return new Promise( (resolve) => {
            const IMAGES_TMP_SRC = path.resolve(`./${process.env.IMAGES_TMP_SRC}`);
            if (fs.existsSync(IMAGES_TMP_SRC)){ 
                resolve();
            } else {
                fs.mkdirSync(IMAGES_TMP_SRC);
                resolve();
            };
        });
    };
    
};

export const imageHelper: ImageHelper = new ImageHelper();

