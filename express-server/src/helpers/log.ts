import dotenv from 'dotenv';
dotenv.config({ path: '.env' });

export const log = ( str: string ) => {

    let testArg = process.argv[2] === 'test'; 
    let testEnv = process.env.NODE_ENV === 'test'; 

    if( !(testArg || testEnv) ) {
        console.log('\x1b[36m%s\x1b[0m', str);
    };

}

export const isLog = (): boolean => {
   
    let testArg = process.argv[2] === 'test'; 
    let testEnv = process.env.NODE_ENV === 'test'; 

    if( ! (testArg || testEnv) ) {
        return true;
    };

    return false;
    
}