import bcrypt from 'bcrypt';

export class MainHelper {
    constructor() {};
    public isExists(str: string): boolean {
        return typeof str !== 'undefined';
    };
    public isUniqueError(_error: any): boolean {
        return _error.code === 11000;
    };
    public isUpdateFail(result: any): boolean {
        return result.n === 0;
    };
    public hashSync(password: string): string {
        return bcrypt.hashSync(password , 5);
    };
    public compareSync(password: string , hash: string): boolean {
        return bcrypt.compareSync(password  , hash);
    };
};

export const mainHelper: MainHelper = new MainHelper();