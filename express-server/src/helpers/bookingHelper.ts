import { BranchModel, IBranchModel } from './../models/branch';
import { ICarModel, CarModel, Estatus } from './../models/car';
import { CarGroupModel, ICarGroupModel } from '../models/carGroup';
import { IBookingModel, BookingModel, EbookingType } from '../models/booking';
import dayjs from 'dayjs';
import math from 'mathjs';

export interface IViewCarGroups {
    page: number,
    limit: number,
    pickUpDate: string|Date,
    returnDate: string|Date,
    filters: {
        branch?: string|any;
        carType?: string;
        carModel?: string;
        year?: string;
    }
};

class BookingHelper {
    constructor(){};


    private getDaysBetweenTwoDates = (from: string, to: string): string[] => {
        const _from = new Date(from);
        const _to = new Date(to);

        const dateArray: string[] = new Array();
        let currentDate = _from;
        while (currentDate <= _to) {
            let _currentDate = dayjs(currentDate);
            dateArray.push(
                _currentDate.toISOString()
            );
            currentDate = _currentDate.add(1 , 'day').toDate();
        };
        return dateArray;
    };
    public getTotlaPrice = (pricePerDay: number ,discountPerDay: number ,daysCount: number): number => {
        return math
        .chain(pricePerDay)
        .subtract(discountPerDay)
        .multiply(daysCount)
        .done();
    };
    public getDaysCount = (pickUpDate: string|Date ,returnDate: string|Date): number => {
        if( new Date(returnDate as any) > new Date(pickUpDate  as any) ) {
            const date1 = new Date(returnDate as any);
            const date2 = new Date(pickUpDate as any);
            const timeDiff = Math.abs(date2.getTime() - date1.getTime());
            const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
            return diffDays
            // return dayjs(returnDate).diff( dayjs(pickUpDate) , 'day');
        } else {
            return 0
        }
    };


    // is can add car to booking 
    public isCanAddCarToBooking = async (car_id: string, booking_id: string): Promise<boolean> => {
       try {
           const booking: IBookingModel = await BookingModel.findOne({
                _id : booking_id,
                deleted : 0,
            })
            .select('pickUpDate returnDate')
            .exec();

            if(booking) {
                const carBookingsCount: number = await BookingModel.countDocuments({
                    car : car_id,
                    deleted : 0,
                    bookingType : EbookingType.active,
                    _id : {
                        $ne : booking_id
                    },
                    $or: [ 
                    
                        //  - [ ] -
                        {
                            pickUpDate : {
                                $gte : booking.pickUpDate
                            },
                            returnDate : {
                                $lte : booking.returnDate
                            },
                        },

                        //  [ - - ]
                        {
                            pickUpDate : {
                                $lte : booking.pickUpDate
                            },
                            returnDate : {
                                $gte : booking.returnDate
                            },
                        },

                        //  [ - ] -
                        {
                            $and : [
                                {
                                    pickUpDate : {
                                        $gte : booking.pickUpDate
                                    },
                                    returnDate : {
                                        $gte : booking.returnDate
                                    },
                                },
                                {
                                    pickUpDate : {
                                        $lte : booking.returnDate
                                    },
                                    returnDate : {
                                        $gte : booking.pickUpDate
                                    },
                                }
                            ]
                        },

                        //  - [ - ]
                        {
                            $and : [
                                {
                                    pickUpDate : {
                                        $lte : booking.pickUpDate
                                    },
                                    returnDate : {
                                        $lte : booking.returnDate
                                    },
                                },
                                {
                                    pickUpDate : {
                                        $lte : booking.returnDate
                                    },
                                    returnDate : {
                                        $gte : booking.pickUpDate
                                    },
                                }
                            ]
                        },

                    ]
                })
                .exec();

                return ! carBookingsCount;

            } else {
                return false
            };
        } catch(_) {
            return false
        };
    };
    // ./is can add car to booking 


    // is can edit booking date
    public isCanEditBookingDate = async (booking_id: string , pickUpDate: string|Date ,returnDate: string|Date): Promise<any> => {
        try {

            if( new Date(returnDate  as any) > new Date(pickUpDate  as any) ) {

                const booking: IBookingModel = await BookingModel.findOne({
                    _id: booking_id,
                    deleted : 0,
                })
                .exec();

                if(booking) {
                    if(booking.car) {
                    // check by car
                        return this.isCanEditBookingDateInCar(
                            booking.car as string ,
                            pickUpDate, 
                            returnDate,
                            booking_id
                        )
                    } else {
                    // check by carGroup
                        return this.isCanEditBookingDateInCarGroup(
                            booking.carGroup as string ,
                            pickUpDate, 
                            returnDate,
                            booking_id
                        )
                    };

                } else {
                    return false
                };

            } else {

                
            };


        } catch(_) {
            return false;
        };
        
    };
    private isCanEditBookingDateInCar = async (car_id: string, pickUpDate: string|Date ,returnDate: string|Date, booking_id: string): Promise<boolean> => {
        try {
            const carBookingsCount: number = await BookingModel.countDocuments({
                car : car_id,
                bookingType : EbookingType.active,
                deleted : 0,
                _id : {
                    $ne : booking_id
                },
                $or: [ 
                
                    //  - [ ] -
                    {
                        pickUpDate : {
                            $gte : pickUpDate
                        },
                        returnDate : {
                            $lte : returnDate
                        },
                    },

                    //  [ - - ]
                    {
                        pickUpDate : {
                            $lte : pickUpDate
                        },
                        returnDate : {
                            $gte : returnDate
                        },
                    },

                    //  [ - ] -
                    {
                        $and : [
                            {
                                pickUpDate : {
                                    $gte : pickUpDate
                                },
                                returnDate : {
                                    $gte : returnDate
                                },
                            },
                            {
                                pickUpDate : {
                                    $lte : returnDate
                                },
                                returnDate : {
                                    $gte : pickUpDate
                                },
                            }
                        ]
                    },

                    //  - [ - ]
                    {
                        $and : [
                            {
                                pickUpDate : {
                                    $lte : pickUpDate
                                },
                                returnDate : {
                                    $lte : returnDate
                                },
                            },
                            {
                                pickUpDate : {
                                    $lte : returnDate
                                },
                                returnDate : {
                                    $gte : pickUpDate
                                },
                            }
                        ]
                    },

                ]
            })
            .exec();

            return ! carBookingsCount;
        } catch (_) {
            return false;
        };
    };
    private isCanEditBookingDateInCarGroup = async (carGroup_id: string, pickUpDate: string|Date ,returnDate: string|Date, booking_id: string): Promise<boolean> => {
        try {

            const carsInGroups: ICarModel[] = await CarModel.find({
                carGroup : carGroup_id
            })
            .select('_id')
            .lean()
            .exec();

            const cars_idsInCarGroup = carsInGroups.map( car => {
                return car._id
            });

            const carsBookingsCount: number = await BookingModel.countDocuments({
                car : {
                    $in : cars_idsInCarGroup
                },
                deleted : 0,
                _id : {
                    $ne : booking_id
                },
                bookingType : EbookingType.active,
                $or: [ 
                
                    //  - [ ] -
                    {
                        pickUpDate : {
                            $gte : pickUpDate
                        },
                        returnDate : {
                            $lte : returnDate
                        },
                    },

                    //  [ - - ]
                    {
                        pickUpDate : {
                            $lte : pickUpDate
                        },
                        returnDate : {
                            $gte : returnDate
                        },
                    },

                    //  [ - ] -
                    {
                        $and : [
                            {
                                pickUpDate : {
                                    $gte : pickUpDate
                                },
                                returnDate : {
                                    $gte : returnDate
                                },
                            },
                            {
                                pickUpDate : {
                                    $lte : returnDate
                                },
                                returnDate : {
                                    $gte : pickUpDate
                                },
                            }
                        ]
                    },

                    //  - [ - ]
                    {
                        $and : [
                            {
                                pickUpDate : {
                                    $lte : pickUpDate
                                },
                                returnDate : {
                                    $lte : returnDate
                                },
                            },
                            {
                                pickUpDate : {
                                    $lte : returnDate
                                },
                                returnDate : {
                                    $gte : pickUpDate
                                },
                            }
                        ]
                    },

                ]
            })
            .exec();

            return carsBookingsCount < cars_idsInCarGroup.length;

        } catch (_) {
            return false;
        };
    };
    // ./is can edit booking date


    // is can edit expiredAt
    public isCanEditExpiredAt = async (booking_id: string , expiredAt: string|Date): Promise<boolean> => {
        try {
            const booking: IBookingModel = await BookingModel.findOne({
                _id : booking_id,
                bookingType : EbookingType.waiting_to_confirm,
                deleted : 0
            })
            .select('pickUpDate')
            .exec();
            return new Date(expiredAt as any) < new Date(booking.pickUpDate as any) &&
                new Date(expiredAt as any) > new Date() ;
        } catch(_) {
            return false
        };   

    };
    // ./is can edit expiredAt


    // view carGroups to booking [in client]
    private getAllCarGroupsWhoHaveAvailableCars = async (): Promise<string[]> => {
        const cars: ICarModel[] = await CarModel.find({
            deleted : 0,
            status: Estatus.available
        })
        .select('carGroup')
        .exec()
        const carGroups_ids: string[] = cars.map( car => car.carGroup as string );
        return carGroups_ids
    };
    private getAllCarGroupsWhoBookingInDate = async (pickUpDate: string|Date ,returnDate: string|Date): Promise<string[]> => {
        const bookings: IBookingModel[] = await BookingModel.find({
            deleted : 0,
            bookingType : EbookingType.active,
            $or: [ 
            
                //  - [ ] -
                {
                    pickUpDate : {
                        $gte : pickUpDate
                    },
                    returnDate : {
                        $lte : returnDate
                    },
                },

                //  [ - - ]
                {
                    pickUpDate : {
                        $lte : pickUpDate
                    },
                    returnDate : {
                        $gte : returnDate
                    },
                },

                //  [ - ] -
                {
                    $and : [
                        {
                            pickUpDate : {
                                $gte : pickUpDate
                            },
                            returnDate : {
                                $gte : returnDate
                            },
                        },
                        {
                            pickUpDate : {
                                $lte : returnDate
                            },
                            returnDate : {
                                $gte : pickUpDate
                            },
                        }
                    ]
                },

                //  - [ - ]
                {
                    $and : [
                        {
                            pickUpDate : {
                                $lte : pickUpDate
                            },
                            returnDate : {
                                $lte : returnDate
                            },
                        },
                        {
                            pickUpDate : {
                                $lte : returnDate
                            },
                            returnDate : {
                                $gte : pickUpDate
                            },
                        }
                    ]
                },

            ]
        })
        .select('carGroup')
        .exec();
        const carGroups_ids: string[] = bookings.map( booking => booking.carGroup as string );
        return carGroups_ids
    };
    public getBranches_idsByCity_id = async (city_id: string): Promise<string[]> => {
        const branches_ids: IBranchModel[] = await BranchModel.find({
            city : city_id
        })
        .select('_id')
        .exec();

        return branches_ids.map( v => v._id);
    }
    public viewCarGroupsToBookingOne = async ($event: IViewCarGroups): Promise<ICarGroupModel[]> => {
        return CarGroupModel.find({
            deleted : 0,
            ...$event.filters,
            _id : {
                $nin : await this.getAllCarGroupsWhoBookingInDate($event.pickUpDate ,$event.returnDate),
                $in : await this.getAllCarGroupsWhoHaveAvailableCars()
            }
        })
        .skip(($event.page - 1) * $event.limit) 
        .limit($event.limit) 
        .populate('carType carModel')
        .populate({
            path : 'branch',
            populate : {
                path : 'city'
            }
        })
        .lean()
        .exec();
    };
    // ./view carGroups to booking [in client]


    // is can make booking 
    public isCanBookingCarGroupInDate = async (carGroup_id: string, pickUpDate: string|Date, returnDate: string|Date): Promise<boolean> => {
        try {
            const carsInGroups: ICarModel[] = await CarModel.find({
                carGroup : carGroup_id,
            })
            .select('_id')
            .lean()
            .exec();

            const cars_idsInCarGroup = carsInGroups.map( car => {
                return car._id
            });

            const carsBookingsCount: number = await BookingModel.countDocuments({
                car : {
                    $in : cars_idsInCarGroup
                },
                bookingType : EbookingType.active ,
                deleted : 0,
                $or: [ 
                
                    //  - [ ] -
                    {
                        pickUpDate : {
                            $gte : pickUpDate
                        },
                        returnDate : {
                            $lte : returnDate
                        },
                    },

                    //  [ - - ]
                    {
                        pickUpDate : {
                            $lte : pickUpDate
                        },
                        returnDate : {
                            $gte : returnDate
                        },
                    },

                    //  [ - ] -
                    {
                        $and : [
                            {
                                pickUpDate : {
                                    $gte : pickUpDate
                                },
                                returnDate : {
                                    $gte : returnDate
                                },
                            },
                            {
                                pickUpDate : {
                                    $lte : returnDate
                                },
                                returnDate : {
                                    $gte : pickUpDate
                                },
                            }
                        ]
                    },

                    //  - [ - ]
                    {
                        $and : [
                            {
                                pickUpDate : {
                                    $lte : pickUpDate
                                },
                                returnDate : {
                                    $lte : returnDate
                                },
                            },
                            {
                                pickUpDate : {
                                    $lte : returnDate
                                },
                                returnDate : {
                                    $gte : pickUpDate
                                },
                            }
                        ]
                    },

                ]
            })
            .exec();

            return carsBookingsCount < cars_idsInCarGroup.length;

        } catch(_) {
            return false;
        };
    };
    // ./is can make booking 


    // is can active booking
    public isCanActiveBooking = async (booking_id: string): Promise<boolean> => {
        try {
            const booking: IBookingModel = await BookingModel.findOne({
                _id: booking_id,
                deleted : 0,
            })
            .exec();

            if(booking) {

                if(booking.car) {
                // check by car
                    return await this.isCanActiveBookingInCar(
                        booking.car as string ,
                        booking.pickUpDate, 
                        booking.returnDate,
                    )
                } else {
                // check by carGroup
                    return await this.isCanActiveBookingInCarGroup(
                        booking.carGroup as string ,
                        booking.pickUpDate, 
                        booking.returnDate,
                    )
                };

            } else {
                return false
            };
        } catch(_){
            return false;
        }
    };
    private isCanActiveBookingInCar = async (car_id: string, pickUpDate: string|Date ,returnDate: string|Date): Promise<boolean> => {
        try {
            const carBookingsCount: number = await BookingModel.countDocuments({
                car : car_id,
                bookingType : EbookingType.active,
                deleted : 0,
                $or: [ 
                
                    //  - [ ] -
                    {
                        pickUpDate : {
                            $gte : pickUpDate
                        },
                        returnDate : {
                            $lte : returnDate
                        },
                    },

                    //  [ - - ]
                    {
                        pickUpDate : {
                            $lte : pickUpDate
                        },
                        returnDate : {
                            $gte : returnDate
                        },
                    },

                    //  [ - ] -
                    {
                        $and : [
                            {
                                pickUpDate : {
                                    $gte : pickUpDate
                                },
                                returnDate : {
                                    $gte : returnDate
                                },
                            },
                            {
                                pickUpDate : {
                                    $lte : returnDate
                                },
                                returnDate : {
                                    $gte : pickUpDate
                                },
                            }
                        ]
                    },

                    //  - [ - ]
                    {
                        $and : [
                            {
                                pickUpDate : {
                                    $lte : pickUpDate
                                },
                                returnDate : {
                                    $lte : returnDate
                                },
                            },
                            {
                                pickUpDate : {
                                    $lte : returnDate
                                },
                                returnDate : {
                                    $gte : pickUpDate
                                },
                            }
                        ]
                    },

                ]
            })
            .exec();

            return ! carBookingsCount;
        } catch (_) {
            return false;
        };
    };
    private isCanActiveBookingInCarGroup = async (carGroup_id: string, pickUpDate: string|Date ,returnDate: string|Date): Promise<boolean> => {
        try {

            const carsInGroups: ICarModel[] = await CarModel.find({
                carGroup : carGroup_id
            })
            .select('_id')
            .lean()
            .exec();

            const cars_idsInCarGroup = carsInGroups.map( car => {
                return car._id
            });

            const carsBookingsCount: number = await BookingModel.countDocuments({
                car : {
                    $in : cars_idsInCarGroup
                },
                deleted : 0,
                bookingType : EbookingType.active,
                $or: [ 
                
                    //  - [ ] -
                    {
                        pickUpDate : {
                            $gte : pickUpDate
                        },
                        returnDate : {
                            $lte : returnDate
                        },
                    },

                    //  [ - - ]
                    {
                        pickUpDate : {
                            $lte : pickUpDate
                        },
                        returnDate : {
                            $gte : returnDate
                        },
                    },

                    //  [ - ] -
                    {
                        $and : [
                            {
                                pickUpDate : {
                                    $gte : pickUpDate
                                },
                                returnDate : {
                                    $gte : returnDate
                                },
                            },
                            {
                                pickUpDate : {
                                    $lte : returnDate
                                },
                                returnDate : {
                                    $gte : pickUpDate
                                },
                            }
                        ]
                    },

                    //  - [ - ]
                    {
                        $and : [
                            {
                                pickUpDate : {
                                    $lte : pickUpDate
                                },
                                returnDate : {
                                    $lte : returnDate
                                },
                            },
                            {
                                pickUpDate : {
                                    $lte : returnDate
                                },
                                returnDate : {
                                    $gte : pickUpDate
                                },
                            }
                        ]
                    },

                ]
            })
            .exec();

            return carsBookingsCount < cars_idsInCarGroup.length;

        } catch (_) {
            return false;
        };
    };
    // ./is can active booking


    public viewAllCarsWhoCanAddToBooking = async (booking_id: string): Promise<ICarModel[]> => {

        const booking: IBookingModel = await BookingModel.findOne({
            _id: booking_id
        })
        .exec();

        const bookings: IBookingModel[] = await BookingModel.find({
            car : {$ne : null},
            deleted : 0,
            bookingType : EbookingType.active,
            carGroup : booking.carGroup,
            _id : {
                $ne : booking_id
            },
            $or: [ 
            
                //  - [ ] -
                {
                    pickUpDate : {
                        $gte : booking.pickUpDate
                    },
                    returnDate : {
                        $lte : booking.returnDate
                    },
                },

                //  [ - - ]
                {
                    pickUpDate : {
                        $lte : booking.pickUpDate
                    },
                    returnDate : {
                        $gte : booking.returnDate
                    },
                },

                //  [ - ] -
                {
                    $and : [
                        {
                            pickUpDate : {
                                $gte : booking.pickUpDate
                            },
                            returnDate : {
                                $gte : booking.returnDate
                            },
                        },
                        {
                            pickUpDate : {
                                $lte : booking.returnDate
                            },
                            returnDate : {
                                $gte : booking.pickUpDate
                            },
                        }
                    ]
                },

                //  - [ - ]
                {
                    $and : [
                        {
                            pickUpDate : {
                                $lte : booking.pickUpDate
                            },
                            returnDate : {
                                $lte : booking.returnDate
                            },
                        },
                        {
                            pickUpDate : {
                                $lte : booking.returnDate
                            },
                            returnDate : {
                                $gte : booking.pickUpDate
                            },
                        }
                    ]
                },

            ]
        })
        .exec();

        const blackListCars: string[] = bookings.map( v => v.car as string);

        return CarModel.find({
            deleted : 0,
            status : Estatus.available,
            _id : {
                $nin : blackListCars
            }
        })
        .sort('-createdAt')
        .select('plateNumber')
        .exec()

    };

    public setBookingsType = (bookings: IBookingModel[]): IBookingModel[] => {
        return bookings.map( booking => this.setBookingType(booking));
    };
    public setBookingType = (booking: IBookingModel): IBookingModel => {
        if(
            booking.bookingType === EbookingType.waiting_to_confirm 
            && new Date() > new Date(booking.expiredAt)
        ) {
            booking.bookingType = EbookingType.expired
        } else if(
            booking.bookingType === EbookingType.active 
            && new Date() > new Date(booking.returnDate)
        ) {
            booking.bookingType = EbookingType.done
        } 
        
        return booking
    };

};

export const bookingHelper: BookingHelper = new BookingHelper()


