import { dateHelper } from './dateHelper';
import { IEmployeeTimerModel, IEmployeeStatistics } from '../models/employeeTimer';

class EmployeeTimerHelper {
    constructor(){
    };

    // use in AdminCTRL
    public createCompleteStatisticsOfYear = (statistics: IEmployeeTimerModel[] , year: number): IEmployeeStatistics[] => {
      
        const allDatesOfThisYear: string[] = this.getAllDatesOfYear( year );
        const completeStatistic: IEmployeeStatistics[] = [];

        for(let i = 0 , len = allDatesOfThisYear.length ; i < len ; ++i ) {
            
            let tmp: IEmployeeStatistics = {
                timeMs : 0,
                date : allDatesOfThisYear[i]
            }

            // check if this date exist
            for(let x = 0 , _len = statistics.length ; x < _len ; ++x ) {
                if( statistics[x].date === allDatesOfThisYear[i] ) {
                    tmp.timeMs = statistics[x].timeMs;
                    break;
                };
            };

            completeStatistic.push( tmp );

        };

        return completeStatistic;

    };
    public createCompleteStatisticsOfMonth = (statistics: IEmployeeTimerModel[] , year: number , month: number): IEmployeeStatistics[] => {
        
        const allDatesOfThisMonth: string[] = this.getAllDatesOfMonth( year , month );
        const completeStatistic: IEmployeeStatistics[] = [];

        for(let i = 0 , len = allDatesOfThisMonth.length ; i < len ; ++i ) {
            
            let tmp: IEmployeeStatistics = {
                timeMs : 0,
                date : allDatesOfThisMonth[i]
            }

            // check if this date exist
            for(let x = 0 , _len = statistics.length ; x < _len ; ++x ) {
                if( statistics[x].date === allDatesOfThisMonth[i] ) {
                    tmp.timeMs = statistics[x].timeMs;
                    break;
                };
            };

            completeStatistic.push( tmp );

        };

        return completeStatistic;

    };
    public getDaysCountOfMonth = (year: number, month: number): number => {
        var isLeap = ((year % 4) == 0 && ((year % 100) != 0 || (year % 400) == 0));
        return [31, (isLeap ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][ month];
    };

    // use here
    private getAllDatesOfMonth = (year: number , month: number): string[] => {
    
        let allDaysOfMonth = [];

        let mLen = this.getDaysCountOfMonth(year , month);

        for(let d = 1 ; d <= mLen ; d += 1) {
    
            allDaysOfMonth.push( 
                dateHelper.getDayFromDates(year , month  , d)
            );
    
        };
    
        return allDaysOfMonth
        
    };
    private getAllDatesOfYear = (year: number): string[] => {
    
        let allDaysOfYear = [];
        
        for(let m = 0 ; m < 12 ; m += 1) {
            allDaysOfYear.push(
                ...this.getAllDatesOfMonth(year , m)
            );
        };
    
        return allDaysOfYear
        
    };
};

export const employeeTimerHelper: EmployeeTimerHelper = new EmployeeTimerHelper();