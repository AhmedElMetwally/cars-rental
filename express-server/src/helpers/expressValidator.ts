import { mainRegExp } from '../regExp/mainRegExp';
import { Escreens } from '../models/employeeGroup';
import { $enum } from "ts-enum-util";


// sent error Code only
// await req.validationErrors(); errorCode[]
const errorFormatter = ( _param: any , errorCode: any|number ) => errorCode;


// check functions
const checkScreens = ( screens: Escreens[] ): boolean => {
  
  if( ! Array.isArray(screens) ) return false;
  if(screens.length === 0) return true;

  // array of all screens numbers
  const allScreens = $enum( Escreens as any ).getValues();

  // check if all screens item in Escreens
  return screens.every( v => allScreens.includes(v) );

};
const checkBranches = ( branches_ids: string[] ): boolean => {

  if( ! Array.isArray(branches_ids) ) return false;
  if(branches_ids.length === 0) return true;
  
  return branches_ids.every( v => mainRegExp.branch._id.test( v ) );

};
const isLocation = ( location: [ number , number ] ): boolean => {
  if(location.length !== 2) return false;
  if( ! mainRegExp.branch.location.test(location[0] + '') ) return false;
  if( ! mainRegExp.branch.location.test(location[1] + '') ) return false;
  return true
};

const customValidators =  {
  checkScreens,
  checkBranches ,
  isLocation ,
};

export const expressValidatorConfig = {
  errorFormatter,
  customValidators
}