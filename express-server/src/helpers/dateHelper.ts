import dayjs from 'dayjs';


(Date as any).prototype.addDays = function(days: any) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
};

class DateHelper {
    constructor(){};
    public getThisDay = (): string => {
        const currentDate: Date = new Date();
        const date: string = dayjs( 
            new Date( 
                currentDate.getFullYear() , 
                currentDate.getMonth()  , 
                currentDate.getDate() 
            )  
        ).format('YYYY-MM-DD');
        return date;
    };
    public getDayFromFullDate = (date: string|Date): string => {
        return dayjs(date).format('YYYY-MM-DD');
    };
    public getDayFromDates = (y: number , m: number , d: number): string => {
        return dayjs( new Date(y ,  m  , d)  ).format('YYYY-MM-DD');
    };
};

export const dateHelper: DateHelper = new DateHelper();