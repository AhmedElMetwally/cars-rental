import { ICarGroupModel, CarGroupModel } from '../models/carGroup';
import { CarModel } from '../models/car';

class CarGroupHelper {
    constructor(){};

    public setCarsCountInCarGroups = (carGroups: ICarGroupModel[]): Promise<ICarGroupModel[]> => {
        return new Promise( resolve => {
            
            const carGroups_ids = [];
            
            for(let i = 0 ;i < carGroups.length; ++i) {
                // default value
                carGroups[i].carsCount = 0;
                
                // get _ids 
                carGroups_ids.push(carGroups[i]._id);
            };

            CarModel.aggregate([
                { "$match": { "carGroup": {$in : carGroups_ids } }},
                { "$group": {
                    "_id": {
                        "carGroup": "$carGroup",
                    },
                    "count": { "$sum": 1 }
                }},
            ])
            .then( (results: any[]) => {

                for(let j = 0 ;j < results.length; ++j) {
                    const carGroup_id = results[j]._id.carGroup;
                    const count = results[j].count;

                    for(let i = 0 ;i < carGroups.length; ++i) {
                        if(carGroups[i]._id.toString() === carGroup_id.toString()) {
                            carGroups[i].carsCount = count;
                            // break;
                        }; 
                    };

                };
                
                resolve(carGroups);
            })
            .catch( () => {
                resolve(carGroups);
            });

        });
    };
    public setCarsCountInCarGroup = (carGroup: ICarGroupModel): Promise<ICarGroupModel> => {
        return new Promise( resolve => {
            // default value
            carGroup.carsCount = 0;

            CarModel.countDocuments({
                carGroup : carGroup._id
            })
            .exec()
            .then( carsCount => {
                carGroup.carsCount = carsCount;
                resolve(carGroup);
            })
            .catch( () => {
                resolve(carGroup);
            });
        });
    };
    public setViewInCarGroup = (carGroup: ICarGroupModel): void => {
        CarGroupModel.updateOne({
            _id : carGroup._id,
            deleted : 0
        } , {
            $inc : {
                views : 1
            }
        })
        .exec()
        .then(() => {})
        .catch(() => {})
    };
    public setViewInCarGroups = (carGroups: ICarGroupModel[]): void => {
        CarGroupModel.updateOne({
            _id : {
                $in : carGroups.map( v => v._id),
            },
            deleted : 0
        } , {
            $inc : {
                views : 1
            }
        })
        .exec()
        .then(() => {})
        .catch(() => {})
    };

};

export const carGroupHelper: CarGroupHelper = new CarGroupHelper();