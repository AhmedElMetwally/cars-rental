import JwtExpressRoles from 'jwt-express-roles';
import dotenv from 'dotenv';

dotenv.config({ path: '.env' });

export const jwt = new JwtExpressRoles({
    roleField : 'roles',
    secretKey : process.env.JWT_SECRET,
    expiresIn : '1000h',
});