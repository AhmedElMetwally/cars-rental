import { expressHelper } from '../helpers/expressHelper';
import { employeeCTRL } from '../controllers/employee';
import { Router } from 'express';
import fileUpload from 'express-fileupload';
import { Escreens } from '../models/employeeGroup';

export const EmployeeRouter: Router = Router(); // /api/employee/[endPoint]


// auth
EmployeeRouter.post('/signin', employeeCTRL.signin);
EmployeeRouter.get('/refresh-token', employeeCTRL.guard, employeeCTRL.refreshToken);



// This Employee
EmployeeRouter.get('/view-this-employee', employeeCTRL.guard, employeeCTRL.viewThisEmployee);
EmployeeRouter.put('/edit-this-employee', employeeCTRL.guard, employeeCTRL.editThisEmployee);
EmployeeRouter.put('/edit-this-employee-password', employeeCTRL.guard, employeeCTRL.editThisEmployeePassword);
EmployeeRouter.get('/view/this-employee-time-statistics-in-year', employeeCTRL.guard, employeeCTRL.viewThisEmployeeTimeStatisticsInYear);
EmployeeRouter.get('/view/this-employee-time-statistics-in-month', employeeCTRL.guard, employeeCTRL.viewThisEmployeeTimeStatisticsInMonth);
EmployeeRouter.get('/view/this-employee-actions', employeeCTRL.guard, employeeCTRL.viewThisEmployeeActions);
EmployeeRouter.get('/view/this-employee-actions-by-item-name-filter', employeeCTRL.guard, employeeCTRL.viewEmployeeActionsByItemNameFilter);



// client
EmployeeRouter.put('/client/edit', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.edit_client), employeeCTRL.editClient);
EmployeeRouter.put('/client/edit-password', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.edit_client_password), employeeCTRL.editClientPassword);
EmployeeRouter.delete('/client/delete', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.delete_client), employeeCTRL.deleteClient);
EmployeeRouter.post('/client/restore', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.restore_client), employeeCTRL.restoreClient);
EmployeeRouter.post('/client/add-image', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.edit_client_image), fileUpload(), employeeCTRL.addClientImage);
EmployeeRouter.get('/view/client', employeeCTRL.guard, employeeCTRL.viewClient);
EmployeeRouter.get('/view/clients', employeeCTRL.guard, employeeCTRL.viewClients);
EmployeeRouter.get('/view/clients-search', employeeCTRL.guard, employeeCTRL.viewClientsSearch);
EmployeeRouter.get('/view/bookings-for-client', employeeCTRL.guard, employeeCTRL.viewBookingsForClient);
EmployeeRouter.get('/view/online-payments-for-client', employeeCTRL.guard, employeeCTRL.viewOnlinePaymentsForClient);


// guest
EmployeeRouter.get('/view/guest', employeeCTRL.guard, employeeCTRL.viewGuest);
EmployeeRouter.get('/view/guests', employeeCTRL.guard, employeeCTRL.viewGuests);
EmployeeRouter.get('/view/guests-search', employeeCTRL.guard, employeeCTRL.viewGuestsSearch);


// car
EmployeeRouter.post('/car/add', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.add_car), employeeCTRL.addCar);
EmployeeRouter.put('/car/edit', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.edit_car), employeeCTRL.editCar);
EmployeeRouter.delete('/car/delete', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.delete_car), employeeCTRL.deleteCar);
EmployeeRouter.post('/car/restore', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.restore_car), employeeCTRL.restoreCar);
EmployeeRouter.get('/view/car', employeeCTRL.guard,employeeCTRL.checkScreen(null), employeeCTRL.viewCar);
EmployeeRouter.get('/view/cars', employeeCTRL.guard, employeeCTRL.checkScreen(null), employeeCTRL.viewCars);
EmployeeRouter.get('/view/cars-search', employeeCTRL.guard, employeeCTRL.checkScreen(null), employeeCTRL.viewCarsSearch);
EmployeeRouter.get('/view/cars-by-filter', employeeCTRL.guard, employeeCTRL.checkScreen(null), employeeCTRL.viewCarsByFilter);



// branch
EmployeeRouter.post('/branch/add', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.add_branch), employeeCTRL.addBranch);
EmployeeRouter.put('/branch/edit', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.edit_branch), employeeCTRL.editBranch);
EmployeeRouter.delete('/branch/delete', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.delete_branch), employeeCTRL.deleteBranch);
EmployeeRouter.post('/branch/restore', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.restore_branch), employeeCTRL.restoreBranch);
EmployeeRouter.delete('/branch/delete-image', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.edit_branch_image), employeeCTRL.deleteBranchImage);
EmployeeRouter.post('/branch/add-image', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.edit_branch_image), fileUpload(), employeeCTRL.addBranchImage);
EmployeeRouter.get('/view/branch', employeeCTRL.guard, employeeCTRL.checkScreen(null) ,employeeCTRL.viewBranch);
EmployeeRouter.get('/view/branches', employeeCTRL.guard, employeeCTRL.checkScreen(null) , employeeCTRL.viewBranches);
EmployeeRouter.get('/view/branches-search', employeeCTRL.guard, employeeCTRL.checkScreen(null) , employeeCTRL.viewBranchesSearch);



// CarGroup
EmployeeRouter.post('/car-group/add', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.add_car_group), employeeCTRL.addCarGroup);
EmployeeRouter.put('/car-group/edit', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.edit_car_group), employeeCTRL.editCarGroup);
EmployeeRouter.delete('/car-group/delete', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.delete_car_group), employeeCTRL.deleteCarGroup);
EmployeeRouter.post('/car-group/restore', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.restore_car_group), employeeCTRL.restoreCarGroup);
EmployeeRouter.delete('/car-group/delete-image', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.edit_car_group_image), employeeCTRL.deleteCarGroupImage);
EmployeeRouter.post('/car-group/add-image', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.edit_car_group_image), fileUpload(), employeeCTRL.addCarGroupImage);
EmployeeRouter.get('/view/car-group', employeeCTRL.guard, employeeCTRL.checkScreen(null), employeeCTRL.viewCarGroup);
EmployeeRouter.get('/view/car-groups', employeeCTRL.guard, employeeCTRL.checkScreen(null), employeeCTRL.viewCarGroups);
EmployeeRouter.get('/view/car-groups-by-filter', employeeCTRL.guard, employeeCTRL.checkScreen(null), employeeCTRL.viewCarGroupsByFilter);



// CarModel
EmployeeRouter.post('/car-model/add', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.add_car_model), employeeCTRL.addCarModel);
EmployeeRouter.delete('/car-model/delete', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.delete_car_model), employeeCTRL.deleteCarModel);



// CarType 
EmployeeRouter.post('/car-type/add', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.add_car_type), employeeCTRL.addCarType);
EmployeeRouter.delete('/car-type/delete', employeeCTRL.guard, employeeCTRL.checkScreen(Escreens.delete_car_type), employeeCTRL.deleteCarType);



// Booking
EmployeeRouter.post('/booking/add-car',
    employeeCTRL.guard,
    employeeCTRL.checkScreen(Escreens.edit_booking),
    employeeCTRL.addCarToBooking
);
EmployeeRouter.put('/booking/edit-date',
    employeeCTRL.guard,
    employeeCTRL.checkScreen(Escreens.edit_booking),
    employeeCTRL.editBookingDate
);
EmployeeRouter.put('/booking/edit-expiredAt',
    employeeCTRL.guard,
    employeeCTRL.checkScreen(Escreens.edit_booking),
    employeeCTRL.editBookingExpiredAt
);
EmployeeRouter.put('/booking/edit-pricePerDay',
    employeeCTRL.guard,
    employeeCTRL.checkScreen(Escreens.edit_booking),
    employeeCTRL.editBookingPricePerDay
);
EmployeeRouter.put('/booking/edit-discountPerDay',
    employeeCTRL.guard,
    employeeCTRL.checkScreen(Escreens.edit_booking),
    employeeCTRL.editBookingDiscountPerDay
);

EmployeeRouter.delete('/booking/delete',
    employeeCTRL.guard,
    employeeCTRL.checkScreen(Escreens.delete_booking),
    employeeCTRL.deleteBooking
);
EmployeeRouter.post('/booking/restore',
    employeeCTRL.guard,
    employeeCTRL.checkScreen(Escreens.restore_booking),
    employeeCTRL.restoreBooking
);
EmployeeRouter.post('/booking/cancel',
    employeeCTRL.guard,
    employeeCTRL.checkScreen(Escreens.cancel_booking),
    employeeCTRL.cancelBooking
);
EmployeeRouter.post('/booking/approve',
    employeeCTRL.guard,
    employeeCTRL.checkScreen(Escreens.approve_booking),
    employeeCTRL.approveBooking
);
EmployeeRouter.post('/booking/active',
    employeeCTRL.guard,
    employeeCTRL.checkScreen(Escreens.active_booking),
    employeeCTRL.activeBooking
);
EmployeeRouter.get('/view/booking', employeeCTRL.guard, employeeCTRL.checkScreen(null),employeeCTRL.viewBooking);
EmployeeRouter.get('/view/bookings', employeeCTRL.guard, employeeCTRL.checkScreen(null),employeeCTRL.viewBookings); // page limit soryBy 
EmployeeRouter.get('/view/bookings-by-filter', employeeCTRL.guard, employeeCTRL.checkScreen(null),employeeCTRL.viewBookingsByFilter); // page limit soryBy branch_id bookingType
EmployeeRouter.get('/view/bookings-search', employeeCTRL.guard, employeeCTRL.checkScreen(null),employeeCTRL.viewBookingsSearch);



// options
EmployeeRouter.get('/view/all-branches-can-this-employee-access', employeeCTRL.guard, employeeCTRL.checkScreen(null), employeeCTRL.viewAllBranchesCanThisEmployeeAccess);
EmployeeRouter.get('/view/all-car-groups-can-this-employee-access', employeeCTRL.guard, employeeCTRL.checkScreen(null), employeeCTRL.viewAllCarGroupsCanThisEmployeeAccess);
EmployeeRouter.get('/this-location', employeeCTRL.guard, employeeCTRL.getThislocation);
EmployeeRouter.get('/view/all-nationalities', employeeCTRL.guard, employeeCTRL.viewAllNationalities);
EmployeeRouter.get('/view/all-cities', employeeCTRL.guard, employeeCTRL.viewAllCities);
EmployeeRouter.get('/view/all-car-types', employeeCTRL.guard, employeeCTRL.viewAllCarTypes);
EmployeeRouter.get('/view/all-car-models-by-car-type', employeeCTRL.guard, employeeCTRL.viewAllCarModelsByCarType);
EmployeeRouter.get('/view/all-cars-who-can-add-to-booking', employeeCTRL.guard , employeeCTRL.viewAllCarsWhoCanAddToBooking);




EmployeeRouter.use(expressHelper.expressErrorHandler);
