import { expressHelper } from '../helpers/expressHelper';
import { clientCTRL } from '../controllers/client';
import { Router } from 'express';
import fileUpload from 'express-fileupload';

export const ClientRouter: Router = Router();

// Auth
ClientRouter.post('/signup' , clientCTRL.signup );
ClientRouter.post('/signin' , clientCTRL.signin );
ClientRouter.get('/refresh-token' , clientCTRL.guard , clientCTRL.refreshToken ); // guard


// Client
ClientRouter.get('/view-this-client' , clientCTRL.guard , clientCTRL.viewThisClient ); // guard
ClientRouter.put('/edit' , clientCTRL.guard , clientCTRL.edit ); // guard
ClientRouter.put('/edit-password' , clientCTRL.guard , clientCTRL.editPassword ); // guard
ClientRouter.post('/add-image' , clientCTRL.guard  , fileUpload() , clientCTRL.addClientImage ); // guard


// booking
ClientRouter.post('/add-booking' , clientCTRL.guard , clientCTRL.addBooking ); // guard
ClientRouter.get('/view-this-client-booking' , clientCTRL.guard , clientCTRL.viewThisClientBooking ); // guard
ClientRouter.get('/view-this-client-bookings' , clientCTRL.guard , clientCTRL.viewThisClientBookings ); // guard
ClientRouter.get('/view-this-client-bookings-search' , clientCTRL.guard , clientCTRL.viewThisClientBookingsSearch ); // guard
ClientRouter.get('/view/booking-by-code-and-iqama' , clientCTRL.viewBookingByCodeAndIqama ); 

// message
ClientRouter.post('/send-message' , clientCTRL.sendMessage ); 


// car group
ClientRouter.get('/view/car-group' , clientCTRL.viewCarGroup ); 
ClientRouter.post('/view/car-groups' , clientCTRL.viewCarGroupsToBookingOne );


// branch
ClientRouter.get('/view/all-branches' , clientCTRL.viewAllBranches );
ClientRouter.get('/view/all-branches-location' , clientCTRL.viewAllBranchesLocation );


// options
ClientRouter.get('/view/all-nationalities' , clientCTRL.viewAllNationalities );
ClientRouter.get('/view/all-car-types' , clientCTRL.viewAllCarTypes );
ClientRouter.get('/view/all-car-models-by-car-type' , clientCTRL.viewAllCarModelsByCarType );
ClientRouter.get('/view/all-cities', clientCTRL.viewAllCities);
ClientRouter.get('/view/all-branches-by-city-filter', clientCTRL.viewAllBranchesByCityFilter);


ClientRouter.use( expressHelper.expressErrorHandler );

