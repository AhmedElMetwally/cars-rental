import { guestCTRL } from './../controllers/guest';
import { expressHelper } from '../helpers/expressHelper';
import { Router } from 'express';

export const GuestRouter: Router = Router();

GuestRouter.post('/signup-guest-to-client' , guestCTRL.signupGuestToClient );
GuestRouter.post('/add-booking' , guestCTRL.addBooking );
GuestRouter.post('/get-or-create-guest' , guestCTRL.getOrCreateGuest );

GuestRouter.use(expressHelper.expressErrorHandler);

