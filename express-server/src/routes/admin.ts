import { expressHelper } from '../helpers/expressHelper';
import { Router} from 'express';
import { adminCTRL } from '../controllers/admin';

export const AdminRouter: Router = Router();


// Auth
AdminRouter.post('/signup', adminCTRL.signup );
AdminRouter.post('/signin', adminCTRL.signin );
AdminRouter.get('/refresh-token' , adminCTRL.guard  , adminCTRL.refreshToken );


// EmployeeGroup
AdminRouter.post('/employee-group/add' , adminCTRL.guard , adminCTRL.addEmployeeGroup );
AdminRouter.put('/employee-group/edit', adminCTRL.guard , adminCTRL.editEmployeeGroup);
AdminRouter.delete('/employee-group/delete' , adminCTRL.guard , adminCTRL.deleteEmployeeGroup );
AdminRouter.post('/employee-group/restore' , adminCTRL.guard , adminCTRL.restoreEmployeeGroup );
AdminRouter.get('/view/employee-group' , adminCTRL.guard , adminCTRL.viewEmployeeGroup  );
AdminRouter.get('/view/employee-groups' , adminCTRL.guard , adminCTRL.viewEmployeeGroups );
AdminRouter.get('/view/employee-groups-search' , adminCTRL.guard , adminCTRL.viewEmployeeGroupsSearch );


// Employee
AdminRouter.post('/employee/add' , adminCTRL.guard , adminCTRL.addEmployee );
AdminRouter.put('/employee/edit' , adminCTRL.guard , adminCTRL.editEmployee);
AdminRouter.put('/employee/edit-password' , adminCTRL.guard , adminCTRL.editEmployeePassword  );
AdminRouter.delete('/employee/delete', adminCTRL.guard , adminCTRL.deleteEmployee );
AdminRouter.post('/employee/restore' , adminCTRL.guard , adminCTRL.restoreEmployee );
AdminRouter.get('/view/employee' , adminCTRL.guard , adminCTRL.viewEmployee);
AdminRouter.get('/view/employees', adminCTRL.guard , adminCTRL.viewEmployees   );
AdminRouter.get('/view/employees-search' , adminCTRL.guard , adminCTRL.viewEmployeesSearch );
AdminRouter.get('/view/employees-by-employee-group-filter', adminCTRL.guard , adminCTRL.viewEmployeesByEmployeeGroupFilter );
AdminRouter.get('/view/employee-time-statistics-in-year' , adminCTRL.guard , adminCTRL.viewEmployeeTimeStatisticsInYear );
AdminRouter.get('/view/employee-time-statistics-in-month', adminCTRL.guard , adminCTRL.viewEmployeeTimeStatisticsInMonth );
AdminRouter.get('/view/employee-actions' , adminCTRL.guard , adminCTRL.viewEmployeeActions );
AdminRouter.get('/view/employee-actions-by-item-name-filter' , adminCTRL.guard , adminCTRL.viewEmployeeActionsByItemNameFilter );




// options
AdminRouter.get('/view/all-employee-groups' , adminCTRL.guard , adminCTRL.viewAllEmployeeGroups   );
AdminRouter.get('/view/all-branches' , adminCTRL.guard , adminCTRL.viewAllBranches );


// nextError
AdminRouter.use(expressHelper.expressErrorHandler);
