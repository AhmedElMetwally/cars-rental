interface JwtError {
    title?: string;
    message?: string;
};

export interface InextError extends JwtError {
    status : number;
    errorEnum : number
};