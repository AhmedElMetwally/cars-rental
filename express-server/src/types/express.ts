import { Request } from 'express';
import { Escreens } from '../models/employeeGroup';

interface ICLient {
    _id: string;
}

interface IAdmin {
    _id: string;
}

interface IEmployee {
    _id: string;
    branches_ids: string[];
    employeeGroup_id: string;
}

export type EmployeeRequest = Request & {
    employee: IEmployee;
    screen: Escreens;
};
export type ClientRequest = Request & {
    client: ICLient;
};
export type AdminRequest = Request & {
    admin: IAdmin;
};