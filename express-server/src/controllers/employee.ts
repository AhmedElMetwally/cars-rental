import { carGroupHelper } from '../helpers/carGroupHelper';
import { CityModel, ICityModel } from '../models/city';
import { mainHelper } from '../helpers/mainHelper';
import { bookingHelper } from '../helpers/bookingHelper';
import { imageHelper } from '../helpers/imagesHelper';
import { expressHelper } from '../helpers/expressHelper';
import { Response, NextFunction , RequestHandler , Request } from 'express';
import { EmployeeRequest } from '../types/express';
import { GuestModel, IGuestModel } from '../models/guest';
import { adminCTRL } from './admin';
import { NationalityModel } from '../models/nationality';
import { CarGroupModel, ICarGroupModel } from '../models/carGroup';
import { IBookingModel, EbookingType } from '../models/booking';
import { IEmployeeActionModel, EmployeeActionModel, EitemName } from '../models/employeeAction';
import { Escreens } from '../models/employeeGroup';
import { mainErrorEnum } from './../enums/error.enum';
import { mainRegExp } from '../regExp/mainRegExp';
import { jwt } from '../helpers/jwt';
import { ClientModel, IClientModel } from '../models/client';
import { BranchModel, IBranchModel } from '../models/branch';
import { CarModel, ICarModel, Estatus } from '../models/car';
import { EmployeeModel, IEmployeeModel } from '../models/employee';
import { CarTypeModel, ICarTypeModel } from '../models/carType';
import { BookingModel } from '../models/booking';
import { OnlinePaymentModel, IOnlinePaymentModel } from '../models/onlinePayment';
import { INationalityModel } from '../models/nationality';
import { CarModelModel, ICarModelModel } from '../models/carModel';
 
import request from 'request';

type IEmployeeAcationTarget = 'client'|'car'|'carGroup'|'carType'|'carModel'|'booking'|'branch';

class EmployeeCTRLHelper {
    
    constructor(){};

    // use in router
    public checkScreen( screen: Escreens ): RequestHandler {
 
        return (req: EmployeeRequest , _res: Response , next: NextFunction ): void => {
            EmployeeModel.findOne({
                _id : req.employee._id,
            })
            .select('-_id employeeGroup')
            .populate('employeeGroup')
            .exec()
            .then( ( employee: IEmployeeModel ) => {

                if( ! employee || employee.deleted ) {
                    expressHelper.sendCustomError(
                        mainErrorEnum.employee_not_found,
                        next
                    );
                };

                if( ! employee.employeeGroup || employee.employeeGroup.deleted ) {
                    expressHelper.sendCustomError(
                        mainErrorEnum.employeeGroup_not_found,
                        next
                    );
                };

                if( screen === null || employee.employeeGroup.screens.indexOf( screen )  !== -1 ) {

                    const employee_id: string = req.employee._id;
                    const branches: string[] = employee.employeeGroup.branches;

                    req.employee = {
                        _id : employee_id,
                        branches_ids : branches,
                        employeeGroup_id : employee.employeeGroup._id,
                    };

                    req.screen = screen;

                    next();
                    
                } else {

                    expressHelper.sendCustomError(
                        mainErrorEnum.not_have_access,
                        next
                    );

                };
                
            })
            .catch( () => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

        };
        
    };

    // use in EmployeeCTRL
    public isHaveAccessToThisCarGroup = (carGroup_id: string , branches_ids: string[]): Promise<boolean> => {
        return new Promise( resolve => {
            CarGroupModel.findOne({
                _id : carGroup_id
            })
            .select('branch')
            .exec()
            .then( (carGroup: ICarGroupModel) => {
                resolve(
                    this.checkIfObjectidInObjectids(
                        carGroup.branch as string , 
                        branches_ids
                    )
                );
            })
            .catch(() => {
                resolve(false);
            });
        });
    };
    public isHaveAccessToThisCar = (car_id: string , branches_ids: string[]): Promise<boolean> => {
        return new Promise( resolve => {
            CarModel.findOne({
                _id : car_id
            })
            .select('carGroup')
            .populate({
                path : 'carGroup',
                select : 'branch'
            })
            .exec()
            .then((car: ICarModel) => {
                resolve(
                    this.checkIfObjectidInObjectids(
                        (car.carGroup as any).branch , 
                        branches_ids
                    )
                );
            }) 
            .catch( () => {
               resolve(false);
            });
        });
    };
    public isHaveAccessToThisBranch = (branch_id: string , branches_ids: string[]): boolean => {
       return this.checkIfObjectidInObjectids(branch_id , branches_ids);
    };
    public isHaveAccessToThisBooking = (booking_id: string , branches_ids: string[]): Promise<boolean> => {
        return new Promise( resolve => {
            BookingModel.findOne({
                _id : booking_id,
                pickUpBranch : {
                    $in : branches_ids
                },
                returnBranch : {
                    $in : branches_ids
                }
            })
            .exec()
            .then( (booking: IBookingModel) => {
                resolve(!!booking);
            })
            .catch(() => {
                resolve(false);
            })
        });
    };
    private checkIfObjectidInObjectids = (_id: string , _ids: string[]):boolean => {
        _id = _id.toString();
        for(let i = 0, len = _ids.length ; i < len ; ++i) {
            if(_ids[i].toString() === _id) {
                return true;
            };
        };
    };
    public addEmployeeAction = (req: EmployeeRequest , target: IEmployeeAcationTarget , target_id: string ): void => {
        
        const employee_id: string = req.employee._id;
        const screen: Escreens = req.screen;

        const employeeAction: IEmployeeActionModel|any = {
            client: null,
            car: null,
            carGroup: null,
            carType: null,
            carModel: null,
            booking: null,
            branch: null,
            employee_id,
            screen,

            itemName: null,

            query : JSON.stringify( req.query ),
            body : JSON.stringify( req.body ),
        };

        const obj: any = {};
        obj[ target ] = target_id;
        obj.itemName = EitemName[target];

        EmployeeActionModel.create({
            ...employeeAction,
            ...obj
        }).catch(() => {});

    };
    public getCarGroups_idsByBranches_ids = (branches_ids: string[]): Promise<string[]> => {
        return new Promise( resolve => {
            CarGroupModel.find({
                branch : {
                    $in : branches_ids
                }
            })
            .exec()
            .then( (carGroups: ICarGroupModel[]) => {
                resolve( carGroups.map( v => v._id));
            })
            .catch( (_error) => {
                resolve([]);
            })
        });
    };
    public isCanEditCarGroupByCar = (car_id: string): Promise<boolean> => {
        return new Promise( resolve => {
            BookingModel.countDocuments({
                car : car_id ,
                pickUpDate : {
                    $lt : new Date()
                },
                returnDate : {
                    $gt : new Date()
                },
                bookingType : EbookingType.active,
                deleted : 0
            }).exec()
            .then( count => {
                resolve(!count)
            })
            .catch( () => {
                resolve(false)
            })
        });
    };

};

class EmployeeCTRL extends EmployeeCTRLHelper {
    
    constructor(){
        super();
    };
    
    // Guard
    public guard = jwt.allaw_express({
        roles     : ['employee'],
        dataField : 'employee',
        header_name_of_token : 'token'
    });

    // Auth
    public signin = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        req.checkBody('email' , mainErrorEnum.email_required ).exists();
        req.checkBody('email' , mainErrorEnum.email_invalid ).matches( mainRegExp.employee.email );

        req.checkBody('password' , mainErrorEnum.password_required ).exists();
        req.checkBody('password' , mainErrorEnum.password_invalid ).matches( mainRegExp.employee.password );
    
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        EmployeeModel.findOne({
            email : req.body.email ,
            deleted : 0
        })
        .populate('employeeGroup')
        .exec()
        .then( async (employee: IEmployeeModel ) => {
            
            if(employee) {

                if( ! mainHelper.compareSync(req.body.password , employee.password) || ! employee ) {
                    return expressHelper.sendCustomError( 
                        mainErrorEnum.employee_not_found , 
                        next
                    );
                };
    
                if( ! employee.employeeGroup || employee.employeeGroup.deleted ) {
                    return expressHelper.sendCustomError( 
                        mainErrorEnum.employeeGroup_not_found , 
                        next
                    );
                };
                
                const token: string = await jwt.create({
                    _id : employee._id,
                    roles: employee.roles
                });
    
                employee.password = '';
    
                return res.status(200).json({ employee , token })

            } else {
                return expressHelper.sendCustomError(
                    mainErrorEnum.employee_not_found , 
                    next
                );
            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase  , next) );

    };
    public refreshToken = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
            
        EmployeeModel.findOne({
            _id : req.employee._id,
            deleted : 0
        })
        .populate('employeeGroup')
        .exec()
        .then( async ( employee: IEmployeeModel ) => {
            
            if(employee) {

                if( ! employee.employeeGroup || employee.employeeGroup.deleted ) {
                    return expressHelper.sendCustomError(
                        mainErrorEnum.employeeGroup_not_found , 
                        next
                    ); 
                };

                const token: string = await jwt.create({
                    _id : employee._id,
                    roles: employee.roles
                });
                
                return res.status(200).json({ 
                    token , employee
                });

            } else {
                return expressHelper.sendCustomError( 
                    mainErrorEnum.employee_not_found , 
                    next
                );
            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next));

    };

    // This Employee
    public editThisEmployee = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        const editObject: any = {};

        if ( mainHelper.isExists(req.body.name) ) {
            editObject.name = req.body.name;
            req.checkBody('name' , mainErrorEnum.name_invalid ).matches( mainRegExp.employee.name );
        };

        if ( mainHelper.isExists(req.body.email) ) {
            editObject.email = req.body.email;
            req.checkBody( 'email' , mainErrorEnum.email_invalid ).matches( mainRegExp.employee.email);
        };
        
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);
        
        if ( Object.keys(editObject).length === 0 ) {
            return expressHelper.sendCustomError( 
                mainErrorEnum.edit_date_not_found,
                next 
            ); 
        };

        EmployeeModel.updateOne({
            _id   : req.employee._id,
        } , {
            ...editObject,
        })
        .exec()
        .then( async ( result ) => {

            if( mainHelper.isUpdateFail(result) ) {
                
                return expressHelper.sendCustomError( 
                    mainErrorEnum.employee_not_found,
                    next 
                ); 

            } else {

                return res.status(200).end();

            };

        })
        .catch( _error => {
            
            if ( mainHelper.isUniqueError(_error) ) {

                return expressHelper.sendCustomError(
                    mainErrorEnum.email_used_before,
                    next
                );
            
            } else {

                return expressHelper.sendCustomError(_error , next );
                
            };

        });

    };
    public editThisEmployeePassword = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        req.checkBody('password' , mainErrorEnum.password_required ).exists();
        req.checkBody('password' , mainErrorEnum.password_invalid ).matches( mainRegExp.employee.password );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        EmployeeModel.updateOne({
            _id : req.employee._id,
        } , {
            password : mainHelper.hashSync(req.body.password)
        })
        .exec()
        .then( async ( result ) => {

            if( mainHelper.isUpdateFail(result) ) {

                return expressHelper.sendCustomError( 
                    mainErrorEnum.employee_not_found , 
                    next
                );

            } else {

                return res.status(200).end();

            };

        })
        .catch(_error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewThisEmployee = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
        EmployeeModel.findById( req.employee._id )
        .populate('employeeGroup')
        .select('-password')
        .exec()        
        .then( async (employee: IEmployeeModel) => {
            res.status(200).json({ employee });
        })
        .catch(_error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
    };
    public viewThisEmployeeTimeStatisticsInYear = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        // use end point from AdminCTRL
        // adminCTRL.viewEmployeeTimeStatisticsInYear
        // import query { employee_id , year } 

        // set in query employee_id
        req.query.employee_id = req.employee._id;

        return adminCTRL.viewEmployeeTimeStatisticsInYear(req , res , next);

    };
    public viewThisEmployeeTimeStatisticsInMonth = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        // year 2018 2019 2020
        // month 1 - 12

        // use end point from AdminCTRL
        // adminCTRL.viewEmployeeTimeStatisticsInYear
        // import query { employee_id , year , month } 

        // set in query employee_id
        req.query.employee_id = req.employee._id;

        return adminCTRL.viewEmployeeTimeStatisticsInMonth(req , res , next);

    };
    public viewThisEmployeeActions = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        // use end point from AdminCTRL
        // adminCTRL.viewEmployeeTimeStatisticsInYear
        // import query { employee_id , page , page , sortBy } 

        // set in query employee_id
        req.query.employee_id = req.employee._id;

        return adminCTRL.viewEmployeeActions(req , res , next);

    };
    public viewEmployeeActionsByItemNameFilter = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        // use end point from AdminCTRL
        // adminCTRL.viewEmployeeTimeStatisticsInYear
        // import query { employee_id , page , page , sortBy } 

        // set in query employee_id
        req.query.employee_id = req.employee._id;

        return adminCTRL.viewEmployeeActionsByItemNameFilter(req , res , next);

    };

    

    // Client
    public editClient = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        req.checkBody('client_id' , mainErrorEnum.client_id_required ).exists();
        req.checkBody('client_id' , mainErrorEnum.client_id_invalid ).matches( mainRegExp.client._id );

        const editObject: any = {};

        if ( mainHelper.isExists(req.body.name) ) {
            editObject.name = req.body.name;
            req.checkBody('name' , mainErrorEnum.name_invalid ).matches( mainRegExp.client.name );
        };

        if ( mainHelper.isExists(req.body.phone) ) {
            editObject.phone = req.body.phone
            req.checkBody('phone' , mainErrorEnum.phone_invalid ).matches( mainRegExp.client.phone );
        };

        if ( mainHelper.isExists(req.body.email) ) {
            editObject.email = req.body.email
            req.checkBody('email' , mainErrorEnum.email_invalid ).matches( mainRegExp.client.email );
        };

        if ( mainHelper.isExists(req.body.address) ) {
            editObject.address = req.body.address;
            req.checkBody('address' , mainErrorEnum.address_invalid ).matches( mainRegExp.client.address );
        };
        
        if ( mainHelper.isExists(req.body.iqama) ) {
            editObject.iqama = req.body.iqama;
            req.checkBody('iqama' , mainErrorEnum.iqama_invalid ).matches( mainRegExp.client.iqama );
        };

        if ( mainHelper.isExists(req.body.job) ) {
            editObject.job = req.body.job;
            req.checkBody('job' , mainErrorEnum.job_invalid ).matches(mainRegExp.client.job )
        };

        if ( mainHelper.isExists(req.body.iqamaExpiryDate) ) {
            editObject.iqamaExpiryDate = req.body.iqamaExpiryDate;
            req.checkBody('iqamaExpiryDate' , mainErrorEnum.iqamaExpiryDate_invalid ).matches( mainRegExp.client.iqamaExpiryDate  )
        };

        if ( mainHelper.isExists(req.body.drivingLicence) ) {
            editObject.drivingLicence = req.body.drivingLicence;
            req.checkBody('drivingLicence' , mainErrorEnum.drivingLicence_invalid ).matches( mainRegExp.client.drivingLicence )
        };

        if ( mainHelper.isExists(req.body.drivingLicenceExpiryDate) ) {
            editObject.drivingLicenceExpiryDate = req.body.drivingLicenceExpiryDate;
            req.checkBody('drivingLicenceExpiryDate' , mainErrorEnum.drivingLicenceExpiryDate_invalid ).matches( mainRegExp.client.drivingLicenceExpiryDate )
        };

        if ( mainHelper.isExists(req.body.birthday) ) {
            editObject.birthday = req.body.birthday;
            req.checkBody('birthday' , mainErrorEnum.birthday_invalid ).matches( mainRegExp.client.birthday );
        };

        if ( mainHelper.isExists(req.body.nationality_id) ) {
            editObject.nationality = req.body.nationality_id;
            req.checkBody('nationality_id' , mainErrorEnum.nationality_id_invalid ).matches(mainRegExp.nationality._id )
        };

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if ( Object.keys(editObject).length === 0 ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.client_not_found,
                next
            );
        };

        ClientModel.updateOne({
            _id   : req.body.client_id,
        } , {
            ...editObject,
        })
        .exec()
        .then( async ( result ) => {

            if( mainHelper.isUpdateFail(result) ) {
                
                return expressHelper.sendCustomError(
                    mainErrorEnum.client_not_found,
                    next
                );

            } else {
                
                this.addEmployeeAction( 
                    req , 
                    'client' , 
                    req.body.client_id 
                );
                
                return res.status(200).end();

            };

        })
        .catch(_error => {

            if ( mainHelper.isUniqueError(_error) ) {

                if ( _error.message.indexOf('email') !== -1 ) {
                    return expressHelper.sendCustomError(
                        mainErrorEnum.email_used_before,
                        next
                    );
                };
                
                if ( _error.message.indexOf('phone') !== -1 ) {
                    return expressHelper.sendCustomError(
                        mainErrorEnum.phone_used_before,
                        next
                    );
                };
            
            
            } else {

                return expressHelper.sendCustomError(mainErrorEnum.datebase , next) ;

            };

        });

    };
    public editClientPassword = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        req.checkBody('client_id' , mainErrorEnum.client_id_required ).exists();
        req.checkBody('client_id' , mainErrorEnum.client_id_invalid ).matches( mainRegExp.client._id );

        req.checkBody('password' , mainErrorEnum.password_required ).exists();
        req.checkBody('password' , mainErrorEnum.password_invalid ).matches( mainRegExp.client.password );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        ClientModel.updateOne({
            _id : req.body.client_id,
        } , {
            password : mainHelper.hashSync( req.body.password )
        })
        .exec()
        .then( async ( result ) => {

            if( mainHelper.isUpdateFail(result) ) {
                
                return expressHelper.sendCustomError(
                    mainErrorEnum.client_not_found,
                    next
                );

            } else {

                this.addEmployeeAction( 
                    req , 
                    'client' , 
                    req.body.client_id 
                );
                
                return res.status(200).end();

            };

        })
        .catch(_error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public deleteClient = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        req.checkQuery('client_id' , mainErrorEnum.client_id_required ).exists();
        req.checkQuery('client_id' , mainErrorEnum.client_id_invalid ).matches( mainRegExp.client._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);
        
        ClientModel.updateOne({
            _id : req.query.client_id,
            deleted : 0
        } , {
            deleted : 1
        })
        .exec()
        .then( async (result: any) => {

            if( mainHelper.isUpdateFail(result) ) {

                return expressHelper.sendCustomError(
                    mainErrorEnum.client_not_found,
                    next
                );

            } else {

                this.addEmployeeAction( 
                    req , 
                    'client' , 
                    req.body.client_id 
                );

                return res.status(200).end();

            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next));

    };
    public restoreClient = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
        
        req.checkBody('client_id' , mainErrorEnum.client_id_required ).exists();
        req.checkBody('client_id' , mainErrorEnum.client_id_invalid ).matches( mainRegExp.client._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);


        ClientModel.updateOne({
            _id : req.body.client_id,
            deleted : 1
        } , {
            deleted : 0
        })
        .exec()
        .then( async (result: any) => {

            if( mainHelper.isUpdateFail(result) ) {

                return expressHelper.sendCustomError(
                    mainErrorEnum.client_not_found,
                    next
                );

            } else {

                this.addEmployeeAction( 
                    req , 
                    'client' , 
                    req.body.client_id 
                );

                return res.status(200).end();

            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next));
    };
    public addClientImage = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
  
        req.checkBody('client_id' , mainErrorEnum.client_id_required ).exists();
        req.checkBody('client_id' , mainErrorEnum.client_id_invalid ).matches( mainRegExp.client._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! req.files ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.image_not_found,
                next
            );
        };

        imageHelper.uploadImage(req.files.image)
        .then( src => {
            
            ClientModel.findOneAndUpdate( {
                _id : req.body.client_id,
            } , { 
                image : src
            })
            .select('image -_id')
            .exec()
            .then( async (client: IClientModel) => {
                
                if(client) {
                    
                    if(client.image) imageHelper.deleteImage(client.image);

                    this.addEmployeeAction( 
                        req , 
                        'client' , 
                        req.body.client_id 
                    );

                    return res.status(200).json({ image : src });
    

                } else {

                    return expressHelper.sendCustomError(
                        mainErrorEnum.client_not_found,
                        next
                    );

                };
                
            })
            .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next));

        })
        .catch( () => {
            return expressHelper.sendCustomError(
                mainErrorEnum.image_not_found,
                next
            );
        });

    };
    public viewClient = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        req.checkQuery('client_id' , mainErrorEnum.client_id_required ).exists();
        req.checkQuery('client_id' , mainErrorEnum.client_id_invalid ).matches( mainRegExp.client._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);
            
        ClientModel.findOne({
            _id : req.query.client_id,
        })
        .populate('nationality')
        .select('-password')
        .exec()    
        .then( async (client: IClientModel) => {
            
            if(client) {

                return res.status(200).json({ client });

            } else {

                return expressHelper.sendCustomError(
                    mainErrorEnum.client_not_found,
                    next 
                );
                
            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewBookingsForClient = async (req: Request , res: Response , next: NextFunction) => {
            
        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkQuery('limit', mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit', mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('client_id' , mainErrorEnum.client_id_required ).exists();
        req.checkQuery('client_id' , mainErrorEnum.client_id_invalid ).matches( mainRegExp.client._id );

        req.checkQuery('sortBy', mainErrorEnum.sortBy_required ).exists();
        req.checkQuery('sortBy', mainErrorEnum.sortBy_invalid ).matches( mainRegExp.view.sortBy );
    
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page); 

        BookingModel.find({
            carGroup : req.query.carGroup_id
        })
        .sort(req.query.sortBy)
        .skip( (page - 1) * limit)
        .limit(limit)
        .populate('car carGroup pickUpBranch returnBranch onlinePayment')
        .exec()    
        .then( (bookings: IBookingModel[]) => res.status(200).json({ bookings }) )
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewOnlinePaymentsForClient = async (req: Request , res: Response , next: NextFunction) => {
            
        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkQuery('limit', mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit', mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('client_id' , mainErrorEnum.client_id_required ).exists();
        req.checkQuery('client_id' , mainErrorEnum.client_id_invalid ).matches( mainRegExp.client._id );

        req.checkQuery('sortBy', mainErrorEnum.sortBy_required ).exists();
        req.checkQuery('sortBy', mainErrorEnum.sortBy_invalid ).matches( mainRegExp.view.sortBy );
    
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page); 

        OnlinePaymentModel.find({
            client : req.query.client_id
        })
        .sort(req.query.sortBy)
        .skip( (page - 1 ) * limit)
        .limit(limit)
        .populate('booking')
        .exec()    
        .then( (onlinePayments: IOnlinePaymentModel[]) => res.status(200).json({ onlinePayments }) )
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewClients = async (req: Request , res: Response , next: NextFunction) => {

        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );
        
        req.checkQuery('limit', mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit', mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('sortBy', mainErrorEnum.sortBy_required ).exists();
        req.checkQuery('sortBy', mainErrorEnum.sortBy_invalid ).matches( mainRegExp.view.sortBy );
    
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        ClientModel.find({})
        .sort(req.query.sortBy)
        .skip((page - 1) * limit)
        .limit(limit)
        .populate('nationality')
        .select('-password')
        .exec()    
        .then( async ( clients: IClientModel[] ) => {
            res.status(200).json({ clients });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewClientsSearch = async (req: Request , res: Response , next: NextFunction) => {
        
        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('searchQuery' , mainErrorEnum.searchQuery_required ).exists();
        req.checkQuery('searchQuery',  mainErrorEnum.searchQuery_invalid ).matches( mainRegExp.view.searchQuery );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt( req.query.limit ); 
        const page = parseInt( req.query.page ); 

        ClientModel.find({
            $text : { 
                $search : req.query.searchQuery 
            },
        })
        .sort('-createdAt')
        .skip( (page - 1) * limit )
        .limit(limit)
        .select('-password')
        .populate('nationality')
        .exec()    
        .then( ( clients: IClientModel[] ) => {
            res.status(200).json({ clients })
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };


    // Car
    public addCar = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
        
        req.checkBody('carGroup_id' , mainErrorEnum.carGroup_id_required ).exists();
        req.checkBody('carGroup_id' , mainErrorEnum.carGroup_id_invalid ).matches( mainRegExp.carGroup._id );

        req.checkBody('plateNumber' , mainErrorEnum.plateNumber_required ).exists();
        req.checkBody('plateNumber' , mainErrorEnum.plateNumber_invalid  ).matches( mainRegExp.car.plateNumber );

        req.checkBody('gearType' , mainErrorEnum.gearType_required ).exists();
        req.checkBody('gearType' , mainErrorEnum.gearType_invalid  ).matches( mainRegExp.car.gearType );

        req.checkBody('status' , mainErrorEnum.status_required ).exists();
        req.checkBody('status' , mainErrorEnum.status_invalid ).matches( mainRegExp.car.status );
 
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! await this.isHaveAccessToThisCarGroup(req.body.carGroup_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        CarModel.create({
            plateNumber : req.body.plateNumber,
            carGroup : req.body.carGroup_id,
            gearType : req.body.gearType,
            status : req.body.status,
        })
        .then( async (car: ICarModel) => {
            
            this.addEmployeeAction( 
                req , 
                'car' , 
                car._id 
            );

            res.status(200).json({ _id : car._id });

        })
        .catch( _error => {

            if ( mainHelper.isUniqueError(_error) ) {
                
                return expressHelper.sendCustomError(
                    mainErrorEnum.plateNumber_used_before,
                    next
                );

            } else {

                return expressHelper.sendCustomError(mainErrorEnum.datebase , next) ;

            };

        });

    };
    public editCar = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 
        
        const editObject: any = {};

        req.checkBody('car_id' , mainErrorEnum.car_id_required ).exists();
        req.checkBody('car_id' , mainErrorEnum.car_id_invalid ).matches( mainRegExp.car._id );

        if( mainHelper.isExists(req.body.carGroup_id) ) {
            editObject.carGroup = req.body.carGroup_id;
            req.checkBody('carGroup_id' , mainErrorEnum.carGroup_id_invalid ).matches( mainRegExp.carGroup._id );
        };

        if( mainHelper.isExists(req.body.plateNumber) ) {
            editObject.plateNumber = req.body.plateNumber;
            req.checkBody('plateNumber' , mainErrorEnum.plateNumber_invalid ).matches( mainRegExp.car.plateNumber );
        };

        if( mainHelper.isExists(req.body.gearType) ) {
            editObject.gearType = req.body.gearType;
            req.checkBody('gearType' , mainErrorEnum.gearType_invalid ).matches( mainRegExp.car.gearType );
        };

        if( mainHelper.isExists(req.body.status) ) {
            editObject.status = req.body.status;
            req.checkBody('status' , mainErrorEnum.status_invalid ).matches( mainRegExp.car.status );
        };

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! await this.isHaveAccessToThisCar(req.body.car_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        // if i will edit car carGroup 
        // check if this car is booking in this this carGroup
        if( mainHelper.isExists(req.body.carGroup_id) ) {
            if(! await this.isCanEditCarGroupByCar(req.body.car_id)) {
                return expressHelper.sendCustomError(
                    mainErrorEnum.this_car_is_booking_in_this_car_group,
                    next
                );
            };
        };

        if( Object.keys( editObject ).length === 0 ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.edit_date_not_found,
                next
            );
        };

        CarModel.updateOne({
            _id : req.body.car_id ,
        },{
            ...editObject 
        })
        .exec()
        .then( async (result: any ) => {

            if( mainHelper.isUpdateFail(result) ) {
                
                return expressHelper.sendCustomError(
                    mainErrorEnum.car_not_found,
                    next  
                );

            } else {

                // save action
                this.addEmployeeAction(
                    req , 
                    'car',
                    req.body.car_id
                );

                return res.status(200).end();

            };

        })
        .catch( _error => {

            if ( mainHelper.isUniqueError(_error) ) {
                
                return expressHelper.sendCustomError(
                    mainErrorEnum.plateNumber_used_before,
                    next
                );

            } else {

                return expressHelper.sendCustomError(mainErrorEnum.datebase , next) ;

            };

        });

        
    };
    public deleteCar = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 

        req.checkQuery('car_id' , mainErrorEnum.car_id_required ).exists();
        req.checkQuery('car_id' , mainErrorEnum.car_id_invalid ).matches( mainRegExp.car._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);
        
        if( ! await this.isHaveAccessToThisCar(req.query.car_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        CarModel.updateOne({
            _id : req.query.car_id ,
            deleted : 0
        } , {
            deleted : 1
        })
        .exec()
        .then( async (result: any ) => {

            if( mainHelper.isUpdateFail(result) ) {
                
                return expressHelper.sendCustomError(
                    mainErrorEnum.car_not_found, 
                    next 
                );

            } else {

                this.addEmployeeAction(
                    req , 
                    'car',
                    req.query.car_id
                );

                return res.status(200).end();

            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public restoreCar = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 
    
        req.checkBody('car_id' , mainErrorEnum.car_id_required ).exists();
        req.checkBody('car_id' , mainErrorEnum.car_id_invalid ).matches( mainRegExp.car._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);
        
        if( ! await this.isHaveAccessToThisCar(req.body.car_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        CarModel.updateOne({
            _id : req.body.car_id ,
            deleted : 1
        } , {
            deleted : 0
        })
        .exec()
        .then( async (result: any ) => {

            if( mainHelper.isUpdateFail(result) ) {
                
                return expressHelper.sendCustomError(
                    mainErrorEnum.car_not_found, 
                    next 
                );

            } else {

                this.addEmployeeAction(
                    req , 
                    'car',
                    req.body.car_id
                );

                return res.status(200).end();

            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewCar = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        req.checkQuery('car_id' , mainErrorEnum.car_id_required ).exists();
        req.checkQuery('car_id' , mainErrorEnum.car_id_invalid ).matches( mainRegExp.car._id  );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! await this.isHaveAccessToThisCar(req.query.car_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        CarModel.findOne({
            _id : req.query.car_id,
        })
        .populate({
            path : 'carGroup',
            select : 'carType',
            populate : {
                path : 'carType',
                select : 'nameEn nameAr'
            }
        })
        .populate({
            path : 'carGroup',
            select : 'carModel',
            populate : {
                path : 'carModel',
                select : 'nameEn nameAr'
            }
        })
        .populate({
            path : 'carGroup',
            select : 'branch year',
            populate : {
                path : 'branch',
                select : 'nameEn nameAr'
            }
        })
        .lean()
        .exec()    
        .then( async (car: ICarModel) => {

            if(car) {

                return res.status(200).json({ car });

            } else {

                return expressHelper.sendCustomError(
                    mainErrorEnum.car_not_found, 
                    next 
                );

            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewCars = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
        
        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid  ).matches( mainRegExp.view.page );

        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid  ).matches( mainRegExp.view.limit );

        req.checkQuery('sortBy' , mainErrorEnum.sortBy_required ).exists();
        req.checkQuery('sortBy' , mainErrorEnum.sortBy_invalid  ).matches( mainRegExp.view.sortBy );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);
            
        const carGroups_idsWhoCanAccess: string[] = await this.getCarGroups_idsByBranches_ids(req.employee.branches_ids);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        CarModel.find({
            carGroup : {
                $in : carGroups_idsWhoCanAccess
            }
        })
        .sort(req.query.sortBy) 
        .skip( (page - 1) * limit) 
        .limit(limit) 
        .populate({
            path : 'carGroup',
            select : 'carType',
            populate : {
                path : 'carType',
                select : 'nameEn nameAr'
            }
        })
        .populate({
            path : 'carGroup',
            select : 'carModel',
            populate : {
                path : 'carModel',
                select : 'nameEn nameAr'
            }
        })
        .populate({
            path : 'carGroup',
            select : 'branch',
            populate : {
                path : 'branch',
                select : 'nameEn nameAr'
            }
        })
        .lean()
        .exec()    
        .then( async (cars: ICarModel[]) => {
            res.status(200).json({ cars : cars });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next));

    };
    public viewCarsByFilter = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid  ).matches( mainRegExp.view.page );

        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid  ).matches( mainRegExp.view.limit );

        req.checkQuery('sortBy' , mainErrorEnum.sortBy_required ).exists();
        req.checkQuery('sortBy' , mainErrorEnum.sortBy_invalid  ).matches( mainRegExp.view.sortBy );

        req.checkQuery('carGroup_id' , mainErrorEnum.carGroup_id_required ).exists();
        req.checkQuery('carGroup_id' , mainErrorEnum.carGroup_id_invalid ).matches( mainRegExp.carGroup._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);
            
        if( ! await this.isHaveAccessToThisCarGroup(req.query.carGroup_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        const limit = parseInt( req.query.limit ); 
        const page = parseInt( req.query.page );

        CarModel.find({
            carGroup : req.query.carGroup_id
        })
        .sort(req.query.sortBy) 
        .skip( (page - 1) * limit ) 
        .limit( limit ) 
        .populate({
            path : 'carGroup',
            select : 'carType',
            populate : {
                path : 'carType',
                select : 'nameEn nameAr'
            }
        })
        .populate({
            path : 'carGroup',
            select : 'carModel',
            populate : {
                path : 'carModel',
                select : 'nameEn nameAr'
            }
        })
        .populate({
            path : 'carGroup',
            select : 'branch',
            populate : {
                path : 'branch',
                select : 'nameEn nameAr'
            }
        })
        .lean()
        .exec()    
        .then( async (cars: ICarModel[]) => {
            res.status(200).json({ cars : cars });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next));

    };
    public viewCarsSearch = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
        
        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('searchQuery' , mainErrorEnum.searchQuery_required ).exists();
        req.checkQuery('searchQuery',  mainErrorEnum.searchQuery_invalid ).matches( mainRegExp.view.searchQuery );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const carGroups_idsWhoCanAccess: string[] = await this.getCarGroups_idsByBranches_ids(req.employee.branches_ids);

        const limit = parseInt( req.query.limit ); 
        const page = parseInt( req.query.page ); 

        CarModel.find({
            $text : { $search : req.query.searchQuery },
            carGroup : {
                $in : carGroups_idsWhoCanAccess
            }
        })
        .sort(req.query.sortBy) 
        .skip((page - 1) * limit) 
        .limit(limit) 
        .populate({
            path : 'carGroup',
            select : 'carType',
            populate : {
                path : 'carType',
                select : 'nameEn nameAr'
            }
        })
        .populate({
            path : 'carGroup',
            select : 'carModel',
            populate : {
                path : 'carModel',
                select : 'nameEn nameAr'
            }
        })
        .populate({
            path : 'carGroup',
            select : 'branch',
            populate : {
                path : 'branch',
                select : 'nameEn nameAr'
            }
        })
        .lean()
        .exec()    
        .then( async (cars: ICarModel[]) => {
            res.status(200).json({ cars : cars });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next));

    };


    // Branch
    public addBranch = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 

        req.checkBody('nameEn' , mainErrorEnum.nameEn_required ).exists();
        req.checkBody('nameEn' , mainErrorEnum.nameEn_invalid ).matches( mainRegExp.branch.nameEn );
        
        req.checkBody('nameAr' , mainErrorEnum.nameAr_required ).exists();
        req.checkBody('nameAr' , mainErrorEnum.nameAr_invalid ).matches( mainRegExp.branch.nameAr );
        
        req.checkBody('addressEn' , mainErrorEnum.addressEn_required ).exists()
        req.checkBody('addressEn' , mainErrorEnum.addressEn_invalid ).matches( mainRegExp.branch.addressEn );
        
        req.checkBody('addressAr' , mainErrorEnum.addressAr_required ).exists()
        req.checkBody('addressAr' , mainErrorEnum.addressAr_invalid ).matches( mainRegExp.branch.addressAr );

        req.checkBody('overviewEn' , mainErrorEnum.overviewEn_required ).exists();
        req.checkBody('overviewEn' , mainErrorEnum.overviewEn_invalid ).matches( mainRegExp.branch.overviewEn );

        req.checkBody('overviewAr' , mainErrorEnum.overviewAr_required ).exists();
        req.checkBody('overviewAr' , mainErrorEnum.overviewAr_invalid ).matches( mainRegExp.branch.overviewAr );

        req.checkBody('bookingStartCode' , mainErrorEnum.bookingStartCode_required ).exists()
        req.checkBody('bookingStartCode' , mainErrorEnum.bookingStartCode_invalid ).matches( mainRegExp.branch.bookingStartCode );        
        
        req.checkBody('waitingHoursForConfirm' , mainErrorEnum.waitingHoursForConfirm_required ).exists()
        req.checkBody('waitingHoursForConfirm' , mainErrorEnum.waitingHoursForConfirm_invalid ).matches( mainRegExp.branch.waitingHoursForConfirm );
        
        req.checkBody('phone' , mainErrorEnum.phone_required ).exists()
        req.checkBody('phone' , mainErrorEnum.phone_invalid ).matches( mainRegExp.branch.phone );

        req.checkBody('city_id' , mainErrorEnum.city_id_required ).exists()
        req.checkBody('city_id' , mainErrorEnum.city_id_invalid ).matches( mainRegExp.city._id );

        let location = null;
        if( mainHelper.isExists(req.body.location) ) {
            location = req.body.location;
            (req as any).checkBody('location' , mainErrorEnum.location_invalid ).isLocation()
        };
    
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next)


        BranchModel.create({

            nameEn : req.body.nameEn,
            nameAr : req.body.nameAr,

            addressEn : req.body.addressEn,
            addressAr : req.body.addressAr,

            overviewEn : req.body.overviewEn,
            overviewAr : req.body.overviewAr,

            bookingStartCode : req.body.bookingStartCode,
            waitingHoursForConfirm : req.body.waitingHoursForConfirm,

            phone : req.body.phone,

            city : req.body.city_id,

            // set location if exists
            ...location ? { location } : {} ,

            images : [],

        })
        .then( async ( branch: IBranchModel ) => {
                        
            this.addEmployeeAction( 
                req , 
                'branch' , 
                branch._id 
            );

            return res.status(200).end();

        })
        .catch(_error => {

            if ( mainHelper.isUniqueError(_error) ) {

                return expressHelper.sendCustomError(
                    mainErrorEnum.phone_used_before,
                    next
                );
            
            } else {
                return expressHelper.sendCustomError(_error , next );

            };

        })
        
    };
    public editBranch = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 
        
        req.checkBody('branch_id' , mainErrorEnum.branch_id_required ).exists();
        req.checkBody('branch_id' , mainErrorEnum.branch_id_invalid  ).matches( mainRegExp.branch._id );

        const editObject: any = {};
        
        if( mainHelper.isExists(req.body.nameEn) ) {
            editObject.nameEn = req.body.nameEn;
            req.checkBody('nameEn' , mainErrorEnum.nameEn_invalid ).matches( mainRegExp.branch.nameEn );
        };

        if( mainHelper.isExists(req.body.nameAr) ) {
            editObject.nameAr = req.body.nameAr;
            req.checkBody('nameAr' , mainErrorEnum.nameAr_invalid ).matches( mainRegExp.branch.nameAr );
        };

        if( mainHelper.isExists(req.body.addressEn) ) {
            editObject.addressEn = req.body.addressEn;
            req.checkBody('addressEn' , mainErrorEnum.addressEn_invalid ).matches( mainRegExp.branch.addressEn );
        };

        if( mainHelper.isExists(req.body.addressAr) ) {
            editObject.addressAr = req.body.addressAr;
            req.checkBody('addressAr' , mainErrorEnum.addressAr_invalid ).matches( mainRegExp.branch.addressAr );
        };

        if( mainHelper.isExists(req.body.overviewEn) ) {
            editObject.overviewEn = req.body.overviewEn;
            req.checkBody('overviewEn' , mainErrorEnum.overviewEn_invalid ).matches( mainRegExp.branch.overviewEn );
        };

        if( mainHelper.isExists(req.body.overviewAr) ) {
            editObject.overviewAr = req.body.overviewAr;
            req.checkBody('overviewAr' , mainErrorEnum.overviewAr_invalid ).matches( mainRegExp.branch.overviewAr );
        };

        if( mainHelper.isExists(req.body.bookingStartCode) ) {
            editObject.bookingStartCode = req.body.bookingStartCode;
            req.checkBody('bookingStartCode' , mainErrorEnum.bookingStartCode_invalid ).matches( mainRegExp.branch.bookingStartCode );
        };

        if( mainHelper.isExists(req.body.waitingHoursForConfirm) ) {
            editObject.waitingHoursForConfirm = req.body.waitingHoursForConfirm;
            req.checkBody('waitingHoursForConfirm' , mainErrorEnum.waitingHoursForConfirm_invalid ).matches( mainRegExp.branch.waitingHoursForConfirm );
        };

        if( mainHelper.isExists(req.body.phone) ) {
            editObject.phone = req.body.phone;
            req.checkBody('phone' , mainErrorEnum.phone_invalid ).matches( mainRegExp.branch.phone );
        };

        if( mainHelper.isExists(req.body.location) ) {
            editObject.location = req.body.location;
            (req as any).checkBody('location' , mainErrorEnum.location_invalid ).isLocation();
        };

        if( mainHelper.isExists(req.body.city_id) ) {
            editObject.city = req.body.city_id;
            (req as any).checkBody('city_id' , mainErrorEnum.city_id_invalid ).matches( mainRegExp.city._id );
        };


        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next)

        if( Object.keys( editObject ).length === 0 ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.edit_date_not_found ,
                next
            );
        };

        if( ! this.isHaveAccessToThisBranch( req.body.branch_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        BranchModel.updateOne({
            _id : req.body.branch_id,
        },{
            ...editObject
        })
        .exec()
        .then( async ( result: any ) => {

            if( mainHelper.isUpdateFail(result) ) {

                return expressHelper.sendCustomError( 
                    mainErrorEnum.branch_not_found,
                    next
                );

            } else {

                this.addEmployeeAction( 
                    req , 
                    'branch' , 
                    req.body.branch_id 
                );

                return res.status(200).end();

            };

        })
        .catch( _error => {

            if ( mainHelper.isUniqueError(_error) ) {

                return expressHelper.sendCustomError(
                    mainErrorEnum.phone_used_before,
                    next
                );
            
            } else {

                return expressHelper.sendCustomError(_error , next );

            };

        });
        

    };
    public deleteBranch  = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 
            
        req.checkQuery('branch_id' , mainErrorEnum.branch_id_required ).exists();
        req.checkQuery('branch_id' , mainErrorEnum.branch_id_invalid  ).matches( mainRegExp.branch._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);


        if( ! this.isHaveAccessToThisBranch(req.query.branch_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        BranchModel.updateOne({
            _id : req.query.branch_id,
            deleted : 0

        }, {
            deleted : 1
        })
        .exec()
        .then( async ( result: any) => {
            
            if( mainHelper.isUpdateFail(result) ) {

                return expressHelper.sendCustomError( 
                    mainErrorEnum.branch_not_found,
                    next
                );

            } else {

                this.addEmployeeAction( 
                    req , 
                    'branch' , 
                    req.query.branch_id 
                );

                return res.status(200).end();

            };
            
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public restoreBranch = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 

        req.checkBody('branch_id' , mainErrorEnum.branch_id_required ).exists();
        req.checkBody('branch_id' , mainErrorEnum.branch_id_invalid  ).matches( mainRegExp.branch._id );
        
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! this.isHaveAccessToThisBranch(req.body.branch_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };
        
        BranchModel.updateOne({
            _id : req.body.branch_id,
            deleted : 1

        }, {
            deleted : 0
        })
        .exec()
        .then( async ( result: any) => {

            if( mainHelper.isUpdateFail(result) ) {

                return expressHelper.sendCustomError( 
                    mainErrorEnum.branch_not_found,
                    next
                );

            } else {

                this.addEmployeeAction( 
                    req , 
                    'branch' , 
                    req.body.branch_id 
                );

                return res.status(200).end();

            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public addBranchImage = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        req.checkBody('branch_id' , mainErrorEnum.branch_id_required ).exists();
        req.checkBody('branch_id' , mainErrorEnum.branch_id_invalid ).matches( mainRegExp.branch._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! this.isHaveAccessToThisBranch(req.body.branch_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        if( ! req.files ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.image_not_found,
                next
            );
        };

        imageHelper.uploadImage(req.files.image)
        .then( src => {
            
            BranchModel.updateOne({ 
                _id :  req.body.branch_id,
            },{ 
                $push : {
                    images : src
                },
            })
            .exec()
            .then( async (result: any) => {

                if( mainHelper.isUpdateFail(result) ) {

                    return expressHelper.sendCustomError(
                        mainErrorEnum.branch_not_found,
                        next
                    );

                } else {
                    
                    this.addEmployeeAction( 
                        req , 
                        'branch' , 
                        req.body.branch_id 
                    );

                    return res.status(200).json({ image : src });

                };
               
            })
            .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
        
        })
        .catch( () => {
            return expressHelper.sendCustomError(
                mainErrorEnum.image_not_found,
                next
            );
        });
 

    };
    public deleteBranchImage = async (req: EmployeeRequest , res: Response , next: NextFunction) => {


        req.checkQuery('branch_id' , mainErrorEnum.branch_id_required ).exists();
        req.checkQuery('branch_id' , mainErrorEnum.branch_id_invalid ).matches( mainRegExp.branch._id );

        req.checkQuery('image' , mainErrorEnum.image_required ).exists();
        req.checkQuery('image' , mainErrorEnum.image_invalid ).isString();

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);


        if( ! this.isHaveAccessToThisBranch(req.query.branch_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        BranchModel.updateOne({ 

            _id :  req.query.branch_id ,
            images : req.query.image

        } , { 
            $pull : {
                images :  req.query.image,
            },
        })
        .exec()
        .then( async (result: any) => {

            if( mainHelper.isUpdateFail(result) ) {

                return expressHelper.sendCustomError( 
                    mainErrorEnum.branch_not_found,
                    next
                );

            } else {
                
                imageHelper.deleteImage(req.query.image);
                
                this.addEmployeeAction( 
                    req , 
                    'branch' , 
                    req.body.branch_id 
                );

                return res.status(200).end();

            };
         
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewBranch = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        req.checkQuery('branch_id' , mainErrorEnum.branch_id_required ).exists();
        req.checkQuery('branch_id' , mainErrorEnum.branch_id_invalid ).matches( mainRegExp.branch._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! this.isHaveAccessToThisBranch(req.query.branch_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        BranchModel.findOne({
            _id : req.query.branch_id,
        })
        .populate('city')
        .exec()    
        .then( async (branch: IBranchModel) => {

            if(branch) {

                return res.status(200).json({ branch });

            } else {

                return expressHelper.sendCustomError(
                    mainErrorEnum.branch_not_found,
                    next 
                );
                
            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewBranches = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
        
        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('sortBy' , mainErrorEnum.sortBy_required ).exists();
        req.checkQuery('sortBy' , mainErrorEnum.sortBy_invalid ).matches( mainRegExp.view.sortBy );
    
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        BranchModel.find({
            _id  : {
                $in : req.employee.branches_ids
            }
        })
        .sort(req.query.sortBy)
        .skip( (page - 1) * limit )
        .limit(limit)
        .populate('city')
        .exec()    
        .then( async ( branches: IBranchModel[] ) => {
            res.status(200).json({ branches });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewBranchesSearch = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
        
        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('searchQuery', mainErrorEnum.searchQuery_required ).exists();
        req.checkQuery('searchQuery', mainErrorEnum.searchQuery_invalid ).matches( mainRegExp.view.searchQuery );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        BranchModel.find({
            $text: { $search: req.query.searchQuery },
            _id : {
                $in : req.employee.branches_ids
            }
        })
        .sort('-createdAt')
        .skip( (page - 1) * limit )
        .limit(limit )
        .populate('city')
        .exec()    
        .then( async (branches: IBranchModel[]) => {
            res.status(200).json({ branches })
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
    };


    // CarType 
    public addCarType = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
        
        req.checkBody('nameEn' , mainErrorEnum.nameEn_required ).exists();
        req.checkBody('nameEn' , mainErrorEnum.nameEn_invalid  ).matches( mainRegExp.carType.nameEn );

        req.checkBody('nameAr' , mainErrorEnum.nameAr_required ).exists();
        req.checkBody('nameAr' , mainErrorEnum.nameAr_invalid  ).matches( mainRegExp.carType.nameAr );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        CarTypeModel.create({
            nameEn : req.body.nameEn,
            nameAr : req.body.nameAr,
        })
        .then( (carType: ICarTypeModel) => {
            return res.status(200).json({
                _id : carType._id
            });
        })
        .catch( _error => {

            if( mainHelper.isUniqueError(_error) ) {

                if ( _error.message.indexOf('nameEn') !== -1 ) {
                    return expressHelper.sendCustomError(
                        mainErrorEnum.carType_nameEn_used_before,
                        next
                    );
                };

                if ( _error.message.indexOf('nameAr') !== -1 ) {
                    return expressHelper.sendCustomError(
                        mainErrorEnum.carType_nameAr_used_before,
                        next
                    );
                };

            } else {
              
                expressHelper.sendCustomError(mainErrorEnum.datebase , next);

            };

        });
            
    };
    public deleteCarType = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
        
        req.checkQuery('carType_id' , mainErrorEnum.carType_id_required ).exists();
        req.checkQuery('carType_id' , mainErrorEnum.carType_id_invalid ).matches( mainRegExp.carType._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        CarTypeModel.updateOne({
            _id : req.query.carType_id ,
            deleted : 0
        } , {
            deleted : 1
        })
        .then( async (result: any) => {

            if( mainHelper.isUpdateFail(result) ) {

                return expressHelper.sendCustomError(
                    mainErrorEnum.carType_not_found , 
                    next
                );

            } else {

                return res.status(200).end();

            };
            
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
            
    };


    // CarModel
    public addCarModel = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
             
        req.checkBody('nameEn' , mainErrorEnum.nameEn_required ).exists();
        req.checkBody('nameEn' , mainErrorEnum.nameEn_invalid ).matches( mainRegExp.carModel.nameEn );

        req.checkBody('nameAr' , mainErrorEnum.nameAr_required ).exists();
        req.checkBody('nameAr' , mainErrorEnum.nameAr_invalid ).matches( mainRegExp.carModel.nameAr );

        req.checkBody('carType_id' , mainErrorEnum.carType_id_required ).exists();
        req.checkBody('carType_id' , mainErrorEnum.carType_id_invalid ).matches( mainRegExp.carType._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        CarModelModel.create({
            carType_id : req.body.carType_id,
            nameEn : req.body.nameEn,
            nameAr : req.body.nameAr,
        })
        .then( (carModel: ICarModelModel) => {
            return res.status(200).json({
                _id : carModel._id
            });
        })
        .catch( _error => {

            if( mainHelper.isUniqueError(_error) ) {

                if ( _error.message.indexOf('nameEn') !== -1 ) {
                    return expressHelper.sendCustomError(
                        mainErrorEnum.carModel_nameEn_used_before,
                        next
                    );
                };

                if ( _error.message.indexOf('nameAr') !== -1 ) {
                    return expressHelper.sendCustomError(
                        mainErrorEnum.carModel_nameAr_used_before,
                        next
                    );
                };

            } else {
              
                expressHelper.sendCustomError(mainErrorEnum.datebase , next);

            };

        });
            
    };
    public deleteCarModel = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
                
        req.checkQuery('carModel_id' , mainErrorEnum.carModel_id_required ).exists();
        req.checkQuery('carModel_id' , mainErrorEnum.carModel_id_invalid ).matches( mainRegExp.carModel._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        CarTypeModel.updateOne({
            _id : req.query.carModel_id ,
            deleted : 0
        } , {
            deleted : 1
        })
        .then( async (result: any) => {

            if( mainHelper.isUpdateFail(result) ) {

                return expressHelper.sendCustomError(
                    mainErrorEnum.carModel_not_found , 
                    next
                );

            } else {

                return res.status(200).end();

            };
            
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
            
    };


    // CarGroup
    public addCarGroup = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 

        req.checkBody('carType_id' , mainErrorEnum.carType_id_required ).exists();
        req.checkBody('carType_id' , mainErrorEnum.carType_id_invalid ).matches( mainRegExp.carType._id );
        
        req.checkBody('carModel_id' , mainErrorEnum.carModel_id_required ).exists();
        req.checkBody('carModel_id' , mainErrorEnum.carModel_id_invalid ).matches( mainRegExp.carModel._id );
        
        req.checkBody('branch_id' , mainErrorEnum.branch_id_required ).exists()
        req.checkBody('branch_id' , mainErrorEnum.branch_id_invalid ).matches( mainRegExp.branch._id );
        
        req.checkBody('year' , mainErrorEnum.year_required ).exists()
        req.checkBody('year' , mainErrorEnum.year_invalid ).matches( mainRegExp.carGroup.year );

        req.checkBody('allowedKm' , mainErrorEnum.allowedKm_required ).exists();
        req.checkBody('allowedKm' , mainErrorEnum.allowedKm_invalid ).matches( mainRegExp.carGroup.allowedKm );

        req.checkBody('plusKmPrice' , mainErrorEnum.plusKmPrice_required ).exists();
        req.checkBody('plusKmPrice' , mainErrorEnum.plusKmPrice_invalid ).matches( mainRegExp.carGroup.plusKmPrice );

        req.checkBody('pricePerDay' , mainErrorEnum.pricePerDay_required ).exists()
        req.checkBody('pricePerDay' , mainErrorEnum.pricePerDay_invalid ).matches( mainRegExp.carGroup.pricePerDay );        
        
        req.checkBody('discountPerDay' , mainErrorEnum.discountPerDay_required ).exists()
        req.checkBody('discountPerDay' , mainErrorEnum.discountPerDay_invalid ).matches( mainRegExp.carGroup.discountPerDay );
        
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next)

        CarGroupModel.create({

            carType : req.body.carType_id,
            carModel : req.body.carModel_id,
            branch : req.body.branch_id,
            year : req.body.year,

            allowedKm : req.body.allowedKm,
            plusKmPrice : req.body.plusKmPrice,

            pricePerDay : req.body.pricePerDay,
            discountPerDay : req.body.discountPerDay,

            images : [],

        })
        .then( async (carGroup: ICarGroupModel) => {
                        
            this.addEmployeeAction( 
                req , 
                'carGroup' , 
                carGroup._id 
            );

            return res.status(200).json({ _id : carGroup._id });

        })
        .catch(_error => expressHelper.sendCustomError(_error , next) );
        
    };
    public editCarGroup = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 
        
        const editObject: any = {};

        req.checkBody('carGroup_id' , mainErrorEnum.carGroup_id_required ).exists();
        req.checkBody('carGroup_id' , mainErrorEnum.carGroup_id_invalid ).matches( mainRegExp.carGroup._id );

        if( mainHelper.isExists(req.body.carType_id) ) {
            editObject.carType = req.body.carType_id;
            req.checkBody('carType_id' , mainErrorEnum.carType_id_invalid ).matches( mainRegExp.carType._id );
        };

        if( mainHelper.isExists(req.body.carModel_id) ) {
            editObject.carModel = req.body.carModel_id;
            req.checkBody('carModel_id' , mainErrorEnum.carModel_id_invalid ).matches( mainRegExp.carModel._id );
        };

        if( mainHelper.isExists(req.body.branch_id) ) {
            editObject.branch = req.body.branch_id;
            req.checkBody('branch_id' , mainErrorEnum.branch_id_invalid ).matches( mainRegExp.branch._id );
        };

        if( mainHelper.isExists(req.body.year) ) {
            editObject.year = req.body.year;
            req.checkBody('year' , mainErrorEnum.year_invalid ).matches( mainRegExp.carGroup.year );
        };

        if( mainHelper.isExists(req.body.allowedKm) ) {
            editObject.allowedKm = req.body.allowedKm;
            req.checkBody('allowedKm' , mainErrorEnum.allowedKm_invalid ).matches( mainRegExp.carGroup.allowedKm );
        };

        if( mainHelper.isExists(req.body.plusKmPrice) ) {
            editObject.plusKmPrice = req.body.plusKmPrice;
            req.checkBody('plusKmPrice' , mainErrorEnum.plusKmPrice_invalid ).matches( mainRegExp.carGroup.plusKmPrice );
        };

        if( mainHelper.isExists(req.body.pricePerDay) ) {
            editObject.pricePerDay = req.body.pricePerDay;
            req.checkBody('pricePerDay' , mainErrorEnum.pricePerDay_invalid ).matches( mainRegExp.carGroup.pricePerDay );
        };

        if( mainHelper.isExists(req.body.discountPerDay) ) {
            editObject.discountPerDay = req.body.discountPerDay;
            req.checkBody('discountPerDay' , mainErrorEnum.discountPerDay_invalid ).matches( mainRegExp.carGroup.discountPerDay );
        };

        if( mainHelper.isExists(req.body.views) ) {
            editObject.views = req.body.views;
            req.checkBody('views' , mainErrorEnum.views_invalid ).matches( mainRegExp.carGroup.views );
        };


        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( Object.keys( editObject ).length === 0 ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.edit_date_not_found,
                next
            );
        };

        CarGroupModel.updateOne({
            _id : req.body.carGroup_id ,
            branch  : {
                $in : req.employee.branches_ids
            }
        },{
            ...editObject 
        })
        .exec()
        .then( async (result: any ) => {

            if( mainHelper.isUpdateFail(result) ) {
                
                return expressHelper.sendCustomError(
                    mainErrorEnum.carGroup_not_found ,
                    next
                );

            } else {

                this.addEmployeeAction(
                    req , 
                    'carGroup',
                    req.body.carGroup_id
                );

                return res.status(200).end();

            };

        })
        .catch( _error => {

            if ( mainHelper.isUniqueError(_error) ) {
                
                return expressHelper.sendCustomError(
                    mainErrorEnum.plateNumber_used_before,
                    next
                );

            } else {

                return expressHelper.sendCustomError(mainErrorEnum.datebase , next) ;

            };

        });

        
    };
    public deleteCarGroup = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 

        req.checkQuery('carGroup_id' , mainErrorEnum.carGroup_id_required ).exists();
        req.checkQuery('carGroup_id' , mainErrorEnum.carGroup_id_invalid ).matches( mainRegExp.carGroup._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! await this.isHaveAccessToThisCarGroup(req.query.carGroup_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        CarGroupModel.updateOne({
            _id : req.query.carGroup_id ,
            deleted : 0
        } , {
            deleted : 1
        })
        .exec()
        .then( async (result: any ) => {

            if( mainHelper.isUpdateFail(result) ) {
                
                return expressHelper.sendCustomError(
                    mainErrorEnum.carGroup_not_found, 
                    next 
                );

            } else {

                this.addEmployeeAction(
                    req , 
                    'carGroup',
                    req.query.carGroup_id
                );

                return res.status(200).end();

            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public restoreCarGroup = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 

        req.checkBody('carGroup_id' , mainErrorEnum.carGroup_id_required ).exists();
        req.checkBody('carGroup_id' , mainErrorEnum.carGroup_id_invalid ).matches( mainRegExp.carGroup._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! await this.isHaveAccessToThisCarGroup(req.body.carGroup_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        CarGroupModel.updateOne({
            _id : req.body.carGroup_id ,
            deleted : 1
        } , {
            deleted : 0
        })
        .exec()
        .then( async (result: any ) => {

            if( mainHelper.isUpdateFail(result) ) {
                
                return expressHelper.sendCustomError(
                    mainErrorEnum.carGroup_not_found, 
                    next 
                );

            } else {

                this.addEmployeeAction(
                    req , 
                    'carGroup',
                    req.body.carGroup_id
                );

                return res.status(200).end();

            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public addCarGroupImage = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
        
        req.checkBody('carGroup_id' , mainErrorEnum.carGroup_id_required ).exists();
        req.checkBody('carGroup_id' , mainErrorEnum.carGroup_id_invalid ).matches( mainRegExp.carGroup._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! await this.isHaveAccessToThisCarGroup(req.body.carGroup_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        if( ! req.files ) {
            return expressHelper.sendCustomError( 
                mainErrorEnum.image_not_found , 
                next 
            ) ;
        };

        imageHelper.uploadImage(req.files.image)
        .then( src => {
            
            CarGroupModel.updateOne({
                _id : req.body.carGroup_id ,
            }, { 
                $push : {
                    images : src
                }
            })
            .exec()
            .then( async (result: any) => {
                
                if( mainHelper.isUpdateFail(result) ) {
                    
                    return expressHelper.sendCustomError(
                        mainErrorEnum.carGroup_not_found, 
                        next 
                    );

                } else {

                    this.addEmployeeAction(
                        req , 
                        'carGroup',
                        req.body.carGroup_id
                    );

                    return res.status(200).json({ image : src });

                };

            })
            .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

        })
        .catch( () => {
            return expressHelper.sendCustomError(
                mainErrorEnum.image_not_found,
                next
            );
        });
       

    };
    public deleteCarGroupImage = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
    
        req.checkQuery('carGroup_id' , mainErrorEnum.carGroup_id_required ).exists();
        req.checkQuery('carGroup_id' , mainErrorEnum.carGroup_id_invalid  ).matches( mainRegExp.carGroup._id  );

        req.checkQuery('image' , mainErrorEnum.image_required ).exists();
        req.checkQuery('image' , mainErrorEnum.image_invalid ).isString();

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);


        if( ! await this.isHaveAccessToThisCarGroup(req.query.carGroup_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        CarGroupModel.updateOne({ 
            _id : req.query.carGroup_id ,
            images : req.query.image,
        } , { 

            $pull : {
                images : req.query.image,
            },

        })
        .exec()
        .then( async (result: any) => {
            
            if( mainHelper.isUpdateFail(result) ) {
                
                return expressHelper.sendCustomError(
                    mainErrorEnum.carGroup_not_found, 
                    next 
                );

            } else {
                
                imageHelper.deleteImage(req.query.image);
                this.addEmployeeAction(
                    req , 
                    'carGroup',
                    req.query.carGroup_id
                );

                return res.status(200).end();

            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewCarGroup = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        req.checkQuery('carGroup_id' , mainErrorEnum.carGroup_id_required ).exists();
        req.checkQuery('carGroup_id' , mainErrorEnum.carGroup_id_invalid ).matches( mainRegExp.carGroup._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! await this.isHaveAccessToThisCarGroup(req.query.carGroup_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        CarGroupModel.findOne({
            _id : req.query.carGroup_id,
        })
        .populate('carType carModel branch')
        .lean()
        .exec()  
        .then( async (carGroup: ICarGroupModel) => {
            
            if(carGroup) {

                return res.status(200).json({ 
                    carGroup : await carGroupHelper.setCarsCountInCarGroup(carGroup) 
                });

            } else {

                return expressHelper.sendCustomError(
                    mainErrorEnum.carGroup_not_found,
                    next 
                );
                
            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewCarGroups = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
        
        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('sortBy' , mainErrorEnum.sortBy_required ).exists();
        req.checkQuery('sortBy' , mainErrorEnum.sortBy_invalid ).matches( mainRegExp.view.sortBy );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        CarGroupModel.find({
            branch : {
                $in : req.employee.branches_ids
            }
        })
        .sort(req.query.sortBy)
        .skip( (page - 1) * limit )
        .limit(limit)
        .populate('carType carModel branch')
        .lean()
        .exec()    
        .then( async (carGroups: ICarGroupModel[]) => {
            res.status(200).json({ carGroups : await carGroupHelper.setCarsCountInCarGroups(carGroups) });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewCarGroupsByFilter = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
        
        const query: any = {};

        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('sortBy' , mainErrorEnum.sortBy_required ).exists();
        req.checkQuery('sortBy' , mainErrorEnum.sortBy_invalid ).matches( mainRegExp.view.sortBy );
    
        if( mainHelper.isExists(req.query.branch_id) ) {
            req.checkQuery('branch_id', mainErrorEnum.branch_id_invalid ).matches( mainRegExp.branch._id );
            query.branch = req.query.branch_id
        };

        if( mainHelper.isExists(req.query.carType_id) ) {
            req.checkQuery('carType_id', mainErrorEnum.carType_id_invalid ).matches( mainRegExp.carType._id );
            query.carType = req.query.carType_id
        };

        if( mainHelper.isExists(req.query.carModel_id) ) {
            req.checkQuery('carModel_id', mainErrorEnum.carModel_id_invalid ).matches( mainRegExp.carModel._id );
            query.carModel = req.query.carModel_id
        };

        if( mainHelper.isExists(req.query.year) ) {
            req.checkQuery('year', mainErrorEnum.year_invalid ).matches( mainRegExp.carGroup.year );
            query.year = req.query.year
        };

        if( mainHelper.isExists(req.query.pricePerDay) ) {
            req.checkQuery('pricePerDay', mainErrorEnum.pricePerDay_invalid ).matches( mainRegExp.carGroup.pricePerDay );
            query.pricePerDay = req.query.pricePerDay
        };

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        CarGroupModel.find({
            $and : [
                query , 
                {
                    branch : {
                        $in : req.employee.branches_ids
                    }
                }
            ]
        })
        .sort(req.query.sortBy)
        .skip( (page - 1) * limit )
        .limit(limit)
        .populate('carType carModel branch')
        .lean()
        .exec()    
        .then( async (carGroups: ICarGroupModel[]) => {
            res.status(200).json({ carGroups : await carGroupHelper.setCarsCountInCarGroups(carGroups) });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };


    // Booking
    public addCarToBooking = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 
        
        req.checkBody('booking_id' , mainErrorEnum.booking_id_required ).exists();
        req.checkBody('booking_id' , mainErrorEnum.booking_id_invalid ).matches( mainRegExp.booking._id );

        req.checkBody('car_id' , mainErrorEnum.car_id_required ).exists();
        req.checkBody('car_id' , mainErrorEnum.car_id_invalid ).matches( mainRegExp.car._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! await this.isHaveAccessToThisBooking(req.body.booking_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };
        
        if(await bookingHelper.isCanAddCarToBooking(req.body.car_id , req.body.booking_id) ) {
       
            BookingModel.updateOne({
                _id : req.body.booking_id ,
                deleted : 0
            },{
                car : req.body.car_id
            })
            .exec()
            .then( async (result: any) => {

                if( mainHelper.isUpdateFail(result) ) {
                    
                    return expressHelper.sendCustomError(
                        mainErrorEnum.booking_not_found,
                        next
                    );

                } else {
            
                    this.addEmployeeAction(
                        req , 
                        'booking',
                        req.body.booking_id
                    );

                    return res.status(200).end();

                };

            })
            .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
          
        } else {
        
            return expressHelper.sendCustomError(
                mainErrorEnum.you_cant_booking_this_car_in_this_booking_date , 
                next
            );
        
        };
        
    };
    public editBookingDate = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 
        
        req.checkBody('booking_id' , mainErrorEnum.booking_id_required ).exists();
        req.checkBody('booking_id' , mainErrorEnum.booking_id_invalid ).matches( mainRegExp.booking._id );

        req.checkBody('pickUpDate' , mainErrorEnum.pickUpDate_required ).exists();
        req.checkBody('pickUpDate' , mainErrorEnum.pickUpDate_invalid ).matches( mainRegExp.booking.pickUpDate );

        req.checkBody('returnDate' , mainErrorEnum.returnDate_required ).exists();
        req.checkBody('returnDate' , mainErrorEnum.returnDate_invalid ).matches( mainRegExp.booking.returnDate );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! await this.isHaveAccessToThisBooking(req.body.booking_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        const isCanEditBookingDate: boolean = await bookingHelper.isCanEditBookingDate(
            req.body.booking_id,
            req.body.pickUpDate,
            req.body.returnDate,
        );
        if(isCanEditBookingDate) {

            const booking: IBookingModel = await BookingModel.findOne({
                _id: req.body.booking_id,
                deleted : 0,
            })
            .exec();

            const daysCount: number =  bookingHelper.getDaysCount(req.body.pickUpDate , req.body.returnDate);
            const totalPrice: number =  bookingHelper.getTotlaPrice(booking.pricePerDay , booking.discountPerDay ,daysCount);

            BookingModel.findOneAndUpdate({
                _id : req.body.booking_id ,
                deleted : 0
            },{
                daysCount,
                total : totalPrice,
                pickUpDate : req.body.pickUpDate,
                returnDate : req.body.returnDate,
            } , {
                new : true
            })
            .exec()
            .then( async (booking: any) => {
    
                if(booking) {
                    
                    this.addEmployeeAction(
                        req , 
                        'booking',
                        req.body.booking_id
                    );
    
                    return res.status(200).json({ booking });
    
                } else {

                    return expressHelper.sendCustomError(
                        mainErrorEnum.booking_not_found,
                        next
                    );
                
                };
    
            })
            .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );


        } else {
            return expressHelper.sendCustomError(
                mainErrorEnum.you_cant_edit_booking_date , 
                next
            );
        };

    };
    public editBookingExpiredAt = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 
        
        req.checkBody('booking_id' , mainErrorEnum.booking_id_required ).exists();
        req.checkBody('booking_id' , mainErrorEnum.booking_id_invalid ).matches( mainRegExp.booking._id );

        req.checkBody('expiredAt' , mainErrorEnum.expiredAt_required ).exists();
        req.checkBody('expiredAt' , mainErrorEnum.expiredAt_invalid ).matches( mainRegExp.booking.expiredAt );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! await this.isHaveAccessToThisBooking(req.body.booking_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        const isCanEditExpiredAt: boolean = await bookingHelper.isCanEditExpiredAt(
            req.body.booking_id,
            req.body.expiredAt,
        );
        if(isCanEditExpiredAt) {

            BookingModel.updateOne({
                _id : req.body.booking_id ,
                deleted : 0
            },{
                expiredAt : req.body.expiredAt,
            })
            .exec()
            .then( async (result: any) => {
    
                if(mainHelper.isUpdateFail(result)) {
                    
                    return expressHelper.sendCustomError(
                        mainErrorEnum.booking_not_found,
                        next
                    );
    
                } else {
            
                    this.addEmployeeAction(
                        req , 
                        'booking',
                        req.body.booking_id
                    );
    
                    return res.status(200).json();
    
                };
    
            })
            .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

        } else {

            return expressHelper.sendCustomError(
                mainErrorEnum.you_cant_edit_booking_expiredAt , 
                next
            );

        };

    };
    public editBookingPricePerDay = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 
           
        req.checkBody('booking_id' , mainErrorEnum.booking_id_required ).exists();
        req.checkBody('booking_id' , mainErrorEnum.booking_id_invalid ).matches( mainRegExp.booking._id );

        req.checkBody('pricePerDay' , mainErrorEnum.pricePerDay_required ).exists();
        req.checkBody('pricePerDay' , mainErrorEnum.pricePerDay_invalid ).matches( mainRegExp.booking.pricePerDay );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);
        
        if( ! await this.isHaveAccessToThisBooking(req.body.booking_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        const booking: IBookingModel = await BookingModel.findOne({
            _id: req.body.booking_id,
            deleted : 0,
        })
        .exec();

        const totalPrice: number =  bookingHelper.getTotlaPrice(req.body.pricePerDay , booking.discountPerDay , booking.daysCount);

        BookingModel.findOneAndUpdate({
            _id : req.body.booking_id ,
            deleted : 0
        },{
            total : totalPrice,
            pricePerDay : req.body.pricePerDay,
        } , {
            new : true
        })
        .exec()
        .then( async (booking: any) => {

            if(booking) {
                
                this.addEmployeeAction(
                    req , 
                    'booking',
                    req.body.booking_id
                );

                return res.status(200).json({ booking });

            } else {
        
                return expressHelper.sendCustomError(
                    mainErrorEnum.booking_not_found,
                    next
                );
              
            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );


    };
    public editBookingDiscountPerDay = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 
           
        req.checkBody('booking_id' , mainErrorEnum.booking_id_required ).exists();
        req.checkBody('booking_id' , mainErrorEnum.booking_id_invalid ).matches( mainRegExp.booking._id );

        req.checkBody('discountPerDay' , mainErrorEnum.discountPerDay_required ).exists();
        req.checkBody('discountPerDay' , mainErrorEnum.discountPerDay_invalid ).matches( mainRegExp.booking.discountPerDay );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);


        if( ! await this.isHaveAccessToThisBooking(req.body.booking_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        const booking: IBookingModel = await BookingModel.findOne({
            _id: req.body.booking_id,
            deleted : 0,
        })
        .exec();

        const totalPrice: number = bookingHelper.getTotlaPrice(booking.pricePerDay , req.body.discountPerDay , booking.daysCount);

        BookingModel.findOneAndUpdate({
            _id : req.body.booking_id ,
            deleted : 0
        },{
            total : totalPrice,
            discountPerDay : req.body.discountPerDay,
        } , {
            new : true
        })
        .exec()
        .then( async (booking: any) => {

            if(booking) {
                
                this.addEmployeeAction(
                    req , 
                    'booking',
                    req.body.booking_id
                );

                return res.status(200).json({ booking });

            } else {

                return expressHelper.sendCustomError(
                    mainErrorEnum.booking_not_found,
                    next
                );
          
            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );


    };
    public activeBooking = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 
        
        req.checkBody('booking_id' , mainErrorEnum.booking_id_required ).exists();
        req.checkBody('booking_id' , mainErrorEnum.booking_id_invalid ).matches( mainRegExp.booking._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! await this.isHaveAccessToThisBooking(req.body.booking_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        const isCanActiveBooking = await bookingHelper.isCanActiveBooking(req.body.booking_id);
        if(isCanActiveBooking) {

            BookingModel.updateOne({
                _id : req.body.booking_id ,
                deleted : 0,
                bookingType : EbookingType.waiting_to_confirm, 
                car : {$ne : null},
            },{
                bookingType : EbookingType.active 
            })
            .exec()
            .then( async (result: any ) => {
    
                if( mainHelper.isUpdateFail(result) ) {
                    
                    return expressHelper.sendCustomError(
                        mainErrorEnum.booking_not_found_or_car_not_found ,
                        next
                    );
    
                } else {
    
                    this.addEmployeeAction(
                        req , 
                        'booking',
                        req.body.booking_id
                    );
    
                    return res.status(200).end();
    
                };
    
            })
            .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );


        } else {
            return expressHelper.sendCustomError(
                mainErrorEnum.you_cant_active_booking ,
                next
            );
        };

    };
    public cancelBooking = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 
        
        req.checkBody('booking_id' , mainErrorEnum.booking_id_required ).exists();
        req.checkBody('booking_id' , mainErrorEnum.booking_id_invalid ).matches( mainRegExp.booking._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);


        if( ! await this.isHaveAccessToThisBooking(req.body.booking_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        BookingModel.updateOne({
            _id : req.body.booking_id ,
            deleted : 0,
            bookingType : EbookingType.request, 
        },{
            bookingType : EbookingType.cancelled 
        })
        .exec()
        .then( async (result: any ) => {

            if( mainHelper.isUpdateFail(result) ) {
                
                return expressHelper.sendCustomError(
                    mainErrorEnum.booking_not_found ,
                    next
                );

            } else {

                this.addEmployeeAction(
                    req , 
                    'booking',
                    req.body.booking_id
                );

                return res.status(200).end();

            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
        
    };
    public approveBooking = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 
        
        req.checkBody('booking_id' , mainErrorEnum.booking_id_required ).exists();
        req.checkBody('booking_id' , mainErrorEnum.booking_id_invalid ).matches( mainRegExp.booking._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! await this.isHaveAccessToThisBooking(req.body.booking_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        BookingModel.updateOne({
            _id : req.body.booking_id ,
            deleted : 0,
            bookingType : EbookingType.request, 
        },{
            bookingType : EbookingType.waiting_to_confirm 
        })
        .exec()
        .then( async (result: any ) => {

            if( mainHelper.isUpdateFail(result) ) {
                
                return expressHelper.sendCustomError(
                    mainErrorEnum.booking_not_found ,
                    next
                );

            } else {

                this.addEmployeeAction(
                    req , 
                    'booking',
                    req.body.booking_id
                );

                return res.status(200).end();

            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
        
    };
    public deleteBooking  = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 
            
        req.checkQuery('booking_id' , mainErrorEnum.booking_id_required ).exists();
        req.checkQuery('booking_id' , mainErrorEnum.booking_id_invalid  ).matches( mainRegExp.booking._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! await this.isHaveAccessToThisBooking(req.query.booking_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        BookingModel.updateOne({
            _id : req.query.booking_id,
            deleted : 0
        }, {
            deleted : 1
        })
        .exec()
        .then( async (result: any) => {
            
            if( mainHelper.isUpdateFail(result) ) {

                return expressHelper.sendCustomError( 
                    mainErrorEnum.booking_not_found,
                    next
                );

            } else {

                this.addEmployeeAction( 
                    req , 
                    'booking' , 
                    req.query.booking_id 
                );

                return res.status(200).end();

            };
            
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public restoreBooking = async (req: EmployeeRequest , res: Response , next: NextFunction) => { 

        req.checkBody('booking_id' , mainErrorEnum.booking_id_required ).exists();
        req.checkBody('booking_id' , mainErrorEnum.booking_id_invalid  ).matches( mainRegExp.booking._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! await this.isHaveAccessToThisBooking(req.body.booking_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        BookingModel.updateOne({
            _id : req.body.booking_id,
            deleted : 1,

        }, {
            deleted : 0
        })
        .exec()
        .then( async ( result: any) => {

            if( mainHelper.isUpdateFail(result) ) {

                return expressHelper.sendCustomError( 
                    mainErrorEnum.booking_not_found,
                    next
                );

            } else {

                this.addEmployeeAction( 
                    req , 
                    'booking' , 
                    req.body.booking_id 
                );

                return res.status(200).end();

            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewBooking = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        req.checkQuery('booking_id' , mainErrorEnum.booking_id_required ).exists();
        req.checkQuery('booking_id' , mainErrorEnum.booking_id_invalid ).matches( mainRegExp.booking._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( ! await this.isHaveAccessToThisBooking(req.query.booking_id , req.employee.branches_ids) ) {
            return expressHelper.sendCustomError(
                mainErrorEnum.not_have_access,
                next
            );
        };

        BookingModel.findOne({
            _id : req.query.booking_id,
        })
        .populate('guest onlinePayment')
        .populate({
            path : 'returnBranch',
            select : 'nameEn nameAr'
        })
        .populate({
            path : 'pickUpBranch',
            select : 'nameEn nameAr'
        })
        .populate({
            path : 'client',
            select : 'name'
        })
        .populate({
            path : 'car',
            select : 'plateNumber'
        })
        .populate({
            path : 'carGroup',
            select : 'branch',
            populate : {
                path : 'branch',
                select : 'nameEn nameAr',
            }
        })
        .populate({
            path : 'carGroup',
            select : 'carModel',
            populate : {
                path : 'carModel',
                select : 'nameEn nameAr',
            }
        })
        .populate({
            path : 'carGroup',
            select : 'carType year images',
            populate : {
                path : 'carType',
                select : 'nameEn nameAr',
            }
        })
        .lean()
        .exec()
        .then( async (booking: IBookingModel) => {
                  
            if(booking) {

                return res.status(200).json({ 
                    booking : await bookingHelper.setBookingType(booking) 
                });

            } else {

                return expressHelper.sendCustomError(
                    mainErrorEnum.booking_not_found,
                    next 
                );
                
            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewBookings = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('sortBy' , mainErrorEnum.sortBy_required ).exists();
        req.checkQuery('sortBy' , mainErrorEnum.sortBy_invalid ).matches( mainRegExp.view.sortBy );
    
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        BookingModel.find({
            pickUpBranch : {
                $in : req.employee.branches_ids
            },
            returnBranch : {
                $in : req.employee.branches_ids
            }
        })
        .sort(req.query.sortBy)
        .skip( (page - 1) * limit)
        .limit(limit)
        .populate('guest onlinePayment')
        .populate({
            path : 'returnBranch',
            select : 'nameEn nameAr'
        })
        .populate({
            path : 'pickUpBranch',
            select : 'nameEn nameAr'
        })
        .populate({
            path : 'client',
            select : 'name'
        })
        .populate({
            path : 'car',
            select : 'plateNumber'
        })
        .populate({
            path : 'carGroup',
            select : 'carModel',
            populate : {
                path : 'carModel',
                select : 'nameEn nameAr',
            }
        })
        .populate({
            path : 'carGroup',
            select : 'carType images',
            populate : {
                path : 'carType',
                select : 'nameEn nameAr',
            }
        })
        .lean()
        .exec()    
        .then( async (bookings: IBookingModel[]) => {
            res.status(200).json({ 
                bookings : bookingHelper.setBookingsType(bookings)
            });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewBookingsByFilter = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('sortBy' , mainErrorEnum.sortBy_required ).exists();
        req.checkQuery('sortBy' , mainErrorEnum.sortBy_invalid ).matches( mainRegExp.view.sortBy );
    
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        // create query
        let query: any = {} ;

        // isExists
        if( mainHelper.isExists(req.query.bookingType) ) {

            // check
            if( mainRegExp.booking.bookingType.test(req.query.bookingType) ) {

                // create query

                const bookingType: number = parseInt(req.query.bookingType);

                if(bookingType === EbookingType.expired) {
                    query = {
                        bookingType : EbookingType.waiting_to_confirm , 
                        expiredAt : {
                            $lt : new Date()
                        }
                    }; 
                } else if (bookingType === EbookingType.waiting_to_confirm) {
                    query = {
                        bookingType : EbookingType.waiting_to_confirm , 
                        expiredAt : {
                            $gt : new Date()
                        }
                    }; 
                } else if(bookingType === EbookingType.done) {
                    query = {
                        bookingType : EbookingType.active , 
                        returnDate : {
                            $lt : new Date()
                        }
                    }; 
                } else if(bookingType === EbookingType.active) {
                    query = {
                        bookingType : EbookingType.active , 
                        returnDate : {
                            $gt : new Date()
                        }
                    }; 
                
                } else {
                    query = {
                        bookingType : bookingType , 
                    };
                };

            } else {
                return expressHelper.sendCustomError(mainErrorEnum.bookingType_invalid , next);
            };

        };

        // isExists
        if( mainHelper.isExists(req.query.branch_id) ) {
            
            // check
            if( mainRegExp.branch._id.test(req.query.branch_id) ) {
                
                query.pickUpBranch = req.query.branch_id;
                query.returnBranch = req.query.branch_id;

            } else {
                return expressHelper.sendCustomError(mainErrorEnum.branch_id_invalid , next);
            };

        };

        BookingModel.find({
            $and : [
                query , 
                {
                    pickUpBranch : {
                        $in : req.employee.branches_ids
                    },
                    returnBranch : {
                        $in : req.employee.branches_ids
                    },
                }
            ]
        })
        .sort(req.query.sortBy)
        .skip( (page - 1) * limit)
        .limit(limit)
        .populate('guest onlinePayment')
        .populate({
            path : 'returnBranch',
            select : 'nameEn nameAr'
        })
        .populate({
            path : 'pickUpBranch',
            select : 'nameEn nameAr'
        })
        .populate({
            path : 'client',
            select : 'name'
        })
        .populate({
            path : 'car',
            select : 'plateNumber'
        })
        .populate({
            path : 'carGroup',
            select : 'carModel',
            populate : {
                path : 'carModel',
                select : 'nameEn nameAr',
            }
        })
        .populate({
            path : 'carGroup',
            select : 'carType images',
            populate : {
                path : 'carType',
                select : 'nameEn nameAr',
            }
        })
        .lean()
        .exec()    
        .then( async (bookings: IBookingModel[]) => {
            res.status(200).json({ 
                bookings : bookingHelper.setBookingsType(bookings)
            });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewBookingsSearch = async (req: EmployeeRequest , res: Response , next: NextFunction) => {

        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('searchQuery', mainErrorEnum.searchQuery_required ).exists();
        req.checkQuery('searchQuery', mainErrorEnum.searchQuery_invalid ).matches( mainRegExp.view.searchQuery );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        BookingModel.find({
            $text: { $search: req.query.searchQuery },
            pickUpBranch : {
                $in : req.employee.branches_ids
            },
            returnBranch : {
                $in : req.employee.branches_ids
            },
        })
        .sort('-createdAt')
        .skip( (page - 1) * limit )
        .limit(limit)
        .populate('guest onlinePayment')
        .populate({
            path : 'returnBranch',
            select : 'nameEn nameAr'
        })
        .populate({
            path : 'pickUpBranch',
            select : 'nameEn nameAr'
        })
        .populate({
            path : 'client',
            select : 'name'
        })
        .populate({
            path : 'car',
            select : 'plateNumber'
        })
        .populate({
            path : 'carGroup',
            select : 'carModel',
            populate : {
                path : 'carModel',
                select : 'nameEn nameAr',
            }
        })
        .populate({
            path : 'carGroup',
            select : 'carType images',
            populate : {
                path : 'carType',
                select : 'nameEn nameAr',
            }
        })
        .lean()
        .exec()    
        .then( async (bookings: IBookingModel[]) => {
            res.status(200).json({ 
                bookings : bookingHelper.setBookingsType(bookings)
            });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };


    // Guest
    public viewGuest = async (req: Request , res: Response , next: NextFunction) => {

        req.checkQuery('guest_id' , mainErrorEnum.guest_id_required ).exists();
        req.checkQuery('guest_id' , mainErrorEnum.guest_id_invalid ).matches( mainRegExp.guest._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        GuestModel.findOne({
            _id : req.query.guest_id,
        })
        .exec()  
        .then( async (guest: IGuestModel) => {
                    
            if(guest) {

                return res.status(200).json({ guest });

            } else {

                return expressHelper.sendCustomError(
                    mainErrorEnum.guest_not_found,
                    next 
                );
                
            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewGuests = async (req: Request , res: Response , next: NextFunction) => {
        
        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('sortBy' , mainErrorEnum.sortBy_required ).exists();
        req.checkQuery('sortBy' , mainErrorEnum.sortBy_invalid ).matches( mainRegExp.view.sortBy );
    
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        GuestModel.find({})
        .sort(req.query.sortBy)
        .skip( (page - 1) * limit )
        .limit(limit)
        .exec()    
        .then( async (guests: IGuestModel[]) => {
            res.status(200).json({ guests });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewGuestsSearch = async (req: Request , res: Response , next: NextFunction) => {
        
        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('searchQuery' , mainErrorEnum.searchQuery_required ).exists();
        req.checkQuery('searchQuery',  mainErrorEnum.searchQuery_invalid ).matches( mainRegExp.view.searchQuery );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt( req.query.limit ); 
        const page = parseInt( req.query.page ); 

        GuestModel.find({
            $text : { 
                $search : req.query.searchQuery 
            },
        })
        .sort('-createdAt')
        .skip( (page - 1) * limit )
        .limit(limit)
        .exec()    
        .then( (guests: IGuestModel[]) => {
            res.status(200).json({ guests })
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewBookingsForGuest = async (req: Request , res: Response , next: NextFunction) => {
            
        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkQuery('limit', mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit', mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('guest_id' , mainErrorEnum.guest_id_required ).exists();
        req.checkQuery('guest_id' , mainErrorEnum.guest_id_invalid ).matches( mainRegExp.guest._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page); 

        BookingModel.find({
            guest : req.query.guest_id
        })
        .skip( (page - 1) * limit )
        .limit(limit)
        .populate('car carGroup pickUpBranch returnBranch onlinePayment')
        .exec()    
        .then( (bookings: IBookingModel[]) => res.status(200).json({ bookings }) )
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewOnlinePaymentsForGuest = async (req: Request , res: Response , next: NextFunction) => {
            
        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkQuery('limit', mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit', mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('guest_id' , mainErrorEnum.guest_id_required ).exists();
        req.checkQuery('guest_id' , mainErrorEnum.guest_id_invalid ).matches( mainRegExp.guest._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page); 

        OnlinePaymentModel.find({
            client : req.query.guest_id
        })
        .skip( ( page - 1 ) * limit )
        .limit(limit)
        .populate('booking')
        .exec()    
        .then( (onlinePayments: IOnlinePaymentModel[]) => res.status(200).json({ onlinePayments }) )
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };


    // options
    public viewAllBranchesCanThisEmployeeAccess = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
        BranchModel.find({ 
            _id : {
                $in : req.employee.branches_ids
            },
        })
        .exec() 
        .then( async (branches: IBranchModel[]) => {
            res.status(200).json({ branches });
        })
        .catch( _error =>  expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
    };
    public viewAllCarGroupsCanThisEmployeeAccess = async (req: EmployeeRequest , res: Response , next: NextFunction) => {
        
        CarGroupModel.find({
            branch : {
                $in : req.employee.branches_ids
            },
            deleted : 0,
        })
        .sort('-createdAt')
        .select('_id carType carModel branch year')
        .populate({
            path : 'carType',
            select : '-_id nameEn nameAr'
        })
        .populate({
            path : 'carModel',
            select : '-_id nameEn nameAr'
        })
        .populate({
            path : 'branch',
            select : '-_id nameEn nameAr'
        })
        .exec()    
        .then( async ( carGroups: ICarGroupModel[] ) => {
            res.status(200).json({ carGroups })
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
    
    };
    public viewAllNationalities = async (req: Request , res: Response , next: NextFunction) => {
        NationalityModel.find({})
        .sort('-createdAt')
        .exec()    
        .then( async ( nationalities: INationalityModel[] ) => {
            res.status(200).json({ nationalities });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
    };
    public viewAllCities = async (req: Request , res: Response , next: NextFunction) => {
        CityModel.find({})
        .sort('-createdAt')
        .exec()    
        .then( async ( cities: ICityModel[] ) => {
            res.status(200).json({ cities });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
    };
    public getThislocation = async (req: Request , res: Response , next: NextFunction) => {
    
        const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        
        // geoip.
        request.get(`http://ip-api.com/json/${ip}` , ( error, _response, body ) => {
            
            const _body = JSON.parse( body );

            if( error || _body.status === 'fail' ) {

                res.status(200).json({
                    latitude  : 24.774265  ,
                    longitude : 46.738586  ,  
                });
    
            } else {

                res.status(200).json({
                    latitude  : parseFloat( _body.lat ) ,
                    longitude : parseFloat( _body.lon ) ,  
                });
    
            };
            
        });
    };
    public viewAllCarTypes = async (req: Request , res: Response , next: NextFunction) => {
        CarTypeModel.find({
            // deleted : 0
        })
        .exec()
        .then( (carTypes: ICarTypeModel[]) => {
            return res.status(200).json({ carTypes });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
    };
    public viewAllCarModelsByCarType = async (req: Request , res: Response , next: NextFunction) => {
        
        req.checkQuery('carType_id' , mainErrorEnum.carType_id_required ).exists();
        req.checkQuery('carType_id' , mainErrorEnum.carType_id_invalid ).matches( mainRegExp.carType._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        CarModelModel.find({
            carType_id : req.query.carType_id,
            // deleted : 0
        })
        .exec()
        .then( (carModels: ICarModelModel[] ) => {
            return res.status(200).json({ carModels });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
        
    };
    public viewAllCarsWhoCanAddToBooking = async (req: Request , res: Response , next: NextFunction) => {
        
        req.checkQuery('booking_id' , mainErrorEnum.booking_id_required ).exists();
        req.checkQuery('booking_id' , mainErrorEnum.booking_id_invalid ).matches( mainRegExp.booking._id );
        
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);
            
        bookingHelper.viewAllCarsWhoCanAddToBooking(req.query.booking_id)
        .then( async ( cars: ICarModel[] ) => {
            res.status(200).json({ cars });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
    };


};


export const employeeCTRL = new EmployeeCTRL();


