import { mainErrorEnum } from './../enums/error.enum';
import { employeeTimerHelper } from '../helpers/employeeTimerHelper';
import { mainHelper } from '../helpers/mainHelper';
import { EmployeeActionModel, IEmployeeActionModel, EitemName } from '../models/employeeAction';
import { EmployeeTimerModel , IEmployeeTimerModel} from '../models/employeeTimer';
import { Response, NextFunction , Request } from 'express';
import { AdminRequest } from '../types/express';
import { mainRegExp } from '../regExp/mainRegExp';
import { jwt } from '../helpers/jwt';
import { expressHelper } from '../helpers/expressHelper';
import { EmployeeModel , IEmployeeModel } from '../models/employee';
import { BranchModel , IBranchModel } from '../models/branch';
import { EmployeeGroupModel , IEmployeeGroupModel } from '../models/employeeGroup';
import { AdminModel , IAdminModel } from '../models/admin';
import { SocketIoEmployee } from '../socket.io/socketIoEmployee';

class AdminCTRLHelper extends SocketIoEmployee {
    
    constructor(){
        super();
    };

    // use in AdminCTRL
    public setIsOnlineInEmployees = async (employees: IEmployeeModel[]): Promise<IEmployeeModel[]> => {
        
        const onlineEmployees_ids: string[] = await this.getAllOnlineEmployees_ids();

        for( let i = 0 , len = employees.length ; i < len ; ++i ) {
            employees[i].isOnline = onlineEmployees_ids.indexOf(employees[i]._id.toString()) !== -1 ? 1 : 0;
        };

        return employees

    };
    public setIsOnlineInEmployee = async (employee: IEmployeeModel): Promise<IEmployeeModel> => {
        employee.isOnline = await this.isEmployeeOnline(employee._id);
        return employee;
    };

};

// use in admin router
class AdminCTRL extends AdminCTRLHelper {
    
    constructor(){
        super()
    };

    // Guard
    public guard = jwt.allaw_express({
        roles : ['admin'],
        dataField : 'admin',
        header_name_of_token : 'token'
    });

    // Auth
    public signin = async (req: Request , res: Response , next: NextFunction) => {

        req.checkBody('email' , mainErrorEnum.email_required ).exists();
        req.checkBody('email' , mainErrorEnum.email_invalid ).matches( mainRegExp.admin.email );
        
        req.checkBody('password' , mainErrorEnum.password_required ).exists();
        req.checkBody('password' , mainErrorEnum.password_invalid ).matches( mainRegExp.admin.password );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        AdminModel.findOne({
            email : req.body.email ,
            password : req.body.password
        })
        .exec()
        .then( async (admin: IAdminModel) => {

            if( ! admin ) {
                
                return expressHelper.sendCustomError( 
                    mainErrorEnum.admin_not_found , 
                    next
                );

            } else {

                const token: string = await jwt.create({
                    _id : admin._id,
                    roles: admin.roles
                });
                
                admin.password = '';

                res.status(200).json({ admin , token });

            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase  , next ) );
            
    };
    public signup = async (req: Request , res: Response , next: NextFunction) => {

        req.checkBody('email' , mainErrorEnum.email_required ).exists();
        req.checkBody('email' , mainErrorEnum.email_invalid ).matches(  mainRegExp.admin.email );

        req.checkBody('password' , mainErrorEnum.password_required ).exists();
        req.checkBody('password' , mainErrorEnum.password_invalid).matches(  mainRegExp.admin.password );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);
        
        AdminModel.create({
            email : req.body.email,
            password : req.body.password,
            roles : ['admin']
        })
        .then( async (admin: IAdminModel ) => {
            
            const token: string = await jwt.create({ 
                _id   : admin._id,
                roles : admin.roles
            });
            
            admin.password = '';

            res.status(200).json({ admin , token });

        })
        .catch( _error => {

            if ( mainHelper.isUniqueError(_error) ) {

                return expressHelper.sendCustomError( 
                    mainErrorEnum.email_used_before , 
                    next
                );

            } else {
            
                return expressHelper.sendCustomError(mainErrorEnum.datebase  , next );
            
            };

        });
    
    };
    public refreshToken = async (req: AdminRequest , res: Response , next: NextFunction) =>  {

        AdminModel.findOne({
            _id : req.admin._id,
        })
        .exec()
        .then( async (admin: IAdminModel) => {
            
            if( ! admin ) {
                
                return expressHelper.sendCustomError( 
                    mainErrorEnum.admin_not_found , 
                    next
                );
                
            } else {

                const token: string = await jwt.create({ 
                    _id   : admin._id,
                    roles : admin.roles
                });

                admin.password = '';
    
                return res.status(200).json({ 
                    token , admin
                });

            };
            
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
            

    };


    // Employee Group
    public addEmployeeGroup = async (req: Request , res: Response , next: NextFunction) => {

        req.checkBody('nameEn' , mainErrorEnum.nameEn_required ).exists();
        req.checkBody('nameEn' , mainErrorEnum.nameEn_invalid ).matches( mainRegExp.employeeGroup.nameEn );

        req.checkBody('nameAr' , mainErrorEnum.nameAr_required ).exists();
        req.checkBody('nameAr' , mainErrorEnum.nameAr_invalid ).matches( mainRegExp.employeeGroup.nameAr );

        req.checkBody('screens' , mainErrorEnum.screens_required ).exists();
        (req as any).checkBody('screens' , mainErrorEnum.screens_invalid ).checkScreens();

        req.checkBody('branches', mainErrorEnum.branches_required ).exists();
        (req as any).checkBody('branches', mainErrorEnum.branches_invalid ).checkBranches();

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        EmployeeGroupModel.create({
            nameEn : req.body.nameEn,  
            nameAr : req.body.nameAr,  
            screens : req.body.screens,  
            branches : req.body.branches,
        })
        .then( async (group: IEmployeeGroupModel) => {
            res.status(200).json({_id : group._id});
        })
        .catch( _error => {

            if ( mainHelper.isUniqueError(_error) ) {
                
                if( _error.message.indexOf('nameEn') !== -1 ) {
                    return expressHelper.sendCustomError( 
                        mainErrorEnum.employeeGroup_nameEn_used_before , 
                        next
                    );
                };

                if( _error.message.indexOf('nameAr') !== -1 ) {
                    return expressHelper.sendCustomError( 
                        mainErrorEnum.employeeGroup_nameAr_used_before , 
                        next
                    );
                };

            } else {

                return expressHelper.sendCustomError( 
                    mainErrorEnum.datebase  , 
                    next 
                );

            };

        });
    
    };
    public editEmployeeGroup = async (req: Request , res: Response , next: NextFunction) => {

        req.checkBody('employeeGroup_id' , mainErrorEnum.employeeGroup_id_required ).exists()
        req.checkBody('employeeGroup_id' , mainErrorEnum.employeeGroup_id_invalid ).matches( mainRegExp.employeeGroup._id )

        let editObject: any = {};

        if( mainHelper.isExists(req.body.nameEn) ){
            editObject.nameEn = req.body.nameEn;
            req.checkBody('nameEn' , mainErrorEnum.nameEn_invalid ).matches( mainRegExp.employeeGroup.nameEn );
        };

        if( mainHelper.isExists(req.body.nameAr) ){
            editObject.nameAr = req.body.nameAr;
            req.checkBody('nameAr' , mainErrorEnum.nameAr_invalid ).matches( mainRegExp.employeeGroup.nameAr );
        };
    
        if( mainHelper.isExists(req.body.screens) ){
            editObject.screens = req.body.screens;
            (req as any).checkBody('screens' , mainErrorEnum.screens_invalid ).checkScreens()
        };
        
        if( mainHelper.isExists(req.body.branches) ){
            editObject.branches = req.body.branches;
            (req as any).checkBody('branches' , mainErrorEnum.branches_invalid ).checkBranches()
        };
     
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( Object.keys( editObject ).length === 0 ) {
            return expressHelper.sendCustomError( 
                mainErrorEnum.edit_date_not_found , 
                next
            );
        };

        EmployeeGroupModel.updateOne({ 
            _id : req.body.employeeGroup_id
        } , {
            ...editObject
        })
        .exec()
        .then( async (result: any) => {

            if( mainHelper.isUpdateFail(result) ) {
                
                return expressHelper.sendCustomError( 
                    mainErrorEnum.employeeGroup_not_found , 
                    next
                );

            } else {

                return res.status(200).end();

            };

        })
        .catch( _error => {

            if ( mainHelper.isUniqueError(_error) ) {
                
                if( _error.message.indexOf('nameEn') !== -1 ) {
                    return expressHelper.sendCustomError( 
                        mainErrorEnum.employeeGroup_nameEn_used_before , 
                        next
                    );
                };

                if( _error.message.indexOf('nameAr') !== -1 ) {
                    return expressHelper.sendCustomError( 
                        mainErrorEnum.employeeGroup_nameAr_used_before , 
                        next
                    );
                };

            } else {

                return expressHelper.sendCustomError(mainErrorEnum.datebase , next );

            };

        });
    
    };
    public deleteEmployeeGroup = async (req: Request , res: Response , next: NextFunction) => {

        req.checkQuery('employeeGroup_id' , mainErrorEnum.employeeGroup_id_required ).exists()
        req.checkQuery('employeeGroup_id' , mainErrorEnum.employeeGroup_id_invalid ).matches( mainRegExp.employeeGroup._id )

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        EmployeeGroupModel.updateOne({ 
            _id : req.query.employeeGroup_id,
            deleted : 0
        } , {
            deleted : 1
        })
        .exec()
        .then( async ( result: any ) => {

            if( mainHelper.isUpdateFail(result) ) {

                return expressHelper.sendCustomError( 
                    mainErrorEnum.employeeGroup_not_found , 
                    next
                );
            
            } else {

                return res.status(200).end();

            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase  , next ) );
    
    };
    public restoreEmployeeGroup = async (req: Request , res: Response , next: NextFunction) => {

        req.checkBody('employeeGroup_id' , mainErrorEnum.employeeGroup_id_required ).exists()
        req.checkBody('employeeGroup_id' , mainErrorEnum.employeeGroup_id_invalid ).matches( mainRegExp.employeeGroup._id )

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        EmployeeGroupModel.updateOne({ 
            _id : req.body.employeeGroup_id,
            deleted : 1
        } , {
            deleted : 0
        })
        .exec()
        .then( async ( result: any ) => {

            if( mainHelper.isUpdateFail(result) ) {
                
                return expressHelper.sendCustomError( 
                    mainErrorEnum.employeeGroup_not_found , 
                    next
                ); 

            } else {

                return res.status(200).end();
           
            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase  , next ) );
    
    };
    public viewEmployeeGroup = async (req: Request , res: Response , next: NextFunction) => {

        req.checkQuery('employeeGroup_id' , mainErrorEnum.employeeGroup_id_required ).exists()
        req.checkQuery('employeeGroup_id' , mainErrorEnum.employeeGroup_id_invalid ).matches( mainRegExp.employeeGroup._id )

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        EmployeeGroupModel.findOne({
            _id : req.query.employeeGroup_id,
        })
        .populate({
            path : 'branches',
            select : 'nameEn nameAr'
        })
        .exec()    
        .then( async (employeeGroup: IEmployeeGroupModel) => {
           
            if( ! employeeGroup ) {
               
                return expressHelper.sendCustomError( 
                    mainErrorEnum.employeeGroup_not_found , 
                    next
                );

            } else {

                return res.status(200).json({ employeeGroup });

            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next));

    };
    public viewEmployeeGroups = async (req: Request , res: Response , next: NextFunction) => {
        
        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid  ).matches( mainRegExp.view.page );
        
        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid).matches( mainRegExp.view.limit );

        req.checkQuery('sortBy' , mainErrorEnum.sortBy_required ).exists();
        req.checkQuery('sortBy' , mainErrorEnum.sortBy_invalid  ).matches( mainRegExp.view.sortBy );
    
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        EmployeeGroupModel.find({})
        .sort(req.query.sortBy)
        .skip( (page - 1) * limit )
        .limit(limit)
        .populate({
            path : 'branches',
            select : 'nameEn nameAr'
        })
        .exec()    
        .then( async (employeeGroups: IEmployeeGroupModel[]) => {
            return res.status(200).json({ employeeGroups });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next));
    
    };
    public viewEmployeeGroupsSearch = async (req: Request , res: Response , next: NextFunction) => {
        

        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid  ).matches( mainRegExp.view.page );
        
        req.checkQuery('limit', mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit', mainErrorEnum.limit_invalid  ).matches( mainRegExp.view.limit );

        req.checkQuery('searchQuery', mainErrorEnum.searchQuery_required ).exists();
        req.checkQuery('searchQuery', mainErrorEnum.searchQuery_invalid  ).matches( mainRegExp.view.searchQuery );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        EmployeeGroupModel.find({
            $text: { $search: req.query.searchQuery },
        })
        .sort('-createdAt')
        .skip((page - 1) * limit)
        .limit(limit)
        .populate({
            path : 'branches',
            select : 'nameEn nameAr'
        })
        .exec()    
        .then( async (employeeGroups: IEmployeeGroupModel[]) => {
            return res.status(200).json({ employeeGroups });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };


    // Employee 
    public addEmployee = async (req: Request , res: Response , next: NextFunction) => {

        req.checkBody('name' , mainErrorEnum.name_required ).exists();
        req.checkBody('name' , mainErrorEnum.name_invalid ).matches( mainRegExp.employee.name );

        req.checkBody('email' , mainErrorEnum.email_required ).exists();
        req.checkBody('email' , mainErrorEnum.email_invalid ).matches( mainRegExp.employee.email  );

        req.checkBody('password' , mainErrorEnum.password_required ).exists();
        req.checkBody('password' , mainErrorEnum.password_invalid ).matches( mainRegExp.employee.password );

        req.checkBody('employeeGroup_id' , mainErrorEnum.employeeGroup_id_required ).exists();
        req.checkBody('employeeGroup_id' , mainErrorEnum.employeeGroup_id_invalid ).matches( mainRegExp.employeeGroup._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        EmployeeModel.create({
            name : req.body.name,
            email : req.body.email,
            password : mainHelper.hashSync(req.body.password),
            employeeGroup : req.body.employeeGroup_id,
            roles : ['employee']
        })
        .then( async ( employee: IEmployeeModel ) => {
            
            res.status(200).json({ _id : employee._id });

        })
        .catch( _error => {

            if ( mainHelper.isUniqueError(_error) ) {
                
                return expressHelper.sendCustomError( 
                    mainErrorEnum.email_used_before , 
                    next
                );

            } else {

                return expressHelper.sendCustomError(mainErrorEnum.datebase , next)

            };

        });

        
    };
    public editEmployee = async (req: Request , res: Response , next: NextFunction) => {

        req.checkBody('employee_id' , mainErrorEnum.employee_id_required ).exists();
        req.checkBody('employee_id' , mainErrorEnum.employee_id_invalid  ).matches( mainRegExp.employee._id );

        let editObject: any = {};
    
        if( typeof req.body.name !== 'undefined' ) {
            editObject.name = req.body.name;
            req.checkBody('name' , mainErrorEnum.name_invalid ).matches( mainRegExp.employee.name );
        };
        
        if( typeof req.body.email    !== 'undefined' ) {
            editObject.email = req.body.email;
            req.checkBody('email' , mainErrorEnum.email_invalid ).matches( mainRegExp.employee.email );
        };

        if( typeof req.body.employeeGroup_id !== 'undefined' ) {
            editObject.employeeGroup = req.body.employeeGroup_id;
            req.checkBody('employeeGroup_id' , mainErrorEnum.employeeGroup_id_invalid ).matches( mainRegExp.employeeGroup._id );
        };
    
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if( Object.keys( editObject ).length === 0 ) {
            return expressHelper.sendCustomError( 
                mainErrorEnum.employee_not_found , 
                next
            );
        };

        EmployeeModel.updateOne({
            _id : req.body.employee_id 
        },{
            ...editObject 
        })
        .exec()
        .then( ( result: any) => {

            if( mainHelper.isUpdateFail(result) ) {
                
                return expressHelper.sendCustomError( 
                    mainErrorEnum.employee_not_found , 
                    next
                );

            } else {

                return res.status(200).end();

            };

        })
        .catch( _error => {
            
            if ( mainHelper.isUniqueError(_error) ) {

                return expressHelper.sendCustomError( 
                    mainErrorEnum.email_used_before , 
                    next
                );

            } else {

                return expressHelper.sendCustomError(mainErrorEnum.datebase , next)

            };

        });

    };
    public editEmployeePassword = async (req: Request , res: Response , next: NextFunction) => {

        req.checkBody('employee_id' , mainErrorEnum.employee_id_required ).exists();
        req.checkBody('employee_id' , mainErrorEnum.employee_id_invalid  ).matches( mainRegExp.employee._id );

        req.checkBody('password' ,  mainErrorEnum.password_required ).exists();
        req.checkBody('password' ,  mainErrorEnum.password_invalid  ).matches( mainRegExp.employee.password );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        EmployeeModel.updateOne({
            _id : req.body.employee_id 
        },{
            password : mainHelper.hashSync( req.body.password )
        })
        .exec()
        .then( ( result: any) => {

            if( mainHelper.isUpdateFail(result) ) {

                return expressHelper.sendCustomError( 
                    mainErrorEnum.employee_not_found , 
                    next
                );

            } else {

                return res.status(200).end();

            };

        })
        .catch( _error => {

            if ( mainHelper.isUniqueError(_error) ) {

                return expressHelper.sendCustomError( 
                    mainErrorEnum.email_used_before , 
                    next
                );

            } else {

                return expressHelper.sendCustomError(mainErrorEnum.datebase , next );

            };

        });

    };
    public deleteEmployee = async (req: Request , res: Response , next: NextFunction) => {

        req.checkQuery('employee_id' , mainErrorEnum.employee_id_required ).exists();
        req.checkQuery('employee_id' , mainErrorEnum.employee_id_invalid  ).matches( mainRegExp.employee._id );
    
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);
    
        EmployeeModel.updateOne({ 
            _id     : req.query.employee_id ,
            deleted : 0
        } , {
            deleted : 1
        })
        .exec()
        .then( async ( result: any ) => {

            if( mainHelper.isUpdateFail(result) ) {
                
                return expressHelper.sendCustomError( 
                    mainErrorEnum.employee_not_found , 
                    next
                );

            } else {

                return res.status(200).end();

            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase  , next ) );
        
    };
    public restoreEmployee = async (req: Request , res: Response , next: NextFunction) => {

        req.checkBody('employee_id' , mainErrorEnum.employee_id_required ).exists();
        req.checkBody('employee_id' ,  mainErrorEnum.employee_id_invalid ).matches( mainRegExp.employee._id );
    
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);
            
        EmployeeModel.updateOne({ 
            _id     : req.body.employee_id ,
            deleted : 1
        } , {
            deleted : 0
        })
        .exec()
        .then( async (result: any ) => {

            if( mainHelper.isUpdateFail(result) ) {

                return expressHelper.sendCustomError( 
                    mainErrorEnum.employee_not_found , 
                    next
                );

            } else {

                return res.status(200).end();

            };
            
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase  , next ) );
        
    };
    public viewEmployee = async (req: Request , res: Response , next: NextFunction) => {

        req.checkQuery('employee_id' , mainErrorEnum.employee_id_required ).exists();
        req.checkQuery('employee_id' , mainErrorEnum.employee_id_invalid ).matches( mainRegExp.employee._id );
        
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        EmployeeModel.findOne({
            _id : req.query.employee_id
        })
        .populate({
            path : 'employeeGroup',
        })
        .lean()
        .exec()
        .then( async (employee: IEmployeeModel) => {
            
            if(employee) {

                return res.status(200).json({ 
                    employee : await this.setIsOnlineInEmployee(employee)
                });
               
            } else {

                return expressHelper.sendCustomError( 
                    mainErrorEnum.employee_not_found , 
                    next
                );

            };
       
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase  , next ) );
    
    };
    public viewEmployees = async (req: Request , res: Response , next: NextFunction) => {
       
        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid  ).matches( mainRegExp.view.page );
        
        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid).matches( mainRegExp.view.limit );

        req.checkQuery('sortBy' , mainErrorEnum.sortBy_required ).exists();
        req.checkQuery('sortBy' , mainErrorEnum.sortBy_invalid  ).matches( mainRegExp.view.sortBy );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        EmployeeModel.find({})
        .sort( req.query.sortBy )
        .skip( ( page - 1 ) * limit )
        .limit( limit )
        .populate({
            path : 'employeeGroup',
            populate : {
                path : 'branches',
            }
        })
        .lean()
        .exec()
        .then( async (employees: IEmployeeModel[]) => {
            res.status(200).json({ 
                employees :  await this.setIsOnlineInEmployees(employees)
            });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase  , next ) );
    
    };
    public viewEmployeesSearch = async (req: Request , res: Response , next: NextFunction) => {

        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid  ).matches( mainRegExp.view.page );
        
        req.checkQuery('limit', mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit', mainErrorEnum.limit_invalid  ).matches( mainRegExp.view.limit );

        req.checkQuery('searchQuery', mainErrorEnum.searchQuery_required ).exists();
        req.checkQuery('searchQuery', mainErrorEnum.searchQuery_invalid  ).matches( mainRegExp.view.searchQuery );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        EmployeeModel.find({
            $text : { $search: req.query.searchQuery }
        })
        .sort( '-createdAt' )
        .skip( ( page - 1 ) * limit )
        .limit( limit )
        .populate({
            path : 'employeeGroup',
            populate : {
                path : 'branches',
            }
        })
        .lean()
        .exec()    
        .then( async ( employees: IEmployeeModel[] ) => {
            res.status(200).json({ 
                employees :  await this.setIsOnlineInEmployees(employees)
            });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase  , next ) );
    
    };
    public viewEmployeesByEmployeeGroupFilter  = async (req: Request , res: Response , next: NextFunction) => {
        
        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid  ).matches( mainRegExp.view.page );
        
        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid).matches( mainRegExp.view.limit );

        req.checkQuery('sortBy' , mainErrorEnum.sortBy_required ).exists();
        req.checkQuery('sortBy' , mainErrorEnum.sortBy_invalid  ).matches( mainRegExp.view.sortBy );

        req.checkQuery('employeeGroup_id' , mainErrorEnum.employeeGroup_id_required ).exists();
        req.checkQuery('employeeGroup_id' , mainErrorEnum.employeeGroup_id_invalid  ).matches( mainRegExp.employeeGroup._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        EmployeeModel.find({
            employeeGroup : req.query.employeeGroup_id
        })
        .sort(req.query.sortBy)
        .skip( ( page - 1 ) * limit )
        .limit( limit )
        .populate({
            path : 'employeeGroup',
            populate : {
                path : 'branches',
            }
        })
        .lean()
        .exec()    
        .then( async ( employees: IEmployeeModel[] ) => {
            res.status(200).json({ 
                employees :  await this.setIsOnlineInEmployees(employees)
            });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase  , next ) );

    };
    public viewEmployeeTimeStatisticsInYear = async (req: Request , res: Response , next: NextFunction) => {

        req.checkQuery( 'year' , mainErrorEnum.year_required ).exists();
        req.checkQuery( 'year' , mainErrorEnum.year_invalid  ).matches( mainRegExp.employeeTimer.year );

        req.checkQuery('employee_id' , mainErrorEnum.employee_id_required ).exists();
        req.checkQuery('employee_id' , mainErrorEnum.employee_id_invalid  ).matches( mainRegExp.employee._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const year: number  = parseInt( req.query.year ); 

        EmployeeTimerModel.find({
            employee_id : req.query.employee_id ,  
            date : {
                $gte : `${year}-01-01`,
                $lte : `${year}-12-31`,
            }
        })
        .select('-_id timeMs date')
        .exec()    
        .then( async (statistics: IEmployeeTimerModel[]) => {
            res.status(200).json({ statistics });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase  , next ) );
    
    };
    public viewEmployeeTimeStatisticsInMonth = async (req: Request , res: Response , next: NextFunction) => {

        // year 2018 2019 2020
        // month 1 - 12

        req.checkQuery('year' , mainErrorEnum.year_required ).exists();
        req.checkQuery('year' , mainErrorEnum.year_invalid  ).matches( mainRegExp.employeeTimer.year );

        req.checkQuery('month' , mainErrorEnum.month_required ).exists();
        req.checkQuery('month' , mainErrorEnum.month_invalid  ).matches( mainRegExp.employeeTimer.month );

        req.checkQuery('employee_id' , mainErrorEnum.employee_id_required ).exists();
        req.checkQuery('employee_id' , mainErrorEnum.employee_id_invalid  ).matches( mainRegExp.employee._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);


        const year: number = parseInt(req.query.year); 
        const month: number = parseInt(req.query.month);
        
        const _month: string = month < 10 ? ('0' + month) : ('' + month); // 01 09 10 12

        const daysCountOfMonth: number = employeeTimerHelper.getDaysCountOfMonth( year , month - 1 ); // 29 30 31

        EmployeeTimerModel.find({
            employee_id : req.query.employee_id,  
            date : {
                $gte : `${year}-${_month}-01`,
                $lte : `${year}-${_month}-${daysCountOfMonth}`,
            }
        })
        .select('-_id timeMs date')
        .exec()    
        .then( async (statistics: IEmployeeTimerModel[]) => {
            res.status(200).json({ statistics });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase  , next ) );
    
    };
    public viewEmployeeActions = async (req: Request , res: Response , next: NextFunction) => {

        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );
        
        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('sortBy' , mainErrorEnum.sortBy_required ).exists();
        req.checkQuery('sortBy' , mainErrorEnum.sortBy_invalid ).matches( mainRegExp.view.sortBy );

        req.checkQuery('employee_id' , mainErrorEnum.employee_id_required ).exists();
        req.checkQuery('employee_id' , mainErrorEnum.employee_id_invalid ).matches( mainRegExp.employee._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        EmployeeActionModel.find({
            employee_id : req.query.employee_id
        })
        .sort( req.query.sortBy )
        .skip( ( page - 1 ) * limit )
        .limit( limit )
        .populate('client car carGroup carType carModel booking branch')
        .exec()    
        .then( async (employeeActions: IEmployeeActionModel[]) => {
            res.status(200).json({ 
                employeeActions
            });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase  , next ) );
    
    };
    public viewEmployeeActionsByItemNameFilter = async (req: Request , res: Response , next: NextFunction) => {

        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );
        
        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('sortBy' , mainErrorEnum.sortBy_required ).exists();
        req.checkQuery('sortBy' , mainErrorEnum.sortBy_invalid ).matches( mainRegExp.view.sortBy );

        req.checkQuery('employee_id' , mainErrorEnum.employee_id_required ).exists();
        req.checkQuery('employee_id' , mainErrorEnum.employee_id_invalid ).matches( mainRegExp.employee._id );

        req.checkQuery('itemName' , mainErrorEnum.item_name_required ).exists();
        req.checkQuery('itemName' , mainErrorEnum.item_name_invalid ).matches( mainRegExp.employeeAction.itemName );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);
        const itemName = parseInt(req.query.itemName);

        EmployeeActionModel.find({
            employee_id : req.query.employee_id,
            itemName : itemName
        })
        .sort( req.query.sortBy )
        .skip( (page - 1) * limit )
        .limit(limit)
        .populate(EitemName[itemName])
        .exec()    
        .then( async (employeeActions: IEmployeeActionModel[]) => {
            res.status(200).json({ 
                employeeActions
            });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase  , next ) );
    
    };


    // options
    public viewAllEmployeeGroups = async (req: Request , res: Response , next: NextFunction) => {
        EmployeeGroupModel.find({})
        .select('_id nameEn nameAr')
        .sort('-createdAt')
        .exec()    
        .then( async ( employeeGroups: IEmployeeGroupModel[] ) => {
            res.status(200).json({ employeeGroups });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase  , next ) );
    };
    public viewAllBranches = async (req: Request , res: Response , next: NextFunction) => {
        BranchModel.find({})
        .select('_id nameEn nameAr')
        .sort('-createdAt')
        .exec()    
        .then( async ( branches: IBranchModel[] ) => {
            res.status(200).json({ branches })
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next));
    };


};

export const adminCTRL = new AdminCTRL();
