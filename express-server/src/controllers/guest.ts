import { mainHelper } from './../helpers/mainHelper';
import { IClientModel , EsignupFrom } from './../models/client';
import { jwt } from './../helpers/jwt';
import { GuestModel , IGuestModel} from '../models/guest';
import { CarGroupModel } from '../models/carGroup';
import { BookingModel, IBookingModel , EbookingType} from '../models/booking';
import { branchBookingCounter } from '../models/branch';
import { bookingHelper } from '../helpers/bookingHelper';
import { mainRegExp } from '../regExp/mainRegExp';
import { expressHelper } from '../helpers/expressHelper';
import { mainErrorEnum } from './../enums/error.enum';
import { Response, NextFunction, Request } from 'express';
import { BranchModel, IBranchModel } from '../models/branch';
import { ICarGroupModel } from '../models/carGroup';

import moment from 'moment-timezone';
import { ClientModel } from '../models/client';


class GuestCTRLHelper {
    constructor(){};

    private createGuest = (guest: IGuestModel): Promise<IGuestModel> => {
       return new Promise( (resolve , reject) => {
            GuestModel.create({
                name: guest.name,
                phone: guest.phone,
                iqama: guest.iqama,
                iqamaExpiryDate: guest.iqamaExpiryDate,
                drivingLicence: guest.drivingLicence,
                drivingLicenceExpiryDate: guest.drivingLicenceExpiryDate,
                birthday: guest.birthday,
            })
            .then( (_guest : IGuestModel) => {
                resolve(_guest);
            })
            .catch( _error => {
                reject(_error);
            });

       });
    };
    private getGuest = (guest: IGuestModel): Promise<IGuestModel> => {
        return new Promise( (resolve , reject) => {
            GuestModel.findOne({
                phone: guest.phone,
                iqama: guest.iqama,
            })
            .then( (_guest : IGuestModel) => {
                if(_guest) {
                    if(_guest.deleted) {
                        resolve(null);
                    } else {
                        resolve(_guest);
                    }
                } else {
                    reject();
                };
            })
            .catch( _error => {
                reject(_error);
            });
 
        });
    };
    public _getOrCreateGuest = (guest: IGuestModel): Promise<IGuestModel> => {
        return new Promise( (resolve , reject) => {
            this.getGuest(guest)
            .then( (_guest: IGuestModel) => {
                resolve(_guest);
            })
            .catch(() => {
                
                this.createGuest(guest)
                .then( (_guest: IGuestModel) => {
                    resolve(_guest);
                })
                .catch(_error => {
                    reject(_error);
                });

            });
        });
    };
    public updateAllGuestBookingsToClient = (guest_id: string , client_id: string) => {
        BookingModel.updateMany({
            guest : guest_id
        } , {
            guest: null,
            client : client_id
        })
        .exec()
        .then( () => {})
        .catch( () => {});
    };

};

class GuestCTRL extends GuestCTRLHelper {
    
    constructor(){ 
        super();
    };

    public signupGuestToClient = async (req: Request , res: Response , next: NextFunction) => {

        req.checkBody('name' , mainErrorEnum.name_required ).exists();
        req.checkBody('name' , mainErrorEnum.name_invalid ).matches( mainRegExp.client.name );

        req.checkBody('phone' , mainErrorEnum.phone_required ).exists();
        req.checkBody('phone' , mainErrorEnum.phone_invalid ).matches( mainRegExp.client.phone );

        req.checkBody('email' , mainErrorEnum.email_required ).exists();
        req.checkBody('email' , mainErrorEnum.email_invalid ).matches( mainRegExp.client.email );

        req.checkBody('iqama' , mainErrorEnum.iqama_required ).exists();
        req.checkBody('iqama' , mainErrorEnum.iqama_invalid ).matches( mainRegExp.client.iqama );

        req.checkBody('password' , mainErrorEnum.password_required ).exists();
        req.checkBody('password' , mainErrorEnum.password_invalid ).matches( mainRegExp.client.password );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        GuestModel.findOne({
            deleted : 0,
            iqama : req.body.iqama,
            phone : req.body.phone,
        })
        .exec()
        .then( (guest: IGuestModel) => {
            if(guest) {
               
                ClientModel.create({
                    name : req.body.name,
                    email : req.body.email,
                    phone : req.body.phone,
                    iqama : req.body.iqama,
                    password : mainHelper.hashSync(req.body.password ) ,
                    roles : ['client'],
                    signupFrom : EsignupFrom.booking_before_and_signup,
                })
                .then( async (client: IClientModel) => {

                    this.updateAllGuestBookingsToClient(guest._id , client._id);

                    const token = await jwt.create({
                        _id : client._id,
                        roles : client.roles
                    });

                    client.password = '';

                    return res.status(200).json({ client , token });

                })
                .catch( _error => {

                    if ( mainHelper.isUniqueError(_error)) {

                        if (_error.message.indexOf('email') !== -1) {
                            return expressHelper.sendCustomError(
                                mainErrorEnum.email_used_before,
                                next
                            );
                        } else if (_error.message.indexOf('phone') !== -1) {
                            return expressHelper.sendCustomError(
                                mainErrorEnum.phone_used_before,
                                next
                            );
                        } else if (_error.message.indexOf('iqama') !== -1) {
                            return expressHelper.sendCustomError(
                                mainErrorEnum.iqama_used_before,
                                next
                            );
                        };

                    } else {

                        return expressHelper.sendCustomError(mainErrorEnum.datebase , next );
                        
                    };

                });

            } else {
                expressHelper.sendCustomError(
                    mainErrorEnum.guest_not_found , 
                    next
                );
            }
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };


    public addBooking = async (req: Request , res: Response , next: NextFunction) => {

        req.checkBody('carGroup_id' , mainErrorEnum.carGroup_id_required ).exists();
        req.checkBody('carGroup_id' , mainErrorEnum.carGroup_id_invalid ).matches( mainRegExp.carGroup._id );

        req.checkBody('pickUpDate' , mainErrorEnum.pickUpDate_required ).exists();
        req.checkBody('pickUpDate' , mainErrorEnum.pickUpDate_invalid ).matches( mainRegExp.booking.pickUpDate );

        req.checkBody('returnDate' , mainErrorEnum.returnDate_required ).exists();
        req.checkBody('returnDate' , mainErrorEnum.returnDate_invalid ).matches( mainRegExp.booking.returnDate );

        req.checkBody('guest_id' , mainErrorEnum.guest_id_required ).exists();
        req.checkBody('guest_id' , mainErrorEnum.guest_id_invalid ).matches( mainRegExp.guest._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const isCanBookingCarGroupInDate: boolean = await bookingHelper.isCanBookingCarGroupInDate(
            req.body.carGroup_id,
            req.body.pickUpDate,
            req.body.returnDate,
        );
        if(isCanBookingCarGroupInDate) {
            
            const guest: IGuestModel = await GuestModel.findOne({
                _id : req.body.guest_id,
            })
            .select('iqama')
            .exec();

            const carGroup: ICarGroupModel = await CarGroupModel.findOne({
                _id : req.body.carGroup_id
            })
            .select('branch pricePerDay discountPerDay')
            .exec();

            const branch: IBranchModel = await BranchModel.findOne({
                _id : carGroup.branch
            })
            .select('bookingStartCode waitingHoursForConfirm')
            .exec();

            if(guest && carGroup && branch) {

                const bookingCode: string = `${branch.bookingStartCode}${await branchBookingCounter()}`;

                const daysCount: number = bookingHelper.getDaysCount(
                    req.body.pickUpDate , 
                    req.body.returnDate
                );
                const totalPrice: number = bookingHelper.getTotlaPrice(
                    carGroup.pricePerDay , 
                    carGroup.discountPerDay,
                    daysCount
                );

                const expiredAt: string = moment()
                .add(
                    branch.waitingHoursForConfirm,
                    'hour'
                )
                .toISOString();

                // save
                BookingModel.create({
                    client : null,
                    car : null,
                    onlinePayment : null,

                    guest : guest._id,

                    carGroup : req.body.carGroup_id,
                    returnDate : req.body.returnDate,
                    pickUpDate : req.body.pickUpDate,

                    pickUpBranch : branch._id,
                    returnBranch : branch._id,
                
                    bookingCode : bookingCode,

                    daysCount : daysCount,
                    pricePerDay : carGroup.pricePerDay,
                    discountPerDay : carGroup.discountPerDay,
                    total : totalPrice,

                    bookingType : EbookingType.request,

                    expiredAt : expiredAt,

                    iqama : guest.iqama

                }).then( (booking: IBookingModel) => {
                    return res.status(200).json({
                        booking
                    });
                })
                .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next));

            } else {

                return expressHelper.sendCustomError(
                    mainErrorEnum.check_your_data , 
                    next
                );
    
            };


        } else {

            return expressHelper.sendCustomError(
                mainErrorEnum.you_cant_booking_this_car_Group_in_this_booking_date , 
                next
            );

        };
    };

    public getOrCreateGuest = async (req: Request , res: Response , next: NextFunction) => {
       
        req.checkBody('name' , mainErrorEnum.name_required ).exists();
        req.checkBody('name' , mainErrorEnum.name_invalid ).matches( mainRegExp.guest.name );

        req.checkBody('phone' , mainErrorEnum.phone_required ).exists();
        req.checkBody('phone' , mainErrorEnum.phone_invalid ).matches( mainRegExp.guest.phone );

        req.checkBody('iqama' , mainErrorEnum.iqama_required ).exists();
        req.checkBody('iqama' , mainErrorEnum.iqama_invalid ).matches( mainRegExp.guest.iqama );

        req.checkBody('iqamaExpiryDate' , mainErrorEnum.iqamaExpiryDate_required ).exists();
        req.checkBody('iqamaExpiryDate' , mainErrorEnum.iqamaExpiryDate_invalid ).matches( mainRegExp.guest.iqamaExpiryDate );

        req.checkBody('drivingLicence' , mainErrorEnum.drivingLicence_required ).exists();
        req.checkBody('drivingLicence' , mainErrorEnum.drivingLicence_invalid ).matches( mainRegExp.guest.drivingLicence );

        req.checkBody('drivingLicenceExpiryDate' , mainErrorEnum.drivingLicenceExpiryDate_required ).exists();
        req.checkBody('drivingLicenceExpiryDate' , mainErrorEnum.drivingLicenceExpiryDate_invalid ).matches( mainRegExp.guest.drivingLicenceExpiryDate );

        req.checkBody('birthday' , mainErrorEnum.birthday_required ).exists();
        req.checkBody('birthday' , mainErrorEnum.birthday_invalid ).matches( mainRegExp.guest.birthday );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        this._getOrCreateGuest(req.body)
        .then( (guest: IGuestModel) => {
            if(guest) {
                res.status(200).json({ guest });
            } else {
                expressHelper.sendCustomError(
                    mainErrorEnum.guest_not_found , 
                    next
                ); 
            };
        })
        .catch( _error => {
            
            if ( mainHelper.isUniqueError(_error)) {

                if (_error.message.indexOf('iqama') !== -1) {
                    return expressHelper.sendCustomError(
                        mainErrorEnum.iqama_used_before,
                        next
                    );
                } else if (_error.message.indexOf('phone') !== -1) {
                    return expressHelper.sendCustomError(
                        mainErrorEnum.phone_used_before,
                        next
                    );
                };

            } else {
                return expressHelper.sendCustomError(mainErrorEnum.datebase , next );
            };

        });

    };

};

export const guestCTRL = new GuestCTRL();
