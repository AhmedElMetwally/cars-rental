import { MessageModel } from './../models/message';
import { carGroupHelper } from '../helpers/carGroupHelper';
import { CarGroupModel } from '../models/carGroup';
import { BookingModel, IBookingModel , EbookingType} from '../models/booking';
import { branchBookingCounter } from '../models/branch';
import { bookingHelper, IViewCarGroups } from '../helpers/bookingHelper';
import { imageHelper } from '../helpers/imagesHelper';
import { employeeCTRL } from './employee';
import { EsignupFrom, IClientModel } from '../models/client';
import { mainRegExp } from '../regExp/mainRegExp';
import { jwt } from '../helpers/jwt';
import { expressHelper } from '../helpers/expressHelper';
import { ClientModel } from '../models/client';
import { mainErrorEnum } from './../enums/error.enum';
import { Response, NextFunction, Request } from 'express';
import { ClientRequest } from '../types/express';
import { BranchModel, IBranchModel } from '../models/branch';
import { mainHelper } from '../helpers/mainHelper';

import moment from 'moment-timezone';
import { ICarGroupModel } from '../models/carGroup';


class ClientCTRLHelper {
    constructor(){};
};

class ClientCTRL extends ClientCTRLHelper {
    
    constructor(){ 
        super();
    };


    // Guard
    public guard = jwt.allaw_express({
        roles     : ['client'],
        dataField : 'client',
        header_name_of_token : 'token'
    });


    // Auth
    public signup = async (req: ClientRequest , res: Response , next: NextFunction) =>  {

        req.checkBody('name' , mainErrorEnum.name_required ).exists();
        req.checkBody('name' , mainErrorEnum.name_invalid ).matches( mainRegExp.client.name );

        req.checkBody('email' , mainErrorEnum.email_required ).exists();
        req.checkBody('email' , mainErrorEnum.email_invalid).matches( mainRegExp.client.email );

        req.checkBody('phone', mainErrorEnum.phone_required ).exists();
        req.checkBody('phone', mainErrorEnum.phone_invalid ).matches( mainRegExp.client.phone );

        req.checkBody('password' , mainErrorEnum.password_required ).exists();
        req.checkBody('password' , mainErrorEnum.password_invalid ).matches( mainRegExp.client.password );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        ClientModel.create({
            name : req.body.name,
            email : req.body.email,
            phone : req.body.phone,
            password : mainHelper.hashSync(req.body.password ) ,
            roles : ['client'],
            signupFrom : EsignupFrom.normal_signup,
        })
        .then( async (client: IClientModel) => {

            const token = await jwt.create({
                _id : client._id,
                roles : client.roles
            });

            client.password = '';

            return res.status(200).json({ client , token });

        })
        .catch( _error => {
            if ( mainHelper.isUniqueError(_error)) {

                if (_error.message.indexOf('email') !== -1) {
                    return expressHelper.sendCustomError(
                        mainErrorEnum.email_used_before,
                        next
                    );
                } else if (_error.message.indexOf('phone') !== -1) {
                    return expressHelper.sendCustomError(
                        mainErrorEnum.phone_used_before,
                        next
                    );
                } else if (_error.message.indexOf('iqama') !== -1) {
                    return expressHelper.sendCustomError(
                        mainErrorEnum.iqama_used_before,
                        next
                    );
                };

            } else {

                return expressHelper.sendCustomError(mainErrorEnum.datebase , next );
                
            };

        });

 
    };
    public signin = async (req: ClientRequest , res: Response , next: NextFunction) =>  {
        
        req.checkBody('emailOrPhone' , mainErrorEnum.emailOrPhone_required ).exists();
        
        req.checkBody('password' , mainErrorEnum.password_required ).exists();
        req.checkBody('password' , mainErrorEnum.password_invalid ).matches( mainRegExp.client.password );
        
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const query = mainRegExp.client.email.test(req.body.emailOrPhone ) 
        ? { email : req.body.emailOrPhone } 
        : { phone : req.body.emailOrPhone };

        ClientModel.findOne({
            ...query,
            deleted : 0
        })
        .populate('nationality')
        .lean()
        .exec()
        .then( async (client: IClientModel) => {
            
            if(client) {
               
                if(mainHelper.compareSync(req.body.password , client.password)) {
                
                    const token: string = await jwt.create({
                        _id : client._id,
                        roles: client.roles
                    });

                    client.password = '';

                    return res.status(200).json({ client , token });
                    
                } else {
                    
                    return expressHelper.sendCustomError(
                        mainErrorEnum.client_not_found,
                        next
                    );

                };

            } else {
                
                return expressHelper.sendCustomError(
                    mainErrorEnum.client_not_found,
                    next
                );

            };
            
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase  , next ) );

    };
    public refreshToken = async (req: ClientRequest , res: Response , next: NextFunction) =>  {
            
        ClientModel.findOne({
            _id : req.client._id,
            deleted : 0
        })
        .populate('nationality')
        .lean()
        .exec()
        .then( async (client: IClientModel) => {

            if(client) {

                const token: string = await jwt.create({
                    _id : client._id,
                    roles: client.roles
                });
                
                client.password = '';
                
                return res.status(200).json({ 
                    token , client
                });

            } else {

                return expressHelper.sendCustomError( 
                    mainErrorEnum.client_not_found , 
                    next
                );
            
            };

           

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };


    // This Client
    public viewThisClient = async (req: ClientRequest , res: Response , next: NextFunction) =>  {
        ClientModel.findOne({
            _id : req.client._id,
            deleted : 0,
        })
        .then( async (client: IClientModel) => {
            if(client) {
               
                return res.status(200).json({ client });

            } else {
                
                return expressHelper.sendCustomError(
                    mainErrorEnum.client_not_found,
                    next
                );

            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next ))
    };
    public edit = async (req: ClientRequest , res: Response , next: NextFunction) =>  { 
        
        const editObject: any = {};

        if ( mainHelper.isExists(req.body.name) ) {
            editObject.name = req.body.name;
            req.checkBody('name' , mainErrorEnum.name_invalid ).matches( mainRegExp.client.name );
        };

        if ( mainHelper.isExists(req.body.phone) ) {
            editObject.phone = req.body.phone;
            req.checkBody('phone' , mainErrorEnum.phone_invalid ).matches( mainRegExp.client.phone );
        };

        if ( mainHelper.isExists(req.body.email) ) {
            editObject.email = req.body.email;
            req.checkBody('email' , mainErrorEnum.email_invalid ).matches( mainRegExp.client.email );
        };

        if ( mainHelper.isExists(req.body.iqama) ) {
            editObject.iqama = req.body.iqama;
            req.checkBody('iqama' , mainErrorEnum.iqama_invalid ).matches( mainRegExp.client.iqama );
        };

        if ( mainHelper.isExists(req.body.iqamaExpiryDate) ) {
            editObject.iqamaExpiryDate = req.body.iqamaExpiryDate;
            req.checkBody('iqamaExpiryDate' , mainErrorEnum.iqamaExpiryDate_invalid ).matches( mainRegExp.client.iqamaExpiryDate );
        };

        if ( mainHelper.isExists(req.body.drivingLicence) ) {
            editObject.drivingLicence = req.body.drivingLicence;
            req.checkBody('drivingLicence' , mainErrorEnum.drivingLicence_invalid ).matches( mainRegExp.client.drivingLicence );
        };
    
        if ( mainHelper.isExists(req.body.drivingLicenceExpiryDate) ) {
            editObject.drivingLicenceExpiryDate = req.body.drivingLicenceExpiryDate;
            req.checkBody('drivingLicenceExpiryDate' , mainErrorEnum.drivingLicenceExpiryDate_invalid ).matches( mainRegExp.client.drivingLicenceExpiryDate );
        };

        if ( mainHelper.isExists(req.body.birthday) ) {
            editObject.birthday = req.body.birthday;
            req.checkBody('birthday' , mainErrorEnum.birthday_invalid ).matches( mainRegExp.client.birthday );
        };

        if ( mainHelper.isExists(req.body.address) ) {
            editObject.address = req.body.address;
            req.checkBody('address' , mainErrorEnum.address_invalid ).matches( mainRegExp.client.address );
        };

        if ( mainHelper.isExists(req.body.job) ) {
            editObject.job = req.body.job;
            req.checkBody('job' , mainErrorEnum.job_invalid ).matches( mainRegExp.client.job );
        };

        if ( mainHelper.isExists(req.body.nationality_id) ) {
            editObject.nationality = req.body.nationality_id;
            req.checkBody('nationality_id' , mainErrorEnum.nationality_id_invalid ).matches( mainRegExp.nationality._id );
        };

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        if (Object.keys(editObject).length === 0) {
            return expressHelper.sendCustomError( 
                mainErrorEnum.edit_date_not_found , 
                next 
            ); 
        };

        ClientModel.updateOne({
            _id : req.client._id,
            deleted : 0
        } , {
            ...editObject,
        })
        .exec()
        .then( async (result) => {

            if( mainHelper.isUpdateFail(result) ) {

                return expressHelper.sendCustomError( 
                    mainErrorEnum.client_not_found , 
                    next 
                ); 

            } else {

                return res.status(200).end();

            };

        })
        .catch( _error => {

            if ( mainHelper.isUniqueError(_error) ) {

                if ( _error.message.indexOf('email') !== -1 ) {
                    return expressHelper.sendCustomError(
                        mainErrorEnum.email_used_before,
                        next
                    );
                };

                if ( _error.message.indexOf('phone') !== -1 ) {
                    return expressHelper.sendCustomError(
                        mainErrorEnum.email_used_before,
                        next
                    );
                };

                if ( _error.message.indexOf('iqama') !== -1 ) {
                    return expressHelper.sendCustomError(
                        mainErrorEnum.iqama_used_before,
                        next
                    );
                };

            } else {

                return expressHelper.sendCustomError(mainErrorEnum.datebase , next );
                
            };

        });

    };
    public editPassword = async (req: ClientRequest , res: Response , next: NextFunction) =>  { 

        req.checkBody('password' , mainErrorEnum.password_required ).exists();
        req.checkBody('password' , mainErrorEnum.password_invalid ).matches( mainRegExp.client.password );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        ClientModel.updateOne({
            _id : req.client._id,
            deleted : 0
        } , {
            password : mainHelper.hashSync(req.body.password ),
        })
        .exec()
        .then( async (result: any) => {

            if( mainHelper.isUpdateFail(result) ) {
                
                return expressHelper.sendCustomError( 
                    mainErrorEnum.client_not_found ,
                    next
                );

            } else {

                return res.status(200).end();

            };
            
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next));

    };
    public addClientImage = async (req: ClientRequest , res: Response , next: NextFunction) => {

        if(req.files) {
    
            imageHelper.uploadImage(req.files.image)
            .then( src => {

                ClientModel.findOneAndUpdate( {
                    _id : req.client._id,
                } , { 
                    image : src
                })
                .select('image -_id')
                .exec()
                .then( async (client: IClientModel) => {
                    
                    if( ! client ) {
                        
                        return expressHelper.sendCustomError(
                            mainErrorEnum.client_not_found,
                            next
                        );

                    } else {

                        if( client.image ) imageHelper.deleteImage(client.image);

                        return res.status(200).json({ image : src });
        
                    };
                    
                })
                .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next));

            })
            .catch( () => {
                return expressHelper.sendCustomError(
                    mainErrorEnum.image_not_found,
                    next
                );
            });

        } else {
            
            return expressHelper.sendCustomError(
                mainErrorEnum.image_not_found,
                next
            );

        };

    };


    // booking
    public addBooking = async (req: ClientRequest , res: Response , next: NextFunction) => {

        req.checkBody('carGroup_id' , mainErrorEnum.carGroup_id_required ).exists();
        req.checkBody('carGroup_id' , mainErrorEnum.carGroup_id_invalid ).matches( mainRegExp.carGroup._id );

        req.checkBody('pickUpDate' , mainErrorEnum.pickUpDate_required ).exists();
        req.checkBody('pickUpDate' , mainErrorEnum.pickUpDate_invalid ).matches( mainRegExp.booking.pickUpDate );

        req.checkBody('returnDate' , mainErrorEnum.returnDate_required ).exists();
        req.checkBody('returnDate' , mainErrorEnum.returnDate_invalid ).matches( mainRegExp.booking.returnDate );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const isCanBookingCarGroupInDate: boolean = await bookingHelper.isCanBookingCarGroupInDate(
            req.body.carGroup_id,
            req.body.pickUpDate,
            req.body.returnDate,
        );
        if(isCanBookingCarGroupInDate) {
            
            const client: IClientModel = await ClientModel.findOne({
                _id : req.client._id,
                iqama : {$exists : true},
                iqamaExpiryDate : {$exists : true},
                drivingLicence : {$exists : true},
                drivingLicenceExpiryDate : {$exists : true},
                birthday : {$exists : true},
            })
            .select('iqama')
            .exec();

            const carGroup: ICarGroupModel = await CarGroupModel.findOne({
                _id : req.body.carGroup_id
            })
            .select('branch pricePerDay discountPerDay')
            .exec();

            const branch: IBranchModel = await BranchModel.findOne({
                _id : carGroup.branch
            })
            .select('bookingStartCode waitingHoursForConfirm')
            .exec();


            if(client && carGroup && branch) {

                const bookingCode: string = `${branch.bookingStartCode}${await branchBookingCounter()}`;

                const daysCount: number = bookingHelper.getDaysCount(
                    req.body.pickUpDate , 
                    req.body.returnDate
                );
                const totalPrice: number = bookingHelper.getTotlaPrice(
                    carGroup.pricePerDay , 
                    carGroup.discountPerDay,
                    daysCount
                );

                const expiredAt: string = moment()
                .add(
                    branch.waitingHoursForConfirm,
                    'hour'
                )
                .toISOString();

                // save
                BookingModel.create({
                    guest : null,
                    car : null,
                    onlinePayment : null,

                    client : req.client._id,

                    carGroup : req.body.carGroup_id,
                    returnDate : req.body.returnDate,
                    pickUpDate : req.body.pickUpDate,

                    pickUpBranch : branch._id,
                    returnBranch : branch._id,
                
                    bookingCode : bookingCode,

                    daysCount : daysCount,
                    pricePerDay : carGroup.pricePerDay,
                    discountPerDay : carGroup.discountPerDay,
                    total : totalPrice,

                    bookingType : EbookingType.request,

                    expiredAt : expiredAt,

                    iqama : client.iqama

                }).then( (booking: IBookingModel) => {
                    return res.status(200).json({
                        booking
                    });
                })
                .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next));

            } else {

                return expressHelper.sendCustomError(
                    mainErrorEnum.check_your_data , 
                    next
                );
    
            };


        } else {

            return expressHelper.sendCustomError(
                mainErrorEnum.you_cant_booking_this_car_Group_in_this_booking_date , 
                next
            );

        };
    };
    public viewThisClientBooking = async (req: ClientRequest , res: Response , next: NextFunction) => {

        req.checkQuery('booking_id' , mainErrorEnum.booking_id_required ).exists();
        req.checkQuery('booking_id' , mainErrorEnum.booking_id_invalid ).matches( mainRegExp.booking._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        BookingModel.findOne({
            _id : req.query.booking_id,
            client : req.client._id,
            deleted : 0
        })
        .populate({
            path : 'returnBranch',
            select : 'nameEn nameAr'
        })
        .populate({
            path : 'pickUpBranch',
            select : 'nameEn nameAr'
        })
        .populate({
            path : 'car',
            select : 'plateNumber'
        })
        .populate({
            path : 'carGroup',
            select : 'carModel',
            populate : {
                path : 'carModel',
                select : 'nameEn nameAr',
            }
        })
        .populate({
            path : 'carGroup',
            populate : {
                path : 'carType',
                select : 'nameEn nameAr',
            }
        })
        .lean()
        .exec()
        .then( async (booking: IBookingModel) => {
                  
            if(booking) {

                return res.status(200).json({ 
                    booking : await bookingHelper.setBookingType(booking) 
                });

            } else {

                return expressHelper.sendCustomError(
                    mainErrorEnum.booking_not_found,
                    next 
                );
                
            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewThisClientBookings = async (req: ClientRequest , res: Response , next: NextFunction) => {
       
        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('sortBy' , mainErrorEnum.sortBy_required ).exists();
        req.checkQuery('sortBy' , mainErrorEnum.sortBy_invalid ).matches( mainRegExp.view.sortBy );
    
        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        BookingModel.find({
            client : req.client._id,
            deleted : 0
        })
        .sort(req.query.sortBy)
        .skip( (page - 1) * limit)
        .limit(limit)
        .populate({
            path : 'returnBranch',
            select : 'nameEn nameAr'
        })
        .populate({
            path : 'pickUpBranch',
            select : 'nameEn nameAr'
        })
        .populate({
            path : 'car',
            select : 'plateNumber'
        })
        .populate({
            path : 'carGroup',
            select : 'carModel',
            populate : {
                path : 'carModel',
                select : 'nameEn nameAr',
            }
        })
        .populate({
            path : 'carGroup',
            populate : {
                path : 'carType',
                select : 'nameEn nameAr',
            }
        })
        .lean()
        .exec()    
        .then( async (bookings: IBookingModel[]) => {
            res.status(200).json({ 
                bookings : bookingHelper.setBookingsType(bookings)
            });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
    };
    public viewThisClientBookingsSearch = async (req: ClientRequest , res: Response , next: NextFunction) => {

        req.checkQuery('page' , mainErrorEnum.page_required ).exists();
        req.checkQuery('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkQuery('limit' , mainErrorEnum.limit_required ).exists();
        req.checkQuery('limit' , mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkQuery('searchQuery', mainErrorEnum.searchQuery_required ).exists();
        req.checkQuery('searchQuery', mainErrorEnum.searchQuery_invalid ).matches( mainRegExp.view.searchQuery );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        const limit = parseInt(req.query.limit); 
        const page = parseInt(req.query.page);

        BookingModel.find({
            $text: { $search: req.query.searchQuery },
            client : req.client._id
        })
        .sort('-createdAt')
        .skip( (page - 1) * limit )
        .limit(limit)
        .populate({
            path : 'returnBranch',
            select : 'nameEn nameAr'
        })
        .populate({
            path : 'pickUpBranch',
            select : 'nameEn nameAr'
        })
        .populate({
            path : 'car',
            select : 'plateNumber'
        })
        .populate({
            path : 'carGroup',
            select : 'carModel',
            populate : {
                path : 'carModel',
                select : 'nameEn nameAr',
            }
        })
        .populate({
            path : 'carGroup',
            populate : {
                path : 'carType',
                select : 'nameEn nameAr',
            }
        })
        .lean()
        .exec()    
        .then( async (bookings: IBookingModel[]) => {
            res.status(200).json({ 
                bookings : bookingHelper.setBookingsType(bookings)
            });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewBookingByCodeAndIqama = async (req: Request , res: Response , next: NextFunction) => {
        
        req.checkQuery('iqama' , mainErrorEnum.iqama_required ).exists();
        req.checkQuery('iqama' , mainErrorEnum.iqama_invalid ).matches( mainRegExp.booking.iqama );

        req.checkQuery('bookingCode' , mainErrorEnum.bookingCode_required ).exists();
        req.checkQuery('bookingCode' , mainErrorEnum.bookingCode_invalid ).matches( mainRegExp.booking.bookingCode );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        BookingModel.findOne({
            bookingCode : req.query.bookingCode,
            iqama : req.query.iqama,
            deleted : 0
        })
        .populate('guest onlinePayment')
        .populate({
            path : 'returnBranch',
            select : 'nameEn nameAr'
        })
        .populate({
            path : 'pickUpBranch',
            select : 'nameEn nameAr'
        })
        .populate({
            path : 'client',
            select : 'name'
        })
        .populate({
            path : 'guest',
            select : 'name'
        })
        .populate({
            path : 'car',
            select : 'plateNumber'
        })
        .populate({
            path : 'carGroup',
            select : 'branch',
            populate : {
                path : 'branch',
                select : 'nameEn nameAr',
            }
        })
        .populate({
            path : 'carGroup',
            select : 'carModel',
            populate : {
                path : 'carModel',
                select : 'nameEn nameAr',
            }
        })
        .populate({
            path : 'carGroup',
            populate : {
                path : 'carType',
                select : 'nameEn nameAr',
            }
        })
        .lean()
        .exec()
        .then( async (booking: IBookingModel) => {
                     
            if(booking) {

                return res.status(200).json({ 
                    booking : await bookingHelper.setBookingType(booking) 
                });

            } else {

                return expressHelper.sendCustomError(
                    mainErrorEnum.booking_not_found,
                    next 
                );
                
            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
    };

    
    // CarGroup
    public viewCarGroup = async (req: Request , res: Response , next: NextFunction) => {
       
        req.checkQuery('carGroup_id' , mainErrorEnum.carGroup_id_required ).exists();
        req.checkQuery('carGroup_id' , mainErrorEnum.carGroup_id_invalid ).matches( mainRegExp.carGroup._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        CarGroupModel.findOne({
            _id : req.query.carGroup_id,
            deleted : 0
        })
        .populate('carType carModel branch')
        .lean()
        .exec()  
        .then( async (carGroup: ICarGroupModel) => {
            
            if(carGroup) {

                await carGroupHelper.setViewInCarGroup(carGroup);

                res.status(200).json({ 
                    carGroup : await carGroupHelper.setCarsCountInCarGroup(carGroup) 
                });

            } else {

                return expressHelper.sendCustomError(
                    mainErrorEnum.carGroup_not_found,
                    next 
                );
                
            };

        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };
    public viewCarGroupsToBookingOne = async (req: Request , res: Response , next: NextFunction) => {

        const $event: IViewCarGroups = {
            page : req.body.page,
            limit : req.body.limit,
            pickUpDate : req.body.pickUpDate,
            returnDate : req.body.returnDate,
            filters : {}
        };

        req.checkBody('page' , mainErrorEnum.page_required ).exists();
        req.checkBody('page' , mainErrorEnum.page_invalid ).matches( mainRegExp.view.page );

        req.checkBody('limit' , mainErrorEnum.limit_required ).exists();
        req.checkBody('limit' , mainErrorEnum.limit_invalid ).matches( mainRegExp.view.limit );

        req.checkBody('pickUpDate' , mainErrorEnum.pickUpDate_required ).exists();
        req.checkBody('pickUpDate' , mainErrorEnum.pickUpDate_invalid ).matches( mainRegExp.booking.pickUpDate );

        req.checkBody('returnDate' , mainErrorEnum.returnDate_required ).exists();
        req.checkBody('returnDate' , mainErrorEnum.returnDate_invalid ).matches( mainRegExp.booking.returnDate );


        // isExists
        if( mainHelper.isExists(req.body.city_id) ) {
            req.checkBody('city_id' , mainErrorEnum.city_id_invalid ).matches( mainRegExp.city._id );
            $event.filters.branch = {
                $in : await bookingHelper.getBranches_idsByCity_id(req.body.city_id)
            };
        };

        if( mainHelper.isExists(req.body.filters.branch_id) ) {
            req.checkBody('filters.branch_id' , mainErrorEnum.branch_id_invalid ).matches( mainRegExp.branch._id );
            $event.filters.branch = req.body.filters.branch_id;
        };

        // isExists
        if( mainHelper.isExists(req.body.filters.carModel_id) ) {
            req.checkBody('filters.carModel_id' , mainErrorEnum.carModel_id_invalid ).matches( mainRegExp.carModel._id );
            $event.filters.carModel = req.body.filters.carModel_id
        };

        // isExists
        if( mainHelper.isExists(req.body.filters.carType_id) ) {
            req.checkBody('filters.carType_id' , mainErrorEnum.carType_id_invalid ).matches( mainRegExp.carType._id );
            $event.filters.carType = req.body.filters.carType_id;
        };

        // isExists
        if( mainHelper.isExists(req.body.filters.year) ) {
            req.checkBody('filters.year' , mainErrorEnum.year_invalid ).matches( mainRegExp.carGroup.year );
            $event.filters.year = req.body.filters.year
        };

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);


        bookingHelper.viewCarGroupsToBookingOne($event)
        .then( async (carGroups: ICarGroupModel[]) => {
            await carGroupHelper.setViewInCarGroups(carGroups);
            res.status(200).json({ 
                carGroups : await carGroupHelper.setCarsCountInCarGroups(carGroups) 
            });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };



    // Branch
    public viewAllBranches = async (req: Request , res: Response , next: NextFunction) => {
        BranchModel.find({
            deleted : 0
        })
        .sort('-createdAt')
        .populate('city')
        .exec()    
        .then( async (branches: IBranchModel[]) => {
            res.status(200).json({ branches });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
    };
    public viewAllBranchesLocation = async (req: Request , res: Response , next: NextFunction) => {
        BranchModel.find({
            deleted : 0,
            location : {
                $exists: true
            }
        })
        .sort('-createdAt')
        .select('location city nameEn nameAr')
        .populate('city')
        .exec()    
        .then( async (branches: IBranchModel[]) => {
            res.status(200).json({ branches });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
    };
    public viewBranchesNearLocation = async (req: Request , res: Response , next: NextFunction) => {

        req.checkBody('location' , mainErrorEnum.location_required ).exists();
        (req as any).checkBody('location' , mainErrorEnum.location_invalid ).isLocation()

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next)

        BranchModel.aggregate([
            { "$geoNear" : {
                "near" : { "type" : "Point", "coordinates" : req.body.location },
                "distanceField" : "dist",
                "maxDistance" : 2,
                "spherical" : true
            } },
            { "$sort" : { "dist" : 1, "_id" : 1 } }
        ])
        .exec()
        .then( async (branches: IBranchModel[]) => {
            res.status(200).json({ branches });
        })
        .catch( () => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    };

    // Message
    public sendMessage = async (req: Request , res: Response , next: NextFunction) => {
       
        req.checkBody('name' , mainErrorEnum.name_required ).exists();
        req.checkBody('name' , mainErrorEnum.name_invalid ).matches( mainRegExp.message.name );

        req.checkBody('email' , mainErrorEnum.email_required ).exists();
        req.checkBody('email' , mainErrorEnum.email_invalid).matches( mainRegExp.message.email );

        req.checkBody('message' , mainErrorEnum.message_required ).exists();
        req.checkBody('message' , mainErrorEnum.message_invalid).matches( mainRegExp.message.message );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        MessageModel.create({
            name : req.body.name,      
            email : req.body.email,      
            message : req.body.message, 
        })
        .then( () => {
            res.status(200).end();
        })
        .catch( () => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );

    }    

    // options
    public viewAllNationalities = async (req: Request , res: Response , next: NextFunction) => {
        employeeCTRL.viewAllNationalities(req , res , next);
    };
    public viewAllCarTypes = async (req: Request , res: Response , next: NextFunction) => {
        employeeCTRL.viewAllCarTypes(req , res , next);
    };
    public viewAllCarModelsByCarType = async (req: Request , res: Response , next: NextFunction) => {
        employeeCTRL.viewAllCarModelsByCarType(req , res , next);
    };
    public viewAllCities = async (req: Request , res: Response , next: NextFunction) => {
        employeeCTRL.viewAllCities(req , res , next);
    };
    public viewAllBranchesByCityFilter = async (req: Request , res: Response , next: NextFunction) => {
        
        req.checkQuery('city_id' , mainErrorEnum.city_id_required ).exists();
        req.checkQuery('city_id' , mainErrorEnum.city_id_invalid ).matches( mainRegExp.city._id );

        const errors: any = await req.validationErrors();
        if (errors) return expressHelper.sendCustomError(errors[0] , next);

        BranchModel.find({
            deleted : 0,
            city : req.query.city_id
        })
        .sort('-createdAt')
        .exec()    
        .then( async (branches: IBranchModel[]) => {
            res.status(200).json({ branches });
        })
        .catch( _error => expressHelper.sendCustomError(mainErrorEnum.datebase , next) );
    };

    
};

export const clientCTRL = new ClientCTRL();



