"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const carGroupHelper_1 = require("../helpers/carGroupHelper");
const city_1 = require("../models/city");
const mainHelper_1 = require("../helpers/mainHelper");
const bookingHelper_1 = require("../helpers/bookingHelper");
const imagesHelper_1 = require("../helpers/imagesHelper");
const expressHelper_1 = require("../helpers/expressHelper");
const guest_1 = require("../models/guest");
const admin_1 = require("./admin");
const nationality_1 = require("../models/nationality");
const carGroup_1 = require("../models/carGroup");
const booking_1 = require("../models/booking");
const employeeAction_1 = require("../models/employeeAction");
const error_enum_1 = require("./../enums/error.enum");
const mainRegExp_1 = require("../regExp/mainRegExp");
const jwt_1 = require("../helpers/jwt");
const client_1 = require("../models/client");
const branch_1 = require("../models/branch");
const car_1 = require("../models/car");
const employee_1 = require("../models/employee");
const carType_1 = require("../models/carType");
const booking_2 = require("../models/booking");
const onlinePayment_1 = require("../models/onlinePayment");
const carModel_1 = require("../models/carModel");
const request_1 = __importDefault(require("request"));
class EmployeeCTRLHelper {
    constructor() {
        // use in EmployeeCTRL
        this.isHaveAccessToThisCarGroup = (carGroup_id, branches_ids) => {
            return new Promise(resolve => {
                carGroup_1.CarGroupModel.findOne({
                    _id: carGroup_id
                })
                    .select('branch')
                    .exec()
                    .then((carGroup) => {
                    resolve(this.checkIfObjectidInObjectids(carGroup.branch, branches_ids));
                })
                    .catch(() => {
                    resolve(false);
                });
            });
        };
        this.isHaveAccessToThisCar = (car_id, branches_ids) => {
            return new Promise(resolve => {
                car_1.CarModel.findOne({
                    _id: car_id
                })
                    .select('carGroup')
                    .populate({
                    path: 'carGroup',
                    select: 'branch'
                })
                    .exec()
                    .then((car) => {
                    resolve(this.checkIfObjectidInObjectids(car.carGroup.branch, branches_ids));
                })
                    .catch(() => {
                    resolve(false);
                });
            });
        };
        this.isHaveAccessToThisBranch = (branch_id, branches_ids) => {
            return this.checkIfObjectidInObjectids(branch_id, branches_ids);
        };
        this.isHaveAccessToThisBooking = (booking_id, branches_ids) => {
            return new Promise(resolve => {
                booking_2.BookingModel.findOne({
                    _id: booking_id,
                    pickUpBranch: {
                        $in: branches_ids
                    },
                    returnBranch: {
                        $in: branches_ids
                    }
                })
                    .exec()
                    .then((booking) => {
                    resolve(!!booking);
                })
                    .catch(() => {
                    resolve(false);
                });
            });
        };
        this.checkIfObjectidInObjectids = (_id, _ids) => {
            _id = _id.toString();
            for (let i = 0, len = _ids.length; i < len; ++i) {
                if (_ids[i].toString() === _id) {
                    return true;
                }
                ;
            }
            ;
        };
        this.addEmployeeAction = (req, target, target_id) => {
            const employee_id = req.employee._id;
            const screen = req.screen;
            const employeeAction = {
                client: null,
                car: null,
                carGroup: null,
                carType: null,
                carModel: null,
                booking: null,
                branch: null,
                employee_id,
                screen,
                itemName: null,
                query: JSON.stringify(req.query),
                body: JSON.stringify(req.body),
            };
            const obj = {};
            obj[target] = target_id;
            obj.itemName = employeeAction_1.EitemName[target];
            employeeAction_1.EmployeeActionModel.create(Object.assign({}, employeeAction, obj)).catch(() => { });
        };
        this.getCarGroups_idsByBranches_ids = (branches_ids) => {
            return new Promise(resolve => {
                carGroup_1.CarGroupModel.find({
                    branch: {
                        $in: branches_ids
                    }
                })
                    .exec()
                    .then((carGroups) => {
                    resolve(carGroups.map(v => v._id));
                })
                    .catch((_error) => {
                    resolve([]);
                });
            });
        };
        this.isCanEditCarGroupByCar = (car_id) => {
            return new Promise(resolve => {
                booking_2.BookingModel.countDocuments({
                    car: car_id,
                    pickUpDate: {
                        $lt: new Date()
                    },
                    returnDate: {
                        $gt: new Date()
                    },
                    bookingType: booking_1.EbookingType.active,
                    deleted: 0
                }).exec()
                    .then(count => {
                    resolve(!count);
                })
                    .catch(() => {
                    resolve(false);
                });
            });
        };
    }
    ;
    // use in router
    checkScreen(screen) {
        return (req, _res, next) => {
            employee_1.EmployeeModel.findOne({
                _id: req.employee._id,
            })
                .select('-_id employeeGroup')
                .populate('employeeGroup')
                .exec()
                .then((employee) => {
                if (!employee || employee.deleted) {
                    expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employee_not_found, next);
                }
                ;
                if (!employee.employeeGroup || employee.employeeGroup.deleted) {
                    expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employeeGroup_not_found, next);
                }
                ;
                if (screen === null || employee.employeeGroup.screens.indexOf(screen) !== -1) {
                    const employee_id = req.employee._id;
                    const branches = employee.employeeGroup.branches;
                    req.employee = {
                        _id: employee_id,
                        branches_ids: branches,
                        employeeGroup_id: employee.employeeGroup._id,
                    };
                    req.screen = screen;
                    next();
                }
                else {
                    expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
                }
                ;
            })
                .catch(() => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
    }
    ;
}
;
class EmployeeCTRL extends EmployeeCTRLHelper {
    constructor() {
        super();
        // Guard
        this.guard = jwt_1.jwt.allaw_express({
            roles: ['employee'],
            dataField: 'employee',
            header_name_of_token: 'token'
        });
        // Auth
        this.signin = async (req, res, next) => {
            req.checkBody('email', error_enum_1.mainErrorEnum.email_required).exists();
            req.checkBody('email', error_enum_1.mainErrorEnum.email_invalid).matches(mainRegExp_1.mainRegExp.employee.email);
            req.checkBody('password', error_enum_1.mainErrorEnum.password_required).exists();
            req.checkBody('password', error_enum_1.mainErrorEnum.password_invalid).matches(mainRegExp_1.mainRegExp.employee.password);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            employee_1.EmployeeModel.findOne({
                email: req.body.email,
                deleted: 0
            })
                .populate('employeeGroup')
                .exec()
                .then(async (employee) => {
                if (employee) {
                    if (!mainHelper_1.mainHelper.compareSync(req.body.password, employee.password) || !employee) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employee_not_found, next);
                    }
                    ;
                    if (!employee.employeeGroup || employee.employeeGroup.deleted) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employeeGroup_not_found, next);
                    }
                    ;
                    const token = await jwt_1.jwt.create({
                        _id: employee._id,
                        roles: employee.roles
                    });
                    employee.password = '';
                    return res.status(200).json({ employee, token });
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employee_not_found, next);
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.refreshToken = async (req, res, next) => {
            employee_1.EmployeeModel.findOne({
                _id: req.employee._id,
                deleted: 0
            })
                .populate('employeeGroup')
                .exec()
                .then(async (employee) => {
                if (employee) {
                    if (!employee.employeeGroup || employee.employeeGroup.deleted) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employeeGroup_not_found, next);
                    }
                    ;
                    const token = await jwt_1.jwt.create({
                        _id: employee._id,
                        roles: employee.roles
                    });
                    return res.status(200).json({
                        token, employee
                    });
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employee_not_found, next);
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        // This Employee
        this.editThisEmployee = async (req, res, next) => {
            const editObject = {};
            if (mainHelper_1.mainHelper.isExists(req.body.name)) {
                editObject.name = req.body.name;
                req.checkBody('name', error_enum_1.mainErrorEnum.name_invalid).matches(mainRegExp_1.mainRegExp.employee.name);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.email)) {
                editObject.email = req.body.email;
                req.checkBody('email', error_enum_1.mainErrorEnum.email_invalid).matches(mainRegExp_1.mainRegExp.employee.email);
            }
            ;
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (Object.keys(editObject).length === 0) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.edit_date_not_found, next);
            }
            ;
            employee_1.EmployeeModel.updateOne({
                _id: req.employee._id,
            }, Object.assign({}, editObject))
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employee_not_found, next);
                }
                else {
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.email_used_before, next);
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(_error, next);
                }
                ;
            });
        };
        this.editThisEmployeePassword = async (req, res, next) => {
            req.checkBody('password', error_enum_1.mainErrorEnum.password_required).exists();
            req.checkBody('password', error_enum_1.mainErrorEnum.password_invalid).matches(mainRegExp_1.mainRegExp.employee.password);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            employee_1.EmployeeModel.updateOne({
                _id: req.employee._id,
            }, {
                password: mainHelper_1.mainHelper.hashSync(req.body.password)
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employee_not_found, next);
                }
                else {
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewThisEmployee = async (req, res, next) => {
            employee_1.EmployeeModel.findById(req.employee._id)
                .populate('employeeGroup')
                .select('-password')
                .exec()
                .then(async (employee) => {
                res.status(200).json({ employee });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewThisEmployeeTimeStatisticsInYear = async (req, res, next) => {
            // use end point from AdminCTRL
            // adminCTRL.viewEmployeeTimeStatisticsInYear
            // import query { employee_id , year } 
            // set in query employee_id
            req.query.employee_id = req.employee._id;
            return admin_1.adminCTRL.viewEmployeeTimeStatisticsInYear(req, res, next);
        };
        this.viewThisEmployeeTimeStatisticsInMonth = async (req, res, next) => {
            // year 2018 2019 2020
            // month 1 - 12
            // use end point from AdminCTRL
            // adminCTRL.viewEmployeeTimeStatisticsInYear
            // import query { employee_id , year , month } 
            // set in query employee_id
            req.query.employee_id = req.employee._id;
            return admin_1.adminCTRL.viewEmployeeTimeStatisticsInMonth(req, res, next);
        };
        this.viewThisEmployeeActions = async (req, res, next) => {
            // use end point from AdminCTRL
            // adminCTRL.viewEmployeeTimeStatisticsInYear
            // import query { employee_id , page , page , sortBy } 
            // set in query employee_id
            req.query.employee_id = req.employee._id;
            return admin_1.adminCTRL.viewEmployeeActions(req, res, next);
        };
        this.viewEmployeeActionsByItemNameFilter = async (req, res, next) => {
            // use end point from AdminCTRL
            // adminCTRL.viewEmployeeTimeStatisticsInYear
            // import query { employee_id , page , page , sortBy } 
            // set in query employee_id
            req.query.employee_id = req.employee._id;
            return admin_1.adminCTRL.viewEmployeeActionsByItemNameFilter(req, res, next);
        };
        // Client
        this.editClient = async (req, res, next) => {
            req.checkBody('client_id', error_enum_1.mainErrorEnum.client_id_required).exists();
            req.checkBody('client_id', error_enum_1.mainErrorEnum.client_id_invalid).matches(mainRegExp_1.mainRegExp.client._id);
            const editObject = {};
            if (mainHelper_1.mainHelper.isExists(req.body.name)) {
                editObject.name = req.body.name;
                req.checkBody('name', error_enum_1.mainErrorEnum.name_invalid).matches(mainRegExp_1.mainRegExp.client.name);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.phone)) {
                editObject.phone = req.body.phone;
                req.checkBody('phone', error_enum_1.mainErrorEnum.phone_invalid).matches(mainRegExp_1.mainRegExp.client.phone);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.email)) {
                editObject.email = req.body.email;
                req.checkBody('email', error_enum_1.mainErrorEnum.email_invalid).matches(mainRegExp_1.mainRegExp.client.email);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.address)) {
                editObject.address = req.body.address;
                req.checkBody('address', error_enum_1.mainErrorEnum.address_invalid).matches(mainRegExp_1.mainRegExp.client.address);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.iqama)) {
                editObject.iqama = req.body.iqama;
                req.checkBody('iqama', error_enum_1.mainErrorEnum.iqama_invalid).matches(mainRegExp_1.mainRegExp.client.iqama);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.job)) {
                editObject.job = req.body.job;
                req.checkBody('job', error_enum_1.mainErrorEnum.job_invalid).matches(mainRegExp_1.mainRegExp.client.job);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.iqamaExpiryDate)) {
                editObject.iqamaExpiryDate = req.body.iqamaExpiryDate;
                req.checkBody('iqamaExpiryDate', error_enum_1.mainErrorEnum.iqamaExpiryDate_invalid).matches(mainRegExp_1.mainRegExp.client.iqamaExpiryDate);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.drivingLicence)) {
                editObject.drivingLicence = req.body.drivingLicence;
                req.checkBody('drivingLicence', error_enum_1.mainErrorEnum.drivingLicence_invalid).matches(mainRegExp_1.mainRegExp.client.drivingLicence);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.drivingLicenceExpiryDate)) {
                editObject.drivingLicenceExpiryDate = req.body.drivingLicenceExpiryDate;
                req.checkBody('drivingLicenceExpiryDate', error_enum_1.mainErrorEnum.drivingLicenceExpiryDate_invalid).matches(mainRegExp_1.mainRegExp.client.drivingLicenceExpiryDate);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.birthday)) {
                editObject.birthday = req.body.birthday;
                req.checkBody('birthday', error_enum_1.mainErrorEnum.birthday_invalid).matches(mainRegExp_1.mainRegExp.client.birthday);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.nationality_id)) {
                editObject.nationality = req.body.nationality_id;
                req.checkBody('nationality_id', error_enum_1.mainErrorEnum.nationality_id_invalid).matches(mainRegExp_1.mainRegExp.nationality._id);
            }
            ;
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (Object.keys(editObject).length === 0) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.client_not_found, next);
            }
            ;
            client_1.ClientModel.updateOne({
                _id: req.body.client_id,
            }, Object.assign({}, editObject))
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.client_not_found, next);
                }
                else {
                    this.addEmployeeAction(req, 'client', req.body.client_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    if (_error.message.indexOf('email') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.email_used_before, next);
                    }
                    ;
                    if (_error.message.indexOf('phone') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.phone_used_before, next);
                    }
                    ;
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next);
                }
                ;
            });
        };
        this.editClientPassword = async (req, res, next) => {
            req.checkBody('client_id', error_enum_1.mainErrorEnum.client_id_required).exists();
            req.checkBody('client_id', error_enum_1.mainErrorEnum.client_id_invalid).matches(mainRegExp_1.mainRegExp.client._id);
            req.checkBody('password', error_enum_1.mainErrorEnum.password_required).exists();
            req.checkBody('password', error_enum_1.mainErrorEnum.password_invalid).matches(mainRegExp_1.mainRegExp.client.password);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            client_1.ClientModel.updateOne({
                _id: req.body.client_id,
            }, {
                password: mainHelper_1.mainHelper.hashSync(req.body.password)
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.client_not_found, next);
                }
                else {
                    this.addEmployeeAction(req, 'client', req.body.client_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.deleteClient = async (req, res, next) => {
            req.checkQuery('client_id', error_enum_1.mainErrorEnum.client_id_required).exists();
            req.checkQuery('client_id', error_enum_1.mainErrorEnum.client_id_invalid).matches(mainRegExp_1.mainRegExp.client._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            client_1.ClientModel.updateOne({
                _id: req.query.client_id,
                deleted: 0
            }, {
                deleted: 1
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.client_not_found, next);
                }
                else {
                    this.addEmployeeAction(req, 'client', req.body.client_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.restoreClient = async (req, res, next) => {
            req.checkBody('client_id', error_enum_1.mainErrorEnum.client_id_required).exists();
            req.checkBody('client_id', error_enum_1.mainErrorEnum.client_id_invalid).matches(mainRegExp_1.mainRegExp.client._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            client_1.ClientModel.updateOne({
                _id: req.body.client_id,
                deleted: 1
            }, {
                deleted: 0
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.client_not_found, next);
                }
                else {
                    this.addEmployeeAction(req, 'client', req.body.client_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.addClientImage = async (req, res, next) => {
            req.checkBody('client_id', error_enum_1.mainErrorEnum.client_id_required).exists();
            req.checkBody('client_id', error_enum_1.mainErrorEnum.client_id_invalid).matches(mainRegExp_1.mainRegExp.client._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!req.files) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.image_not_found, next);
            }
            ;
            imagesHelper_1.imageHelper.uploadImage(req.files.image)
                .then(src => {
                client_1.ClientModel.findOneAndUpdate({
                    _id: req.body.client_id,
                }, {
                    image: src
                })
                    .select('image -_id')
                    .exec()
                    .then(async (client) => {
                    if (client) {
                        if (client.image)
                            imagesHelper_1.imageHelper.deleteImage(client.image);
                        this.addEmployeeAction(req, 'client', req.body.client_id);
                        return res.status(200).json({ image: src });
                    }
                    else {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.client_not_found, next);
                    }
                    ;
                })
                    .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
            })
                .catch(() => {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.image_not_found, next);
            });
        };
        this.viewClient = async (req, res, next) => {
            req.checkQuery('client_id', error_enum_1.mainErrorEnum.client_id_required).exists();
            req.checkQuery('client_id', error_enum_1.mainErrorEnum.client_id_invalid).matches(mainRegExp_1.mainRegExp.client._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            client_1.ClientModel.findOne({
                _id: req.query.client_id,
            })
                .populate('nationality')
                .select('-password')
                .exec()
                .then(async (client) => {
                if (client) {
                    return res.status(200).json({ client });
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.client_not_found, next);
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewBookingsForClient = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('client_id', error_enum_1.mainErrorEnum.client_id_required).exists();
            req.checkQuery('client_id', error_enum_1.mainErrorEnum.client_id_invalid).matches(mainRegExp_1.mainRegExp.client._id);
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_required).exists();
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_invalid).matches(mainRegExp_1.mainRegExp.view.sortBy);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            booking_2.BookingModel.find({
                carGroup: req.query.carGroup_id
            })
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .populate('car carGroup pickUpBranch returnBranch onlinePayment')
                .exec()
                .then((bookings) => res.status(200).json({ bookings }))
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewOnlinePaymentsForClient = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('client_id', error_enum_1.mainErrorEnum.client_id_required).exists();
            req.checkQuery('client_id', error_enum_1.mainErrorEnum.client_id_invalid).matches(mainRegExp_1.mainRegExp.client._id);
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_required).exists();
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_invalid).matches(mainRegExp_1.mainRegExp.view.sortBy);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            onlinePayment_1.OnlinePaymentModel.find({
                client: req.query.client_id
            })
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .populate('booking')
                .exec()
                .then((onlinePayments) => res.status(200).json({ onlinePayments }))
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewClients = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_required).exists();
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_invalid).matches(mainRegExp_1.mainRegExp.view.sortBy);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            client_1.ClientModel.find({})
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .populate('nationality')
                .select('-password')
                .exec()
                .then(async (clients) => {
                res.status(200).json({ clients });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewClientsSearch = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('searchQuery', error_enum_1.mainErrorEnum.searchQuery_required).exists();
            req.checkQuery('searchQuery', error_enum_1.mainErrorEnum.searchQuery_invalid).matches(mainRegExp_1.mainRegExp.view.searchQuery);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            client_1.ClientModel.find({
                $text: {
                    $search: req.query.searchQuery
                },
            })
                .sort('-createdAt')
                .skip((page - 1) * limit)
                .limit(limit)
                .select('-password')
                .populate('nationality')
                .exec()
                .then((clients) => {
                res.status(200).json({ clients });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        // Car
        this.addCar = async (req, res, next) => {
            req.checkBody('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_required).exists();
            req.checkBody('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_invalid).matches(mainRegExp_1.mainRegExp.carGroup._id);
            req.checkBody('plateNumber', error_enum_1.mainErrorEnum.plateNumber_required).exists();
            req.checkBody('plateNumber', error_enum_1.mainErrorEnum.plateNumber_invalid).matches(mainRegExp_1.mainRegExp.car.plateNumber);
            req.checkBody('gearType', error_enum_1.mainErrorEnum.gearType_required).exists();
            req.checkBody('gearType', error_enum_1.mainErrorEnum.gearType_invalid).matches(mainRegExp_1.mainRegExp.car.gearType);
            req.checkBody('status', error_enum_1.mainErrorEnum.status_required).exists();
            req.checkBody('status', error_enum_1.mainErrorEnum.status_invalid).matches(mainRegExp_1.mainRegExp.car.status);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisCarGroup(req.body.carGroup_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            car_1.CarModel.create({
                plateNumber: req.body.plateNumber,
                carGroup: req.body.carGroup_id,
                gearType: req.body.gearType,
                status: req.body.status,
            })
                .then(async (car) => {
                this.addEmployeeAction(req, 'car', car._id);
                res.status(200).json({ _id: car._id });
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.plateNumber_used_before, next);
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next);
                }
                ;
            });
        };
        this.editCar = async (req, res, next) => {
            const editObject = {};
            req.checkBody('car_id', error_enum_1.mainErrorEnum.car_id_required).exists();
            req.checkBody('car_id', error_enum_1.mainErrorEnum.car_id_invalid).matches(mainRegExp_1.mainRegExp.car._id);
            if (mainHelper_1.mainHelper.isExists(req.body.carGroup_id)) {
                editObject.carGroup = req.body.carGroup_id;
                req.checkBody('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_invalid).matches(mainRegExp_1.mainRegExp.carGroup._id);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.plateNumber)) {
                editObject.plateNumber = req.body.plateNumber;
                req.checkBody('plateNumber', error_enum_1.mainErrorEnum.plateNumber_invalid).matches(mainRegExp_1.mainRegExp.car.plateNumber);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.gearType)) {
                editObject.gearType = req.body.gearType;
                req.checkBody('gearType', error_enum_1.mainErrorEnum.gearType_invalid).matches(mainRegExp_1.mainRegExp.car.gearType);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.status)) {
                editObject.status = req.body.status;
                req.checkBody('status', error_enum_1.mainErrorEnum.status_invalid).matches(mainRegExp_1.mainRegExp.car.status);
            }
            ;
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisCar(req.body.car_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            // if i will edit car carGroup 
            // check if this car is booking in this this carGroup
            if (mainHelper_1.mainHelper.isExists(req.body.carGroup_id)) {
                if (!await this.isCanEditCarGroupByCar(req.body.car_id)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.this_car_is_booking_in_this_car_group, next);
                }
                ;
            }
            ;
            if (Object.keys(editObject).length === 0) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.edit_date_not_found, next);
            }
            ;
            car_1.CarModel.updateOne({
                _id: req.body.car_id,
            }, Object.assign({}, editObject))
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.car_not_found, next);
                }
                else {
                    // save action
                    this.addEmployeeAction(req, 'car', req.body.car_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.plateNumber_used_before, next);
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next);
                }
                ;
            });
        };
        this.deleteCar = async (req, res, next) => {
            req.checkQuery('car_id', error_enum_1.mainErrorEnum.car_id_required).exists();
            req.checkQuery('car_id', error_enum_1.mainErrorEnum.car_id_invalid).matches(mainRegExp_1.mainRegExp.car._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisCar(req.query.car_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            car_1.CarModel.updateOne({
                _id: req.query.car_id,
                deleted: 0
            }, {
                deleted: 1
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.car_not_found, next);
                }
                else {
                    this.addEmployeeAction(req, 'car', req.query.car_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.restoreCar = async (req, res, next) => {
            req.checkBody('car_id', error_enum_1.mainErrorEnum.car_id_required).exists();
            req.checkBody('car_id', error_enum_1.mainErrorEnum.car_id_invalid).matches(mainRegExp_1.mainRegExp.car._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisCar(req.body.car_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            car_1.CarModel.updateOne({
                _id: req.body.car_id,
                deleted: 1
            }, {
                deleted: 0
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.car_not_found, next);
                }
                else {
                    this.addEmployeeAction(req, 'car', req.body.car_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewCar = async (req, res, next) => {
            req.checkQuery('car_id', error_enum_1.mainErrorEnum.car_id_required).exists();
            req.checkQuery('car_id', error_enum_1.mainErrorEnum.car_id_invalid).matches(mainRegExp_1.mainRegExp.car._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisCar(req.query.car_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            car_1.CarModel.findOne({
                _id: req.query.car_id,
            })
                .populate({
                path: 'carGroup',
                select: 'carType',
                populate: {
                    path: 'carType',
                    select: 'nameEn nameAr'
                }
            })
                .populate({
                path: 'carGroup',
                select: 'carModel',
                populate: {
                    path: 'carModel',
                    select: 'nameEn nameAr'
                }
            })
                .populate({
                path: 'carGroup',
                select: 'branch year',
                populate: {
                    path: 'branch',
                    select: 'nameEn nameAr'
                }
            })
                .lean()
                .exec()
                .then(async (car) => {
                if (car) {
                    return res.status(200).json({ car });
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.car_not_found, next);
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewCars = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_required).exists();
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_invalid).matches(mainRegExp_1.mainRegExp.view.sortBy);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const carGroups_idsWhoCanAccess = await this.getCarGroups_idsByBranches_ids(req.employee.branches_ids);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            car_1.CarModel.find({
                carGroup: {
                    $in: carGroups_idsWhoCanAccess
                }
            })
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .populate({
                path: 'carGroup',
                select: 'carType',
                populate: {
                    path: 'carType',
                    select: 'nameEn nameAr'
                }
            })
                .populate({
                path: 'carGroup',
                select: 'carModel',
                populate: {
                    path: 'carModel',
                    select: 'nameEn nameAr'
                }
            })
                .populate({
                path: 'carGroup',
                select: 'branch',
                populate: {
                    path: 'branch',
                    select: 'nameEn nameAr'
                }
            })
                .lean()
                .exec()
                .then(async (cars) => {
                res.status(200).json({ cars: cars });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewCarsByFilter = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_required).exists();
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_invalid).matches(mainRegExp_1.mainRegExp.view.sortBy);
            req.checkQuery('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_required).exists();
            req.checkQuery('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_invalid).matches(mainRegExp_1.mainRegExp.carGroup._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisCarGroup(req.query.carGroup_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            car_1.CarModel.find({
                carGroup: req.query.carGroup_id
            })
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .populate({
                path: 'carGroup',
                select: 'carType',
                populate: {
                    path: 'carType',
                    select: 'nameEn nameAr'
                }
            })
                .populate({
                path: 'carGroup',
                select: 'carModel',
                populate: {
                    path: 'carModel',
                    select: 'nameEn nameAr'
                }
            })
                .populate({
                path: 'carGroup',
                select: 'branch',
                populate: {
                    path: 'branch',
                    select: 'nameEn nameAr'
                }
            })
                .lean()
                .exec()
                .then(async (cars) => {
                res.status(200).json({ cars: cars });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewCarsSearch = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('searchQuery', error_enum_1.mainErrorEnum.searchQuery_required).exists();
            req.checkQuery('searchQuery', error_enum_1.mainErrorEnum.searchQuery_invalid).matches(mainRegExp_1.mainRegExp.view.searchQuery);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const carGroups_idsWhoCanAccess = await this.getCarGroups_idsByBranches_ids(req.employee.branches_ids);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            car_1.CarModel.find({
                $text: { $search: req.query.searchQuery },
                carGroup: {
                    $in: carGroups_idsWhoCanAccess
                }
            })
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .populate({
                path: 'carGroup',
                select: 'carType',
                populate: {
                    path: 'carType',
                    select: 'nameEn nameAr'
                }
            })
                .populate({
                path: 'carGroup',
                select: 'carModel',
                populate: {
                    path: 'carModel',
                    select: 'nameEn nameAr'
                }
            })
                .populate({
                path: 'carGroup',
                select: 'branch',
                populate: {
                    path: 'branch',
                    select: 'nameEn nameAr'
                }
            })
                .lean()
                .exec()
                .then(async (cars) => {
                res.status(200).json({ cars: cars });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        // Branch
        this.addBranch = async (req, res, next) => {
            req.checkBody('nameEn', error_enum_1.mainErrorEnum.nameEn_required).exists();
            req.checkBody('nameEn', error_enum_1.mainErrorEnum.nameEn_invalid).matches(mainRegExp_1.mainRegExp.branch.nameEn);
            req.checkBody('nameAr', error_enum_1.mainErrorEnum.nameAr_required).exists();
            req.checkBody('nameAr', error_enum_1.mainErrorEnum.nameAr_invalid).matches(mainRegExp_1.mainRegExp.branch.nameAr);
            req.checkBody('addressEn', error_enum_1.mainErrorEnum.addressEn_required).exists();
            req.checkBody('addressEn', error_enum_1.mainErrorEnum.addressEn_invalid).matches(mainRegExp_1.mainRegExp.branch.addressEn);
            req.checkBody('addressAr', error_enum_1.mainErrorEnum.addressAr_required).exists();
            req.checkBody('addressAr', error_enum_1.mainErrorEnum.addressAr_invalid).matches(mainRegExp_1.mainRegExp.branch.addressAr);
            req.checkBody('overviewEn', error_enum_1.mainErrorEnum.overviewEn_required).exists();
            req.checkBody('overviewEn', error_enum_1.mainErrorEnum.overviewEn_invalid).matches(mainRegExp_1.mainRegExp.branch.overviewEn);
            req.checkBody('overviewAr', error_enum_1.mainErrorEnum.overviewAr_required).exists();
            req.checkBody('overviewAr', error_enum_1.mainErrorEnum.overviewAr_invalid).matches(mainRegExp_1.mainRegExp.branch.overviewAr);
            req.checkBody('bookingStartCode', error_enum_1.mainErrorEnum.bookingStartCode_required).exists();
            req.checkBody('bookingStartCode', error_enum_1.mainErrorEnum.bookingStartCode_invalid).matches(mainRegExp_1.mainRegExp.branch.bookingStartCode);
            req.checkBody('waitingHoursForConfirm', error_enum_1.mainErrorEnum.waitingHoursForConfirm_required).exists();
            req.checkBody('waitingHoursForConfirm', error_enum_1.mainErrorEnum.waitingHoursForConfirm_invalid).matches(mainRegExp_1.mainRegExp.branch.waitingHoursForConfirm);
            req.checkBody('phone', error_enum_1.mainErrorEnum.phone_required).exists();
            req.checkBody('phone', error_enum_1.mainErrorEnum.phone_invalid).matches(mainRegExp_1.mainRegExp.branch.phone);
            req.checkBody('city_id', error_enum_1.mainErrorEnum.city_id_required).exists();
            req.checkBody('city_id', error_enum_1.mainErrorEnum.city_id_invalid).matches(mainRegExp_1.mainRegExp.city._id);
            let location = null;
            if (mainHelper_1.mainHelper.isExists(req.body.location)) {
                location = req.body.location;
                req.checkBody('location', error_enum_1.mainErrorEnum.location_invalid).isLocation();
            }
            ;
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            branch_1.BranchModel.create(Object.assign({ nameEn: req.body.nameEn, nameAr: req.body.nameAr, addressEn: req.body.addressEn, addressAr: req.body.addressAr, overviewEn: req.body.overviewEn, overviewAr: req.body.overviewAr, bookingStartCode: req.body.bookingStartCode, waitingHoursForConfirm: req.body.waitingHoursForConfirm, phone: req.body.phone, city: req.body.city_id }, location ? { location } : {}, { images: [] }))
                .then(async (branch) => {
                this.addEmployeeAction(req, 'branch', branch._id);
                return res.status(200).end();
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.phone_used_before, next);
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(_error, next);
                }
                ;
            });
        };
        this.editBranch = async (req, res, next) => {
            req.checkBody('branch_id', error_enum_1.mainErrorEnum.branch_id_required).exists();
            req.checkBody('branch_id', error_enum_1.mainErrorEnum.branch_id_invalid).matches(mainRegExp_1.mainRegExp.branch._id);
            const editObject = {};
            if (mainHelper_1.mainHelper.isExists(req.body.nameEn)) {
                editObject.nameEn = req.body.nameEn;
                req.checkBody('nameEn', error_enum_1.mainErrorEnum.nameEn_invalid).matches(mainRegExp_1.mainRegExp.branch.nameEn);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.nameAr)) {
                editObject.nameAr = req.body.nameAr;
                req.checkBody('nameAr', error_enum_1.mainErrorEnum.nameAr_invalid).matches(mainRegExp_1.mainRegExp.branch.nameAr);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.addressEn)) {
                editObject.addressEn = req.body.addressEn;
                req.checkBody('addressEn', error_enum_1.mainErrorEnum.addressEn_invalid).matches(mainRegExp_1.mainRegExp.branch.addressEn);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.addressAr)) {
                editObject.addressAr = req.body.addressAr;
                req.checkBody('addressAr', error_enum_1.mainErrorEnum.addressAr_invalid).matches(mainRegExp_1.mainRegExp.branch.addressAr);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.overviewEn)) {
                editObject.overviewEn = req.body.overviewEn;
                req.checkBody('overviewEn', error_enum_1.mainErrorEnum.overviewEn_invalid).matches(mainRegExp_1.mainRegExp.branch.overviewEn);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.overviewAr)) {
                editObject.overviewAr = req.body.overviewAr;
                req.checkBody('overviewAr', error_enum_1.mainErrorEnum.overviewAr_invalid).matches(mainRegExp_1.mainRegExp.branch.overviewAr);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.bookingStartCode)) {
                editObject.bookingStartCode = req.body.bookingStartCode;
                req.checkBody('bookingStartCode', error_enum_1.mainErrorEnum.bookingStartCode_invalid).matches(mainRegExp_1.mainRegExp.branch.bookingStartCode);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.waitingHoursForConfirm)) {
                editObject.waitingHoursForConfirm = req.body.waitingHoursForConfirm;
                req.checkBody('waitingHoursForConfirm', error_enum_1.mainErrorEnum.waitingHoursForConfirm_invalid).matches(mainRegExp_1.mainRegExp.branch.waitingHoursForConfirm);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.phone)) {
                editObject.phone = req.body.phone;
                req.checkBody('phone', error_enum_1.mainErrorEnum.phone_invalid).matches(mainRegExp_1.mainRegExp.branch.phone);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.location)) {
                editObject.location = req.body.location;
                req.checkBody('location', error_enum_1.mainErrorEnum.location_invalid).isLocation();
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.city_id)) {
                editObject.city = req.body.city_id;
                req.checkBody('city_id', error_enum_1.mainErrorEnum.city_id_invalid).matches(mainRegExp_1.mainRegExp.city._id);
            }
            ;
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (Object.keys(editObject).length === 0) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.edit_date_not_found, next);
            }
            ;
            if (!this.isHaveAccessToThisBranch(req.body.branch_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            branch_1.BranchModel.updateOne({
                _id: req.body.branch_id,
            }, Object.assign({}, editObject))
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.branch_not_found, next);
                }
                else {
                    this.addEmployeeAction(req, 'branch', req.body.branch_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.phone_used_before, next);
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(_error, next);
                }
                ;
            });
        };
        this.deleteBranch = async (req, res, next) => {
            req.checkQuery('branch_id', error_enum_1.mainErrorEnum.branch_id_required).exists();
            req.checkQuery('branch_id', error_enum_1.mainErrorEnum.branch_id_invalid).matches(mainRegExp_1.mainRegExp.branch._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!this.isHaveAccessToThisBranch(req.query.branch_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            branch_1.BranchModel.updateOne({
                _id: req.query.branch_id,
                deleted: 0
            }, {
                deleted: 1
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.branch_not_found, next);
                }
                else {
                    this.addEmployeeAction(req, 'branch', req.query.branch_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.restoreBranch = async (req, res, next) => {
            req.checkBody('branch_id', error_enum_1.mainErrorEnum.branch_id_required).exists();
            req.checkBody('branch_id', error_enum_1.mainErrorEnum.branch_id_invalid).matches(mainRegExp_1.mainRegExp.branch._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!this.isHaveAccessToThisBranch(req.body.branch_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            branch_1.BranchModel.updateOne({
                _id: req.body.branch_id,
                deleted: 1
            }, {
                deleted: 0
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.branch_not_found, next);
                }
                else {
                    this.addEmployeeAction(req, 'branch', req.body.branch_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.addBranchImage = async (req, res, next) => {
            req.checkBody('branch_id', error_enum_1.mainErrorEnum.branch_id_required).exists();
            req.checkBody('branch_id', error_enum_1.mainErrorEnum.branch_id_invalid).matches(mainRegExp_1.mainRegExp.branch._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!this.isHaveAccessToThisBranch(req.body.branch_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            if (!req.files) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.image_not_found, next);
            }
            ;
            imagesHelper_1.imageHelper.uploadImage(req.files.image)
                .then(src => {
                branch_1.BranchModel.updateOne({
                    _id: req.body.branch_id,
                }, {
                    $push: {
                        images: src
                    },
                })
                    .exec()
                    .then(async (result) => {
                    if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.branch_not_found, next);
                    }
                    else {
                        this.addEmployeeAction(req, 'branch', req.body.branch_id);
                        return res.status(200).json({ image: src });
                    }
                    ;
                })
                    .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
            })
                .catch(() => {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.image_not_found, next);
            });
        };
        this.deleteBranchImage = async (req, res, next) => {
            req.checkQuery('branch_id', error_enum_1.mainErrorEnum.branch_id_required).exists();
            req.checkQuery('branch_id', error_enum_1.mainErrorEnum.branch_id_invalid).matches(mainRegExp_1.mainRegExp.branch._id);
            req.checkQuery('image', error_enum_1.mainErrorEnum.image_required).exists();
            req.checkQuery('image', error_enum_1.mainErrorEnum.image_invalid).isString();
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!this.isHaveAccessToThisBranch(req.query.branch_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            branch_1.BranchModel.updateOne({
                _id: req.query.branch_id,
                images: req.query.image
            }, {
                $pull: {
                    images: req.query.image,
                },
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.branch_not_found, next);
                }
                else {
                    imagesHelper_1.imageHelper.deleteImage(req.query.image);
                    this.addEmployeeAction(req, 'branch', req.body.branch_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewBranch = async (req, res, next) => {
            req.checkQuery('branch_id', error_enum_1.mainErrorEnum.branch_id_required).exists();
            req.checkQuery('branch_id', error_enum_1.mainErrorEnum.branch_id_invalid).matches(mainRegExp_1.mainRegExp.branch._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!this.isHaveAccessToThisBranch(req.query.branch_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            branch_1.BranchModel.findOne({
                _id: req.query.branch_id,
            })
                .populate('city')
                .exec()
                .then(async (branch) => {
                if (branch) {
                    return res.status(200).json({ branch });
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.branch_not_found, next);
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewBranches = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_required).exists();
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_invalid).matches(mainRegExp_1.mainRegExp.view.sortBy);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            branch_1.BranchModel.find({
                _id: {
                    $in: req.employee.branches_ids
                }
            })
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .populate('city')
                .exec()
                .then(async (branches) => {
                res.status(200).json({ branches });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewBranchesSearch = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('searchQuery', error_enum_1.mainErrorEnum.searchQuery_required).exists();
            req.checkQuery('searchQuery', error_enum_1.mainErrorEnum.searchQuery_invalid).matches(mainRegExp_1.mainRegExp.view.searchQuery);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            branch_1.BranchModel.find({
                $text: { $search: req.query.searchQuery },
                _id: {
                    $in: req.employee.branches_ids
                }
            })
                .sort('-createdAt')
                .skip((page - 1) * limit)
                .limit(limit)
                .populate('city')
                .exec()
                .then(async (branches) => {
                res.status(200).json({ branches });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        // CarType 
        this.addCarType = async (req, res, next) => {
            req.checkBody('nameEn', error_enum_1.mainErrorEnum.nameEn_required).exists();
            req.checkBody('nameEn', error_enum_1.mainErrorEnum.nameEn_invalid).matches(mainRegExp_1.mainRegExp.carType.nameEn);
            req.checkBody('nameAr', error_enum_1.mainErrorEnum.nameAr_required).exists();
            req.checkBody('nameAr', error_enum_1.mainErrorEnum.nameAr_invalid).matches(mainRegExp_1.mainRegExp.carType.nameAr);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            carType_1.CarTypeModel.create({
                nameEn: req.body.nameEn,
                nameAr: req.body.nameAr,
            })
                .then((carType) => {
                return res.status(200).json({
                    _id: carType._id
                });
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    if (_error.message.indexOf('nameEn') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.carType_nameEn_used_before, next);
                    }
                    ;
                    if (_error.message.indexOf('nameAr') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.carType_nameAr_used_before, next);
                    }
                    ;
                }
                else {
                    expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next);
                }
                ;
            });
        };
        this.deleteCarType = async (req, res, next) => {
            req.checkQuery('carType_id', error_enum_1.mainErrorEnum.carType_id_required).exists();
            req.checkQuery('carType_id', error_enum_1.mainErrorEnum.carType_id_invalid).matches(mainRegExp_1.mainRegExp.carType._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            carType_1.CarTypeModel.updateOne({
                _id: req.query.carType_id,
                deleted: 0
            }, {
                deleted: 1
            })
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.carType_not_found, next);
                }
                else {
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        // CarModel
        this.addCarModel = async (req, res, next) => {
            req.checkBody('nameEn', error_enum_1.mainErrorEnum.nameEn_required).exists();
            req.checkBody('nameEn', error_enum_1.mainErrorEnum.nameEn_invalid).matches(mainRegExp_1.mainRegExp.carModel.nameEn);
            req.checkBody('nameAr', error_enum_1.mainErrorEnum.nameAr_required).exists();
            req.checkBody('nameAr', error_enum_1.mainErrorEnum.nameAr_invalid).matches(mainRegExp_1.mainRegExp.carModel.nameAr);
            req.checkBody('carType_id', error_enum_1.mainErrorEnum.carType_id_required).exists();
            req.checkBody('carType_id', error_enum_1.mainErrorEnum.carType_id_invalid).matches(mainRegExp_1.mainRegExp.carType._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            carModel_1.CarModelModel.create({
                carType_id: req.body.carType_id,
                nameEn: req.body.nameEn,
                nameAr: req.body.nameAr,
            })
                .then((carModel) => {
                return res.status(200).json({
                    _id: carModel._id
                });
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    if (_error.message.indexOf('nameEn') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.carModel_nameEn_used_before, next);
                    }
                    ;
                    if (_error.message.indexOf('nameAr') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.carModel_nameAr_used_before, next);
                    }
                    ;
                }
                else {
                    expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next);
                }
                ;
            });
        };
        this.deleteCarModel = async (req, res, next) => {
            req.checkQuery('carModel_id', error_enum_1.mainErrorEnum.carModel_id_required).exists();
            req.checkQuery('carModel_id', error_enum_1.mainErrorEnum.carModel_id_invalid).matches(mainRegExp_1.mainRegExp.carModel._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            carType_1.CarTypeModel.updateOne({
                _id: req.query.carModel_id,
                deleted: 0
            }, {
                deleted: 1
            })
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.carModel_not_found, next);
                }
                else {
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        // CarGroup
        this.addCarGroup = async (req, res, next) => {
            req.checkBody('carType_id', error_enum_1.mainErrorEnum.carType_id_required).exists();
            req.checkBody('carType_id', error_enum_1.mainErrorEnum.carType_id_invalid).matches(mainRegExp_1.mainRegExp.carType._id);
            req.checkBody('carModel_id', error_enum_1.mainErrorEnum.carModel_id_required).exists();
            req.checkBody('carModel_id', error_enum_1.mainErrorEnum.carModel_id_invalid).matches(mainRegExp_1.mainRegExp.carModel._id);
            req.checkBody('branch_id', error_enum_1.mainErrorEnum.branch_id_required).exists();
            req.checkBody('branch_id', error_enum_1.mainErrorEnum.branch_id_invalid).matches(mainRegExp_1.mainRegExp.branch._id);
            req.checkBody('year', error_enum_1.mainErrorEnum.year_required).exists();
            req.checkBody('year', error_enum_1.mainErrorEnum.year_invalid).matches(mainRegExp_1.mainRegExp.carGroup.year);
            req.checkBody('allowedKm', error_enum_1.mainErrorEnum.allowedKm_required).exists();
            req.checkBody('allowedKm', error_enum_1.mainErrorEnum.allowedKm_invalid).matches(mainRegExp_1.mainRegExp.carGroup.allowedKm);
            req.checkBody('plusKmPrice', error_enum_1.mainErrorEnum.plusKmPrice_required).exists();
            req.checkBody('plusKmPrice', error_enum_1.mainErrorEnum.plusKmPrice_invalid).matches(mainRegExp_1.mainRegExp.carGroup.plusKmPrice);
            req.checkBody('pricePerDay', error_enum_1.mainErrorEnum.pricePerDay_required).exists();
            req.checkBody('pricePerDay', error_enum_1.mainErrorEnum.pricePerDay_invalid).matches(mainRegExp_1.mainRegExp.carGroup.pricePerDay);
            req.checkBody('discountPerDay', error_enum_1.mainErrorEnum.discountPerDay_required).exists();
            req.checkBody('discountPerDay', error_enum_1.mainErrorEnum.discountPerDay_invalid).matches(mainRegExp_1.mainRegExp.carGroup.discountPerDay);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            carGroup_1.CarGroupModel.create({
                carType: req.body.carType_id,
                carModel: req.body.carModel_id,
                branch: req.body.branch_id,
                year: req.body.year,
                allowedKm: req.body.allowedKm,
                plusKmPrice: req.body.plusKmPrice,
                pricePerDay: req.body.pricePerDay,
                discountPerDay: req.body.discountPerDay,
                images: [],
            })
                .then(async (carGroup) => {
                this.addEmployeeAction(req, 'carGroup', carGroup._id);
                return res.status(200).json({ _id: carGroup._id });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(_error, next));
        };
        this.editCarGroup = async (req, res, next) => {
            const editObject = {};
            req.checkBody('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_required).exists();
            req.checkBody('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_invalid).matches(mainRegExp_1.mainRegExp.carGroup._id);
            if (mainHelper_1.mainHelper.isExists(req.body.carType_id)) {
                editObject.carType = req.body.carType_id;
                req.checkBody('carType_id', error_enum_1.mainErrorEnum.carType_id_invalid).matches(mainRegExp_1.mainRegExp.carType._id);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.carModel_id)) {
                editObject.carModel = req.body.carModel_id;
                req.checkBody('carModel_id', error_enum_1.mainErrorEnum.carModel_id_invalid).matches(mainRegExp_1.mainRegExp.carModel._id);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.branch_id)) {
                editObject.branch = req.body.branch_id;
                req.checkBody('branch_id', error_enum_1.mainErrorEnum.branch_id_invalid).matches(mainRegExp_1.mainRegExp.branch._id);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.year)) {
                editObject.year = req.body.year;
                req.checkBody('year', error_enum_1.mainErrorEnum.year_invalid).matches(mainRegExp_1.mainRegExp.carGroup.year);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.allowedKm)) {
                editObject.allowedKm = req.body.allowedKm;
                req.checkBody('allowedKm', error_enum_1.mainErrorEnum.allowedKm_invalid).matches(mainRegExp_1.mainRegExp.carGroup.allowedKm);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.plusKmPrice)) {
                editObject.plusKmPrice = req.body.plusKmPrice;
                req.checkBody('plusKmPrice', error_enum_1.mainErrorEnum.plusKmPrice_invalid).matches(mainRegExp_1.mainRegExp.carGroup.plusKmPrice);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.pricePerDay)) {
                editObject.pricePerDay = req.body.pricePerDay;
                req.checkBody('pricePerDay', error_enum_1.mainErrorEnum.pricePerDay_invalid).matches(mainRegExp_1.mainRegExp.carGroup.pricePerDay);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.discountPerDay)) {
                editObject.discountPerDay = req.body.discountPerDay;
                req.checkBody('discountPerDay', error_enum_1.mainErrorEnum.discountPerDay_invalid).matches(mainRegExp_1.mainRegExp.carGroup.discountPerDay);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.views)) {
                editObject.views = req.body.views;
                req.checkBody('views', error_enum_1.mainErrorEnum.views_invalid).matches(mainRegExp_1.mainRegExp.carGroup.views);
            }
            ;
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (Object.keys(editObject).length === 0) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.edit_date_not_found, next);
            }
            ;
            carGroup_1.CarGroupModel.updateOne({
                _id: req.body.carGroup_id,
                branch: {
                    $in: req.employee.branches_ids
                }
            }, Object.assign({}, editObject))
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.carGroup_not_found, next);
                }
                else {
                    this.addEmployeeAction(req, 'carGroup', req.body.carGroup_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.plateNumber_used_before, next);
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next);
                }
                ;
            });
        };
        this.deleteCarGroup = async (req, res, next) => {
            req.checkQuery('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_required).exists();
            req.checkQuery('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_invalid).matches(mainRegExp_1.mainRegExp.carGroup._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisCarGroup(req.query.carGroup_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            carGroup_1.CarGroupModel.updateOne({
                _id: req.query.carGroup_id,
                deleted: 0
            }, {
                deleted: 1
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.carGroup_not_found, next);
                }
                else {
                    this.addEmployeeAction(req, 'carGroup', req.query.carGroup_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.restoreCarGroup = async (req, res, next) => {
            req.checkBody('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_required).exists();
            req.checkBody('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_invalid).matches(mainRegExp_1.mainRegExp.carGroup._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisCarGroup(req.body.carGroup_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            carGroup_1.CarGroupModel.updateOne({
                _id: req.body.carGroup_id,
                deleted: 1
            }, {
                deleted: 0
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.carGroup_not_found, next);
                }
                else {
                    this.addEmployeeAction(req, 'carGroup', req.body.carGroup_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.addCarGroupImage = async (req, res, next) => {
            req.checkBody('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_required).exists();
            req.checkBody('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_invalid).matches(mainRegExp_1.mainRegExp.carGroup._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisCarGroup(req.body.carGroup_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            if (!req.files) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.image_not_found, next);
            }
            ;
            imagesHelper_1.imageHelper.uploadImage(req.files.image)
                .then(src => {
                carGroup_1.CarGroupModel.updateOne({
                    _id: req.body.carGroup_id,
                }, {
                    $push: {
                        images: src
                    }
                })
                    .exec()
                    .then(async (result) => {
                    if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.carGroup_not_found, next);
                    }
                    else {
                        this.addEmployeeAction(req, 'carGroup', req.body.carGroup_id);
                        return res.status(200).json({ image: src });
                    }
                    ;
                })
                    .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
            })
                .catch(() => {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.image_not_found, next);
            });
        };
        this.deleteCarGroupImage = async (req, res, next) => {
            req.checkQuery('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_required).exists();
            req.checkQuery('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_invalid).matches(mainRegExp_1.mainRegExp.carGroup._id);
            req.checkQuery('image', error_enum_1.mainErrorEnum.image_required).exists();
            req.checkQuery('image', error_enum_1.mainErrorEnum.image_invalid).isString();
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisCarGroup(req.query.carGroup_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            carGroup_1.CarGroupModel.updateOne({
                _id: req.query.carGroup_id,
                images: req.query.image,
            }, {
                $pull: {
                    images: req.query.image,
                },
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.carGroup_not_found, next);
                }
                else {
                    imagesHelper_1.imageHelper.deleteImage(req.query.image);
                    this.addEmployeeAction(req, 'carGroup', req.query.carGroup_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewCarGroup = async (req, res, next) => {
            req.checkQuery('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_required).exists();
            req.checkQuery('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_invalid).matches(mainRegExp_1.mainRegExp.carGroup._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisCarGroup(req.query.carGroup_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            carGroup_1.CarGroupModel.findOne({
                _id: req.query.carGroup_id,
            })
                .populate('carType carModel branch')
                .lean()
                .exec()
                .then(async (carGroup) => {
                if (carGroup) {
                    return res.status(200).json({
                        carGroup: await carGroupHelper_1.carGroupHelper.setCarsCountInCarGroup(carGroup)
                    });
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.carGroup_not_found, next);
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewCarGroups = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_required).exists();
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_invalid).matches(mainRegExp_1.mainRegExp.view.sortBy);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            carGroup_1.CarGroupModel.find({
                branch: {
                    $in: req.employee.branches_ids
                }
            })
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .populate('carType carModel branch')
                .lean()
                .exec()
                .then(async (carGroups) => {
                res.status(200).json({ carGroups: await carGroupHelper_1.carGroupHelper.setCarsCountInCarGroups(carGroups) });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewCarGroupsByFilter = async (req, res, next) => {
            const query = {};
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_required).exists();
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_invalid).matches(mainRegExp_1.mainRegExp.view.sortBy);
            if (mainHelper_1.mainHelper.isExists(req.query.branch_id)) {
                req.checkQuery('branch_id', error_enum_1.mainErrorEnum.branch_id_invalid).matches(mainRegExp_1.mainRegExp.branch._id);
                query.branch = req.query.branch_id;
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.query.carType_id)) {
                req.checkQuery('carType_id', error_enum_1.mainErrorEnum.carType_id_invalid).matches(mainRegExp_1.mainRegExp.carType._id);
                query.carType = req.query.carType_id;
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.query.carModel_id)) {
                req.checkQuery('carModel_id', error_enum_1.mainErrorEnum.carModel_id_invalid).matches(mainRegExp_1.mainRegExp.carModel._id);
                query.carModel = req.query.carModel_id;
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.query.year)) {
                req.checkQuery('year', error_enum_1.mainErrorEnum.year_invalid).matches(mainRegExp_1.mainRegExp.carGroup.year);
                query.year = req.query.year;
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.query.pricePerDay)) {
                req.checkQuery('pricePerDay', error_enum_1.mainErrorEnum.pricePerDay_invalid).matches(mainRegExp_1.mainRegExp.carGroup.pricePerDay);
                query.pricePerDay = req.query.pricePerDay;
            }
            ;
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            carGroup_1.CarGroupModel.find({
                $and: [
                    query,
                    {
                        branch: {
                            $in: req.employee.branches_ids
                        }
                    }
                ]
            })
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .populate('carType carModel branch')
                .lean()
                .exec()
                .then(async (carGroups) => {
                res.status(200).json({ carGroups: await carGroupHelper_1.carGroupHelper.setCarsCountInCarGroups(carGroups) });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        // Booking
        this.addCarToBooking = async (req, res, next) => {
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_required).exists();
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_invalid).matches(mainRegExp_1.mainRegExp.booking._id);
            req.checkBody('car_id', error_enum_1.mainErrorEnum.car_id_required).exists();
            req.checkBody('car_id', error_enum_1.mainErrorEnum.car_id_invalid).matches(mainRegExp_1.mainRegExp.car._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisBooking(req.body.booking_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            if (await bookingHelper_1.bookingHelper.isCanAddCarToBooking(req.body.car_id, req.body.booking_id)) {
                booking_2.BookingModel.updateOne({
                    _id: req.body.booking_id,
                    deleted: 0
                }, {
                    car: req.body.car_id
                })
                    .exec()
                    .then(async (result) => {
                    if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.booking_not_found, next);
                    }
                    else {
                        this.addEmployeeAction(req, 'booking', req.body.booking_id);
                        return res.status(200).end();
                    }
                    ;
                })
                    .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
            }
            else {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.you_cant_booking_this_car_in_this_booking_date, next);
            }
            ;
        };
        this.editBookingDate = async (req, res, next) => {
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_required).exists();
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_invalid).matches(mainRegExp_1.mainRegExp.booking._id);
            req.checkBody('pickUpDate', error_enum_1.mainErrorEnum.pickUpDate_required).exists();
            req.checkBody('pickUpDate', error_enum_1.mainErrorEnum.pickUpDate_invalid).matches(mainRegExp_1.mainRegExp.booking.pickUpDate);
            req.checkBody('returnDate', error_enum_1.mainErrorEnum.returnDate_required).exists();
            req.checkBody('returnDate', error_enum_1.mainErrorEnum.returnDate_invalid).matches(mainRegExp_1.mainRegExp.booking.returnDate);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisBooking(req.body.booking_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            const isCanEditBookingDate = await bookingHelper_1.bookingHelper.isCanEditBookingDate(req.body.booking_id, req.body.pickUpDate, req.body.returnDate);
            if (isCanEditBookingDate) {
                const booking = await booking_2.BookingModel.findOne({
                    _id: req.body.booking_id,
                    deleted: 0,
                })
                    .exec();
                const daysCount = bookingHelper_1.bookingHelper.getDaysCount(req.body.pickUpDate, req.body.returnDate);
                const totalPrice = bookingHelper_1.bookingHelper.getTotlaPrice(booking.pricePerDay, booking.discountPerDay, daysCount);
                booking_2.BookingModel.findOneAndUpdate({
                    _id: req.body.booking_id,
                    deleted: 0
                }, {
                    daysCount,
                    total: totalPrice,
                    pickUpDate: req.body.pickUpDate,
                    returnDate: req.body.returnDate,
                }, {
                    new: true
                })
                    .exec()
                    .then(async (booking) => {
                    if (booking) {
                        this.addEmployeeAction(req, 'booking', req.body.booking_id);
                        return res.status(200).json({ booking });
                    }
                    else {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.booking_not_found, next);
                    }
                    ;
                })
                    .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
            }
            else {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.you_cant_edit_booking_date, next);
            }
            ;
        };
        this.editBookingExpiredAt = async (req, res, next) => {
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_required).exists();
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_invalid).matches(mainRegExp_1.mainRegExp.booking._id);
            req.checkBody('expiredAt', error_enum_1.mainErrorEnum.expiredAt_required).exists();
            req.checkBody('expiredAt', error_enum_1.mainErrorEnum.expiredAt_invalid).matches(mainRegExp_1.mainRegExp.booking.expiredAt);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisBooking(req.body.booking_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            const isCanEditExpiredAt = await bookingHelper_1.bookingHelper.isCanEditExpiredAt(req.body.booking_id, req.body.expiredAt);
            if (isCanEditExpiredAt) {
                booking_2.BookingModel.updateOne({
                    _id: req.body.booking_id,
                    deleted: 0
                }, {
                    expiredAt: req.body.expiredAt,
                })
                    .exec()
                    .then(async (result) => {
                    if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.booking_not_found, next);
                    }
                    else {
                        this.addEmployeeAction(req, 'booking', req.body.booking_id);
                        return res.status(200).json();
                    }
                    ;
                })
                    .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
            }
            else {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.you_cant_edit_booking_expiredAt, next);
            }
            ;
        };
        this.editBookingPricePerDay = async (req, res, next) => {
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_required).exists();
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_invalid).matches(mainRegExp_1.mainRegExp.booking._id);
            req.checkBody('pricePerDay', error_enum_1.mainErrorEnum.pricePerDay_required).exists();
            req.checkBody('pricePerDay', error_enum_1.mainErrorEnum.pricePerDay_invalid).matches(mainRegExp_1.mainRegExp.booking.pricePerDay);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisBooking(req.body.booking_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            const booking = await booking_2.BookingModel.findOne({
                _id: req.body.booking_id,
                deleted: 0,
            })
                .exec();
            const totalPrice = bookingHelper_1.bookingHelper.getTotlaPrice(req.body.pricePerDay, booking.discountPerDay, booking.daysCount);
            booking_2.BookingModel.findOneAndUpdate({
                _id: req.body.booking_id,
                deleted: 0
            }, {
                total: totalPrice,
                pricePerDay: req.body.pricePerDay,
            }, {
                new: true
            })
                .exec()
                .then(async (booking) => {
                if (booking) {
                    this.addEmployeeAction(req, 'booking', req.body.booking_id);
                    return res.status(200).json({ booking });
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.booking_not_found, next);
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.editBookingDiscountPerDay = async (req, res, next) => {
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_required).exists();
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_invalid).matches(mainRegExp_1.mainRegExp.booking._id);
            req.checkBody('discountPerDay', error_enum_1.mainErrorEnum.discountPerDay_required).exists();
            req.checkBody('discountPerDay', error_enum_1.mainErrorEnum.discountPerDay_invalid).matches(mainRegExp_1.mainRegExp.booking.discountPerDay);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisBooking(req.body.booking_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            const booking = await booking_2.BookingModel.findOne({
                _id: req.body.booking_id,
                deleted: 0,
            })
                .exec();
            const totalPrice = bookingHelper_1.bookingHelper.getTotlaPrice(booking.pricePerDay, req.body.discountPerDay, booking.daysCount);
            booking_2.BookingModel.findOneAndUpdate({
                _id: req.body.booking_id,
                deleted: 0
            }, {
                total: totalPrice,
                discountPerDay: req.body.discountPerDay,
            }, {
                new: true
            })
                .exec()
                .then(async (booking) => {
                if (booking) {
                    this.addEmployeeAction(req, 'booking', req.body.booking_id);
                    return res.status(200).json({ booking });
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.booking_not_found, next);
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.activeBooking = async (req, res, next) => {
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_required).exists();
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_invalid).matches(mainRegExp_1.mainRegExp.booking._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisBooking(req.body.booking_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            const isCanActiveBooking = await bookingHelper_1.bookingHelper.isCanActiveBooking(req.body.booking_id);
            if (isCanActiveBooking) {
                booking_2.BookingModel.updateOne({
                    _id: req.body.booking_id,
                    deleted: 0,
                    bookingType: booking_1.EbookingType.waiting_to_confirm,
                    car: { $ne: null },
                }, {
                    bookingType: booking_1.EbookingType.active
                })
                    .exec()
                    .then(async (result) => {
                    if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.booking_not_found_or_car_not_found, next);
                    }
                    else {
                        this.addEmployeeAction(req, 'booking', req.body.booking_id);
                        return res.status(200).end();
                    }
                    ;
                })
                    .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
            }
            else {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.you_cant_active_booking, next);
            }
            ;
        };
        this.cancelBooking = async (req, res, next) => {
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_required).exists();
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_invalid).matches(mainRegExp_1.mainRegExp.booking._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisBooking(req.body.booking_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            booking_2.BookingModel.updateOne({
                _id: req.body.booking_id,
                deleted: 0,
                bookingType: booking_1.EbookingType.request,
            }, {
                bookingType: booking_1.EbookingType.cancelled
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.booking_not_found, next);
                }
                else {
                    this.addEmployeeAction(req, 'booking', req.body.booking_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.approveBooking = async (req, res, next) => {
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_required).exists();
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_invalid).matches(mainRegExp_1.mainRegExp.booking._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisBooking(req.body.booking_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            booking_2.BookingModel.updateOne({
                _id: req.body.booking_id,
                deleted: 0,
                bookingType: booking_1.EbookingType.request,
            }, {
                bookingType: booking_1.EbookingType.waiting_to_confirm
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.booking_not_found, next);
                }
                else {
                    this.addEmployeeAction(req, 'booking', req.body.booking_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.deleteBooking = async (req, res, next) => {
            req.checkQuery('booking_id', error_enum_1.mainErrorEnum.booking_id_required).exists();
            req.checkQuery('booking_id', error_enum_1.mainErrorEnum.booking_id_invalid).matches(mainRegExp_1.mainRegExp.booking._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisBooking(req.query.booking_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            booking_2.BookingModel.updateOne({
                _id: req.query.booking_id,
                deleted: 0
            }, {
                deleted: 1
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.booking_not_found, next);
                }
                else {
                    this.addEmployeeAction(req, 'booking', req.query.booking_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.restoreBooking = async (req, res, next) => {
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_required).exists();
            req.checkBody('booking_id', error_enum_1.mainErrorEnum.booking_id_invalid).matches(mainRegExp_1.mainRegExp.booking._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisBooking(req.body.booking_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            booking_2.BookingModel.updateOne({
                _id: req.body.booking_id,
                deleted: 1,
            }, {
                deleted: 0
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.booking_not_found, next);
                }
                else {
                    this.addEmployeeAction(req, 'booking', req.body.booking_id);
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewBooking = async (req, res, next) => {
            req.checkQuery('booking_id', error_enum_1.mainErrorEnum.booking_id_required).exists();
            req.checkQuery('booking_id', error_enum_1.mainErrorEnum.booking_id_invalid).matches(mainRegExp_1.mainRegExp.booking._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (!await this.isHaveAccessToThisBooking(req.query.booking_id, req.employee.branches_ids)) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.not_have_access, next);
            }
            ;
            booking_2.BookingModel.findOne({
                _id: req.query.booking_id,
            })
                .populate('guest onlinePayment')
                .populate({
                path: 'returnBranch',
                select: 'nameEn nameAr'
            })
                .populate({
                path: 'pickUpBranch',
                select: 'nameEn nameAr'
            })
                .populate({
                path: 'client',
                select: 'name'
            })
                .populate({
                path: 'car',
                select: 'plateNumber'
            })
                .populate({
                path: 'carGroup',
                select: 'branch',
                populate: {
                    path: 'branch',
                    select: 'nameEn nameAr',
                }
            })
                .populate({
                path: 'carGroup',
                select: 'carModel',
                populate: {
                    path: 'carModel',
                    select: 'nameEn nameAr',
                }
            })
                .populate({
                path: 'carGroup',
                select: 'carType year images',
                populate: {
                    path: 'carType',
                    select: 'nameEn nameAr',
                }
            })
                .lean()
                .exec()
                .then(async (booking) => {
                if (booking) {
                    return res.status(200).json({
                        booking: await bookingHelper_1.bookingHelper.setBookingType(booking)
                    });
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.booking_not_found, next);
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewBookings = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_required).exists();
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_invalid).matches(mainRegExp_1.mainRegExp.view.sortBy);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            booking_2.BookingModel.find({
                pickUpBranch: {
                    $in: req.employee.branches_ids
                },
                returnBranch: {
                    $in: req.employee.branches_ids
                }
            })
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .populate('guest onlinePayment')
                .populate({
                path: 'returnBranch',
                select: 'nameEn nameAr'
            })
                .populate({
                path: 'pickUpBranch',
                select: 'nameEn nameAr'
            })
                .populate({
                path: 'client',
                select: 'name'
            })
                .populate({
                path: 'car',
                select: 'plateNumber'
            })
                .populate({
                path: 'carGroup',
                select: 'carModel',
                populate: {
                    path: 'carModel',
                    select: 'nameEn nameAr',
                }
            })
                .populate({
                path: 'carGroup',
                select: 'carType images',
                populate: {
                    path: 'carType',
                    select: 'nameEn nameAr',
                }
            })
                .lean()
                .exec()
                .then(async (bookings) => {
                res.status(200).json({
                    bookings: bookingHelper_1.bookingHelper.setBookingsType(bookings)
                });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewBookingsByFilter = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_required).exists();
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_invalid).matches(mainRegExp_1.mainRegExp.view.sortBy);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            // create query
            let query = {};
            // isExists
            if (mainHelper_1.mainHelper.isExists(req.query.bookingType)) {
                // check
                if (mainRegExp_1.mainRegExp.booking.bookingType.test(req.query.bookingType)) {
                    // create query
                    const bookingType = parseInt(req.query.bookingType);
                    if (bookingType === booking_1.EbookingType.expired) {
                        query = {
                            bookingType: booking_1.EbookingType.waiting_to_confirm,
                            expiredAt: {
                                $lt: new Date()
                            }
                        };
                    }
                    else if (bookingType === booking_1.EbookingType.waiting_to_confirm) {
                        query = {
                            bookingType: booking_1.EbookingType.waiting_to_confirm,
                            expiredAt: {
                                $gt: new Date()
                            }
                        };
                    }
                    else if (bookingType === booking_1.EbookingType.done) {
                        query = {
                            bookingType: booking_1.EbookingType.active,
                            returnDate: {
                                $lt: new Date()
                            }
                        };
                    }
                    else if (bookingType === booking_1.EbookingType.active) {
                        query = {
                            bookingType: booking_1.EbookingType.active,
                            returnDate: {
                                $gt: new Date()
                            }
                        };
                    }
                    else {
                        query = {
                            bookingType: bookingType,
                        };
                    }
                    ;
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.bookingType_invalid, next);
                }
                ;
            }
            ;
            // isExists
            if (mainHelper_1.mainHelper.isExists(req.query.branch_id)) {
                // check
                if (mainRegExp_1.mainRegExp.branch._id.test(req.query.branch_id)) {
                    query.pickUpBranch = req.query.branch_id;
                    query.returnBranch = req.query.branch_id;
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.branch_id_invalid, next);
                }
                ;
            }
            ;
            booking_2.BookingModel.find({
                $and: [
                    query,
                    {
                        pickUpBranch: {
                            $in: req.employee.branches_ids
                        },
                        returnBranch: {
                            $in: req.employee.branches_ids
                        },
                    }
                ]
            })
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .populate('guest onlinePayment')
                .populate({
                path: 'returnBranch',
                select: 'nameEn nameAr'
            })
                .populate({
                path: 'pickUpBranch',
                select: 'nameEn nameAr'
            })
                .populate({
                path: 'client',
                select: 'name'
            })
                .populate({
                path: 'car',
                select: 'plateNumber'
            })
                .populate({
                path: 'carGroup',
                select: 'carModel',
                populate: {
                    path: 'carModel',
                    select: 'nameEn nameAr',
                }
            })
                .populate({
                path: 'carGroup',
                select: 'carType images',
                populate: {
                    path: 'carType',
                    select: 'nameEn nameAr',
                }
            })
                .lean()
                .exec()
                .then(async (bookings) => {
                res.status(200).json({
                    bookings: bookingHelper_1.bookingHelper.setBookingsType(bookings)
                });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewBookingsSearch = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('searchQuery', error_enum_1.mainErrorEnum.searchQuery_required).exists();
            req.checkQuery('searchQuery', error_enum_1.mainErrorEnum.searchQuery_invalid).matches(mainRegExp_1.mainRegExp.view.searchQuery);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            booking_2.BookingModel.find({
                $text: { $search: req.query.searchQuery },
                pickUpBranch: {
                    $in: req.employee.branches_ids
                },
                returnBranch: {
                    $in: req.employee.branches_ids
                },
            })
                .sort('-createdAt')
                .skip((page - 1) * limit)
                .limit(limit)
                .populate('guest onlinePayment')
                .populate({
                path: 'returnBranch',
                select: 'nameEn nameAr'
            })
                .populate({
                path: 'pickUpBranch',
                select: 'nameEn nameAr'
            })
                .populate({
                path: 'client',
                select: 'name'
            })
                .populate({
                path: 'car',
                select: 'plateNumber'
            })
                .populate({
                path: 'carGroup',
                select: 'carModel',
                populate: {
                    path: 'carModel',
                    select: 'nameEn nameAr',
                }
            })
                .populate({
                path: 'carGroup',
                select: 'carType images',
                populate: {
                    path: 'carType',
                    select: 'nameEn nameAr',
                }
            })
                .lean()
                .exec()
                .then(async (bookings) => {
                res.status(200).json({
                    bookings: bookingHelper_1.bookingHelper.setBookingsType(bookings)
                });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        // Guest
        this.viewGuest = async (req, res, next) => {
            req.checkQuery('guest_id', error_enum_1.mainErrorEnum.guest_id_required).exists();
            req.checkQuery('guest_id', error_enum_1.mainErrorEnum.guest_id_invalid).matches(mainRegExp_1.mainRegExp.guest._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            guest_1.GuestModel.findOne({
                _id: req.query.guest_id,
            })
                .exec()
                .then(async (guest) => {
                if (guest) {
                    return res.status(200).json({ guest });
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.guest_not_found, next);
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewGuests = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_required).exists();
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_invalid).matches(mainRegExp_1.mainRegExp.view.sortBy);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            guest_1.GuestModel.find({})
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .exec()
                .then(async (guests) => {
                res.status(200).json({ guests });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewGuestsSearch = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('searchQuery', error_enum_1.mainErrorEnum.searchQuery_required).exists();
            req.checkQuery('searchQuery', error_enum_1.mainErrorEnum.searchQuery_invalid).matches(mainRegExp_1.mainRegExp.view.searchQuery);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            guest_1.GuestModel.find({
                $text: {
                    $search: req.query.searchQuery
                },
            })
                .sort('-createdAt')
                .skip((page - 1) * limit)
                .limit(limit)
                .exec()
                .then((guests) => {
                res.status(200).json({ guests });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewBookingsForGuest = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('guest_id', error_enum_1.mainErrorEnum.guest_id_required).exists();
            req.checkQuery('guest_id', error_enum_1.mainErrorEnum.guest_id_invalid).matches(mainRegExp_1.mainRegExp.guest._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            booking_2.BookingModel.find({
                guest: req.query.guest_id
            })
                .skip((page - 1) * limit)
                .limit(limit)
                .populate('car carGroup pickUpBranch returnBranch onlinePayment')
                .exec()
                .then((bookings) => res.status(200).json({ bookings }))
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewOnlinePaymentsForGuest = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('guest_id', error_enum_1.mainErrorEnum.guest_id_required).exists();
            req.checkQuery('guest_id', error_enum_1.mainErrorEnum.guest_id_invalid).matches(mainRegExp_1.mainRegExp.guest._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            onlinePayment_1.OnlinePaymentModel.find({
                client: req.query.guest_id
            })
                .skip((page - 1) * limit)
                .limit(limit)
                .populate('booking')
                .exec()
                .then((onlinePayments) => res.status(200).json({ onlinePayments }))
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        // options
        this.viewAllBranchesCanThisEmployeeAccess = async (req, res, next) => {
            branch_1.BranchModel.find({
                _id: {
                    $in: req.employee.branches_ids
                },
            })
                .exec()
                .then(async (branches) => {
                res.status(200).json({ branches });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewAllCarGroupsCanThisEmployeeAccess = async (req, res, next) => {
            carGroup_1.CarGroupModel.find({
                branch: {
                    $in: req.employee.branches_ids
                },
                deleted: 0,
            })
                .sort('-createdAt')
                .select('_id carType carModel branch year')
                .populate({
                path: 'carType',
                select: '-_id nameEn nameAr'
            })
                .populate({
                path: 'carModel',
                select: '-_id nameEn nameAr'
            })
                .populate({
                path: 'branch',
                select: '-_id nameEn nameAr'
            })
                .exec()
                .then(async (carGroups) => {
                res.status(200).json({ carGroups });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewAllNationalities = async (req, res, next) => {
            nationality_1.NationalityModel.find({})
                .sort('-createdAt')
                .exec()
                .then(async (nationalities) => {
                res.status(200).json({ nationalities });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewAllCities = async (req, res, next) => {
            city_1.CityModel.find({})
                .sort('-createdAt')
                .exec()
                .then(async (cities) => {
                res.status(200).json({ cities });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.getThislocation = async (req, res, next) => {
            const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
            // geoip.
            request_1.default.get(`http://ip-api.com/json/${ip}`, (error, _response, body) => {
                const _body = JSON.parse(body);
                if (error || _body.status === 'fail') {
                    res.status(200).json({
                        latitude: 24.774265,
                        longitude: 46.738586,
                    });
                }
                else {
                    res.status(200).json({
                        latitude: parseFloat(_body.lat),
                        longitude: parseFloat(_body.lon),
                    });
                }
                ;
            });
        };
        this.viewAllCarTypes = async (req, res, next) => {
            carType_1.CarTypeModel.find({})
                .exec()
                .then((carTypes) => {
                return res.status(200).json({ carTypes });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewAllCarModelsByCarType = async (req, res, next) => {
            req.checkQuery('carType_id', error_enum_1.mainErrorEnum.carType_id_required).exists();
            req.checkQuery('carType_id', error_enum_1.mainErrorEnum.carType_id_invalid).matches(mainRegExp_1.mainRegExp.carType._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            carModel_1.CarModelModel.find({
                carType_id: req.query.carType_id,
            })
                .exec()
                .then((carModels) => {
                return res.status(200).json({ carModels });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewAllCarsWhoCanAddToBooking = async (req, res, next) => {
            req.checkQuery('booking_id', error_enum_1.mainErrorEnum.booking_id_required).exists();
            req.checkQuery('booking_id', error_enum_1.mainErrorEnum.booking_id_invalid).matches(mainRegExp_1.mainRegExp.booking._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            bookingHelper_1.bookingHelper.viewAllCarsWhoCanAddToBooking(req.query.booking_id)
                .then(async (cars) => {
                res.status(200).json({ cars });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
    }
    ;
}
;
exports.employeeCTRL = new EmployeeCTRL();
