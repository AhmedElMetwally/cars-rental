"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const mainHelper_1 = require("./../helpers/mainHelper");
const client_1 = require("./../models/client");
const jwt_1 = require("./../helpers/jwt");
const guest_1 = require("../models/guest");
const carGroup_1 = require("../models/carGroup");
const booking_1 = require("../models/booking");
const branch_1 = require("../models/branch");
const bookingHelper_1 = require("../helpers/bookingHelper");
const mainRegExp_1 = require("../regExp/mainRegExp");
const expressHelper_1 = require("../helpers/expressHelper");
const error_enum_1 = require("./../enums/error.enum");
const branch_2 = require("../models/branch");
const moment_timezone_1 = __importDefault(require("moment-timezone"));
const client_2 = require("../models/client");
class GuestCTRLHelper {
    constructor() {
        this.createGuest = (guest) => {
            return new Promise((resolve, reject) => {
                guest_1.GuestModel.create({
                    name: guest.name,
                    phone: guest.phone,
                    iqama: guest.iqama,
                    iqamaExpiryDate: guest.iqamaExpiryDate,
                    drivingLicence: guest.drivingLicence,
                    drivingLicenceExpiryDate: guest.drivingLicenceExpiryDate,
                    birthday: guest.birthday,
                })
                    .then((_guest) => {
                    resolve(_guest);
                })
                    .catch(_error => {
                    reject(_error);
                });
            });
        };
        this.getGuest = (guest) => {
            return new Promise((resolve, reject) => {
                guest_1.GuestModel.findOne({
                    phone: guest.phone,
                    iqama: guest.iqama,
                })
                    .then((_guest) => {
                    if (_guest) {
                        if (_guest.deleted) {
                            resolve(null);
                        }
                        else {
                            resolve(_guest);
                        }
                    }
                    else {
                        reject();
                    }
                    ;
                })
                    .catch(_error => {
                    reject(_error);
                });
            });
        };
        this._getOrCreateGuest = (guest) => {
            return new Promise((resolve, reject) => {
                this.getGuest(guest)
                    .then((_guest) => {
                    resolve(_guest);
                })
                    .catch(() => {
                    this.createGuest(guest)
                        .then((_guest) => {
                        resolve(_guest);
                    })
                        .catch(_error => {
                        reject(_error);
                    });
                });
            });
        };
        this.updateAllGuestBookingsToClient = (guest_id, client_id) => {
            booking_1.BookingModel.updateMany({
                guest: guest_id
            }, {
                guest: null,
                client: client_id
            })
                .exec()
                .then(() => { })
                .catch(() => { });
        };
    }
    ;
}
;
class GuestCTRL extends GuestCTRLHelper {
    constructor() {
        super();
        this.signupGuestToClient = async (req, res, next) => {
            req.checkBody('name', error_enum_1.mainErrorEnum.name_required).exists();
            req.checkBody('name', error_enum_1.mainErrorEnum.name_invalid).matches(mainRegExp_1.mainRegExp.client.name);
            req.checkBody('phone', error_enum_1.mainErrorEnum.phone_required).exists();
            req.checkBody('phone', error_enum_1.mainErrorEnum.phone_invalid).matches(mainRegExp_1.mainRegExp.client.phone);
            req.checkBody('email', error_enum_1.mainErrorEnum.email_required).exists();
            req.checkBody('email', error_enum_1.mainErrorEnum.email_invalid).matches(mainRegExp_1.mainRegExp.client.email);
            req.checkBody('iqama', error_enum_1.mainErrorEnum.iqama_required).exists();
            req.checkBody('iqama', error_enum_1.mainErrorEnum.iqama_invalid).matches(mainRegExp_1.mainRegExp.client.iqama);
            req.checkBody('password', error_enum_1.mainErrorEnum.password_required).exists();
            req.checkBody('password', error_enum_1.mainErrorEnum.password_invalid).matches(mainRegExp_1.mainRegExp.client.password);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            guest_1.GuestModel.findOne({
                deleted: 0,
                iqama: req.body.iqama,
                phone: req.body.phone,
            })
                .exec()
                .then((guest) => {
                if (guest) {
                    client_2.ClientModel.create({
                        name: req.body.name,
                        email: req.body.email,
                        phone: req.body.phone,
                        iqama: req.body.iqama,
                        password: mainHelper_1.mainHelper.hashSync(req.body.password),
                        roles: ['client'],
                        signupFrom: client_1.EsignupFrom.booking_before_and_signup,
                    })
                        .then(async (client) => {
                        this.updateAllGuestBookingsToClient(guest._id, client._id);
                        const token = await jwt_1.jwt.create({
                            _id: client._id,
                            roles: client.roles
                        });
                        client.password = '';
                        return res.status(200).json({ client, token });
                    })
                        .catch(_error => {
                        if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                            if (_error.message.indexOf('email') !== -1) {
                                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.email_used_before, next);
                            }
                            else if (_error.message.indexOf('phone') !== -1) {
                                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.phone_used_before, next);
                            }
                            else if (_error.message.indexOf('iqama') !== -1) {
                                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.iqama_used_before, next);
                            }
                            ;
                        }
                        else {
                            return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next);
                        }
                        ;
                    });
                }
                else {
                    expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.guest_not_found, next);
                }
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.addBooking = async (req, res, next) => {
            req.checkBody('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_required).exists();
            req.checkBody('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_invalid).matches(mainRegExp_1.mainRegExp.carGroup._id);
            req.checkBody('pickUpDate', error_enum_1.mainErrorEnum.pickUpDate_required).exists();
            req.checkBody('pickUpDate', error_enum_1.mainErrorEnum.pickUpDate_invalid).matches(mainRegExp_1.mainRegExp.booking.pickUpDate);
            req.checkBody('returnDate', error_enum_1.mainErrorEnum.returnDate_required).exists();
            req.checkBody('returnDate', error_enum_1.mainErrorEnum.returnDate_invalid).matches(mainRegExp_1.mainRegExp.booking.returnDate);
            req.checkBody('guest_id', error_enum_1.mainErrorEnum.guest_id_required).exists();
            req.checkBody('guest_id', error_enum_1.mainErrorEnum.guest_id_invalid).matches(mainRegExp_1.mainRegExp.guest._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const isCanBookingCarGroupInDate = await bookingHelper_1.bookingHelper.isCanBookingCarGroupInDate(req.body.carGroup_id, req.body.pickUpDate, req.body.returnDate);
            if (isCanBookingCarGroupInDate) {
                const guest = await guest_1.GuestModel.findOne({
                    _id: req.body.guest_id,
                })
                    .select('iqama')
                    .exec();
                const carGroup = await carGroup_1.CarGroupModel.findOne({
                    _id: req.body.carGroup_id
                })
                    .select('branch pricePerDay discountPerDay')
                    .exec();
                const branch = await branch_2.BranchModel.findOne({
                    _id: carGroup.branch
                })
                    .select('bookingStartCode waitingHoursForConfirm')
                    .exec();
                if (guest && carGroup && branch) {
                    const bookingCode = `${branch.bookingStartCode}${await branch_1.branchBookingCounter()}`;
                    const daysCount = bookingHelper_1.bookingHelper.getDaysCount(req.body.pickUpDate, req.body.returnDate);
                    const totalPrice = bookingHelper_1.bookingHelper.getTotlaPrice(carGroup.pricePerDay, carGroup.discountPerDay, daysCount);
                    const expiredAt = moment_timezone_1.default()
                        .add(branch.waitingHoursForConfirm, 'hour')
                        .toISOString();
                    // save
                    booking_1.BookingModel.create({
                        client: null,
                        car: null,
                        onlinePayment: null,
                        guest: guest._id,
                        carGroup: req.body.carGroup_id,
                        returnDate: req.body.returnDate,
                        pickUpDate: req.body.pickUpDate,
                        pickUpBranch: branch._id,
                        returnBranch: branch._id,
                        bookingCode: bookingCode,
                        daysCount: daysCount,
                        pricePerDay: carGroup.pricePerDay,
                        discountPerDay: carGroup.discountPerDay,
                        total: totalPrice,
                        bookingType: booking_1.EbookingType.request,
                        expiredAt: expiredAt,
                        iqama: guest.iqama
                    }).then((booking) => {
                        return res.status(200).json({
                            booking
                        });
                    })
                        .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.check_your_data, next);
                }
                ;
            }
            else {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.you_cant_booking_this_car_Group_in_this_booking_date, next);
            }
            ;
        };
        this.getOrCreateGuest = async (req, res, next) => {
            req.checkBody('name', error_enum_1.mainErrorEnum.name_required).exists();
            req.checkBody('name', error_enum_1.mainErrorEnum.name_invalid).matches(mainRegExp_1.mainRegExp.guest.name);
            req.checkBody('phone', error_enum_1.mainErrorEnum.phone_required).exists();
            req.checkBody('phone', error_enum_1.mainErrorEnum.phone_invalid).matches(mainRegExp_1.mainRegExp.guest.phone);
            req.checkBody('iqama', error_enum_1.mainErrorEnum.iqama_required).exists();
            req.checkBody('iqama', error_enum_1.mainErrorEnum.iqama_invalid).matches(mainRegExp_1.mainRegExp.guest.iqama);
            req.checkBody('iqamaExpiryDate', error_enum_1.mainErrorEnum.iqamaExpiryDate_required).exists();
            req.checkBody('iqamaExpiryDate', error_enum_1.mainErrorEnum.iqamaExpiryDate_invalid).matches(mainRegExp_1.mainRegExp.guest.iqamaExpiryDate);
            req.checkBody('drivingLicence', error_enum_1.mainErrorEnum.drivingLicence_required).exists();
            req.checkBody('drivingLicence', error_enum_1.mainErrorEnum.drivingLicence_invalid).matches(mainRegExp_1.mainRegExp.guest.drivingLicence);
            req.checkBody('drivingLicenceExpiryDate', error_enum_1.mainErrorEnum.drivingLicenceExpiryDate_required).exists();
            req.checkBody('drivingLicenceExpiryDate', error_enum_1.mainErrorEnum.drivingLicenceExpiryDate_invalid).matches(mainRegExp_1.mainRegExp.guest.drivingLicenceExpiryDate);
            req.checkBody('birthday', error_enum_1.mainErrorEnum.birthday_required).exists();
            req.checkBody('birthday', error_enum_1.mainErrorEnum.birthday_invalid).matches(mainRegExp_1.mainRegExp.guest.birthday);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            this._getOrCreateGuest(req.body)
                .then((guest) => {
                if (guest) {
                    res.status(200).json({ guest });
                }
                else {
                    expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.guest_not_found, next);
                }
                ;
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    if (_error.message.indexOf('iqama') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.iqama_used_before, next);
                    }
                    else if (_error.message.indexOf('phone') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.phone_used_before, next);
                    }
                    ;
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next);
                }
                ;
            });
        };
    }
    ;
}
;
exports.guestCTRL = new GuestCTRL();
