"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const message_1 = require("./../models/message");
const carGroupHelper_1 = require("../helpers/carGroupHelper");
const carGroup_1 = require("../models/carGroup");
const booking_1 = require("../models/booking");
const branch_1 = require("../models/branch");
const bookingHelper_1 = require("../helpers/bookingHelper");
const imagesHelper_1 = require("../helpers/imagesHelper");
const employee_1 = require("./employee");
const client_1 = require("../models/client");
const mainRegExp_1 = require("../regExp/mainRegExp");
const jwt_1 = require("../helpers/jwt");
const expressHelper_1 = require("../helpers/expressHelper");
const client_2 = require("../models/client");
const error_enum_1 = require("./../enums/error.enum");
const branch_2 = require("../models/branch");
const mainHelper_1 = require("../helpers/mainHelper");
const moment_timezone_1 = __importDefault(require("moment-timezone"));
class ClientCTRLHelper {
    constructor() { }
    ;
}
;
class ClientCTRL extends ClientCTRLHelper {
    constructor() {
        super();
        // Guard
        this.guard = jwt_1.jwt.allaw_express({
            roles: ['client'],
            dataField: 'client',
            header_name_of_token: 'token'
        });
        // Auth
        this.signup = async (req, res, next) => {
            req.checkBody('name', error_enum_1.mainErrorEnum.name_required).exists();
            req.checkBody('name', error_enum_1.mainErrorEnum.name_invalid).matches(mainRegExp_1.mainRegExp.client.name);
            req.checkBody('email', error_enum_1.mainErrorEnum.email_required).exists();
            req.checkBody('email', error_enum_1.mainErrorEnum.email_invalid).matches(mainRegExp_1.mainRegExp.client.email);
            req.checkBody('phone', error_enum_1.mainErrorEnum.phone_required).exists();
            req.checkBody('phone', error_enum_1.mainErrorEnum.phone_invalid).matches(mainRegExp_1.mainRegExp.client.phone);
            req.checkBody('password', error_enum_1.mainErrorEnum.password_required).exists();
            req.checkBody('password', error_enum_1.mainErrorEnum.password_invalid).matches(mainRegExp_1.mainRegExp.client.password);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            client_2.ClientModel.create({
                name: req.body.name,
                email: req.body.email,
                phone: req.body.phone,
                password: mainHelper_1.mainHelper.hashSync(req.body.password),
                roles: ['client'],
                signupFrom: client_1.EsignupFrom.normal_signup,
            })
                .then(async (client) => {
                const token = await jwt_1.jwt.create({
                    _id: client._id,
                    roles: client.roles
                });
                client.password = '';
                return res.status(200).json({ client, token });
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    if (_error.message.indexOf('email') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.email_used_before, next);
                    }
                    else if (_error.message.indexOf('phone') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.phone_used_before, next);
                    }
                    else if (_error.message.indexOf('iqama') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.iqama_used_before, next);
                    }
                    ;
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next);
                }
                ;
            });
        };
        this.signin = async (req, res, next) => {
            req.checkBody('emailOrPhone', error_enum_1.mainErrorEnum.emailOrPhone_required).exists();
            req.checkBody('password', error_enum_1.mainErrorEnum.password_required).exists();
            req.checkBody('password', error_enum_1.mainErrorEnum.password_invalid).matches(mainRegExp_1.mainRegExp.client.password);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const query = mainRegExp_1.mainRegExp.client.email.test(req.body.emailOrPhone)
                ? { email: req.body.emailOrPhone }
                : { phone: req.body.emailOrPhone };
            client_2.ClientModel.findOne(Object.assign({}, query, { deleted: 0 }))
                .populate('nationality')
                .lean()
                .exec()
                .then(async (client) => {
                if (client) {
                    if (mainHelper_1.mainHelper.compareSync(req.body.password, client.password)) {
                        const token = await jwt_1.jwt.create({
                            _id: client._id,
                            roles: client.roles
                        });
                        client.password = '';
                        return res.status(200).json({ client, token });
                    }
                    else {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.client_not_found, next);
                    }
                    ;
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.client_not_found, next);
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.refreshToken = async (req, res, next) => {
            client_2.ClientModel.findOne({
                _id: req.client._id,
                deleted: 0
            })
                .populate('nationality')
                .lean()
                .exec()
                .then(async (client) => {
                if (client) {
                    const token = await jwt_1.jwt.create({
                        _id: client._id,
                        roles: client.roles
                    });
                    client.password = '';
                    return res.status(200).json({
                        token, client
                    });
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.client_not_found, next);
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        // This Client
        this.viewThisClient = async (req, res, next) => {
            client_2.ClientModel.findOne({
                _id: req.client._id,
                deleted: 0,
            })
                .then(async (client) => {
                if (client) {
                    return res.status(200).json({ client });
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.client_not_found, next);
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.edit = async (req, res, next) => {
            const editObject = {};
            if (mainHelper_1.mainHelper.isExists(req.body.name)) {
                editObject.name = req.body.name;
                req.checkBody('name', error_enum_1.mainErrorEnum.name_invalid).matches(mainRegExp_1.mainRegExp.client.name);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.phone)) {
                editObject.phone = req.body.phone;
                req.checkBody('phone', error_enum_1.mainErrorEnum.phone_invalid).matches(mainRegExp_1.mainRegExp.client.phone);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.email)) {
                editObject.email = req.body.email;
                req.checkBody('email', error_enum_1.mainErrorEnum.email_invalid).matches(mainRegExp_1.mainRegExp.client.email);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.iqama)) {
                editObject.iqama = req.body.iqama;
                req.checkBody('iqama', error_enum_1.mainErrorEnum.iqama_invalid).matches(mainRegExp_1.mainRegExp.client.iqama);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.iqamaExpiryDate)) {
                editObject.iqamaExpiryDate = req.body.iqamaExpiryDate;
                req.checkBody('iqamaExpiryDate', error_enum_1.mainErrorEnum.iqamaExpiryDate_invalid).matches(mainRegExp_1.mainRegExp.client.iqamaExpiryDate);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.drivingLicence)) {
                editObject.drivingLicence = req.body.drivingLicence;
                req.checkBody('drivingLicence', error_enum_1.mainErrorEnum.drivingLicence_invalid).matches(mainRegExp_1.mainRegExp.client.drivingLicence);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.drivingLicenceExpiryDate)) {
                editObject.drivingLicenceExpiryDate = req.body.drivingLicenceExpiryDate;
                req.checkBody('drivingLicenceExpiryDate', error_enum_1.mainErrorEnum.drivingLicenceExpiryDate_invalid).matches(mainRegExp_1.mainRegExp.client.drivingLicenceExpiryDate);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.birthday)) {
                editObject.birthday = req.body.birthday;
                req.checkBody('birthday', error_enum_1.mainErrorEnum.birthday_invalid).matches(mainRegExp_1.mainRegExp.client.birthday);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.address)) {
                editObject.address = req.body.address;
                req.checkBody('address', error_enum_1.mainErrorEnum.address_invalid).matches(mainRegExp_1.mainRegExp.client.address);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.job)) {
                editObject.job = req.body.job;
                req.checkBody('job', error_enum_1.mainErrorEnum.job_invalid).matches(mainRegExp_1.mainRegExp.client.job);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.nationality_id)) {
                editObject.nationality = req.body.nationality_id;
                req.checkBody('nationality_id', error_enum_1.mainErrorEnum.nationality_id_invalid).matches(mainRegExp_1.mainRegExp.nationality._id);
            }
            ;
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (Object.keys(editObject).length === 0) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.edit_date_not_found, next);
            }
            ;
            client_2.ClientModel.updateOne({
                _id: req.client._id,
                deleted: 0
            }, Object.assign({}, editObject))
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.client_not_found, next);
                }
                else {
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    if (_error.message.indexOf('email') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.email_used_before, next);
                    }
                    ;
                    if (_error.message.indexOf('phone') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.email_used_before, next);
                    }
                    ;
                    if (_error.message.indexOf('iqama') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.iqama_used_before, next);
                    }
                    ;
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next);
                }
                ;
            });
        };
        this.editPassword = async (req, res, next) => {
            req.checkBody('password', error_enum_1.mainErrorEnum.password_required).exists();
            req.checkBody('password', error_enum_1.mainErrorEnum.password_invalid).matches(mainRegExp_1.mainRegExp.client.password);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            client_2.ClientModel.updateOne({
                _id: req.client._id,
                deleted: 0
            }, {
                password: mainHelper_1.mainHelper.hashSync(req.body.password),
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.client_not_found, next);
                }
                else {
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.addClientImage = async (req, res, next) => {
            if (req.files) {
                imagesHelper_1.imageHelper.uploadImage(req.files.image)
                    .then(src => {
                    client_2.ClientModel.findOneAndUpdate({
                        _id: req.client._id,
                    }, {
                        image: src
                    })
                        .select('image -_id')
                        .exec()
                        .then(async (client) => {
                        if (!client) {
                            return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.client_not_found, next);
                        }
                        else {
                            if (client.image)
                                imagesHelper_1.imageHelper.deleteImage(client.image);
                            return res.status(200).json({ image: src });
                        }
                        ;
                    })
                        .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
                })
                    .catch(() => {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.image_not_found, next);
                });
            }
            else {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.image_not_found, next);
            }
            ;
        };
        // booking
        this.addBooking = async (req, res, next) => {
            req.checkBody('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_required).exists();
            req.checkBody('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_invalid).matches(mainRegExp_1.mainRegExp.carGroup._id);
            req.checkBody('pickUpDate', error_enum_1.mainErrorEnum.pickUpDate_required).exists();
            req.checkBody('pickUpDate', error_enum_1.mainErrorEnum.pickUpDate_invalid).matches(mainRegExp_1.mainRegExp.booking.pickUpDate);
            req.checkBody('returnDate', error_enum_1.mainErrorEnum.returnDate_required).exists();
            req.checkBody('returnDate', error_enum_1.mainErrorEnum.returnDate_invalid).matches(mainRegExp_1.mainRegExp.booking.returnDate);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const isCanBookingCarGroupInDate = await bookingHelper_1.bookingHelper.isCanBookingCarGroupInDate(req.body.carGroup_id, req.body.pickUpDate, req.body.returnDate);
            if (isCanBookingCarGroupInDate) {
                const client = await client_2.ClientModel.findOne({
                    _id: req.client._id,
                    iqama: { $exists: true },
                    iqamaExpiryDate: { $exists: true },
                    drivingLicence: { $exists: true },
                    drivingLicenceExpiryDate: { $exists: true },
                    birthday: { $exists: true },
                })
                    .select('iqama')
                    .exec();
                const carGroup = await carGroup_1.CarGroupModel.findOne({
                    _id: req.body.carGroup_id
                })
                    .select('branch pricePerDay discountPerDay')
                    .exec();
                const branch = await branch_2.BranchModel.findOne({
                    _id: carGroup.branch
                })
                    .select('bookingStartCode waitingHoursForConfirm')
                    .exec();
                if (client && carGroup && branch) {
                    const bookingCode = `${branch.bookingStartCode}${await branch_1.branchBookingCounter()}`;
                    const daysCount = bookingHelper_1.bookingHelper.getDaysCount(req.body.pickUpDate, req.body.returnDate);
                    const totalPrice = bookingHelper_1.bookingHelper.getTotlaPrice(carGroup.pricePerDay, carGroup.discountPerDay, daysCount);
                    const expiredAt = moment_timezone_1.default()
                        .add(branch.waitingHoursForConfirm, 'hour')
                        .toISOString();
                    // save
                    booking_1.BookingModel.create({
                        guest: null,
                        car: null,
                        onlinePayment: null,
                        client: req.client._id,
                        carGroup: req.body.carGroup_id,
                        returnDate: req.body.returnDate,
                        pickUpDate: req.body.pickUpDate,
                        pickUpBranch: branch._id,
                        returnBranch: branch._id,
                        bookingCode: bookingCode,
                        daysCount: daysCount,
                        pricePerDay: carGroup.pricePerDay,
                        discountPerDay: carGroup.discountPerDay,
                        total: totalPrice,
                        bookingType: booking_1.EbookingType.request,
                        expiredAt: expiredAt,
                        iqama: client.iqama
                    }).then((booking) => {
                        return res.status(200).json({
                            booking
                        });
                    })
                        .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.check_your_data, next);
                }
                ;
            }
            else {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.you_cant_booking_this_car_Group_in_this_booking_date, next);
            }
            ;
        };
        this.viewThisClientBooking = async (req, res, next) => {
            req.checkQuery('booking_id', error_enum_1.mainErrorEnum.booking_id_required).exists();
            req.checkQuery('booking_id', error_enum_1.mainErrorEnum.booking_id_invalid).matches(mainRegExp_1.mainRegExp.booking._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            booking_1.BookingModel.findOne({
                _id: req.query.booking_id,
                client: req.client._id,
                deleted: 0
            })
                .populate({
                path: 'returnBranch',
                select: 'nameEn nameAr'
            })
                .populate({
                path: 'pickUpBranch',
                select: 'nameEn nameAr'
            })
                .populate({
                path: 'car',
                select: 'plateNumber'
            })
                .populate({
                path: 'carGroup',
                select: 'carModel',
                populate: {
                    path: 'carModel',
                    select: 'nameEn nameAr',
                }
            })
                .populate({
                path: 'carGroup',
                populate: {
                    path: 'carType',
                    select: 'nameEn nameAr',
                }
            })
                .lean()
                .exec()
                .then(async (booking) => {
                if (booking) {
                    return res.status(200).json({
                        booking: await bookingHelper_1.bookingHelper.setBookingType(booking)
                    });
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.booking_not_found, next);
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewThisClientBookings = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_required).exists();
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_invalid).matches(mainRegExp_1.mainRegExp.view.sortBy);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            booking_1.BookingModel.find({
                client: req.client._id,
                deleted: 0
            })
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .populate({
                path: 'returnBranch',
                select: 'nameEn nameAr'
            })
                .populate({
                path: 'pickUpBranch',
                select: 'nameEn nameAr'
            })
                .populate({
                path: 'car',
                select: 'plateNumber'
            })
                .populate({
                path: 'carGroup',
                select: 'carModel',
                populate: {
                    path: 'carModel',
                    select: 'nameEn nameAr',
                }
            })
                .populate({
                path: 'carGroup',
                populate: {
                    path: 'carType',
                    select: 'nameEn nameAr',
                }
            })
                .lean()
                .exec()
                .then(async (bookings) => {
                res.status(200).json({
                    bookings: bookingHelper_1.bookingHelper.setBookingsType(bookings)
                });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewThisClientBookingsSearch = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('searchQuery', error_enum_1.mainErrorEnum.searchQuery_required).exists();
            req.checkQuery('searchQuery', error_enum_1.mainErrorEnum.searchQuery_invalid).matches(mainRegExp_1.mainRegExp.view.searchQuery);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            booking_1.BookingModel.find({
                $text: { $search: req.query.searchQuery },
                client: req.client._id
            })
                .sort('-createdAt')
                .skip((page - 1) * limit)
                .limit(limit)
                .populate({
                path: 'returnBranch',
                select: 'nameEn nameAr'
            })
                .populate({
                path: 'pickUpBranch',
                select: 'nameEn nameAr'
            })
                .populate({
                path: 'car',
                select: 'plateNumber'
            })
                .populate({
                path: 'carGroup',
                select: 'carModel',
                populate: {
                    path: 'carModel',
                    select: 'nameEn nameAr',
                }
            })
                .populate({
                path: 'carGroup',
                populate: {
                    path: 'carType',
                    select: 'nameEn nameAr',
                }
            })
                .lean()
                .exec()
                .then(async (bookings) => {
                res.status(200).json({
                    bookings: bookingHelper_1.bookingHelper.setBookingsType(bookings)
                });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewBookingByCodeAndIqama = async (req, res, next) => {
            req.checkQuery('iqama', error_enum_1.mainErrorEnum.iqama_required).exists();
            req.checkQuery('iqama', error_enum_1.mainErrorEnum.iqama_invalid).matches(mainRegExp_1.mainRegExp.booking.iqama);
            req.checkQuery('bookingCode', error_enum_1.mainErrorEnum.bookingCode_required).exists();
            req.checkQuery('bookingCode', error_enum_1.mainErrorEnum.bookingCode_invalid).matches(mainRegExp_1.mainRegExp.booking.bookingCode);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            booking_1.BookingModel.findOne({
                bookingCode: req.query.bookingCode,
                iqama: req.query.iqama,
                deleted: 0
            })
                .populate('guest onlinePayment')
                .populate({
                path: 'returnBranch',
                select: 'nameEn nameAr'
            })
                .populate({
                path: 'pickUpBranch',
                select: 'nameEn nameAr'
            })
                .populate({
                path: 'client',
                select: 'name'
            })
                .populate({
                path: 'guest',
                select: 'name'
            })
                .populate({
                path: 'car',
                select: 'plateNumber'
            })
                .populate({
                path: 'carGroup',
                select: 'branch',
                populate: {
                    path: 'branch',
                    select: 'nameEn nameAr',
                }
            })
                .populate({
                path: 'carGroup',
                select: 'carModel',
                populate: {
                    path: 'carModel',
                    select: 'nameEn nameAr',
                }
            })
                .populate({
                path: 'carGroup',
                populate: {
                    path: 'carType',
                    select: 'nameEn nameAr',
                }
            })
                .lean()
                .exec()
                .then(async (booking) => {
                if (booking) {
                    return res.status(200).json({
                        booking: await bookingHelper_1.bookingHelper.setBookingType(booking)
                    });
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.booking_not_found, next);
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        // CarGroup
        this.viewCarGroup = async (req, res, next) => {
            req.checkQuery('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_required).exists();
            req.checkQuery('carGroup_id', error_enum_1.mainErrorEnum.carGroup_id_invalid).matches(mainRegExp_1.mainRegExp.carGroup._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            carGroup_1.CarGroupModel.findOne({
                _id: req.query.carGroup_id,
                deleted: 0
            })
                .populate('carType carModel branch')
                .lean()
                .exec()
                .then(async (carGroup) => {
                if (carGroup) {
                    await carGroupHelper_1.carGroupHelper.setViewInCarGroup(carGroup);
                    res.status(200).json({
                        carGroup: await carGroupHelper_1.carGroupHelper.setCarsCountInCarGroup(carGroup)
                    });
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.carGroup_not_found, next);
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewCarGroupsToBookingOne = async (req, res, next) => {
            const $event = {
                page: req.body.page,
                limit: req.body.limit,
                pickUpDate: req.body.pickUpDate,
                returnDate: req.body.returnDate,
                filters: {}
            };
            req.checkBody('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkBody('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkBody('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkBody('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkBody('pickUpDate', error_enum_1.mainErrorEnum.pickUpDate_required).exists();
            req.checkBody('pickUpDate', error_enum_1.mainErrorEnum.pickUpDate_invalid).matches(mainRegExp_1.mainRegExp.booking.pickUpDate);
            req.checkBody('returnDate', error_enum_1.mainErrorEnum.returnDate_required).exists();
            req.checkBody('returnDate', error_enum_1.mainErrorEnum.returnDate_invalid).matches(mainRegExp_1.mainRegExp.booking.returnDate);
            // isExists
            if (mainHelper_1.mainHelper.isExists(req.body.city_id)) {
                req.checkBody('city_id', error_enum_1.mainErrorEnum.city_id_invalid).matches(mainRegExp_1.mainRegExp.city._id);
                $event.filters.branch = {
                    $in: await bookingHelper_1.bookingHelper.getBranches_idsByCity_id(req.body.city_id)
                };
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.filters.branch_id)) {
                req.checkBody('filters.branch_id', error_enum_1.mainErrorEnum.branch_id_invalid).matches(mainRegExp_1.mainRegExp.branch._id);
                $event.filters.branch = req.body.filters.branch_id;
            }
            ;
            // isExists
            if (mainHelper_1.mainHelper.isExists(req.body.filters.carModel_id)) {
                req.checkBody('filters.carModel_id', error_enum_1.mainErrorEnum.carModel_id_invalid).matches(mainRegExp_1.mainRegExp.carModel._id);
                $event.filters.carModel = req.body.filters.carModel_id;
            }
            ;
            // isExists
            if (mainHelper_1.mainHelper.isExists(req.body.filters.carType_id)) {
                req.checkBody('filters.carType_id', error_enum_1.mainErrorEnum.carType_id_invalid).matches(mainRegExp_1.mainRegExp.carType._id);
                $event.filters.carType = req.body.filters.carType_id;
            }
            ;
            // isExists
            if (mainHelper_1.mainHelper.isExists(req.body.filters.year)) {
                req.checkBody('filters.year', error_enum_1.mainErrorEnum.year_invalid).matches(mainRegExp_1.mainRegExp.carGroup.year);
                $event.filters.year = req.body.filters.year;
            }
            ;
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            bookingHelper_1.bookingHelper.viewCarGroupsToBookingOne($event)
                .then(async (carGroups) => {
                await carGroupHelper_1.carGroupHelper.setViewInCarGroups(carGroups);
                res.status(200).json({
                    carGroups: await carGroupHelper_1.carGroupHelper.setCarsCountInCarGroups(carGroups)
                });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        // Branch
        this.viewAllBranches = async (req, res, next) => {
            branch_2.BranchModel.find({
                deleted: 0
            })
                .sort('-createdAt')
                .populate('city')
                .exec()
                .then(async (branches) => {
                res.status(200).json({ branches });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewAllBranchesLocation = async (req, res, next) => {
            branch_2.BranchModel.find({
                deleted: 0,
                location: {
                    $exists: true
                }
            })
                .sort('-createdAt')
                .select('location city nameEn nameAr')
                .populate('city')
                .exec()
                .then(async (branches) => {
                res.status(200).json({ branches });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewBranchesNearLocation = async (req, res, next) => {
            req.checkBody('location', error_enum_1.mainErrorEnum.location_required).exists();
            req.checkBody('location', error_enum_1.mainErrorEnum.location_invalid).isLocation();
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            branch_2.BranchModel.aggregate([
                { "$geoNear": {
                        "near": { "type": "Point", "coordinates": req.body.location },
                        "distanceField": "dist",
                        "maxDistance": 2,
                        "spherical": true
                    } },
                { "$sort": { "dist": 1, "_id": 1 } }
            ])
                .exec()
                .then(async (branches) => {
                res.status(200).json({ branches });
            })
                .catch(() => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        // Message
        this.sendMessage = async (req, res, next) => {
            req.checkBody('name', error_enum_1.mainErrorEnum.name_required).exists();
            req.checkBody('name', error_enum_1.mainErrorEnum.name_invalid).matches(mainRegExp_1.mainRegExp.message.name);
            req.checkBody('email', error_enum_1.mainErrorEnum.email_required).exists();
            req.checkBody('email', error_enum_1.mainErrorEnum.email_invalid).matches(mainRegExp_1.mainRegExp.message.email);
            req.checkBody('message', error_enum_1.mainErrorEnum.message_required).exists();
            req.checkBody('message', error_enum_1.mainErrorEnum.message_invalid).matches(mainRegExp_1.mainRegExp.message.message);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            message_1.MessageModel.create({
                name: req.body.name,
                email: req.body.email,
                message: req.body.message,
            })
                .then(() => {
                res.status(200).end();
            })
                .catch(() => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        // options
        this.viewAllNationalities = async (req, res, next) => {
            employee_1.employeeCTRL.viewAllNationalities(req, res, next);
        };
        this.viewAllCarTypes = async (req, res, next) => {
            employee_1.employeeCTRL.viewAllCarTypes(req, res, next);
        };
        this.viewAllCarModelsByCarType = async (req, res, next) => {
            employee_1.employeeCTRL.viewAllCarModelsByCarType(req, res, next);
        };
        this.viewAllCities = async (req, res, next) => {
            employee_1.employeeCTRL.viewAllCities(req, res, next);
        };
        this.viewAllBranchesByCityFilter = async (req, res, next) => {
            req.checkQuery('city_id', error_enum_1.mainErrorEnum.city_id_required).exists();
            req.checkQuery('city_id', error_enum_1.mainErrorEnum.city_id_invalid).matches(mainRegExp_1.mainRegExp.city._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            branch_2.BranchModel.find({
                deleted: 0,
                city: req.query.city_id
            })
                .sort('-createdAt')
                .exec()
                .then(async (branches) => {
                res.status(200).json({ branches });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
    }
    ;
}
;
exports.clientCTRL = new ClientCTRL();
