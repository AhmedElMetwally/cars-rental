"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const error_enum_1 = require("./../enums/error.enum");
const employeeTimerHelper_1 = require("../helpers/employeeTimerHelper");
const mainHelper_1 = require("../helpers/mainHelper");
const employeeAction_1 = require("../models/employeeAction");
const employeeTimer_1 = require("../models/employeeTimer");
const mainRegExp_1 = require("../regExp/mainRegExp");
const jwt_1 = require("../helpers/jwt");
const expressHelper_1 = require("../helpers/expressHelper");
const employee_1 = require("../models/employee");
const branch_1 = require("../models/branch");
const employeeGroup_1 = require("../models/employeeGroup");
const admin_1 = require("../models/admin");
const socketIoEmployee_1 = require("../socket.io/socketIoEmployee");
class AdminCTRLHelper extends socketIoEmployee_1.SocketIoEmployee {
    constructor() {
        super();
        // use in AdminCTRL
        this.setIsOnlineInEmployees = async (employees) => {
            const onlineEmployees_ids = await this.getAllOnlineEmployees_ids();
            for (let i = 0, len = employees.length; i < len; ++i) {
                employees[i].isOnline = onlineEmployees_ids.indexOf(employees[i]._id.toString()) !== -1 ? 1 : 0;
            }
            ;
            return employees;
        };
        this.setIsOnlineInEmployee = async (employee) => {
            employee.isOnline = await this.isEmployeeOnline(employee._id);
            return employee;
        };
    }
    ;
}
;
// use in admin router
class AdminCTRL extends AdminCTRLHelper {
    constructor() {
        super();
        // Guard
        this.guard = jwt_1.jwt.allaw_express({
            roles: ['admin'],
            dataField: 'admin',
            header_name_of_token: 'token'
        });
        // Auth
        this.signin = async (req, res, next) => {
            req.checkBody('email', error_enum_1.mainErrorEnum.email_required).exists();
            req.checkBody('email', error_enum_1.mainErrorEnum.email_invalid).matches(mainRegExp_1.mainRegExp.admin.email);
            req.checkBody('password', error_enum_1.mainErrorEnum.password_required).exists();
            req.checkBody('password', error_enum_1.mainErrorEnum.password_invalid).matches(mainRegExp_1.mainRegExp.admin.password);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            admin_1.AdminModel.findOne({
                email: req.body.email,
                password: req.body.password
            })
                .exec()
                .then(async (admin) => {
                if (!admin) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.admin_not_found, next);
                }
                else {
                    const token = await jwt_1.jwt.create({
                        _id: admin._id,
                        roles: admin.roles
                    });
                    admin.password = '';
                    res.status(200).json({ admin, token });
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.signup = async (req, res, next) => {
            req.checkBody('email', error_enum_1.mainErrorEnum.email_required).exists();
            req.checkBody('email', error_enum_1.mainErrorEnum.email_invalid).matches(mainRegExp_1.mainRegExp.admin.email);
            req.checkBody('password', error_enum_1.mainErrorEnum.password_required).exists();
            req.checkBody('password', error_enum_1.mainErrorEnum.password_invalid).matches(mainRegExp_1.mainRegExp.admin.password);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            admin_1.AdminModel.create({
                email: req.body.email,
                password: req.body.password,
                roles: ['admin']
            })
                .then(async (admin) => {
                const token = await jwt_1.jwt.create({
                    _id: admin._id,
                    roles: admin.roles
                });
                admin.password = '';
                res.status(200).json({ admin, token });
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.email_used_before, next);
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next);
                }
                ;
            });
        };
        this.refreshToken = async (req, res, next) => {
            admin_1.AdminModel.findOne({
                _id: req.admin._id,
            })
                .exec()
                .then(async (admin) => {
                if (!admin) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.admin_not_found, next);
                }
                else {
                    const token = await jwt_1.jwt.create({
                        _id: admin._id,
                        roles: admin.roles
                    });
                    admin.password = '';
                    return res.status(200).json({
                        token, admin
                    });
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        // Employee Group
        this.addEmployeeGroup = async (req, res, next) => {
            req.checkBody('nameEn', error_enum_1.mainErrorEnum.nameEn_required).exists();
            req.checkBody('nameEn', error_enum_1.mainErrorEnum.nameEn_invalid).matches(mainRegExp_1.mainRegExp.employeeGroup.nameEn);
            req.checkBody('nameAr', error_enum_1.mainErrorEnum.nameAr_required).exists();
            req.checkBody('nameAr', error_enum_1.mainErrorEnum.nameAr_invalid).matches(mainRegExp_1.mainRegExp.employeeGroup.nameAr);
            req.checkBody('screens', error_enum_1.mainErrorEnum.screens_required).exists();
            req.checkBody('screens', error_enum_1.mainErrorEnum.screens_invalid).checkScreens();
            req.checkBody('branches', error_enum_1.mainErrorEnum.branches_required).exists();
            req.checkBody('branches', error_enum_1.mainErrorEnum.branches_invalid).checkBranches();
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            employeeGroup_1.EmployeeGroupModel.create({
                nameEn: req.body.nameEn,
                nameAr: req.body.nameAr,
                screens: req.body.screens,
                branches: req.body.branches,
            })
                .then(async (group) => {
                res.status(200).json({ _id: group._id });
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    if (_error.message.indexOf('nameEn') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employeeGroup_nameEn_used_before, next);
                    }
                    ;
                    if (_error.message.indexOf('nameAr') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employeeGroup_nameAr_used_before, next);
                    }
                    ;
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next);
                }
                ;
            });
        };
        this.editEmployeeGroup = async (req, res, next) => {
            req.checkBody('employeeGroup_id', error_enum_1.mainErrorEnum.employeeGroup_id_required).exists();
            req.checkBody('employeeGroup_id', error_enum_1.mainErrorEnum.employeeGroup_id_invalid).matches(mainRegExp_1.mainRegExp.employeeGroup._id);
            let editObject = {};
            if (mainHelper_1.mainHelper.isExists(req.body.nameEn)) {
                editObject.nameEn = req.body.nameEn;
                req.checkBody('nameEn', error_enum_1.mainErrorEnum.nameEn_invalid).matches(mainRegExp_1.mainRegExp.employeeGroup.nameEn);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.nameAr)) {
                editObject.nameAr = req.body.nameAr;
                req.checkBody('nameAr', error_enum_1.mainErrorEnum.nameAr_invalid).matches(mainRegExp_1.mainRegExp.employeeGroup.nameAr);
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.screens)) {
                editObject.screens = req.body.screens;
                req.checkBody('screens', error_enum_1.mainErrorEnum.screens_invalid).checkScreens();
            }
            ;
            if (mainHelper_1.mainHelper.isExists(req.body.branches)) {
                editObject.branches = req.body.branches;
                req.checkBody('branches', error_enum_1.mainErrorEnum.branches_invalid).checkBranches();
            }
            ;
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (Object.keys(editObject).length === 0) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.edit_date_not_found, next);
            }
            ;
            employeeGroup_1.EmployeeGroupModel.updateOne({
                _id: req.body.employeeGroup_id
            }, Object.assign({}, editObject))
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employeeGroup_not_found, next);
                }
                else {
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    if (_error.message.indexOf('nameEn') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employeeGroup_nameEn_used_before, next);
                    }
                    ;
                    if (_error.message.indexOf('nameAr') !== -1) {
                        return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employeeGroup_nameAr_used_before, next);
                    }
                    ;
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next);
                }
                ;
            });
        };
        this.deleteEmployeeGroup = async (req, res, next) => {
            req.checkQuery('employeeGroup_id', error_enum_1.mainErrorEnum.employeeGroup_id_required).exists();
            req.checkQuery('employeeGroup_id', error_enum_1.mainErrorEnum.employeeGroup_id_invalid).matches(mainRegExp_1.mainRegExp.employeeGroup._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            employeeGroup_1.EmployeeGroupModel.updateOne({
                _id: req.query.employeeGroup_id,
                deleted: 0
            }, {
                deleted: 1
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employeeGroup_not_found, next);
                }
                else {
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.restoreEmployeeGroup = async (req, res, next) => {
            req.checkBody('employeeGroup_id', error_enum_1.mainErrorEnum.employeeGroup_id_required).exists();
            req.checkBody('employeeGroup_id', error_enum_1.mainErrorEnum.employeeGroup_id_invalid).matches(mainRegExp_1.mainRegExp.employeeGroup._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            employeeGroup_1.EmployeeGroupModel.updateOne({
                _id: req.body.employeeGroup_id,
                deleted: 1
            }, {
                deleted: 0
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employeeGroup_not_found, next);
                }
                else {
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewEmployeeGroup = async (req, res, next) => {
            req.checkQuery('employeeGroup_id', error_enum_1.mainErrorEnum.employeeGroup_id_required).exists();
            req.checkQuery('employeeGroup_id', error_enum_1.mainErrorEnum.employeeGroup_id_invalid).matches(mainRegExp_1.mainRegExp.employeeGroup._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            employeeGroup_1.EmployeeGroupModel.findOne({
                _id: req.query.employeeGroup_id,
            })
                .populate({
                path: 'branches',
                select: 'nameEn nameAr'
            })
                .exec()
                .then(async (employeeGroup) => {
                if (!employeeGroup) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employeeGroup_not_found, next);
                }
                else {
                    return res.status(200).json({ employeeGroup });
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewEmployeeGroups = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_required).exists();
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_invalid).matches(mainRegExp_1.mainRegExp.view.sortBy);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            employeeGroup_1.EmployeeGroupModel.find({})
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .populate({
                path: 'branches',
                select: 'nameEn nameAr'
            })
                .exec()
                .then(async (employeeGroups) => {
                return res.status(200).json({ employeeGroups });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewEmployeeGroupsSearch = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('searchQuery', error_enum_1.mainErrorEnum.searchQuery_required).exists();
            req.checkQuery('searchQuery', error_enum_1.mainErrorEnum.searchQuery_invalid).matches(mainRegExp_1.mainRegExp.view.searchQuery);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            employeeGroup_1.EmployeeGroupModel.find({
                $text: { $search: req.query.searchQuery },
            })
                .sort('-createdAt')
                .skip((page - 1) * limit)
                .limit(limit)
                .populate({
                path: 'branches',
                select: 'nameEn nameAr'
            })
                .exec()
                .then(async (employeeGroups) => {
                return res.status(200).json({ employeeGroups });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        // Employee 
        this.addEmployee = async (req, res, next) => {
            req.checkBody('name', error_enum_1.mainErrorEnum.name_required).exists();
            req.checkBody('name', error_enum_1.mainErrorEnum.name_invalid).matches(mainRegExp_1.mainRegExp.employee.name);
            req.checkBody('email', error_enum_1.mainErrorEnum.email_required).exists();
            req.checkBody('email', error_enum_1.mainErrorEnum.email_invalid).matches(mainRegExp_1.mainRegExp.employee.email);
            req.checkBody('password', error_enum_1.mainErrorEnum.password_required).exists();
            req.checkBody('password', error_enum_1.mainErrorEnum.password_invalid).matches(mainRegExp_1.mainRegExp.employee.password);
            req.checkBody('employeeGroup_id', error_enum_1.mainErrorEnum.employeeGroup_id_required).exists();
            req.checkBody('employeeGroup_id', error_enum_1.mainErrorEnum.employeeGroup_id_invalid).matches(mainRegExp_1.mainRegExp.employeeGroup._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            employee_1.EmployeeModel.create({
                name: req.body.name,
                email: req.body.email,
                password: mainHelper_1.mainHelper.hashSync(req.body.password),
                employeeGroup: req.body.employeeGroup_id,
                roles: ['employee']
            })
                .then(async (employee) => {
                res.status(200).json({ _id: employee._id });
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.email_used_before, next);
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next);
                }
                ;
            });
        };
        this.editEmployee = async (req, res, next) => {
            req.checkBody('employee_id', error_enum_1.mainErrorEnum.employee_id_required).exists();
            req.checkBody('employee_id', error_enum_1.mainErrorEnum.employee_id_invalid).matches(mainRegExp_1.mainRegExp.employee._id);
            let editObject = {};
            if (typeof req.body.name !== 'undefined') {
                editObject.name = req.body.name;
                req.checkBody('name', error_enum_1.mainErrorEnum.name_invalid).matches(mainRegExp_1.mainRegExp.employee.name);
            }
            ;
            if (typeof req.body.email !== 'undefined') {
                editObject.email = req.body.email;
                req.checkBody('email', error_enum_1.mainErrorEnum.email_invalid).matches(mainRegExp_1.mainRegExp.employee.email);
            }
            ;
            if (typeof req.body.employeeGroup_id !== 'undefined') {
                editObject.employeeGroup = req.body.employeeGroup_id;
                req.checkBody('employeeGroup_id', error_enum_1.mainErrorEnum.employeeGroup_id_invalid).matches(mainRegExp_1.mainRegExp.employeeGroup._id);
            }
            ;
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            if (Object.keys(editObject).length === 0) {
                return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employee_not_found, next);
            }
            ;
            employee_1.EmployeeModel.updateOne({
                _id: req.body.employee_id
            }, Object.assign({}, editObject))
                .exec()
                .then((result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employee_not_found, next);
                }
                else {
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.email_used_before, next);
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next);
                }
                ;
            });
        };
        this.editEmployeePassword = async (req, res, next) => {
            req.checkBody('employee_id', error_enum_1.mainErrorEnum.employee_id_required).exists();
            req.checkBody('employee_id', error_enum_1.mainErrorEnum.employee_id_invalid).matches(mainRegExp_1.mainRegExp.employee._id);
            req.checkBody('password', error_enum_1.mainErrorEnum.password_required).exists();
            req.checkBody('password', error_enum_1.mainErrorEnum.password_invalid).matches(mainRegExp_1.mainRegExp.employee.password);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            employee_1.EmployeeModel.updateOne({
                _id: req.body.employee_id
            }, {
                password: mainHelper_1.mainHelper.hashSync(req.body.password)
            })
                .exec()
                .then((result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employee_not_found, next);
                }
                else {
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => {
                if (mainHelper_1.mainHelper.isUniqueError(_error)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.email_used_before, next);
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next);
                }
                ;
            });
        };
        this.deleteEmployee = async (req, res, next) => {
            req.checkQuery('employee_id', error_enum_1.mainErrorEnum.employee_id_required).exists();
            req.checkQuery('employee_id', error_enum_1.mainErrorEnum.employee_id_invalid).matches(mainRegExp_1.mainRegExp.employee._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            employee_1.EmployeeModel.updateOne({
                _id: req.query.employee_id,
                deleted: 0
            }, {
                deleted: 1
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employee_not_found, next);
                }
                else {
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.restoreEmployee = async (req, res, next) => {
            req.checkBody('employee_id', error_enum_1.mainErrorEnum.employee_id_required).exists();
            req.checkBody('employee_id', error_enum_1.mainErrorEnum.employee_id_invalid).matches(mainRegExp_1.mainRegExp.employee._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            employee_1.EmployeeModel.updateOne({
                _id: req.body.employee_id,
                deleted: 1
            }, {
                deleted: 0
            })
                .exec()
                .then(async (result) => {
                if (mainHelper_1.mainHelper.isUpdateFail(result)) {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employee_not_found, next);
                }
                else {
                    return res.status(200).end();
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewEmployee = async (req, res, next) => {
            req.checkQuery('employee_id', error_enum_1.mainErrorEnum.employee_id_required).exists();
            req.checkQuery('employee_id', error_enum_1.mainErrorEnum.employee_id_invalid).matches(mainRegExp_1.mainRegExp.employee._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            employee_1.EmployeeModel.findOne({
                _id: req.query.employee_id
            })
                .populate({
                path: 'employeeGroup',
            })
                .lean()
                .exec()
                .then(async (employee) => {
                if (employee) {
                    return res.status(200).json({
                        employee: await this.setIsOnlineInEmployee(employee)
                    });
                }
                else {
                    return expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.employee_not_found, next);
                }
                ;
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewEmployees = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_required).exists();
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_invalid).matches(mainRegExp_1.mainRegExp.view.sortBy);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            employee_1.EmployeeModel.find({})
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .populate({
                path: 'employeeGroup',
                populate: {
                    path: 'branches',
                }
            })
                .lean()
                .exec()
                .then(async (employees) => {
                res.status(200).json({
                    employees: await this.setIsOnlineInEmployees(employees)
                });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewEmployeesSearch = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('searchQuery', error_enum_1.mainErrorEnum.searchQuery_required).exists();
            req.checkQuery('searchQuery', error_enum_1.mainErrorEnum.searchQuery_invalid).matches(mainRegExp_1.mainRegExp.view.searchQuery);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            employee_1.EmployeeModel.find({
                $text: { $search: req.query.searchQuery }
            })
                .sort('-createdAt')
                .skip((page - 1) * limit)
                .limit(limit)
                .populate({
                path: 'employeeGroup',
                populate: {
                    path: 'branches',
                }
            })
                .lean()
                .exec()
                .then(async (employees) => {
                res.status(200).json({
                    employees: await this.setIsOnlineInEmployees(employees)
                });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewEmployeesByEmployeeGroupFilter = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_required).exists();
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_invalid).matches(mainRegExp_1.mainRegExp.view.sortBy);
            req.checkQuery('employeeGroup_id', error_enum_1.mainErrorEnum.employeeGroup_id_required).exists();
            req.checkQuery('employeeGroup_id', error_enum_1.mainErrorEnum.employeeGroup_id_invalid).matches(mainRegExp_1.mainRegExp.employeeGroup._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            employee_1.EmployeeModel.find({
                employeeGroup: req.query.employeeGroup_id
            })
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .populate({
                path: 'employeeGroup',
                populate: {
                    path: 'branches',
                }
            })
                .lean()
                .exec()
                .then(async (employees) => {
                res.status(200).json({
                    employees: await this.setIsOnlineInEmployees(employees)
                });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewEmployeeTimeStatisticsInYear = async (req, res, next) => {
            req.checkQuery('year', error_enum_1.mainErrorEnum.year_required).exists();
            req.checkQuery('year', error_enum_1.mainErrorEnum.year_invalid).matches(mainRegExp_1.mainRegExp.employeeTimer.year);
            req.checkQuery('employee_id', error_enum_1.mainErrorEnum.employee_id_required).exists();
            req.checkQuery('employee_id', error_enum_1.mainErrorEnum.employee_id_invalid).matches(mainRegExp_1.mainRegExp.employee._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const year = parseInt(req.query.year);
            employeeTimer_1.EmployeeTimerModel.find({
                employee_id: req.query.employee_id,
                date: {
                    $gte: `${year}-01-01`,
                    $lte: `${year}-12-31`,
                }
            })
                .select('-_id timeMs date')
                .exec()
                .then(async (statistics) => {
                res.status(200).json({ statistics });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewEmployeeTimeStatisticsInMonth = async (req, res, next) => {
            // year 2018 2019 2020
            // month 1 - 12
            req.checkQuery('year', error_enum_1.mainErrorEnum.year_required).exists();
            req.checkQuery('year', error_enum_1.mainErrorEnum.year_invalid).matches(mainRegExp_1.mainRegExp.employeeTimer.year);
            req.checkQuery('month', error_enum_1.mainErrorEnum.month_required).exists();
            req.checkQuery('month', error_enum_1.mainErrorEnum.month_invalid).matches(mainRegExp_1.mainRegExp.employeeTimer.month);
            req.checkQuery('employee_id', error_enum_1.mainErrorEnum.employee_id_required).exists();
            req.checkQuery('employee_id', error_enum_1.mainErrorEnum.employee_id_invalid).matches(mainRegExp_1.mainRegExp.employee._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const year = parseInt(req.query.year);
            const month = parseInt(req.query.month);
            const _month = month < 10 ? ('0' + month) : ('' + month); // 01 09 10 12
            const daysCountOfMonth = employeeTimerHelper_1.employeeTimerHelper.getDaysCountOfMonth(year, month - 1); // 29 30 31
            employeeTimer_1.EmployeeTimerModel.find({
                employee_id: req.query.employee_id,
                date: {
                    $gte: `${year}-${_month}-01`,
                    $lte: `${year}-${_month}-${daysCountOfMonth}`,
                }
            })
                .select('-_id timeMs date')
                .exec()
                .then(async (statistics) => {
                res.status(200).json({ statistics });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewEmployeeActions = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_required).exists();
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_invalid).matches(mainRegExp_1.mainRegExp.view.sortBy);
            req.checkQuery('employee_id', error_enum_1.mainErrorEnum.employee_id_required).exists();
            req.checkQuery('employee_id', error_enum_1.mainErrorEnum.employee_id_invalid).matches(mainRegExp_1.mainRegExp.employee._id);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            employeeAction_1.EmployeeActionModel.find({
                employee_id: req.query.employee_id
            })
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .populate('client car carGroup carType carModel booking branch')
                .exec()
                .then(async (employeeActions) => {
                res.status(200).json({
                    employeeActions
                });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewEmployeeActionsByItemNameFilter = async (req, res, next) => {
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_required).exists();
            req.checkQuery('page', error_enum_1.mainErrorEnum.page_invalid).matches(mainRegExp_1.mainRegExp.view.page);
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_required).exists();
            req.checkQuery('limit', error_enum_1.mainErrorEnum.limit_invalid).matches(mainRegExp_1.mainRegExp.view.limit);
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_required).exists();
            req.checkQuery('sortBy', error_enum_1.mainErrorEnum.sortBy_invalid).matches(mainRegExp_1.mainRegExp.view.sortBy);
            req.checkQuery('employee_id', error_enum_1.mainErrorEnum.employee_id_required).exists();
            req.checkQuery('employee_id', error_enum_1.mainErrorEnum.employee_id_invalid).matches(mainRegExp_1.mainRegExp.employee._id);
            req.checkQuery('itemName', error_enum_1.mainErrorEnum.item_name_required).exists();
            req.checkQuery('itemName', error_enum_1.mainErrorEnum.item_name_invalid).matches(mainRegExp_1.mainRegExp.employeeAction.itemName);
            const errors = await req.validationErrors();
            if (errors)
                return expressHelper_1.expressHelper.sendCustomError(errors[0], next);
            const limit = parseInt(req.query.limit);
            const page = parseInt(req.query.page);
            const itemName = parseInt(req.query.itemName);
            employeeAction_1.EmployeeActionModel.find({
                employee_id: req.query.employee_id,
                itemName: itemName
            })
                .sort(req.query.sortBy)
                .skip((page - 1) * limit)
                .limit(limit)
                .populate(employeeAction_1.EitemName[itemName])
                .exec()
                .then(async (employeeActions) => {
                res.status(200).json({
                    employeeActions
                });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        // options
        this.viewAllEmployeeGroups = async (req, res, next) => {
            employeeGroup_1.EmployeeGroupModel.find({})
                .select('_id nameEn nameAr')
                .sort('-createdAt')
                .exec()
                .then(async (employeeGroups) => {
                res.status(200).json({ employeeGroups });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
        this.viewAllBranches = async (req, res, next) => {
            branch_1.BranchModel.find({})
                .select('_id nameEn nameAr')
                .sort('-createdAt')
                .exec()
                .then(async (branches) => {
                res.status(200).json({ branches });
            })
                .catch(_error => expressHelper_1.expressHelper.sendCustomError(error_enum_1.mainErrorEnum.datebase, next));
        };
    }
    ;
}
;
exports.adminCTRL = new AdminCTRL();
