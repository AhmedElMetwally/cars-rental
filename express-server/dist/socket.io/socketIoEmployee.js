"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const log_1 = require("../helpers/log");
const redis_1 = require("../helpers/redis");
const employeeTimer_1 = require("../models/employeeTimer");
const jwt_1 = require("../helpers/jwt");
const dateHelper_1 = require("../helpers/dateHelper");
class SocketIoEmployee {
    constructor() {
        this.employeeGuard = jwt_1.jwt.allaw_socket_io({
            roles: ['employee'],
            dataField: 'employee',
            query_name_of_token: 'token',
            set_id_to_socket_from: '_id'
        });
        this.clearAllOnlineEmployees_socketId = () => {
            redis_1.redisClient.keys('socketsIds_employee_id_*', (_err, result) => {
                result.forEach(v => {
                    redis_1.redisClient.del(v);
                });
            });
        };
        this.clearAllOnlineEmployees_openIn = () => {
            redis_1.redisClient.del('onlines_employees', () => { });
        };
        this.onConnect = (employee_id, socketId) => {
            // set employee openIn if not exist
            // push this socket.id in this employee list 
            redis_1.redisClient
                .multi()
                .hsetnx('onlines_employees', employee_id, new Date().getTime().toString())
                .lpush(`socketsIds_employee_id_${employee_id}`, socketId)
                .exec(() => { });
        };
        this.onDisconnect = (employee_id, socketId) => {
            // get employee openIn
            // delete socket.id from employee socket.io list
            // get length of this employee socket.io ( if connect from many pages )
            redis_1.redisClient
                .multi()
                .hget('onlines_employees', employee_id)
                .lrem(`socketsIds_employee_id_${employee_id}`, 1, socketId)
                .llen(`socketsIds_employee_id_${employee_id}`)
                .exec((err, res) => {
                // if any error stop here
                if (err)
                    return;
                if (!res[0])
                    return;
                if (res[2] !== 0)
                    return;
                // this is last connect
                // save diff (timeMs)
                // then delete this employee first connect date
                // get diff between first connect and now
                const timeMs = new Date().getTime() - res[0];
                const date = dateHelper_1.dateHelper.getThisDay();
                // save if this first connect in this day
                // update if this not fitst connect in this day
                employeeTimer_1.EmployeeTimerModel.updateOne({
                    employee_id,
                    date,
                }, {
                    employee_id,
                    date,
                    $inc: {
                        timeMs
                    },
                }, {
                    upsert: true
                }).exec(() => { });
                // delete this employee first connect date
                redis_1.redisClient.hdel('onlines_employees', employee_id, () => { });
            });
        };
        // use in express
        this.getAllOnlineEmployees_ids = () => {
            return new Promise((resolve) => {
                redis_1.redisClient.hkeys('onlines_employees', (err, result) => {
                    return resolve(result);
                });
            });
        };
        this.isEmployeeOnline = (employee_id) => {
            return new Promise((resolve) => {
                redis_1.redisClient.hget('onlines_employees', employee_id.toString(), (err, result) => {
                    if (err)
                        return resolve(0);
                    if (result == null)
                        return resolve(0);
                    return resolve(1);
                    ;
                });
            });
        };
    }
    ;
    // init socket.io server
    init(socketIoServer, cb) {
        // clear redis
        this.clearAllOnlineEmployees_socketId();
        this.clearAllOnlineEmployees_openIn();
        // use /employee namespace
        const io = socketIoServer
            .of('/employee')
            .use(this.employeeGuard);
        // on new connect
        io.on('connection', async (socket) => {
            this.onConnect(socket.employee._id, socket.client.id);
            log_1.log(`employee connect to socket.io [${socket.employee._id}] [${dateHelper_1.dateHelper.getThisDay()}]`);
            // on disConnect
            socket.on('disconnect', async () => {
                this.onDisconnect(socket.employee._id, socket.client.id);
                log_1.log(`employee disconnect from socket.io [${socket.employee._id}] [${dateHelper_1.dateHelper.getThisDay()}]`);
            });
        });
        cb();
    }
    ;
}
exports.SocketIoEmployee = SocketIoEmployee;
;
