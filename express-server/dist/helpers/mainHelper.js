"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt_1 = __importDefault(require("bcrypt"));
class MainHelper {
    constructor() { }
    ;
    isExists(str) {
        return typeof str !== 'undefined';
    }
    ;
    isUniqueError(_error) {
        return _error.code === 11000;
    }
    ;
    isUpdateFail(result) {
        return result.n === 0;
    }
    ;
    hashSync(password) {
        return bcrypt_1.default.hashSync(password, 5);
    }
    ;
    compareSync(password, hash) {
        return bcrypt_1.default.compareSync(password, hash);
    }
    ;
}
exports.MainHelper = MainHelper;
;
exports.mainHelper = new MainHelper();
