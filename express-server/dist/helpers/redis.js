"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const redis_1 = require("redis");
const log_1 = require("./log");
exports.redisClient = redis_1.createClient(process.env.REDIS_URL)
    .on('connect', () => {
    log_1.log('  Redis Work');
})
    .on('error', (err) => {
    throw err;
});
