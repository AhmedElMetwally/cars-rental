"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const uniqid_1 = __importDefault(require("uniqid"));
const fs_extra_1 = __importDefault(require("fs-extra"));
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
class ImageHelper {
    constructor() { }
    ;
    uploadImage(image) {
        return new Promise(async (resolve, reject) => {
            await this.createImagesTmpFolder();
            const src = `${uniqid_1.default()}.jpg`;
            image.mv(path_1.default.resolve(`./${process.env.IMAGES_TMP_SRC}/${src}`), async (err) => {
                if (err) {
                    reject(err);
                }
                else {
                    await this.useImageMin();
                    await this.clearImagesTmp();
                    resolve(src);
                }
                ;
            });
        });
    }
    ;
    deleteImage(imageSrc) {
        fs_extra_1.default.unlink(path_1.default.resolve(`./${process.env.IMAGES_SRC}/${imageSrc}`), _err => { });
    }
    ;
    useImageMin() {
        return new Promise((resolve) => {
            imagemin([
                path_1.default.resolve(`./${process.env.IMAGES_TMP_SRC}/*.jpg`)
            ], path_1.default.resolve(`./${process.env.IMAGES_SRC}`), {
                plugins: [
                    imageminJpegtran(),
                ]
            }).then(() => {
                resolve();
            })
                .catch(() => {
                resolve();
            });
        });
    }
    ;
    clearImagesTmp() {
        return new Promise((resolve) => {
            return fs_extra_1.default.emptyDir(path_1.default.resolve(`./${process.env.IMAGES_TMP_SRC}`))
                .then(() => {
                resolve();
            })
                .catch(() => {
                resolve();
            });
        });
    }
    ;
    createImagesTmpFolder() {
        return new Promise((resolve) => {
            const IMAGES_TMP_SRC = path_1.default.resolve(`./${process.env.IMAGES_TMP_SRC}`);
            if (fs_extra_1.default.existsSync(IMAGES_TMP_SRC)) {
                resolve();
            }
            else {
                fs_extra_1.default.mkdirSync(IMAGES_TMP_SRC);
                resolve();
            }
            ;
        });
    }
    ;
}
;
exports.imageHelper = new ImageHelper();
