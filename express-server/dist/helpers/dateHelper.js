"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const dayjs_1 = __importDefault(require("dayjs"));
Date.prototype.addDays = function (days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
};
class DateHelper {
    constructor() {
        this.getThisDay = () => {
            const currentDate = new Date();
            const date = dayjs_1.default(new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate())).format('YYYY-MM-DD');
            return date;
        };
        this.getDayFromFullDate = (date) => {
            return dayjs_1.default(date).format('YYYY-MM-DD');
        };
        this.getDayFromDates = (y, m, d) => {
            return dayjs_1.default(new Date(y, m, d)).format('YYYY-MM-DD');
        };
    }
    ;
}
;
exports.dateHelper = new DateHelper();
