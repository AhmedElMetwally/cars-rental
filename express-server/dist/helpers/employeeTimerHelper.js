"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dateHelper_1 = require("./dateHelper");
class EmployeeTimerHelper {
    constructor() {
        // use in AdminCTRL
        this.createCompleteStatisticsOfYear = (statistics, year) => {
            const allDatesOfThisYear = this.getAllDatesOfYear(year);
            const completeStatistic = [];
            for (let i = 0, len = allDatesOfThisYear.length; i < len; ++i) {
                let tmp = {
                    timeMs: 0,
                    date: allDatesOfThisYear[i]
                };
                // check if this date exist
                for (let x = 0, _len = statistics.length; x < _len; ++x) {
                    if (statistics[x].date === allDatesOfThisYear[i]) {
                        tmp.timeMs = statistics[x].timeMs;
                        break;
                    }
                    ;
                }
                ;
                completeStatistic.push(tmp);
            }
            ;
            return completeStatistic;
        };
        this.createCompleteStatisticsOfMonth = (statistics, year, month) => {
            const allDatesOfThisMonth = this.getAllDatesOfMonth(year, month);
            const completeStatistic = [];
            for (let i = 0, len = allDatesOfThisMonth.length; i < len; ++i) {
                let tmp = {
                    timeMs: 0,
                    date: allDatesOfThisMonth[i]
                };
                // check if this date exist
                for (let x = 0, _len = statistics.length; x < _len; ++x) {
                    if (statistics[x].date === allDatesOfThisMonth[i]) {
                        tmp.timeMs = statistics[x].timeMs;
                        break;
                    }
                    ;
                }
                ;
                completeStatistic.push(tmp);
            }
            ;
            return completeStatistic;
        };
        this.getDaysCountOfMonth = (year, month) => {
            var isLeap = ((year % 4) == 0 && ((year % 100) != 0 || (year % 400) == 0));
            return [31, (isLeap ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
        };
        // use here
        this.getAllDatesOfMonth = (year, month) => {
            let allDaysOfMonth = [];
            let mLen = this.getDaysCountOfMonth(year, month);
            for (let d = 1; d <= mLen; d += 1) {
                allDaysOfMonth.push(dateHelper_1.dateHelper.getDayFromDates(year, month, d));
            }
            ;
            return allDaysOfMonth;
        };
        this.getAllDatesOfYear = (year) => {
            let allDaysOfYear = [];
            for (let m = 0; m < 12; m += 1) {
                allDaysOfYear.push(...this.getAllDatesOfMonth(year, m));
            }
            ;
            return allDaysOfYear;
        };
    }
    ;
}
;
exports.employeeTimerHelper = new EmployeeTimerHelper();
