"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mainRegExp_1 = require("../regExp/mainRegExp");
const employeeGroup_1 = require("../models/employeeGroup");
const ts_enum_util_1 = require("ts-enum-util");
// sent error Code only
// await req.validationErrors(); errorCode[]
const errorFormatter = (_param, errorCode) => errorCode;
// check functions
const checkScreens = (screens) => {
    if (!Array.isArray(screens))
        return false;
    if (screens.length === 0)
        return true;
    // array of all screens numbers
    const allScreens = ts_enum_util_1.$enum(employeeGroup_1.Escreens).getValues();
    // check if all screens item in Escreens
    return screens.every(v => allScreens.includes(v));
};
const checkBranches = (branches_ids) => {
    if (!Array.isArray(branches_ids))
        return false;
    if (branches_ids.length === 0)
        return true;
    return branches_ids.every(v => mainRegExp_1.mainRegExp.branch._id.test(v));
};
const isLocation = (location) => {
    if (location.length !== 2)
        return false;
    if (!mainRegExp_1.mainRegExp.branch.location.test(location[0] + ''))
        return false;
    if (!mainRegExp_1.mainRegExp.branch.location.test(location[1] + ''))
        return false;
    return true;
};
const customValidators = {
    checkScreens,
    checkBranches,
    isLocation,
};
exports.expressValidatorConfig = {
    errorFormatter,
    customValidators
};
