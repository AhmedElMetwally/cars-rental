"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const error_enum_1 = require("./../enums/error.enum");
class ExpressHelper {
    constructor() {
        this.expressErrorHandler = async (nextError, req, res, next) => {
            if (nextError.title === 'jwt-express-roles') {
                // error from jwt
                // i will create my error here
                return res.status(401).json({
                    errorEnum: error_enum_1.mainErrorEnum.token_invalid
                });
            }
            else {
                // sendCustomError 
                return res.status(nextError.status).json({
                    errorEnum: nextError.errorEnum
                });
            }
            ;
        };
    }
    ;
    sendCustomError(errorEnum, next) {
        const nextError = {
            status: 400,
            errorEnum
        };
        return next(nextError);
    }
    ;
}
exports.ExpressHelper = ExpressHelper;
;
exports.expressHelper = new ExpressHelper();
