"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const jwt_express_roles_1 = __importDefault(require("jwt-express-roles"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config({ path: '.env' });
exports.jwt = new jwt_express_roles_1.default({
    roleField: 'roles',
    secretKey: process.env.JWT_SECRET,
    expiresIn: '1000h',
});
