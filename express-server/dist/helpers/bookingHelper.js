"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const branch_1 = require("./../models/branch");
const car_1 = require("./../models/car");
const carGroup_1 = require("../models/carGroup");
const booking_1 = require("../models/booking");
const dayjs_1 = __importDefault(require("dayjs"));
const mathjs_1 = __importDefault(require("mathjs"));
;
class BookingHelper {
    constructor() {
        this.getDaysBetweenTwoDates = (from, to) => {
            const _from = new Date(from);
            const _to = new Date(to);
            const dateArray = new Array();
            let currentDate = _from;
            while (currentDate <= _to) {
                let _currentDate = dayjs_1.default(currentDate);
                dateArray.push(_currentDate.toISOString());
                currentDate = _currentDate.add(1, 'day').toDate();
            }
            ;
            return dateArray;
        };
        this.getTotlaPrice = (pricePerDay, discountPerDay, daysCount) => {
            return mathjs_1.default
                .chain(pricePerDay)
                .subtract(discountPerDay)
                .multiply(daysCount)
                .done();
        };
        this.getDaysCount = (pickUpDate, returnDate) => {
            if (new Date(returnDate) > new Date(pickUpDate)) {
                const date1 = new Date(returnDate);
                const date2 = new Date(pickUpDate);
                const timeDiff = Math.abs(date2.getTime() - date1.getTime());
                const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                return diffDays;
                // return dayjs(returnDate).diff( dayjs(pickUpDate) , 'day');
            }
            else {
                return 0;
            }
        };
        // is can add car to booking 
        this.isCanAddCarToBooking = async (car_id, booking_id) => {
            try {
                const booking = await booking_1.BookingModel.findOne({
                    _id: booking_id,
                    deleted: 0,
                })
                    .select('pickUpDate returnDate')
                    .exec();
                if (booking) {
                    const carBookingsCount = await booking_1.BookingModel.countDocuments({
                        car: car_id,
                        deleted: 0,
                        bookingType: booking_1.EbookingType.active,
                        _id: {
                            $ne: booking_id
                        },
                        $or: [
                            //  - [ ] -
                            {
                                pickUpDate: {
                                    $gte: booking.pickUpDate
                                },
                                returnDate: {
                                    $lte: booking.returnDate
                                },
                            },
                            //  [ - - ]
                            {
                                pickUpDate: {
                                    $lte: booking.pickUpDate
                                },
                                returnDate: {
                                    $gte: booking.returnDate
                                },
                            },
                            //  [ - ] -
                            {
                                $and: [
                                    {
                                        pickUpDate: {
                                            $gte: booking.pickUpDate
                                        },
                                        returnDate: {
                                            $gte: booking.returnDate
                                        },
                                    },
                                    {
                                        pickUpDate: {
                                            $lte: booking.returnDate
                                        },
                                        returnDate: {
                                            $gte: booking.pickUpDate
                                        },
                                    }
                                ]
                            },
                            //  - [ - ]
                            {
                                $and: [
                                    {
                                        pickUpDate: {
                                            $lte: booking.pickUpDate
                                        },
                                        returnDate: {
                                            $lte: booking.returnDate
                                        },
                                    },
                                    {
                                        pickUpDate: {
                                            $lte: booking.returnDate
                                        },
                                        returnDate: {
                                            $gte: booking.pickUpDate
                                        },
                                    }
                                ]
                            },
                        ]
                    })
                        .exec();
                    return !carBookingsCount;
                }
                else {
                    return false;
                }
                ;
            }
            catch (_) {
                return false;
            }
            ;
        };
        // ./is can add car to booking 
        // is can edit booking date
        this.isCanEditBookingDate = async (booking_id, pickUpDate, returnDate) => {
            try {
                if (new Date(returnDate) > new Date(pickUpDate)) {
                    const booking = await booking_1.BookingModel.findOne({
                        _id: booking_id,
                        deleted: 0,
                    })
                        .exec();
                    if (booking) {
                        if (booking.car) {
                            // check by car
                            return this.isCanEditBookingDateInCar(booking.car, pickUpDate, returnDate, booking_id);
                        }
                        else {
                            // check by carGroup
                            return this.isCanEditBookingDateInCarGroup(booking.carGroup, pickUpDate, returnDate, booking_id);
                        }
                        ;
                    }
                    else {
                        return false;
                    }
                    ;
                }
                else {
                }
                ;
            }
            catch (_) {
                return false;
            }
            ;
        };
        this.isCanEditBookingDateInCar = async (car_id, pickUpDate, returnDate, booking_id) => {
            try {
                const carBookingsCount = await booking_1.BookingModel.countDocuments({
                    car: car_id,
                    bookingType: booking_1.EbookingType.active,
                    deleted: 0,
                    _id: {
                        $ne: booking_id
                    },
                    $or: [
                        //  - [ ] -
                        {
                            pickUpDate: {
                                $gte: pickUpDate
                            },
                            returnDate: {
                                $lte: returnDate
                            },
                        },
                        //  [ - - ]
                        {
                            pickUpDate: {
                                $lte: pickUpDate
                            },
                            returnDate: {
                                $gte: returnDate
                            },
                        },
                        //  [ - ] -
                        {
                            $and: [
                                {
                                    pickUpDate: {
                                        $gte: pickUpDate
                                    },
                                    returnDate: {
                                        $gte: returnDate
                                    },
                                },
                                {
                                    pickUpDate: {
                                        $lte: returnDate
                                    },
                                    returnDate: {
                                        $gte: pickUpDate
                                    },
                                }
                            ]
                        },
                        //  - [ - ]
                        {
                            $and: [
                                {
                                    pickUpDate: {
                                        $lte: pickUpDate
                                    },
                                    returnDate: {
                                        $lte: returnDate
                                    },
                                },
                                {
                                    pickUpDate: {
                                        $lte: returnDate
                                    },
                                    returnDate: {
                                        $gte: pickUpDate
                                    },
                                }
                            ]
                        },
                    ]
                })
                    .exec();
                return !carBookingsCount;
            }
            catch (_) {
                return false;
            }
            ;
        };
        this.isCanEditBookingDateInCarGroup = async (carGroup_id, pickUpDate, returnDate, booking_id) => {
            try {
                const carsInGroups = await car_1.CarModel.find({
                    carGroup: carGroup_id
                })
                    .select('_id')
                    .lean()
                    .exec();
                const cars_idsInCarGroup = carsInGroups.map(car => {
                    return car._id;
                });
                const carsBookingsCount = await booking_1.BookingModel.countDocuments({
                    car: {
                        $in: cars_idsInCarGroup
                    },
                    deleted: 0,
                    _id: {
                        $ne: booking_id
                    },
                    bookingType: booking_1.EbookingType.active,
                    $or: [
                        //  - [ ] -
                        {
                            pickUpDate: {
                                $gte: pickUpDate
                            },
                            returnDate: {
                                $lte: returnDate
                            },
                        },
                        //  [ - - ]
                        {
                            pickUpDate: {
                                $lte: pickUpDate
                            },
                            returnDate: {
                                $gte: returnDate
                            },
                        },
                        //  [ - ] -
                        {
                            $and: [
                                {
                                    pickUpDate: {
                                        $gte: pickUpDate
                                    },
                                    returnDate: {
                                        $gte: returnDate
                                    },
                                },
                                {
                                    pickUpDate: {
                                        $lte: returnDate
                                    },
                                    returnDate: {
                                        $gte: pickUpDate
                                    },
                                }
                            ]
                        },
                        //  - [ - ]
                        {
                            $and: [
                                {
                                    pickUpDate: {
                                        $lte: pickUpDate
                                    },
                                    returnDate: {
                                        $lte: returnDate
                                    },
                                },
                                {
                                    pickUpDate: {
                                        $lte: returnDate
                                    },
                                    returnDate: {
                                        $gte: pickUpDate
                                    },
                                }
                            ]
                        },
                    ]
                })
                    .exec();
                return carsBookingsCount < cars_idsInCarGroup.length;
            }
            catch (_) {
                return false;
            }
            ;
        };
        // ./is can edit booking date
        // is can edit expiredAt
        this.isCanEditExpiredAt = async (booking_id, expiredAt) => {
            try {
                const booking = await booking_1.BookingModel.findOne({
                    _id: booking_id,
                    bookingType: booking_1.EbookingType.waiting_to_confirm,
                    deleted: 0
                })
                    .select('pickUpDate')
                    .exec();
                return new Date(expiredAt) < new Date(booking.pickUpDate) &&
                    new Date(expiredAt) > new Date();
            }
            catch (_) {
                return false;
            }
            ;
        };
        // ./is can edit expiredAt
        // view carGroups to booking [in client]
        this.getAllCarGroupsWhoHaveAvailableCars = async () => {
            const cars = await car_1.CarModel.find({
                deleted: 0,
                status: car_1.Estatus.available
            })
                .select('carGroup')
                .exec();
            const carGroups_ids = cars.map(car => car.carGroup);
            return carGroups_ids;
        };
        this.getAllCarGroupsWhoBookingInDate = async (pickUpDate, returnDate) => {
            const bookings = await booking_1.BookingModel.find({
                deleted: 0,
                bookingType: booking_1.EbookingType.active,
                $or: [
                    //  - [ ] -
                    {
                        pickUpDate: {
                            $gte: pickUpDate
                        },
                        returnDate: {
                            $lte: returnDate
                        },
                    },
                    //  [ - - ]
                    {
                        pickUpDate: {
                            $lte: pickUpDate
                        },
                        returnDate: {
                            $gte: returnDate
                        },
                    },
                    //  [ - ] -
                    {
                        $and: [
                            {
                                pickUpDate: {
                                    $gte: pickUpDate
                                },
                                returnDate: {
                                    $gte: returnDate
                                },
                            },
                            {
                                pickUpDate: {
                                    $lte: returnDate
                                },
                                returnDate: {
                                    $gte: pickUpDate
                                },
                            }
                        ]
                    },
                    //  - [ - ]
                    {
                        $and: [
                            {
                                pickUpDate: {
                                    $lte: pickUpDate
                                },
                                returnDate: {
                                    $lte: returnDate
                                },
                            },
                            {
                                pickUpDate: {
                                    $lte: returnDate
                                },
                                returnDate: {
                                    $gte: pickUpDate
                                },
                            }
                        ]
                    },
                ]
            })
                .select('carGroup')
                .exec();
            const carGroups_ids = bookings.map(booking => booking.carGroup);
            return carGroups_ids;
        };
        this.getBranches_idsByCity_id = async (city_id) => {
            const branches_ids = await branch_1.BranchModel.find({
                city: city_id
            })
                .select('_id')
                .exec();
            return branches_ids.map(v => v._id);
        };
        this.viewCarGroupsToBookingOne = async ($event) => {
            return carGroup_1.CarGroupModel.find(Object.assign({ deleted: 0 }, $event.filters, { _id: {
                    $nin: await this.getAllCarGroupsWhoBookingInDate($event.pickUpDate, $event.returnDate),
                    $in: await this.getAllCarGroupsWhoHaveAvailableCars()
                } }))
                .skip(($event.page - 1) * $event.limit)
                .limit($event.limit)
                .populate('carType carModel')
                .populate({
                path: 'branch',
                populate: {
                    path: 'city'
                }
            })
                .lean()
                .exec();
        };
        // ./view carGroups to booking [in client]
        // is can make booking 
        this.isCanBookingCarGroupInDate = async (carGroup_id, pickUpDate, returnDate) => {
            try {
                const carsInGroups = await car_1.CarModel.find({
                    carGroup: carGroup_id,
                })
                    .select('_id')
                    .lean()
                    .exec();
                const cars_idsInCarGroup = carsInGroups.map(car => {
                    return car._id;
                });
                const carsBookingsCount = await booking_1.BookingModel.countDocuments({
                    car: {
                        $in: cars_idsInCarGroup
                    },
                    bookingType: booking_1.EbookingType.active,
                    deleted: 0,
                    $or: [
                        //  - [ ] -
                        {
                            pickUpDate: {
                                $gte: pickUpDate
                            },
                            returnDate: {
                                $lte: returnDate
                            },
                        },
                        //  [ - - ]
                        {
                            pickUpDate: {
                                $lte: pickUpDate
                            },
                            returnDate: {
                                $gte: returnDate
                            },
                        },
                        //  [ - ] -
                        {
                            $and: [
                                {
                                    pickUpDate: {
                                        $gte: pickUpDate
                                    },
                                    returnDate: {
                                        $gte: returnDate
                                    },
                                },
                                {
                                    pickUpDate: {
                                        $lte: returnDate
                                    },
                                    returnDate: {
                                        $gte: pickUpDate
                                    },
                                }
                            ]
                        },
                        //  - [ - ]
                        {
                            $and: [
                                {
                                    pickUpDate: {
                                        $lte: pickUpDate
                                    },
                                    returnDate: {
                                        $lte: returnDate
                                    },
                                },
                                {
                                    pickUpDate: {
                                        $lte: returnDate
                                    },
                                    returnDate: {
                                        $gte: pickUpDate
                                    },
                                }
                            ]
                        },
                    ]
                })
                    .exec();
                return carsBookingsCount < cars_idsInCarGroup.length;
            }
            catch (_) {
                return false;
            }
            ;
        };
        // ./is can make booking 
        // is can active booking
        this.isCanActiveBooking = async (booking_id) => {
            try {
                const booking = await booking_1.BookingModel.findOne({
                    _id: booking_id,
                    deleted: 0,
                })
                    .exec();
                if (booking) {
                    if (booking.car) {
                        // check by car
                        return await this.isCanActiveBookingInCar(booking.car, booking.pickUpDate, booking.returnDate);
                    }
                    else {
                        // check by carGroup
                        return await this.isCanActiveBookingInCarGroup(booking.carGroup, booking.pickUpDate, booking.returnDate);
                    }
                    ;
                }
                else {
                    return false;
                }
                ;
            }
            catch (_) {
                return false;
            }
        };
        this.isCanActiveBookingInCar = async (car_id, pickUpDate, returnDate) => {
            try {
                const carBookingsCount = await booking_1.BookingModel.countDocuments({
                    car: car_id,
                    bookingType: booking_1.EbookingType.active,
                    deleted: 0,
                    $or: [
                        //  - [ ] -
                        {
                            pickUpDate: {
                                $gte: pickUpDate
                            },
                            returnDate: {
                                $lte: returnDate
                            },
                        },
                        //  [ - - ]
                        {
                            pickUpDate: {
                                $lte: pickUpDate
                            },
                            returnDate: {
                                $gte: returnDate
                            },
                        },
                        //  [ - ] -
                        {
                            $and: [
                                {
                                    pickUpDate: {
                                        $gte: pickUpDate
                                    },
                                    returnDate: {
                                        $gte: returnDate
                                    },
                                },
                                {
                                    pickUpDate: {
                                        $lte: returnDate
                                    },
                                    returnDate: {
                                        $gte: pickUpDate
                                    },
                                }
                            ]
                        },
                        //  - [ - ]
                        {
                            $and: [
                                {
                                    pickUpDate: {
                                        $lte: pickUpDate
                                    },
                                    returnDate: {
                                        $lte: returnDate
                                    },
                                },
                                {
                                    pickUpDate: {
                                        $lte: returnDate
                                    },
                                    returnDate: {
                                        $gte: pickUpDate
                                    },
                                }
                            ]
                        },
                    ]
                })
                    .exec();
                return !carBookingsCount;
            }
            catch (_) {
                return false;
            }
            ;
        };
        this.isCanActiveBookingInCarGroup = async (carGroup_id, pickUpDate, returnDate) => {
            try {
                const carsInGroups = await car_1.CarModel.find({
                    carGroup: carGroup_id
                })
                    .select('_id')
                    .lean()
                    .exec();
                const cars_idsInCarGroup = carsInGroups.map(car => {
                    return car._id;
                });
                const carsBookingsCount = await booking_1.BookingModel.countDocuments({
                    car: {
                        $in: cars_idsInCarGroup
                    },
                    deleted: 0,
                    bookingType: booking_1.EbookingType.active,
                    $or: [
                        //  - [ ] -
                        {
                            pickUpDate: {
                                $gte: pickUpDate
                            },
                            returnDate: {
                                $lte: returnDate
                            },
                        },
                        //  [ - - ]
                        {
                            pickUpDate: {
                                $lte: pickUpDate
                            },
                            returnDate: {
                                $gte: returnDate
                            },
                        },
                        //  [ - ] -
                        {
                            $and: [
                                {
                                    pickUpDate: {
                                        $gte: pickUpDate
                                    },
                                    returnDate: {
                                        $gte: returnDate
                                    },
                                },
                                {
                                    pickUpDate: {
                                        $lte: returnDate
                                    },
                                    returnDate: {
                                        $gte: pickUpDate
                                    },
                                }
                            ]
                        },
                        //  - [ - ]
                        {
                            $and: [
                                {
                                    pickUpDate: {
                                        $lte: pickUpDate
                                    },
                                    returnDate: {
                                        $lte: returnDate
                                    },
                                },
                                {
                                    pickUpDate: {
                                        $lte: returnDate
                                    },
                                    returnDate: {
                                        $gte: pickUpDate
                                    },
                                }
                            ]
                        },
                    ]
                })
                    .exec();
                return carsBookingsCount < cars_idsInCarGroup.length;
            }
            catch (_) {
                return false;
            }
            ;
        };
        // ./is can active booking
        this.viewAllCarsWhoCanAddToBooking = async (booking_id) => {
            const booking = await booking_1.BookingModel.findOne({
                _id: booking_id
            })
                .exec();
            const bookings = await booking_1.BookingModel.find({
                car: { $ne: null },
                deleted: 0,
                bookingType: booking_1.EbookingType.active,
                carGroup: booking.carGroup,
                _id: {
                    $ne: booking_id
                },
                $or: [
                    //  - [ ] -
                    {
                        pickUpDate: {
                            $gte: booking.pickUpDate
                        },
                        returnDate: {
                            $lte: booking.returnDate
                        },
                    },
                    //  [ - - ]
                    {
                        pickUpDate: {
                            $lte: booking.pickUpDate
                        },
                        returnDate: {
                            $gte: booking.returnDate
                        },
                    },
                    //  [ - ] -
                    {
                        $and: [
                            {
                                pickUpDate: {
                                    $gte: booking.pickUpDate
                                },
                                returnDate: {
                                    $gte: booking.returnDate
                                },
                            },
                            {
                                pickUpDate: {
                                    $lte: booking.returnDate
                                },
                                returnDate: {
                                    $gte: booking.pickUpDate
                                },
                            }
                        ]
                    },
                    //  - [ - ]
                    {
                        $and: [
                            {
                                pickUpDate: {
                                    $lte: booking.pickUpDate
                                },
                                returnDate: {
                                    $lte: booking.returnDate
                                },
                            },
                            {
                                pickUpDate: {
                                    $lte: booking.returnDate
                                },
                                returnDate: {
                                    $gte: booking.pickUpDate
                                },
                            }
                        ]
                    },
                ]
            })
                .exec();
            const blackListCars = bookings.map(v => v.car);
            return car_1.CarModel.find({
                deleted: 0,
                status: car_1.Estatus.available,
                _id: {
                    $nin: blackListCars
                }
            })
                .sort('-createdAt')
                .select('plateNumber')
                .exec();
        };
        this.setBookingsType = (bookings) => {
            return bookings.map(booking => this.setBookingType(booking));
        };
        this.setBookingType = (booking) => {
            if (booking.bookingType === booking_1.EbookingType.waiting_to_confirm
                && new Date() > new Date(booking.expiredAt)) {
                booking.bookingType = booking_1.EbookingType.expired;
            }
            else if (booking.bookingType === booking_1.EbookingType.active
                && new Date() > new Date(booking.returnDate)) {
                booking.bookingType = booking_1.EbookingType.done;
            }
            return booking;
        };
    }
    ;
}
;
exports.bookingHelper = new BookingHelper();
