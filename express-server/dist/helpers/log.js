"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config({ path: '.env' });
exports.log = (str) => {
    let testArg = process.argv[2] === 'test';
    let testEnv = process.env.NODE_ENV === 'test';
    if (!(testArg || testEnv)) {
        console.log('\x1b[36m%s\x1b[0m', str);
    }
    ;
};
exports.isLog = () => {
    let testArg = process.argv[2] === 'test';
    let testEnv = process.env.NODE_ENV === 'test';
    if (!(testArg || testEnv)) {
        return true;
    }
    ;
    return false;
};
