"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const body_parser_1 = __importDefault(require("body-parser"));
const mongoose_1 = __importDefault(require("mongoose"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
const http_1 = __importDefault(require("http"));
const socket_io_1 = __importDefault(require("socket.io"));
const dotenv_1 = __importDefault(require("dotenv"));
const express_validator_1 = __importDefault(require("express-validator"));
const socketIoEmployee_1 = require("./socket.io/socketIoEmployee");
const client_1 = require("./routes/client");
const admin_1 = require("./routes/admin");
const employee_1 = require("./routes/employee");
const expressValidator_1 = require("./helpers/expressValidator");
const log_1 = require("./helpers/log");
const guest_1 = require("./routes/guest");
const express_1 = __importDefault(require("express"));
dotenv_1.default.config({ path: '.env' });
class Server {
    constructor() {
        this.init();
    }
    ;
    init() {
        // create express new app 
        this.app = express_1.default();
        // http server
        this.HttpServer = http_1.default.createServer(this.app);
        // init socket.io
        const socketIoEmployee = new socketIoEmployee_1.SocketIoEmployee();
        const socketIoServer = socket_io_1.default(this.HttpServer);
        // middleware
        this.app.set('etag', false);
        this.app.set('x-powered-by', false);
        this.app.use(cors_1.default());
        // hide http logs on hide logs 
        if (log_1.isLog()) {
            this.app.use(morgan_1.default('dev'));
        }
        ;
        this.app.use(body_parser_1.default.urlencoded({ extended: false }));
        this.app.use(body_parser_1.default.json());
        this.app.use(express_validator_1.default(expressValidator_1.expressValidatorConfig));
        // api routes
        this.app.use('/api/client', client_1.ClientRouter);
        this.app.use('/api/admin', admin_1.AdminRouter);
        this.app.use('/api/employee', employee_1.EmployeeRouter);
        this.app.use('/api/guest', guest_1.GuestRouter);
        if (process.env.NODE_ENV === 'development') {
            this.app.use('/images', express_1.default.static(process.env.IMAGES_SRC));
        }
        ;
        // error message on fail Request
        this.app.use('*', (_req, res) => {
            res.status(404).send('Not Found');
        });
        // listen 
        // mongoose
        mongoose_1.default.connect(process.env.MONGO_URL, { useNewUrlParser: true, autoReconnect: true })
            .then(() => log_1.log(`  Mongoose Work [${process.env.MONGO_URL}]`))
            .catch(err => {
            throw err;
        });
        // http
        this.HttpServer.listen(process.env.HTTP_PORT, () => log_1.log(`  HTTP Server Work`));
        // socket.io
        socketIoEmployee.init(socketIoServer, () => log_1.log(`  Socket.io Server Work [/employee]`));
        // on SIGINT close mongoose
        process.on('SIGINT', () => {
            mongoose_1.default.connection.close(() => {
                process.exit();
            });
        });
    }
    ;
    getApp() {
        return this.app;
    }
    ;
}
exports.Server = Server;
;
