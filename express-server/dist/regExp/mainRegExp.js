"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const message_1 = require("./../models/message");
const city_1 = require("../models/city");
const onlinePayment_1 = require("../models/onlinePayment");
const guest_1 = require("../models/guest");
const nationality_1 = require("../models/nationality");
const booking_1 = require("../models/booking");
const client_1 = require("../models/client");
const carType_1 = require("../models/carType");
const carModel_1 = require("../models/carModel");
const car_1 = require("../models/car");
const branch_1 = require("../models/branch");
const employeeGroup_1 = require("../models/employeeGroup");
const employeeAction_1 = require("../models/employeeAction");
const admin_1 = require("../models/admin");
const employee_1 = require("../models/employee");
const employeeTimer_1 = require("../models/employeeTimer");
const carGroup_1 = require("../models/carGroup");
exports.mainRegExp = {
    branch: branch_1.RegExpBranchModel,
    employeeGroup: employeeGroup_1.RegExpEmployeeGroupModel,
    employeeAction: employeeAction_1.RegExpEmployeeActionModel,
    admin: admin_1.RegExpAdminModel,
    employee: employee_1.RegExpEmployeeModel,
    employeeTimer: employeeTimer_1.RegExpEmployeeTimerModel,
    carGroup: carGroup_1.RegExpCarGroupModel,
    car: car_1.RegExpCarModel,
    carModel: carModel_1.RegExpCarModelModel,
    carType: carType_1.RegExpCarTypeModel,
    client: client_1.RegExpClientModel,
    booking: booking_1.RegExpBookingModel,
    nationality: nationality_1.RegExpNationalityModel,
    guest: guest_1.RegExpGuestModel,
    onlinePayment: onlinePayment_1.RegExpOnlinePaymentModel,
    city: city_1.RegExpCityModel,
    message: message_1.RegExpMessageModel,
    view: {
        page: /^(([1-9])|([1-9][0-9])){1,4}$/,
        limit: /^(([1-9])|([1-9][0-9])){1,3}$/,
        sortBy: /^((\-\w+)|\w+)$/,
        searchQuery: /^.{1,250}$/
    },
};
