"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const expressHelper_1 = require("../helpers/expressHelper");
const employee_1 = require("../controllers/employee");
const express_1 = require("express");
const express_fileupload_1 = __importDefault(require("express-fileupload"));
const employeeGroup_1 = require("../models/employeeGroup");
exports.EmployeeRouter = express_1.Router(); // /api/employee/[endPoint]
// auth
exports.EmployeeRouter.post('/signin', employee_1.employeeCTRL.signin);
exports.EmployeeRouter.get('/refresh-token', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.refreshToken);
// This Employee
exports.EmployeeRouter.get('/view-this-employee', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewThisEmployee);
exports.EmployeeRouter.put('/edit-this-employee', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.editThisEmployee);
exports.EmployeeRouter.put('/edit-this-employee-password', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.editThisEmployeePassword);
exports.EmployeeRouter.get('/view/this-employee-time-statistics-in-year', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewThisEmployeeTimeStatisticsInYear);
exports.EmployeeRouter.get('/view/this-employee-time-statistics-in-month', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewThisEmployeeTimeStatisticsInMonth);
exports.EmployeeRouter.get('/view/this-employee-actions', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewThisEmployeeActions);
exports.EmployeeRouter.get('/view/this-employee-actions-by-item-name-filter', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewEmployeeActionsByItemNameFilter);
// client
exports.EmployeeRouter.put('/client/edit', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.edit_client), employee_1.employeeCTRL.editClient);
exports.EmployeeRouter.put('/client/edit-password', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.edit_client_password), employee_1.employeeCTRL.editClientPassword);
exports.EmployeeRouter.delete('/client/delete', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.delete_client), employee_1.employeeCTRL.deleteClient);
exports.EmployeeRouter.post('/client/restore', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.restore_client), employee_1.employeeCTRL.restoreClient);
exports.EmployeeRouter.post('/client/add-image', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.edit_client_image), express_fileupload_1.default(), employee_1.employeeCTRL.addClientImage);
exports.EmployeeRouter.get('/view/client', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewClient);
exports.EmployeeRouter.get('/view/clients', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewClients);
exports.EmployeeRouter.get('/view/clients-search', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewClientsSearch);
exports.EmployeeRouter.get('/view/bookings-for-client', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewBookingsForClient);
exports.EmployeeRouter.get('/view/online-payments-for-client', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewOnlinePaymentsForClient);
// guest
exports.EmployeeRouter.get('/view/guest', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewGuest);
exports.EmployeeRouter.get('/view/guests', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewGuests);
exports.EmployeeRouter.get('/view/guests-search', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewGuestsSearch);
// car
exports.EmployeeRouter.post('/car/add', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.add_car), employee_1.employeeCTRL.addCar);
exports.EmployeeRouter.put('/car/edit', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.edit_car), employee_1.employeeCTRL.editCar);
exports.EmployeeRouter.delete('/car/delete', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.delete_car), employee_1.employeeCTRL.deleteCar);
exports.EmployeeRouter.post('/car/restore', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.restore_car), employee_1.employeeCTRL.restoreCar);
exports.EmployeeRouter.get('/view/car', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(null), employee_1.employeeCTRL.viewCar);
exports.EmployeeRouter.get('/view/cars', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(null), employee_1.employeeCTRL.viewCars);
exports.EmployeeRouter.get('/view/cars-search', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(null), employee_1.employeeCTRL.viewCarsSearch);
exports.EmployeeRouter.get('/view/cars-by-filter', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(null), employee_1.employeeCTRL.viewCarsByFilter);
// branch
exports.EmployeeRouter.post('/branch/add', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.add_branch), employee_1.employeeCTRL.addBranch);
exports.EmployeeRouter.put('/branch/edit', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.edit_branch), employee_1.employeeCTRL.editBranch);
exports.EmployeeRouter.delete('/branch/delete', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.delete_branch), employee_1.employeeCTRL.deleteBranch);
exports.EmployeeRouter.post('/branch/restore', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.restore_branch), employee_1.employeeCTRL.restoreBranch);
exports.EmployeeRouter.delete('/branch/delete-image', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.edit_branch_image), employee_1.employeeCTRL.deleteBranchImage);
exports.EmployeeRouter.post('/branch/add-image', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.edit_branch_image), express_fileupload_1.default(), employee_1.employeeCTRL.addBranchImage);
exports.EmployeeRouter.get('/view/branch', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(null), employee_1.employeeCTRL.viewBranch);
exports.EmployeeRouter.get('/view/branches', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(null), employee_1.employeeCTRL.viewBranches);
exports.EmployeeRouter.get('/view/branches-search', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(null), employee_1.employeeCTRL.viewBranchesSearch);
// CarGroup
exports.EmployeeRouter.post('/car-group/add', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.add_car_group), employee_1.employeeCTRL.addCarGroup);
exports.EmployeeRouter.put('/car-group/edit', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.edit_car_group), employee_1.employeeCTRL.editCarGroup);
exports.EmployeeRouter.delete('/car-group/delete', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.delete_car_group), employee_1.employeeCTRL.deleteCarGroup);
exports.EmployeeRouter.post('/car-group/restore', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.restore_car_group), employee_1.employeeCTRL.restoreCarGroup);
exports.EmployeeRouter.delete('/car-group/delete-image', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.edit_car_group_image), employee_1.employeeCTRL.deleteCarGroupImage);
exports.EmployeeRouter.post('/car-group/add-image', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.edit_car_group_image), express_fileupload_1.default(), employee_1.employeeCTRL.addCarGroupImage);
exports.EmployeeRouter.get('/view/car-group', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(null), employee_1.employeeCTRL.viewCarGroup);
exports.EmployeeRouter.get('/view/car-groups', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(null), employee_1.employeeCTRL.viewCarGroups);
exports.EmployeeRouter.get('/view/car-groups-by-filter', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(null), employee_1.employeeCTRL.viewCarGroupsByFilter);
// CarModel
exports.EmployeeRouter.post('/car-model/add', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.add_car_model), employee_1.employeeCTRL.addCarModel);
exports.EmployeeRouter.delete('/car-model/delete', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.delete_car_model), employee_1.employeeCTRL.deleteCarModel);
// CarType 
exports.EmployeeRouter.post('/car-type/add', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.add_car_type), employee_1.employeeCTRL.addCarType);
exports.EmployeeRouter.delete('/car-type/delete', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.delete_car_type), employee_1.employeeCTRL.deleteCarType);
// Booking
exports.EmployeeRouter.post('/booking/add-car', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.edit_booking), employee_1.employeeCTRL.addCarToBooking);
exports.EmployeeRouter.put('/booking/edit-date', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.edit_booking), employee_1.employeeCTRL.editBookingDate);
exports.EmployeeRouter.put('/booking/edit-expiredAt', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.edit_booking), employee_1.employeeCTRL.editBookingExpiredAt);
exports.EmployeeRouter.put('/booking/edit-pricePerDay', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.edit_booking), employee_1.employeeCTRL.editBookingPricePerDay);
exports.EmployeeRouter.put('/booking/edit-discountPerDay', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.edit_booking), employee_1.employeeCTRL.editBookingDiscountPerDay);
exports.EmployeeRouter.delete('/booking/delete', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.delete_booking), employee_1.employeeCTRL.deleteBooking);
exports.EmployeeRouter.post('/booking/restore', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.restore_booking), employee_1.employeeCTRL.restoreBooking);
exports.EmployeeRouter.post('/booking/cancel', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.cancel_booking), employee_1.employeeCTRL.cancelBooking);
exports.EmployeeRouter.post('/booking/approve', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.approve_booking), employee_1.employeeCTRL.approveBooking);
exports.EmployeeRouter.post('/booking/active', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(employeeGroup_1.Escreens.active_booking), employee_1.employeeCTRL.activeBooking);
exports.EmployeeRouter.get('/view/booking', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(null), employee_1.employeeCTRL.viewBooking);
exports.EmployeeRouter.get('/view/bookings', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(null), employee_1.employeeCTRL.viewBookings); // page limit soryBy 
exports.EmployeeRouter.get('/view/bookings-by-filter', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(null), employee_1.employeeCTRL.viewBookingsByFilter); // page limit soryBy branch_id bookingType
exports.EmployeeRouter.get('/view/bookings-search', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(null), employee_1.employeeCTRL.viewBookingsSearch);
// options
exports.EmployeeRouter.get('/view/all-branches-can-this-employee-access', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(null), employee_1.employeeCTRL.viewAllBranchesCanThisEmployeeAccess);
exports.EmployeeRouter.get('/view/all-car-groups-can-this-employee-access', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.checkScreen(null), employee_1.employeeCTRL.viewAllCarGroupsCanThisEmployeeAccess);
exports.EmployeeRouter.get('/this-location', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.getThislocation);
exports.EmployeeRouter.get('/view/all-nationalities', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewAllNationalities);
exports.EmployeeRouter.get('/view/all-cities', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewAllCities);
exports.EmployeeRouter.get('/view/all-car-types', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewAllCarTypes);
exports.EmployeeRouter.get('/view/all-car-models-by-car-type', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewAllCarModelsByCarType);
exports.EmployeeRouter.get('/view/all-cars-who-can-add-to-booking', employee_1.employeeCTRL.guard, employee_1.employeeCTRL.viewAllCarsWhoCanAddToBooking);
exports.EmployeeRouter.use(expressHelper_1.expressHelper.expressErrorHandler);
