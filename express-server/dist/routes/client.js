"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const expressHelper_1 = require("../helpers/expressHelper");
const client_1 = require("../controllers/client");
const express_1 = require("express");
const express_fileupload_1 = __importDefault(require("express-fileupload"));
exports.ClientRouter = express_1.Router();
// Auth
exports.ClientRouter.post('/signup', client_1.clientCTRL.signup);
exports.ClientRouter.post('/signin', client_1.clientCTRL.signin);
exports.ClientRouter.get('/refresh-token', client_1.clientCTRL.guard, client_1.clientCTRL.refreshToken); // guard
// Client
exports.ClientRouter.get('/view-this-client', client_1.clientCTRL.guard, client_1.clientCTRL.viewThisClient); // guard
exports.ClientRouter.put('/edit', client_1.clientCTRL.guard, client_1.clientCTRL.edit); // guard
exports.ClientRouter.put('/edit-password', client_1.clientCTRL.guard, client_1.clientCTRL.editPassword); // guard
exports.ClientRouter.post('/add-image', client_1.clientCTRL.guard, express_fileupload_1.default(), client_1.clientCTRL.addClientImage); // guard
// booking
exports.ClientRouter.post('/add-booking', client_1.clientCTRL.guard, client_1.clientCTRL.addBooking); // guard
exports.ClientRouter.get('/view-this-client-booking', client_1.clientCTRL.guard, client_1.clientCTRL.viewThisClientBooking); // guard
exports.ClientRouter.get('/view-this-client-bookings', client_1.clientCTRL.guard, client_1.clientCTRL.viewThisClientBookings); // guard
exports.ClientRouter.get('/view-this-client-bookings-search', client_1.clientCTRL.guard, client_1.clientCTRL.viewThisClientBookingsSearch); // guard
exports.ClientRouter.get('/view/booking-by-code-and-iqama', client_1.clientCTRL.viewBookingByCodeAndIqama);
// message
exports.ClientRouter.post('/send-message', client_1.clientCTRL.sendMessage);
// car group
exports.ClientRouter.get('/view/car-group', client_1.clientCTRL.viewCarGroup);
exports.ClientRouter.post('/view/car-groups', client_1.clientCTRL.viewCarGroupsToBookingOne);
// branch
exports.ClientRouter.get('/view/all-branches', client_1.clientCTRL.viewAllBranches);
exports.ClientRouter.get('/view/all-branches-location', client_1.clientCTRL.viewAllBranchesLocation);
// options
exports.ClientRouter.get('/view/all-nationalities', client_1.clientCTRL.viewAllNationalities);
exports.ClientRouter.get('/view/all-car-types', client_1.clientCTRL.viewAllCarTypes);
exports.ClientRouter.get('/view/all-car-models-by-car-type', client_1.clientCTRL.viewAllCarModelsByCarType);
exports.ClientRouter.get('/view/all-cities', client_1.clientCTRL.viewAllCities);
exports.ClientRouter.get('/view/all-branches-by-city-filter', client_1.clientCTRL.viewAllBranchesByCityFilter);
exports.ClientRouter.use(expressHelper_1.expressHelper.expressErrorHandler);
