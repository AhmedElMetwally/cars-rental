"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const expressHelper_1 = require("../helpers/expressHelper");
const express_1 = require("express");
const admin_1 = require("../controllers/admin");
exports.AdminRouter = express_1.Router();
// Auth
exports.AdminRouter.post('/signup', admin_1.adminCTRL.signup);
exports.AdminRouter.post('/signin', admin_1.adminCTRL.signin);
exports.AdminRouter.get('/refresh-token', admin_1.adminCTRL.guard, admin_1.adminCTRL.refreshToken);
// EmployeeGroup
exports.AdminRouter.post('/employee-group/add', admin_1.adminCTRL.guard, admin_1.adminCTRL.addEmployeeGroup);
exports.AdminRouter.put('/employee-group/edit', admin_1.adminCTRL.guard, admin_1.adminCTRL.editEmployeeGroup);
exports.AdminRouter.delete('/employee-group/delete', admin_1.adminCTRL.guard, admin_1.adminCTRL.deleteEmployeeGroup);
exports.AdminRouter.post('/employee-group/restore', admin_1.adminCTRL.guard, admin_1.adminCTRL.restoreEmployeeGroup);
exports.AdminRouter.get('/view/employee-group', admin_1.adminCTRL.guard, admin_1.adminCTRL.viewEmployeeGroup);
exports.AdminRouter.get('/view/employee-groups', admin_1.adminCTRL.guard, admin_1.adminCTRL.viewEmployeeGroups);
exports.AdminRouter.get('/view/employee-groups-search', admin_1.adminCTRL.guard, admin_1.adminCTRL.viewEmployeeGroupsSearch);
// Employee
exports.AdminRouter.post('/employee/add', admin_1.adminCTRL.guard, admin_1.adminCTRL.addEmployee);
exports.AdminRouter.put('/employee/edit', admin_1.adminCTRL.guard, admin_1.adminCTRL.editEmployee);
exports.AdminRouter.put('/employee/edit-password', admin_1.adminCTRL.guard, admin_1.adminCTRL.editEmployeePassword);
exports.AdminRouter.delete('/employee/delete', admin_1.adminCTRL.guard, admin_1.adminCTRL.deleteEmployee);
exports.AdminRouter.post('/employee/restore', admin_1.adminCTRL.guard, admin_1.adminCTRL.restoreEmployee);
exports.AdminRouter.get('/view/employee', admin_1.adminCTRL.guard, admin_1.adminCTRL.viewEmployee);
exports.AdminRouter.get('/view/employees', admin_1.adminCTRL.guard, admin_1.adminCTRL.viewEmployees);
exports.AdminRouter.get('/view/employees-search', admin_1.adminCTRL.guard, admin_1.adminCTRL.viewEmployeesSearch);
exports.AdminRouter.get('/view/employees-by-employee-group-filter', admin_1.adminCTRL.guard, admin_1.adminCTRL.viewEmployeesByEmployeeGroupFilter);
exports.AdminRouter.get('/view/employee-time-statistics-in-year', admin_1.adminCTRL.guard, admin_1.adminCTRL.viewEmployeeTimeStatisticsInYear);
exports.AdminRouter.get('/view/employee-time-statistics-in-month', admin_1.adminCTRL.guard, admin_1.adminCTRL.viewEmployeeTimeStatisticsInMonth);
exports.AdminRouter.get('/view/employee-actions', admin_1.adminCTRL.guard, admin_1.adminCTRL.viewEmployeeActions);
exports.AdminRouter.get('/view/employee-actions-by-item-name-filter', admin_1.adminCTRL.guard, admin_1.adminCTRL.viewEmployeeActionsByItemNameFilter);
// options
exports.AdminRouter.get('/view/all-employee-groups', admin_1.adminCTRL.guard, admin_1.adminCTRL.viewAllEmployeeGroups);
exports.AdminRouter.get('/view/all-branches', admin_1.adminCTRL.guard, admin_1.adminCTRL.viewAllBranches);
// nextError
exports.AdminRouter.use(expressHelper_1.expressHelper.expressErrorHandler);
