"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const mongooseSchemaOptions_1 = require("./mongooseSchemaOptions");
const mongoose_1 = __importDefault(require("mongoose"));
const bookingSchema = new mongoose_1.default.Schema({
    client: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        ref: 'tb_client',
        required: 1
    },
    guest: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        ref: 'tb_guest',
        required: 1
    },
    car: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        ref: 'tb_car',
        required: 1
    },
    carGroup: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        ref: 'tb_carGroup',
        required: 1
    },
    pickUpBranch: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'tb_branch',
        required: 1
    },
    returnBranch: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'tb_branch',
        required: 1
    },
    onlinePayment: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        ref: 'tb_onlinePayment',
        required: 1
    },
    // 999-999999999
    bookingCode: {
        type: String,
        required: 1,
        unique: 1
    },
    iqama: {
        type: String,
        trim: 1,
    },
    pickUpDate: {
        type: Date,
        required: 1
    },
    returnDate: {
        type: Date,
        required: 1
    },
    // date.now + waiting_days_for_confirm from branch
    expiredAt: {
        type: Date,
        required: 1
    },
    // enum
    bookingType: {
        type: Number,
        required: 1
    },
    daysCount: {
        type: Number,
        required: 1
    },
    pricePerDay: {
        type: Number,
        required: 1
    },
    discountPerDay: {
        type: Number,
        required: 1
    },
    total: {
        type: Number,
        required: 1
    },
    // 0 not deleted
    // 1 deleted
    deleted: {
        type: Number,
        default: 0,
    }
}, mongooseSchemaOptions_1.mongooseSchemaOptions);
bookingSchema.index({
    'bookingCode': 'text',
    'iqama': 'text',
    'daysCount': 'text',
    'pricePerDay': 'text',
    'discountPerDay': 'text',
    'total': 'text'
});
exports.BookingModel = mongoose_1.default.model('tb_booking', bookingSchema);
;
var EbookingType;
(function (EbookingType) {
    EbookingType[EbookingType["request"] = 0] = "request";
    EbookingType[EbookingType["cancelled"] = 1] = "cancelled";
    EbookingType[EbookingType["waiting_to_confirm"] = 2] = "waiting_to_confirm";
    EbookingType[EbookingType["active"] = 3] = "active";
    EbookingType[EbookingType["expired"] = 4] = "expired";
    EbookingType[EbookingType["done"] = 5] = "done";
    // if returnDate Done and type is active
    // this booking Done
})(EbookingType = exports.EbookingType || (exports.EbookingType = {}));
;
exports.RegExpBookingModel = {
    _id: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    bookingCode: /^[0-9]{1,12}$/,
    iqama: /^[0-9]{1,30}$/,
    // pickUpDate : /^(20(1|2|3)[0-9])-(0[1-9]|(1|2)[0-2])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])$/,
    pickUpDate: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,
    returnDate: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,
    expiredAt: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,
    bookingType: /^[0-5]$/,
    daysCount: /^[1-9]([0-9]{0,3})$/,
    pricePerDay: /^[1-9]([0-9]{0,3})$/,
    discountPerDay: /^[0-9]([0-9]{0,3})$/,
    total: /^[1-9]([0-9]{0,5})$/,
    deleted: /^[0-1]$/,
};
