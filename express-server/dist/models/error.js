"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
// mongoose Schema
const errorSchema = new mongoose_1.default.Schema({
    status: String,
    url: String,
    error: [{
            name: String,
            message: String
        }]
}, { timestamps: true });
exports.ErrorModel = mongoose_1.default.model('error', errorSchema);
