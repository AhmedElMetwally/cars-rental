"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const mongooseSchemaOptions_1 = require("./mongooseSchemaOptions");
const mongoose_1 = __importDefault(require("mongoose"));
const GuestSchema = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: 1,
        trim: 1
    },
    phone: {
        type: String,
        unique: 1,
        required: 1,
        trim: 1
    },
    iqama: {
        type: String,
        trim: 1,
        unique: 1
    },
    iqamaExpiryDate: {
        type: Date,
        trim: 1
    },
    drivingLicence: {
        type: String,
        trim: 1
    },
    drivingLicenceExpiryDate: {
        type: Date,
        trim: 1
    },
    birthday: {
        type: Date,
        trim: 1
    },
    // 0 not deleted
    // 1 deleted
    deleted: {
        type: Number,
        default: 0,
    }
}, mongooseSchemaOptions_1.mongooseSchemaOptions);
GuestSchema.index({
    'name': 'text',
    'phone': 'text',
    'job': 'text',
    'iqama': 'text',
    'drivingLicence': 'text'
});
exports.GuestModel = mongoose_1.default.model('tb_guest', GuestSchema);
;
exports.RegExpGuestModel = {
    _id: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    name: /^[\u0600-\u06FF\w-\s]+$/,
    phone: /^[0-9\-\+]{9,15}$/,
    iqama: /^[0-9]{1,30}$/,
    iqamaExpiryDate: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,
    drivingLicence: /^[0-9]{1,30}$/,
    drivingLicenceExpiryDate: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,
    birthday: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,
    deleted: /^[0-1]$/,
};
