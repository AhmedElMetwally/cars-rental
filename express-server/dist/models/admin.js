"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const mongooseSchemaOptions_1 = require("./mongooseSchemaOptions");
const mongoose_1 = __importDefault(require("mongoose"));
const adminSchema = new mongoose_1.default.Schema({
    email: {
        type: String,
        required: 1,
        unique: 1,
        trim: 1,
        lowercase: 1
    },
    password: {
        type: String,
        required: 1,
    },
    // [admin]
    roles: [{
            type: String,
            required: 1,
        }],
}, mongooseSchemaOptions_1.mongooseSchemaOptions);
exports.AdminModel = mongoose_1.default.model('tb_admin', adminSchema);
;
exports.RegExpAdminModel = {
    _id: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    password: /^.{8,30}$/,
};
const addDefaultData = async () => {
    if (await exports.AdminModel.countDocuments() === 0) {
        exports.AdminModel.create({
            email: 'ahmed@gmail.com',
            password: '123456789',
            roles: ['admin']
        })
            .then(res => {
        })
            .catch(err => {
        });
    }
};
addDefaultData();
