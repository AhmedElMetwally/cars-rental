"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const mongooseSchemaOptions_1 = require("./mongooseSchemaOptions");
const mongoose_1 = __importDefault(require("mongoose"));
const messageSchema = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: 1,
        trim: 1
    },
    email: {
        type: String,
        required: 1,
        lowercase: 1,
        trim: 1,
    },
    message: {
        type: String,
        required: 1,
        trim: 1
    },
    isView: {
        type: Number,
        default: 0,
    },
    // 0 not deleted
    // 1 deleted
    deleted: {
        type: Number,
        default: 0,
    }
}, mongooseSchemaOptions_1.mongooseSchemaOptions);
messageSchema.index({
    'name': 'text',
    'email': 'text',
    'message': 'text',
});
exports.MessageModel = mongoose_1.default.model('tb_message', messageSchema);
;
exports.RegExpMessageModel = {
    _id: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    name: /^[\u0600-\u06FF\w-\s]+$/,
    email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    phone: /^[0-9\-\+]{9,15}$/,
    message: /^(.*){1,500}$/,
};
