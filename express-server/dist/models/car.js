"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const mongooseSchemaOptions_1 = require("./mongooseSchemaOptions");
const mongoose_1 = __importDefault(require("mongoose"));
const CarSchema = new mongoose_1.default.Schema({
    carGroup: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: 1,
        ref: 'tb_carGroup'
    },
    // 123-ABC
    plateNumber: {
        type: String,
        required: 1,
        trim: 1,
        unique: 1,
        lowercase: 1
    },
    // enum
    gearType: {
        type: Number,
        required: 1,
    },
    // enum
    status: {
        type: Number,
        required: 1,
    },
    // 0 not deleted
    // 1 deleted
    deleted: {
        type: Number,
        default: 0,
    },
}, mongooseSchemaOptions_1.mongooseSchemaOptions);
CarSchema.index({
    'plateNumber': 'text',
});
exports.CarModel = mongoose_1.default.model('tb_car', CarSchema);
;
var EgearType;
(function (EgearType) {
    EgearType[EgearType["manual"] = 0] = "manual";
    EgearType[EgearType["automatic"] = 1] = "automatic";
})(EgearType = exports.EgearType || (exports.EgearType = {}));
;
var Estatus;
(function (Estatus) {
    Estatus[Estatus["hiring"] = 0] = "hiring";
    Estatus[Estatus["accident"] = 1] = "accident";
    Estatus[Estatus["sold"] = 2] = "sold";
    Estatus[Estatus["steal"] = 3] = "steal";
    Estatus[Estatus["available"] = 4] = "available";
    Estatus[Estatus["not_available"] = 5] = "not_available";
})(Estatus = exports.Estatus || (exports.Estatus = {}));
;
exports.RegExpCarModel = {
    _id: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    plateNumber: /^[0-9]{4}-[\u0600-\u06FF]{1}\s[\u0600-\u06FF]{1}\s[\u0600-\u06FF]{1}$/,
    gearType: /^[0-1]$/,
    status: /^[0-9]$/,
    deleted: /^[0-1]$/,
};
