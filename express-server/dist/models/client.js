"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const mongooseSchemaOptions_1 = require("./mongooseSchemaOptions");
const mongoose_1 = __importDefault(require("mongoose"));
const clientSchema = new mongoose_1.default.Schema({
    // nationality in en and ar 
    nationality: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'tb_nationality',
    },
    name: {
        type: String,
        required: 1,
        trim: 1
    },
    email: {
        type: String,
        unique: 1,
        required: 1,
        lowercase: 1,
        trim: 1,
    },
    phone: {
        type: String,
        unique: 1,
        required: 1,
        trim: 1
    },
    password: {
        type: String,
        required: 1,
        trim: 1
    },
    iqama: {
        type: String,
        trim: 1,
        index: true,
        unique: true,
        sparse: true
    },
    iqamaExpiryDate: {
        type: Date,
        trim: 1
    },
    drivingLicence: {
        type: String,
        trim: 1
    },
    drivingLicenceExpiryDate: {
        type: Date,
        trim: 1
    },
    birthday: {
        type: Date,
        trim: 1
    },
    address: {
        type: String,
        trim: 1
    },
    job: {
        type: String,
        trim: 1
    },
    image: {
        type: String,
        lowercase: 1
    },
    // enum
    signupFrom: {
        type: Number,
        required: 1
    },
    // [client]
    roles: [{
            type: String,
            required: 1,
        }],
    // 0 not deleted
    // 1 deleted
    deleted: {
        type: Number,
        default: 0,
    }
}, mongooseSchemaOptions_1.mongooseSchemaOptions);
clientSchema.index({
    'name': 'text',
    'email': 'text',
    'phone': 'text',
    'job': 'text',
    'iqama': 'text',
    'drivingLicence': 'text',
    'address': 'text',
});
exports.ClientModel = mongoose_1.default.model('tb_client', clientSchema);
;
var EsignupFrom;
(function (EsignupFrom) {
    EsignupFrom[EsignupFrom["normal_signup"] = 0] = "normal_signup";
    EsignupFrom[EsignupFrom["booking_before_and_signup"] = 1] = "booking_before_and_signup";
})(EsignupFrom = exports.EsignupFrom || (exports.EsignupFrom = {}));
exports.RegExpClientModel = {
    _id: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    name: /^[\u0600-\u06FF\w-\s]+$/,
    email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    phone: /^[0-9\-\+]{9,15}$/,
    password: /^.{8,30}$/,
    iqama: /^[0-9]{1,30}$/,
    iqamaExpiryDate: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,
    drivingLicence: /^[0-9]{1,30}$/,
    drivingLicenceExpiryDate: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,
    birthday: /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,
    address: /^[\u0600-\u06FF\w-\s]+$/,
    job: /^[\u0600-\u06FF\w-\s]+$/,
    signupFrom: /^[0-1]$/,
    deleted: /^[0-1]$/,
};
