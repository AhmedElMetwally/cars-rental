"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const mongooseSchemaOptions_1 = require("./mongooseSchemaOptions");
const mongoose_1 = __importDefault(require("mongoose"));
const EmployeeOnlineTimerSchema = new mongoose_1.default.Schema({
    employee_id: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'tb_employee',
    },
    date: {
        type: String,
        required: 1,
    },
    timeMs: {
        type: Number,
        required: 1
    }
}, mongooseSchemaOptions_1.mongooseSchemaOptions);
exports.EmployeeTimerModel = mongoose_1.default.model('tb_employeeOnlineTimer', EmployeeOnlineTimerSchema);
;
;
exports.RegExpEmployeeTimerModel = {
    _id: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    year: /^20[1-2][0-9]$/,
    month: /^([1-9]|1[0-2])$/,
};
