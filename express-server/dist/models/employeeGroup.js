"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const mongooseSchemaOptions_1 = require("./mongooseSchemaOptions");
const mongoose_1 = __importDefault(require("mongoose"));
const EmployeeGroupSchema = new mongoose_1.default.Schema({
    branches: [{
            type: mongoose_1.default.Schema.Types.ObjectId,
            ref: 'tb_branch'
        }],
    nameEn: {
        type: String,
        unique: 1,
        trim: 1
    },
    nameAr: {
        type: String,
        unique: 1,
        trim: 1
    },
    // enum
    screens: [{
            type: Number,
        }],
    deleted: {
        type: Number,
        default: 0,
    }
}, mongooseSchemaOptions_1.mongooseSchemaOptions);
EmployeeGroupSchema.index({
    'nameEn': 'text',
    'nameAr': 'text',
});
exports.EmployeeGroupModel = mongoose_1.default.model('tb_employeeGroup', EmployeeGroupSchema);
;
var Escreens;
(function (Escreens) {
    Escreens[Escreens["edit_client"] = 0] = "edit_client";
    Escreens[Escreens["edit_client_password"] = 1] = "edit_client_password";
    Escreens[Escreens["delete_client"] = 2] = "delete_client";
    Escreens[Escreens["restore_client"] = 3] = "restore_client";
    Escreens[Escreens["edit_client_image"] = 4] = "edit_client_image";
    Escreens[Escreens["add_car"] = 5] = "add_car";
    Escreens[Escreens["edit_car"] = 6] = "edit_car";
    Escreens[Escreens["delete_car"] = 7] = "delete_car";
    Escreens[Escreens["restore_car"] = 8] = "restore_car";
    Escreens[Escreens["add_car_type"] = 9] = "add_car_type";
    Escreens[Escreens["edit_car_type"] = 10] = "edit_car_type";
    Escreens[Escreens["delete_car_type"] = 11] = "delete_car_type";
    Escreens[Escreens["restore_car_type"] = 12] = "restore_car_type";
    Escreens[Escreens["add_car_model"] = 13] = "add_car_model";
    Escreens[Escreens["edit_car_model"] = 14] = "edit_car_model";
    Escreens[Escreens["delete_car_model"] = 15] = "delete_car_model";
    Escreens[Escreens["restore_car_model"] = 16] = "restore_car_model";
    Escreens[Escreens["add_car_group"] = 17] = "add_car_group";
    Escreens[Escreens["edit_car_group"] = 18] = "edit_car_group";
    Escreens[Escreens["delete_car_group"] = 19] = "delete_car_group";
    Escreens[Escreens["restore_car_group"] = 20] = "restore_car_group";
    Escreens[Escreens["edit_car_group_image"] = 21] = "edit_car_group_image";
    Escreens[Escreens["add_branch"] = 22] = "add_branch";
    Escreens[Escreens["edit_branch"] = 23] = "edit_branch";
    Escreens[Escreens["delete_branch"] = 24] = "delete_branch";
    Escreens[Escreens["restore_branch"] = 25] = "restore_branch";
    Escreens[Escreens["edit_branch_image"] = 26] = "edit_branch_image";
    Escreens[Escreens["edit_booking"] = 27] = "edit_booking";
    Escreens[Escreens["cancel_booking"] = 28] = "cancel_booking";
    Escreens[Escreens["approve_booking"] = 29] = "approve_booking";
    Escreens[Escreens["active_booking"] = 30] = "active_booking";
    Escreens[Escreens["delete_booking"] = 31] = "delete_booking";
    Escreens[Escreens["restore_booking"] = 32] = "restore_booking";
})(Escreens = exports.Escreens || (exports.Escreens = {}));
;
exports.RegExpEmployeeGroupModel = {
    _id: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    nameEn: /^[\w-\s]+$/,
    nameAr: /^[\u0600-\u06FF\w-\s]+$/,
    screens: /^[0-9]{1,3}$/,
    deleted: /^[0-1]$/,
};
