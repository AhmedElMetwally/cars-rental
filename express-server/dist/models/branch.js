"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const redis_1 = require("../helpers/redis");
const mongooseSchemaOptions_1 = require("./mongooseSchemaOptions");
const mongoose_1 = __importDefault(require("mongoose"));
const BranchSchema = new mongoose_1.default.Schema({
    city: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        ref: 'tb_city',
        required: 1
    },
    // branch name in en
    nameEn: {
        type: String,
        required: 1,
        trim: 1,
    },
    // branch name in ar
    nameAr: {
        type: String,
        required: 1,
        trim: 1,
    },
    // branch address in en
    addressEn: {
        type: String,
        required: 1,
        trim: 1,
    },
    // branch address in ar
    addressAr: {
        type: String,
        required: 1,
        trim: 1,
    },
    // html code for en
    overviewEn: {
        type: String,
        required: 1,
    },
    // html code for ar
    overviewAr: {
        type: String,
        required: 1,
    },
    // branch phone
    phone: {
        type: String,
        required: 1,
        trim: 1,
        unique: 1
    },
    // [ longitude, latitude ]
    location: {
        type: [Number, Number],
        index: '2dsphere'
    },
    // booking code [xxx-12345] [bookingStartCode-12345]
    bookingStartCode: {
        type: Number,
        required: 1,
    },
    // how many days to active booking
    waitingHoursForConfirm: {
        type: Number,
        required: 1,
    },
    images: [{
            type: String,
        }],
    // 0 not deleted
    // 1 deleted
    deleted: {
        type: Number,
        default: 0,
    }
}, mongooseSchemaOptions_1.mongooseSchemaOptions);
BranchSchema.index({
    nameEn: 'text',
    nameAr: 'text',
    addressEn: 'text',
    addressAr: 'text',
    bookingStartCode: 'text',
    phone: 'text'
});
exports.BranchModel = mongoose_1.default.model('tb_branch', BranchSchema);
;
exports.RegExpBranchModel = {
    _id: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    nameEn: /^[\w-\s]+$/,
    nameAr: /^[\u0600-\u06FF\w-\s]+$/,
    addressEn: /^[\w-\s]+$/,
    addressAr: /^[\u0600-\u06FF\w-\s]+$/,
    overviewEn: /.*/,
    overviewAr: /.*/,
    phone: /^[0-9\-\+]{9,15}$/,
    location: /^((([1-9])|([1-9]([0-9]+)))|-?\d*(\.\d+)?)$/,
    bookingStartCode: /^[0-9]{3}$/,
    waitingHoursForConfirm: /^[0-9]([0-9]{0,2})$/,
    deleted: /^[0-1]$/,
};
exports.branchBookingCounter = () => {
    return new Promise(async (resolve) => {
        redis_1.redisClient.incr(`branchBookingCounter`, (_err, number) => {
            if (number < 999) {
                resolve(number.toString().padStart(3, '0'));
            }
            else {
                resolve(number.toString());
            }
            ;
        });
    });
};
