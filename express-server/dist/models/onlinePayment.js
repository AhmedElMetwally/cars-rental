"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const mongooseSchemaOptions_1 = require("./mongooseSchemaOptions");
const mongoose_1 = __importDefault(require("mongoose"));
const OnlinePaymentSchema = new mongoose_1.default.Schema({
    client: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        ref: 'tb_client',
        required: 1
    },
    guest: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        ref: 'tb_guest',
        required: 1
    },
    booking: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        ref: 'tb_booking',
        required: 1
    },
    bookingCode: {
        type: String,
        required: 1,
        unique: 1,
    },
    iqama: {
        type: String,
        trim: 1,
    },
    daysCount: {
        type: Number,
        required: 1
    },
    pricePerDay: {
        type: Number,
        required: 1
    },
    discountPerDay: {
        type: Number,
        required: 1
    },
    total: {
        type: Number,
        required: 1
    },
    // 0 not deleted
    // 1 deleted
    deleted: {
        type: Number,
        default: 0,
    }
}, mongooseSchemaOptions_1.mongooseSchemaOptions);
exports.OnlinePaymentModel = mongoose_1.default.model('tb_onlinePayment', OnlinePaymentSchema);
;
exports.RegExpOnlinePaymentModel = {
    _id: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    bookingCode: /^[0-9]{3}-[0-9]{1,9}$/,
    iqama: /^[0-9]{1,30}$/,
    daysCount: /^[1-9]([0-9]{0,3})$/,
    pricePerDay: /^[1-9]([0-9]{0,3})$/,
    discountPerDay: /^[1-9]([0-9]{0,3})$/,
    total: /^[1-9]([0-9]{0,5})$/,
    deleted: /^[0-1]$/,
};
