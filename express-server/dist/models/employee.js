"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const mongooseSchemaOptions_1 = require("./mongooseSchemaOptions");
const employeeSchema = new mongoose_1.default.Schema({
    employeeGroup: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'tb_employeeGroup'
    },
    name: {
        type: String,
        required: 1,
        trim: 1
    },
    email: {
        type: String,
        required: 1,
        unique: 1,
        trim: 1,
        lowercase: 1
    },
    password: {
        type: String,
        required: 1,
        trim: 1
    },
    // [employee]
    roles: [{
            type: String,
            required: 1,
        }],
    // 0 not deleted
    // 1 deleted
    deleted: {
        type: Number,
        default: 0
    }
}, mongooseSchemaOptions_1.mongooseSchemaOptions);
employeeSchema.index({
    'name': 'text',
    'email': 'text',
});
exports.EmployeeModel = mongoose_1.default.model('tb_employee', employeeSchema);
;
exports.RegExpEmployeeModel = {
    _id: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    name: /^[\u0600-\u06FF\w-\s]{1,40}$/,
    email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    password: /^.{8,30}$/,
    deleted: /^[0-1]$/,
};
