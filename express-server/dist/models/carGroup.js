"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const mongooseSchemaOptions_1 = require("./mongooseSchemaOptions");
const mongoose_1 = __importDefault(require("mongoose"));
const CarGroupSchema = new mongoose_1.default.Schema({
    // type name in ar and en
    carType: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: 1,
        ref: 'tb_carType'
    },
    // model name in ar and en
    carModel: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: 1,
        ref: 'tb_carModel'
    },
    // app data for his branch
    branch: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: 1,
        ref: 'tb_branch'
    },
    // year model
    year: {
        type: Number,
        required: 1,
    },
    // price for one day
    pricePerDay: {
        type: Number,
        required: 1,
    },
    allowedKm: {
        type: Number,
        required: 1,
    },
    plusKmPrice: {
        type: Number,
        required: 1,
    },
    discountPerDay: {
        type: Number,
        required: 1,
    },
    images: [{
            type: String,
        }],
    views: {
        type: Number,
        default: 0,
    },
    // 0 not deleted
    // 1 deleted
    deleted: {
        type: Number,
        default: 0,
    },
}, mongooseSchemaOptions_1.mongooseSchemaOptions);
exports.CarGroupModel = mongoose_1.default.model('tb_carGroup', CarGroupSchema);
;
exports.RegExpCarGroupModel = {
    _id: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    year: /^[\d]{4}$/,
    allowedKm: /^[1-9]([0-9]{0,5})$/,
    plusKmPrice: /^([0-9]([0-9]{0,5})|[0-9]([0-9]{0,5}).[1-9]([0-9]{0,5}))$/,
    pricePerDay: /^[1-9]([0-9]{0,3})$/,
    discountPerDay: /^[0-9]([0-9]{0,3})$/,
    views: /^\d+$/,
    deleted: /^[0-1]$/,
};
