"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const mongooseSchemaOptions_1 = require("./mongooseSchemaOptions");
const mongoose_1 = __importDefault(require("mongoose"));
const CarModelSchema = new mongoose_1.default.Schema({
    carType_id: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        ref: 'tb_carType',
        required: 1,
    },
    nameAr: {
        type: String,
        required: 1,
        unique: 1,
        trim: 1,
    },
    nameEn: {
        type: String,
        required: 1,
        unique: 1,
        trim: 1,
    },
    // 0 not deleted
    // 1 deleted
    deleted: {
        type: Number,
        default: 0,
    },
}, mongooseSchemaOptions_1.mongooseSchemaOptions);
exports.CarModelModel = mongoose_1.default.model('tb_carModel', CarModelSchema);
;
exports.RegExpCarModelModel = {
    _id: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    nameEn: /^[\w-\s]+$/,
    nameAr: /^[\u0600-\u06FF\w-\s]+$/,
    deleted: /^[0-1]$/,
};
