"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const mongooseSchemaOptions_1 = require("./mongooseSchemaOptions");
const mongoose_1 = __importDefault(require("mongoose"));
const EmployeeActionSchema = new mongoose_1.default.Schema({
    client: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'tb_client',
        required: 1
    },
    car: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'tb_car',
        required: 1
    },
    carGroup: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'tb_carGroup',
        required: 1
    },
    carType: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'tb_carType',
        required: 1
    },
    carModel: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'tb_carModel',
        required: 1
    },
    booking: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'tb_booking',
        required: 1
    },
    branch: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'tb_branch',
        required: 1
    },
    employee_id: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'tb_employee',
        required: 1
    },
    screen: {
        type: Number,
        required: 1,
    },
    body: {
        type: JSON,
        required: 1
    },
    query: {
        type: JSON,
        required: 1
    },
    itemName: {
        type: Number,
        required: 1,
    },
    deleted: {
        type: Number,
        default: 0,
    }
}, mongooseSchemaOptions_1.mongooseSchemaOptions);
exports.EmployeeActionModel = mongoose_1.default.model('tb_employeeAction', EmployeeActionSchema);
;
var EitemName;
(function (EitemName) {
    EitemName[EitemName["client"] = 0] = "client";
    EitemName[EitemName["car"] = 1] = "car";
    EitemName[EitemName["carGroup"] = 2] = "carGroup";
    EitemName[EitemName["carType"] = 3] = "carType";
    EitemName[EitemName["carModel"] = 4] = "carModel";
    EitemName[EitemName["booking"] = 5] = "booking";
    EitemName[EitemName["branch"] = 6] = "branch";
})(EitemName = exports.EitemName || (exports.EitemName = {}));
;
exports.RegExpEmployeeActionModel = {
    _id: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    screen: /^[0-9]{1,3}$/,
    itemName: /^[0-7]$/,
    deleted: /^[0-1]$/,
};
