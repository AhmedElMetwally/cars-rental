"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const mongooseSchemaOptions_1 = require("./mongooseSchemaOptions");
const mongoose_1 = __importDefault(require("mongoose"));
const path_1 = __importDefault(require("path"));
const citySchema = new mongoose_1.default.Schema({
    nameEn: {
        type: String,
        required: 1,
        unique: 1,
        trim: 1,
    },
    nameAr: {
        type: String,
        required: 1,
        unique: 1,
        trim: 1,
    },
}, mongooseSchemaOptions_1.mongooseSchemaOptions);
exports.CityModel = mongoose_1.default.model('tb_city', citySchema);
;
exports.RegExpCityModel = {
    _id: /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    nameEn: /^[\w-\s]+$/,
    nameAr: /^[\u0600-\u06FF\w-\s]+$/,
    deleted: /^[0-1]$/,
};
const data = require(path_1.default.resolve(`./${process.env.DEFAULT_APP_DATA}/cities-of-saudi-arabia.json`));
const addDefaultData = async () => {
    if (await exports.CityModel.countDocuments() === 0) {
        exports.CityModel.insertMany(data)
            .then(res => {
        })
            .catch(err => {
        });
    }
};
addDefaultData();
