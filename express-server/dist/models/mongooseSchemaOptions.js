"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mongooseSchemaOptions = {
    validateBeforeSave: false,
    versionKey: false,
    timestamps: {
        createdAt: "createdAt",
        updatedAt: 'updatedAt'
    }
};
