#!/bin/bash

# install docker
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
apt-cache policy docker-ce
sudo apt-get install docker-ce -y
sudo usermod -aG docker ${USER}
su - ${USER}
id -nG
sudo usermod -aG docker root
docker --version

# install docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version

# run install sh
# sudo chmod 755 install.sh &&./install.sh

# run and watch ternmal
# docker-compose build --no-cache && docker-compose up

# run prod
# docker-compose up -d

# stop prod
# docker-compose stop

# to copy images from old to new 
# cp -r cars-booking-new/shared/images cars-rental/shared/

# to mongoDB from old to new  
# cp -r cars-booking-new/mongo-database/data cars-rental/mongo-database/

# to redisDB from old to new  
# cp -r cars-booking-new/redis-database/data cars-rental/redis-database/

# Delete all containers
# docker rm $(docker ps -a -q)

# Delete all images
# docker rmi $(docker images -q)




# create passwrod mongo
# db.createUser({
#     user:"username" , 
#     pwd:"password" , 
#     roles : [ 
#         { 
#             role : "readWrite" , 
#             db : "testdatabase"
#         },
#         { 
#             role : "dbOwner" , 
#             db : "testdatabase"
#         },
#     ]
# })
# security:
#   authorization: enabled
