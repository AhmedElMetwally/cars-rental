var express = require('express');
var app = express();

app.use('/.well-known/acme-challenge' ,express.static('./nginx-server/ssl'))

app.get('/', function (req, res) {
  res.send('Hello World')
})

app.listen(80)
