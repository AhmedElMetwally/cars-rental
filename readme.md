# Cars Rental

## Angular 6
## Nodejs - ExpressJS
## Socket.io
## Mongo
## Redis
## Nginx
## Docker

## To Run Project
``` bash
    docker-compose up -d
```

## To Stop Project
``` bash
    docker-compose stop
```