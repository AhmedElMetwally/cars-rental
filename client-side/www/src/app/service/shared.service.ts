import { RegExpBookingModel } from './../models/bookingModel';
import { RegExpGuestModel } from './../models/guestModel';
import { RegExpNationalityModel } from './../models/nationalityModel';
import { RegExpMessageModel } from './../models/message';
import { RegExpClientModel } from './../models/clientModel';
import { Injectable } from '@angular/core';
import { mainErrorEnum , mainErrorEnumEn , mainErrorEnumAr } from './../../../../../shared/enums/error.enum';
import { AbstractControl } from '@angular/forms';
import { EbookingType, EbookingTypeAr, EbookingTypeEn } from '../enums/booking.enum';

@Injectable({
    providedIn : 'root'
})
export class SharedService {

    constructor(){ };

    public mainRegExp = {
        RegExpGuestModel,
        RegExpClientModel,
        RegExpMessageModel,
        RegExpBookingModel,
        RegExpNationalityModel
    };

    public mainErrorEnum = mainErrorEnum;
    public mainErrorEnumEn = mainErrorEnumEn;
    public mainErrorEnumAr = mainErrorEnumAr;

    public EbookingType = EbookingType;
    public EbookingTypeAr = EbookingTypeAr;
    public EbookingTypeEn = EbookingTypeEn;
    

    public interval(cb: any , count:number = 4): void {
        cb();
        let x = 1;
        const interval = setInterval(() => {
          if(x >= count) {
            clearInterval(interval);
          };
          cb();
          x += 1;
        } , 300);
    };
    public numberEnToAr(str: string): string {
        const numberEnToArMap: string[] = [
            '٠', 
            '١',
            '٢',
            '٣', 
            '٤',
            '٥',
            '٦',
            '٧',
            '٨', 
            '٩'
        ];
        return str.replace(/\d/g, function($0) { return numberEnToArMap[$0]})
    };
    public numberArToEn(str: string): string {
        str = str.replace(/٠/g, '0');
        str = str.replace(/١/g, '1');
        str = str.replace(/٢/g, '2');
        str = str.replace(/٣/g, '3');
        str = str.replace(/٤/g, '4');
        str = str.replace(/٥/g, '5');
        str = str.replace(/٦/g, '6');
        str = str.replace(/٧/g, '7');
        str = str.replace(/٨/g, '8');
        str = str.replace(/٩/g, '9');
        return str;
    };
    public clearLocalStorage(): void {
        localStorage.removeItem('token');
        localStorage.removeItem('client');
    };
    public getErrorMsg(errorEnum: mainErrorEnum): string {
        if((window as any).lang == 'ar') {
            return mainErrorEnumAr[errorEnum];
        } else {
            return mainErrorEnumEn[errorEnum];
        };
    };
    public customValidator(requiredErrorEnum: mainErrorEnum , invalidErrorEnum: mainErrorEnum , regExp: RegExp): any {
        return (control: AbstractControl) => {
            if(Array.isArray(control.value) ) {
                if ( ! control.value.every( v => regExp.test(v)) ) {
                    return {
                        errorEnum : invalidErrorEnum
                    };
                } else {
                    return null
                }; 
            } else {
                if(control.value === '' || control.value === null){
                    return {
                        errorEnum : requiredErrorEnum
                    };
                } else if ( !regExp.test(control.value) ) {
                    return {
                        errorEnum : invalidErrorEnum
                    };
                } else {
                    return null
                }; 
            };
        }; 
    };

};
