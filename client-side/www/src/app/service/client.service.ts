import { HttpClient, HttpHeaders , HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { IClientModel } from './../../../../admin/src/app/models/clientModel';
import { Router } from '@angular/router';
import { SharedService } from './shared.service';

export interface IViewCarGroups {
    page: number,
    limit: number,
    pickUpDate: string|Date,
    returnDate: string|Date,
    city_id?: string;
    filters: {
        branch_id?: string;
        carType_id?: string;
        carModel_id?: string;
        year?: string;
    }
};


@Injectable({
    providedIn : 'root'
})
export class ClientService {
    
    constructor(
        private router: Router,
        private http: HttpClient,
        private sharedService: SharedService,
    ){ };

    private get headers(): HttpHeaders {
        return new HttpHeaders()
        .append('Content-Type', 'application/json')
        .append('token', localStorage.getItem('token'))
    };


    public signupClient(client: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/client/signup`,
            client
         ).toPromise();
    };
    public signinClient(emailOrPhone: string , password: string): Promise<any> {
        return this.http.post(
            `${environment.url}/api/client/signin`,
            {
                emailOrPhone,
                password
            }
         ).toPromise();
    };
    public isClient(): boolean {
        return localStorage.getItem('client') !== null;
    };
    public isClientHaveFullDate(): boolean {
        const client: IClientModel = JSON.parse(localStorage.getItem('client'));
        return (client.iqama && 
          client.iqamaExpiryDate && 
          client.drivingLicence && 
          client.drivingLicenceExpiryDate &&
          client.birthday) ? true : false ;
    }
    public refreshClientToken(): Promise<any> {
        return this.http.get(
          `${environment.url}/api/client/refresh-token`,
          {
            headers : this.headers
          }
        )
        .toPromise()
    };
    public signout(): void {
        this.sharedService.clearLocalStorage();
        this.router.navigate(['employee-signin']);
    };
    public getCarGroup(carGroup_id: string): Promise<any> {
        const params = new HttpParams()
        .set('carGroup_id', carGroup_id);
        return this.http.get(
           `${environment.url}/api/client/view/car-group`,
           {
            params
           }
        ).toPromise();
    };
    public getCarGroups($event: IViewCarGroups): Promise<any> {
        return this.http.post(
           `${environment.url}/api/client/view/car-groups`,
            $event 
        ).toPromise();
    };
    public getAllBranchesByCityFilter(city_id: string): Promise<any> {
        const params = new HttpParams()
        .set('city_id', city_id);
        return this.http.get(
            `${environment.url}/api/client/view/all-branches-by-city-filter`,
            {
                params
            }
         ).toPromise();
    };
    public getAllBranches(): Promise<any> {
        return this.http.get(
            `${environment.url}/api/client/view/all-branches`,
         ).toPromise();
    };
    public getAllBranchesLocations(): Promise<any> {
        return this.http.get(
            `${environment.url}/api/client/view/all-branches-location`,
         ).toPromise();
    };
    public getAllCarTypes(): Promise<any> {
        return this.http.get(
            `${environment.url}/api/client/view/all-car-types`,
         ).toPromise();
    };
    public getAllNationalities(): Promise<any> {
        return this.http.get(
            `${environment.url}/api/client/view/all-nationalities`,
         ).toPromise();
    };
    public getAllCarModelsByCarType(carType_id: string): Promise<any> {
        const params = new HttpParams()
        .set('carType_id', carType_id);
        return this.http.get(
            `${environment.url}/api/client/view/all-car-models-by-car-type`,
            {
                params
            }
         ).toPromise();
    };
    public sentMessage(message: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/client/send-message`,
            message
         ).toPromise();
    };
    public editClient(obj): Promise<any> {
        return this.http.put(
            `${environment.url}/api/client/edit`,
            obj,
            { 
                headers : this.headers 
            },
        ).toPromise()
    };
    public editClientPassword(obj): Promise<any> {
        return this.http.put(
            `${environment.url}/api/client/edit-password`,
            obj,
            { 
                headers : this.headers 
            },
        ).toPromise()
    };
    public addClientImage(obj): Promise<any> {
        const headers = new HttpHeaders()
        .append('token', localStorage.getItem('token'))
        return this.http.post(
            `${environment.url}/api/client/add-image`,
            obj,
            { 
                headers 
            },
        ).toPromise()
    };
    public getThisClient(): Promise<any> {
        return this.http.get(
          `${environment.url}/api/client/view-this-client`,
          {
            headers : this.headers
          }
        )
        .toPromise()
    };
    public addBooking(obj): Promise<any> {
        return this.http.post(
            `${environment.url}/api/client/add-booking`,
            obj,
            {
                headers : this.headers
            }
        )
        .toPromise()
    };
    public getBookingsByCodeAndIqama(bookingCode: string, iqama: string): Promise<any> {
        const params = new HttpParams()
        .set('iqama', iqama)
        .set('bookingCode', bookingCode);
        return this.http.get(
            `${environment.url}/api/client/view/booking-by-code-and-iqama`,
            {
                params
            }
        )
        .toPromise()
    };
    public getAllCities(): Promise<any> {
        return this.http.get(
            `${environment.url}/api/client/view/all-cities`,
        ).toPromise()
    };
    public saveClientInLocalStorage(client): void {
        localStorage.setItem('client' , JSON.stringify(client))
    };

    public getThisClientBooking(booking_id: string): Promise<any> {
        const params = new HttpParams()
        .set('booking_id', booking_id);

        return this.http.get(
          `${environment.url}/api/client/view-this-client-booking`,
          {
            headers : this.headers,
            params
          }
        )
        .toPromise()
    };
    public getThisClientBookings(page: number , limit: number , sortBy: string): Promise<any> {
        const params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
        ;
        return this.http.get(
           `${environment.url}/api/client/view-this-client-bookings`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public getThisClientBookingsSearch(page: number , limit: number , searchQuery: string): Promise<any> {
        const params = new HttpParams()
        .set('page', page.toString())
        .set('limit', limit.toString())
        .set('searchQuery', searchQuery)
        ;
        return this.http.get(
           `${environment.url}/api/client/view-this-client-bookings-search`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    

};
