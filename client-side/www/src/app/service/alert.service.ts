import { SharedService } from './shared.service';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup } from '@angular/forms';
import { LoadingService } from './loading.service';
import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
declare let swal;

 
@Injectable({
  providedIn: 'root'
})
export class AlertService {
  
  constructor(
    private loadingService: LoadingService,
    private translateService: TranslateService,
    private sharedService: SharedService,
  ){};


  public alertSuccess(msg: string): void {
    this.translateService.get('close')
    .toPromise()
    .then( v => {
      swal({
        title: '',
        text: msg ,
        icon: 'success',
        buttons: v
      });
    })
  };

  public alertError(msg: string): void {
    this.translateService.get('close')
    .toPromise()
    .then( v => {
      swal({
        title: '',
        text: msg ,
        icon: 'error',
        buttons: v
      });
    });
  };
  public async alertSuccessInSpan(span: HTMLSpanElement): Promise<any> {
    const ok = await this.translateService.get('ok').toPromise()
    return swal({
      title: '',
      content: span ,
      icon: 'success',
      button: ok ,
    });
  };
  public async askInSpan(span: HTMLSpanElement): Promise<any> {
    const obj = await this.translateService.get(['cancel' , 'ok']).toPromise()
    return swal({
      title: '',
      content: span ,
      icon: 'warning',
      // dangerMode: true,
      buttons: [obj.cancel , obj.ok ],
    });
  };
  public async ask(msg: string): Promise<any> {
    const obj = await this.translateService.get(['cancel' , 'ok']).toPromise()
    return swal({
      title: '',
      text: msg ,
      icon: 'warning',
      // dangerMode: true,
      buttons: [obj.cancel , obj.ok ],
    });
  };
  public done(): void {
    this.translateService.get(['done' , 'close'])
    .toPromise()
    .then( obj => {
      swal({
        title: '',
        text: obj.done,
        icon: 'success',
        buttons: obj.close,
      });
    });
  };
  public showFormError(form: FormGroup): void {
    for(const i in form.controls){
      if(form.controls[i].errors){
        this.alertError(
          this.sharedService.getErrorMsg(form.controls[i].errors.errorEnum)
        );
        break;
      };
    };
  };
  public httpError(httpError: HttpErrorResponse): void {
    
    console.log({httpError});
    
    this.loadingService.hide();
    
    if(httpError.error && httpError.error.errorEnum) {
      this.alertError(
        this.sharedService.getErrorMsg(httpError.error.errorEnum)
      );
    };

    if(httpError.status === 401) {
      this.sharedService.clearLocalStorage();
    };

  };

};
