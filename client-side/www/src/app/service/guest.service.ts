import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
 
@Injectable({
    providedIn : 'root'
})
export class GuestService {
    
    constructor(
        private http: HttpClient,
    ){ };

    public getOrCreateGuest(guest: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/guest/get-or-create-guest`,
            guest
         ).toPromise();
    };
    public addBooking(obj): Promise<any> {
        return this.http.post(
            `${environment.url}/api/guest/add-booking`,
            obj,
        )
        .toPromise()
    };
    public isGuest(): boolean {
        if(localStorage.getItem('guestExpiredDate') !== null && localStorage.getItem('guest') !== null) {
            const guestExpiredDate = localStorage.getItem('guestExpiredDate');
            if(parseInt(guestExpiredDate) > new Date().getTime()) {
                return true
            } else {
                localStorage.removeItem('guestExpiredDate');
                localStorage.removeItem('guest');
                return false; 
            }
        };
        return false; 
    };
    public getGuest() {
        try {
            if(this.isGuest()) {
                const guest = localStorage.getItem('guest');
                return JSON.parse(guest);
            } else {
                return {};
            }
        } catch ( err ) {
            return {};
        };
    };


}
