import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class HomeService {
    
    constructor(
        private http: HttpClient,
    ){ };

    public sentMessage(message: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/client/send-message`,
            message
         ).toPromise();
    };

}
