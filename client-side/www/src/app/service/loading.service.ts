import { Injectable , EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  
  public  loading_occured: EventEmitter<boolean> = new EventEmitter<boolean>();
  private display:  boolean;

  private emit(): void {
    this.loading_occured.emit( this.display );
  }

  public hide(): void {
    this.display = false;
    this.emit();
  }

  public show(): void {
      this.display = true;
      this.emit();
  }

}
