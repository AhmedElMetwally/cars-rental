import { ClientService } from './../service/client.service';
import { Injectable } from '@angular/core';
import { CanActivate , Router } from '@angular/router';

@Injectable({
  providedIn : 'root'
})
export class isClientGuard implements CanActivate {
  constructor(
    private clientService: ClientService ,
    private router:Router
  ){ }

  canActivate():  boolean {
    if(this.clientService.isClient()) {
      return true;
    } else {
      this.router.navigate(['/signup']);
      return false;
    };
  };

};

@Injectable({
  providedIn : 'root'
})
export class isNotClientGuard implements CanActivate {
  constructor(
    private clientService: ClientService ,
    private router:Router
  ){ }

  canActivate():  boolean {
    if(this.clientService.isClient()) {
      this.router.navigate(['/home']);
      return false;
    } else {
      return true;
    }
  };
  
};