import { GuestService } from '../service/guest.service';
import { Injectable } from '@angular/core';
import { CanActivate , Router } from '@angular/router';

@Injectable({
  providedIn : 'root'
})
export class isNotGuestGuard implements CanActivate {
  constructor(
    private guestService: GuestService ,
    private router:Router
  ){ }

  canActivate():  boolean {
    if(this.guestService.isGuest()) {
      this.router.navigate(['/home']);
      return false;
    } else {
      return true;
    }
  };

};