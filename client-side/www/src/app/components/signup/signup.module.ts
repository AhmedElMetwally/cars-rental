import { InputErrorInFormModule } from '../common/input-error-in-form/input-error-in-form.module';
import { SignupRoutingModule } from './signup-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SignupComponent } from './signup.component';
import { NgModule } from '@angular/core';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    SignupRoutingModule,
    ReactiveFormsModule,
    TranslateModuleForChild,
    InputErrorInFormModule
  ],
  declarations: [
    SignupComponent,
  ]
})
export class SignupModule { }
