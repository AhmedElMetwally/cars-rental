import { isPlatformBrowser } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../service/shared.service';
import { LoadingService } from '../../service/loading.service';
import { AlertService } from '../../service/alert.service';
import { FormGroup , FormControl } from '@angular/forms';
import { ClientService } from '../../service/client.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
declare const $;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
})
export class SignupComponent implements OnInit , OnDestroy {

  constructor( 
    private title: Title,
    private router: Router,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private sharedService: SharedService,
    private clientService: ClientService,
    private translateService: TranslateService,
  ) { };

  public signupForm: FormGroup;
  private translateServiceSubscribe: any;

  private initForm(): void {
    this.signupForm = new FormGroup({ 
      name : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.name_required , 
          this.sharedService.mainErrorEnum.name_invalid , 
          this.sharedService.mainRegExp.RegExpClientModel.name
        ),
      ]),
      email : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.email_required , 
          this.sharedService.mainErrorEnum.email_invalid , 
          this.sharedService.mainRegExp.RegExpClientModel.email
        ),
      ]),
      phone : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.phone_required , 
          this.sharedService.mainErrorEnum.phone_invalid , 
          this.sharedService.mainRegExp.RegExpClientModel.phone
        ),
      ]),
      password : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.password_required , 
          this.sharedService.mainErrorEnum.password_invalid , 
          this.sharedService.mainRegExp.RegExpClientModel.password 
        ),
      ]),
    });
  };

  public onSubmit(): void {
    if(this.signupForm.invalid) {

      return this.alertService.showFormError( this.signupForm );

    } else {
      
      this.loadingService.show();
    
      let name: string = this.signupForm.controls['name'].value;
      let phone: string = this.signupForm.controls['phone'].value;
      let email: string = this.signupForm.controls['email'].value;
      let password: string = this.signupForm.controls['password'].value;

      const client = {
        name,phone,email,password
      };

      this.clientService.signupClient(client)
      .then( (res: any) => {
        this.sharedService.clearLocalStorage();
        localStorage.setItem('token' , res.token);
        localStorage.setItem('client' , JSON.stringify(res.client) );
        this.router.navigate(['/view-client']);
        this.loadingService.hide();
      })
      .catch( e => this.alertService.httpError(e));

    };
  };
 
  private setTitle(): void {
    this.translateService.get('signin')
    .toPromise()
    .then( title => {
      this.title.setTitle(title);
    });
  };

  private onLangChange(): void {
    this.setTitle();
  };
  private init(): void {
    this.setTitle();
    this.initForm();
    if(isPlatformBrowser) {
      this.sharedService.interval(() => {
        this.initJquert();
      });
    };
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
  };

  private initJquert(): void {
    $('.js-tilt img').tilt();
  };


}

