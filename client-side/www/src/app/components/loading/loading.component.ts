import { LoadingService } from '../../service/loading.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {

  constructor(
    private loadingService: LoadingService
  ) { };

  public display: boolean = false;

  public ngOnInit() {
    this.loadingService
    .loading_occured
    .subscribe( ( display_loading: boolean ): void => {
      this.display = display_loading;
    });
  };

}
