import { ViewCarGroupComponent } from './view-car-group/view-car-group.component';
import { ViewCarGroupsComponent } from './view-car-groups.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path : '',
                component : ViewCarGroupsComponent
            },
            {
                path : ':carGroup_id',
                component : ViewCarGroupComponent
            },
        ])
    ],
    exports: [ RouterModule ]
})
export class ViewCarGroupsRoutingModule {}
