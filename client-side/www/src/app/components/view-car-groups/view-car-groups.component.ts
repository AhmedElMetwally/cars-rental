import { AlertService } from './../../service/alert.service';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { environment } from './../../../environments/environment';
import { ClientService, IViewCarGroups } from './../../service/client.service';
import { ICarGroupModel } from './../../models/carGroupModel';
import { SharedService } from '../../service/shared.service';
import { Component, OnInit} from '@angular/core';
import { SelectItem } from 'primeng/components/common/selectitem';
declare const math ,$;


@Component({
  selector: 'app-view-car-groups',
  templateUrl: './view-car-groups.component.html',
  styleUrls : ['./view-car-groups.component.scss']
})
export class ViewCarGroupsComponent implements OnInit {

  constructor(
    private title: Title,
    private translateService: TranslateService,
    private sharedService: SharedService,
    private clientService: ClientService,
    private alertService: AlertService,
  ){};

  public carGroups: ICarGroupModel[] = [];
  public imageUrl: string = environment.url + '/images/';

  public carTypesOptions: SelectItem[] = [];
  public carModelsOptions: SelectItem[] = [];
  public yearsOptions: SelectItem[] = [];
  public branchesOptions: SelectItem[] = [];
  public citiesOptions: SelectItem[] = [];

  private allCarTypes: any[];
  private allCities: any[];

  public yearModel: any = false;
  public branchModel: any = false;
  public carTypeModel: any = false;
  public carModelModel: any = false;
  public cityModel: any = false;
  public pickUpDateModel: any;
  public returnDateModel: any;

  private page: number = 1;
  private limit: number = 20;

  public minDateForPickUpDate = new Date();
  public maxDateForPickUpDate;
  public minDateForReturnDate = new Date();
  
  private translateServiceSubscribe: any;

  private scrollIsLock: boolean = true;

  public loading: boolean;

  public isRequiredOptionsExists: boolean = false;


  public async start() {
    if(this.pickUpDateModel && this.returnDateModel && this.checkIfDateInvalid()) {
      await this.reset();
      this.onLazyLoad();
      this.setCacheData();
    };
  };

  // required
  private checkIfRequiredOptionsExists(): void {
    if(this.pickUpDateModel && this.returnDateModel && this.checkIfDateInvalid()) {
      this.isRequiredOptionsExists =  true;
      this.start();
    };
  };
  private checkIfDateInvalid(): boolean {
    return this.pickUpDateModel && this.returnDateModel && 
    new Date(this.pickUpDateModel) < new Date(this.returnDateModel) && 
    new Date(this.pickUpDateModel).getTime() !== new Date(this.returnDateModel).getTime();
  };
  public onSelectPickUpDate(): void {
    this.minDateForReturnDate = new Date(this.pickUpDateModel.getTime() + 1000*60*60*24);
    this.checkIfRequiredOptionsExists();      
  };
  public onSelectReturnDate(): void {
    this.maxDateForPickUpDate = new Date(this.returnDateModel.getTime() - 1000*60*60*24);
    this.checkIfRequiredOptionsExists();      
    
  };
  public onSelectCity(): void {
    if(this.cityModel){
      this.setBranchesByCityFilter(this.cityModel);
    } else {
      this.setBranches();
    };
    this.checkIfRequiredOptionsExists();      
  };


  // on open
  private setCities(): void {
    const field = (window as any).lang === 'ar' ? 'nameAr' : 'nameEn';
    const clear: SelectItem = {
      label : (window as any).lang === 'ar' ? 'كل المدن' : 'All Cities',
      value : false 
    };

    if(!this.cityModel) {
      this.cityModel = false;
    };

    if(this.allCities) {

      this.citiesOptions = [ clear , ...this.allCities.map( v => {
        return {
          label : v[field],
          value : v._id,
        };
      })];

    } else {

      this.clientService.getAllCities()
      .then( res => {
        this.allCities = res.cities;
        this.citiesOptions = [ clear , ...res.cities.map( v => {
          return {
            label : v[field],
            value : v._id,
          };
        })];
      })
      .catch( e => this.alertService.httpError(e) );

    };
  };
  private setTitle(): void {
    this.translateService.get('view_car_groups')
    .toPromise()
    .then( title => {
      this.title.setTitle(title);
    });
  };

  
  // ajax
  private getCarGroups(): void {

    const $event: IViewCarGroups = {
      page : this.page,
      limit : this.limit,
      pickUpDate : this.pickUpDateModel,
      returnDate : this.returnDateModel,
      ...this.cityModel ? { city_id : this.cityModel} : {},
      filters : {
        ...this.branchModel ? { branch_id : this.branchModel} : {},
        ...this.carTypeModel ? { carType_id : this.carTypeModel} : {},
        ...this.carModelModel ? { carModel_id : this.carModelModel} : {},
        ...this.yearModel ? { year : this.yearModel} : {},
      }
    };

    this.clientService.getCarGroups($event)
    .then( res => {
      this.showCarGroups(res.carGroups)
    })
    .catch( e => this.alertService.httpError(e) );

  };
  private showCarGroups(carGroups: ICarGroupModel[]): void {

    // if length < limit (stop new loading)
    this.scrollIsLock = carGroups.length < this.limit;

    // add 1 to page
    this.page += 1 ;

    // add new data
    this.carGroups.push(...carGroups);
    
    // stop loading
    this.loading = false;
  };
  public onLazyLoad(): void {
    // if lock stop here
    if( ! this.scrollIsLock ) {

      // lock before get data
      this.scrollIsLock = true;

      this.getCarGroups();

      this.loading = true;

    };
  };
  public reset(): Promise<void> {
    return new Promise( resolve => {
      this.carGroups = [];
      this.page = 1;
      this.scrollIsLock = false;
      resolve();
    });
  };


  // optional
  private setCarTypes(): void {
    const field = (window as any).lang === 'ar' ? 'nameAr' : 'nameEn';
    const clear: SelectItem = {
      label : (window as any).lang === 'ar' ? 'كل انواع السيارات' : 'All Cars Types',
      value : false 
    };

    if(!this.carTypeModel) {
      this.carTypeModel = false;
    };

    if(this.allCarTypes) {

      this.carTypesOptions = [ clear , ...this.allCarTypes.map( v => {
        return {
          label : v[field],
          value : v._id,
        };
      })];

    } else {

      this.clientService.getAllCarTypes()
      .then( res => {
        this.allCarTypes = res.carTypes;
        this.carTypesOptions = [ clear , ...res.carTypes.map( v => {
          return {
            label : v[field],
            value : v._id,
          };
        })];
      })
      .catch( e => this.alertService.httpError(e) );

    };

  };
  public onSelectCarType(): void {
    if(this.carTypeModel) {
      this.setCarModels(this.carTypeModel);
    } else {
      this.carModelsOptions = [];
      this.deleteCacheItem('carTypeModel');
    };
  };
  private setCarModels(carType_id: string): void {
    const field = (window as any).lang === 'ar' ? 'nameAr' : 'nameEn';
    const clear: SelectItem = {
      label : (window as any).lang === 'ar' ? 'كل موديلات السيارات' : 'All Cars Models',
      value : false 
    };

    if(!this.carModelModel) {
      this.carModelModel = false;
    };

    this.clientService.getAllCarModelsByCarType(carType_id)
    .then( res => {
      this.carModelsOptions = [clear , ...res.carModels.map( v => {
        return {
          label : v[field],
          value : v._id,
        };
      })];
    })
    .catch( e => this.alertService.httpError(e) );
   
  };
  private setBranchesByCityFilter(city_id: string): void {
    const field = (window as any).lang === 'ar' ? 'nameAr' : 'nameEn';
    const clear: SelectItem = {
      label : (window as any).lang === 'ar' ? 'كل الفروع ' : 'All Branches',
      value : false 
    };

    if(!this.branchModel) {
      this.branchModel = false;
    };

    this.clientService.getAllBranchesByCityFilter(city_id)
    .then( res => {
      this.branchesOptions = [clear , ...res.branches.map( v => {
        return {
          label : v[field],
          value : v._id,
        };
      })];
    })
    .catch( e => this.alertService.httpError(e) );

  };
  private setBranches(): void {
    const field = (window as any).lang === 'ar' ? 'nameAr' : 'nameEn';
    const clear: SelectItem = {
      label : (window as any).lang === 'ar' ? 'كل الفروع ' : 'All Branches',
      value : false 
    };

    if(!this.branchModel) {
      this.branchModel = false;
    };

    this.clientService.getAllBranches()
    .then( res => {
      this.branchesOptions = [clear , ...res.branches.map( v => {
        return {
          label : v[field],
          value : v._id,
        };
      })];
    })
    .catch( e => this.alertService.httpError(e) );

  };
  private setYears(): void {
    const yearsOptions = [];
    const clear: SelectItem = {
      label : (window as any).lang === 'ar' ? 'كل السنوات ' : 'All Years',
      value : false 
    };

    if(!this.yearModel) {
      this.deleteCacheItem('yearModel');
      this.yearModel = false;
    };

    for(let i = new Date().getFullYear() , len = 1960; i >= len ;--i ) {
      yearsOptions.push({
        label : (window as any).lang === 'ar' ? this.sharedService.numberEnToAr(i.toString()) : i.toString(),
        value : i.toString(),
      });
    };
    this.yearsOptions = [clear , ...yearsOptions];
  };
  

  private deleteCacheItem(item: string): void {
    localStorage.removeItem(item);
  };

  public getCacheData(): void {
    const viewCarGroupsExpiredDate = localStorage.getItem('viewCarGroupsExpiredDate') || '100';
    if(parseInt(viewCarGroupsExpiredDate) < new Date().getTime()) {
      
      localStorage.removeItem('viewCarGroupsExpiredDate');  
      localStorage.removeItem('yearModel');  
      localStorage.removeItem('carTypeModel');  
      localStorage.removeItem('pickUpDateModel');  
      localStorage.removeItem('returnDateModel');  
      localStorage.removeItem('cityModel');

    } else {

      if(localStorage.getItem('yearModel') !== null) {
        this.yearModel = localStorage.getItem('yearModel');
        this.start();
      };
      if(localStorage.getItem('carTypeModel') !== null) {
        this.carTypeModel = localStorage.getItem('carTypeModel');
        this.onSelectCarType();
      };
      if(localStorage.getItem('pickUpDateModel') !== null) {
        this.pickUpDateModel = new Date(localStorage.getItem('pickUpDateModel'));
        this.onSelectPickUpDate();
      };
      if(localStorage.getItem('returnDateModel') !== null) {
        this.returnDateModel = new Date(localStorage.getItem('returnDateModel'));
        this.onSelectReturnDate();
      };
      if(localStorage.getItem('cityModel') !== null) {
        this.cityModel = localStorage.getItem('cityModel');
        this.onSelectCity();
      };

    };

  };
  public setCacheData(): void {
    const viewCarGroupsExpiredDate = (new Date().getTime() + 1000 * 60 * 60 * 1).toString(); 
    if(this.pickUpDateModel){
      localStorage.setItem('viewCarGroupsExpiredDate' ,viewCarGroupsExpiredDate );
      localStorage.setItem('pickUpDateModel' , this.pickUpDateModel);
    };
    if(this.returnDateModel){
      localStorage.setItem('viewCarGroupsExpiredDate' , viewCarGroupsExpiredDate);
      localStorage.setItem('returnDateModel' , this.returnDateModel);
    };
    if(this.cityModel){
      localStorage.setItem('viewCarGroupsExpiredDate' , viewCarGroupsExpiredDate);
      localStorage.setItem('cityModel' , this.cityModel);
    };
    if(this.yearModel){
      localStorage.setItem('viewCarGroupsExpiredDate' , viewCarGroupsExpiredDate);
      localStorage.setItem('yearModel' , this.yearModel);
    };
    if(this.carTypeModel){
      localStorage.setItem('viewCarGroupsExpiredDate' , viewCarGroupsExpiredDate);
      localStorage.setItem('carTypeModel' , this.carTypeModel);
    };
  };

  private init(){
    this.getCacheData();
    this.setTitle();
    this.setCarTypes();
    this.setYears();
    this.setCities();
    this.setBranches();
  };
  private onLangChange(): void {
    this.setTitle();
    this.setCarTypes();
    this.setYears();
    this.setCities();

    if(this.carTypeModel) {
      this.setCarModels(this.carTypeModel);
    };

    if(this.cityModel) {
      this.setBranchesByCityFilter(this.cityModel);
    } else {
      this.setBranches();
    };
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };
  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
  };

  public getTotlaPrice(pricePerDay: number ,discountPerDay: number): number {
    return math
    .chain(pricePerDay)
    .subtract(discountPerDay)
    .multiply(this.getDaysCount())
    .done();
  };
  public getDaysCount(): number {
    const date1 = new Date(this.returnDateModel);
    const date2 = new Date(this.pickUpDateModel);
    const timeDiff = Math.abs(date2.getTime() - date1.getTime());
    const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
    return diffDays
  };
  

};
