import { GuestService } from './../../service/guest.service';
import { PanelHeadingInProfileModule } from './../common/panel-heading-in-profile/panel-heading-in-profile.module';
import { ImageSliderModule } from './../common/image-slider/image-slider.module';
import { InputTextInProfileModule } from './../common/input-text-in-profile/input-text-in-profile.module';
import { ViewCarGroupComponent } from './view-car-group/view-car-group.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { BtnModule } from './../common/btn/btn.module';
import { TranslateModuleForChild } from './../../modules/TranslateModule';
import { FormsModule } from '@angular/forms';
import { SelectOneModule } from './../common/select-one/select-one.module';
import { ViewCarGroupsRoutingModule } from './view-car-groups.routing.module';
import { NgModule } from '@angular/core';
import { ViewCarGroupsComponent } from './view-car-groups.component';
import { CommonModule } from '@angular/common';
import { CalendarModule } from 'primeng/calendar';
import { TooltipModule } from 'primeng/tooltip';
import { PipesModule } from '../../modules/PipesModule';
import { ClientService } from './../../service/client.service';

@NgModule({
  imports: [
    CommonModule,
    ViewCarGroupsRoutingModule,
    SelectOneModule,
    CalendarModule,
    FormsModule,
    TooltipModule,
    TranslateModuleForChild,
    PipesModule,
    BtnModule,
    InfiniteScrollModule,
    InputTextInProfileModule,
    ImageSliderModule,
    PanelHeadingInProfileModule

  ],
  declarations: [
    ViewCarGroupsComponent,
    ViewCarGroupComponent
  ],
  providers: [
    GuestService,
    ClientService
  ],
})
export class ViewCarGroupsModule { }
