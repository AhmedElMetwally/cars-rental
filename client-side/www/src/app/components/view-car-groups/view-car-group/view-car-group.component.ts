import { SharedService } from './../../../service/shared.service';
import { IconCarToBookingService } from './../../common/icon-car-to-booking/icon-car-to-booking.service';
import { ClientService } from './../../../service/client.service';
import { GuestService } from './../../../service/guest.service';
import { environment } from '../../../../environments/environment';
import { ICarGroupModel } from '../../../models/carGroupModel';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LoadingService } from '../../../service/loading.service';
import { AlertService } from '../../../service/alert.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
declare const math ,$ , moment;


@Component({
  selector: 'app-view-car-group',
  templateUrl: './view-car-group.component.html',
  styleUrls : ['./view-car-group.component.scss']
})
export class ViewCarGroupComponent implements OnInit , OnDestroy {
 
  constructor(
    private router: Router,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translateService: TranslateService,
    private sharedService: SharedService,
    private clientService: ClientService,
    private guestService: GuestService,
    private iconCarToBookingService: IconCarToBookingService,
    private activatedRoute: ActivatedRoute
  ) { };
  
  private translateServiceSubscribe: any;
  private activatedRouteSubscribe: any;

  public carGroup: ICarGroupModel;
  public carGroup_id: string;

  public carGroupTitle: string;

  private returnDate: Date;
  private pickUpDate: Date;
  
  public setOptions(): void {
  };

  private getCarGroup(): void {
    this.loadingService.show();
    this.clientService.getCarGroup(this.carGroup_id)
    .then(({ carGroup }) => {
      carGroup.images = carGroup.images.map( image => environment.url + '/images/' + image);
      this.carGroup = carGroup;
      this.loadingService.hide();
      this.afterGetCarGroup();
    })
    .catch( e => this.alertService.httpError(e) );
  };

  private setCarGroupTitle(): void {
    const fieldName = (window as any).lang === 'ar' ? 'nameAr': 'nameEn'
    this.carGroupTitle = `
      ${(this.carGroup as any).branch[fieldName]} - 
      ${(this.carGroup as any).carType[fieldName]} - 
      ${(this.carGroup as any).carModel[fieldName]} -
      ${(this.carGroup as any).year}
    `;
    $('title').text(this.carGroupTitle);
  }

  private init(){
    this.setOptions();
    this.getCarGroup_id();
    this.iconCarToBookingService.icon_occured.emit({
      display : false,
      url : ['/view-car-groups' , this.carGroup_id]
    });
  };
  private onLangChange(): void {
    this.setCarGroupTitle();
    this.setOptions();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
    this.activatedRouteSubscribe.unsubscribe();
  };

  private getCarGroup_id(): void {
    this.activatedRouteSubscribe = this.activatedRoute.params.subscribe( params => {
      this.carGroup_id = params['carGroup_id'];
      this.afterGetCarGroup_id();
    });
  };
  private afterGetCarGroup_id(): void {
    if( 
      localStorage.getItem('returnDateModel') === null || 
      localStorage.getItem('pickUpDateModel') === null 
    ) {
      this.translateService.get('invalid_date')
      .toPromise()
      .then( txt => {
        this.alertService.alertError(txt);
        this.router.navigate(['/view-car-groups']);
      });
    } else {  
      this.getCarGroup();
      this.returnDate = new Date(localStorage.getItem('returnDateModel'));
      this.pickUpDate = new Date(localStorage.getItem('pickUpDateModel'));
    };
  };
  private afterGetCarGroup(): void {
    this.setCarGroupTitle();
  };

  private alertBookingDone(bookingCode: string , expiredAt: Date): void {
    this.translateService.get(['expired_at' , 'booking_code'])
    .toPromise()
    .then( res => {
      const span = document.createElement('span');
      
      span.innerHTML = `
      <br>
      <strong> ${res.booking_code}</strong> :&nbsp;&nbsp;${bookingCode} 
      <br>
      <strong> ${res.expired_at}</strong> :&nbsp;&nbsp;${moment(expiredAt).format((window as any).lang === 'ar' ? 'DD-MM-YYYY hh:mm A' : 'YYYY-MM-DD hh:mm A')} 
      `;

      this.alertService.alertSuccessInSpan(span)
      .then(() => {
        this.onBookingDone();
      });  
    });
  }

  private async clientAddBooking() {

    const res = await this.translateService.get(['total_price' , 'days_count']).toPromise();
    const span = document.createElement('span');
    span.innerHTML = `
      <br>
      <strong>${res.total_price}</strong> :&nbsp;&nbsp;${this.getTotalPrice()} 
      <br>
      <strong>${res.days_count}</strong> :&nbsp;&nbsp;${this.getDaysCount()} 
    `;

    const isOk = await this.alertService.askInSpan(span)

    if(isOk){
        
      const obj = {
        carGroup_id: this.carGroup_id,
        returnDate: new Date(this.returnDate).toISOString(),
        pickUpDate: new Date(this.pickUpDate).toISOString(),
      };

      this.clientService.addBooking(obj)
      .then( (res) => {
        this.alertBookingDone(res.booking.bookingCode , res.booking.expiredAt);
      })
      .catch( e => this.alertService.httpError(e) );

    };
        

  }
  private async bookingInClient() {

    if(this.clientService.isClientHaveFullDate()) {
    
      this.clientAddBooking();
    
    } else {

      const please_complete_your_data: string = await this.translateService.get('please_complete_your_data')
      .toPromise();
      
      const isOk = await this.alertService.ask(please_complete_your_data);
      
      if(isOk) {
        this.iconCarToBookingService.icon_occured.emit({
          display : true,
          url : ['/view-car-groups' , this.carGroup_id]
        });
        this.router.navigate(['/view-client'] , { queryParams: {isWantToCompleteData: true} });
      };

    };

  };
  private async guestAddBooking() {

    const res = await this.translateService.get(['total_price' , 'days_count']).toPromise();
    const span = document.createElement('span');
    span.innerHTML = `
      <br>
      <strong>${res.total_price}</strong> :&nbsp;&nbsp;${this.getTotalPrice()} 
      <br>
      <strong>${res.days_count}</strong> :&nbsp;&nbsp;${this.getDaysCount()} 
    `;

    const isOk = await this.alertService.askInSpan(span)

    if(isOk){
        
      const guest = this.guestService.getGuest();

      const obj = {
        carGroup_id: this.carGroup_id,
        returnDate: new Date(this.returnDate).toISOString(),
        pickUpDate: new Date(this.pickUpDate).toISOString(),
        guest_id : guest._id
      };

      this.guestService.addBooking(obj)
      .then( ({booking}) => {
        this.alertBookingDone(booking.bookingCode , booking.expiredAt);
      })
      .catch( e => this.alertService.httpError(e) );

    };

  }
  private async bookingInGuest() {
    
    if(this.guestService.isGuest()) {

      this.guestAddBooking();

    } else {

      const you_are_guest_please_enter_some_data = await this.translateService.get('you_are_guest_please_enter_some_data')
      .toPromise();

      const isOk = await this.alertService.ask(you_are_guest_please_enter_some_data);

      if(isOk) {
        
        this.iconCarToBookingService.icon_occured.emit({
          display : true,
          url : ['/view-car-groups' , this.carGroup_id]
        });

        this.router.navigate(['create-guest']);

      };

    };

  };
  private onBookingDone(): void {
    this.translateService.get('we_will_call_you_back')
    .toPromise()
    .then( txt => {
      this.alertService.alertSuccess(txt);
    });
  };
  public booking(): void {
    if( this.clientService.isClient() ) {
      this.bookingInClient();
    } else {
      this.bookingInGuest();
    };
  };


  public getTotalPrice(): number {
    return math
    .chain(this.carGroup.pricePerDay)
    .subtract(this.carGroup.discountPerDay)
    .multiply(this.getDaysCount())
    .done();
  };
  public getDaysCount(): number {
    const date1 = new Date(this.returnDate);
    const date2 = new Date(this.pickUpDate);
    const timeDiff = Math.abs(date2.getTime() - date1.getTime());
    const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
    return diffDays
  };
  

};
 


