import { isPlatformBrowser } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ClientService } from '../../service/client.service';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../service/shared.service';
import { LoadingService } from '../../service/loading.service';
import { AlertService } from '../../service/alert.service';
import { FormGroup , FormControl } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
declare const $;

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
})
export class SigninComponent implements OnInit , OnDestroy {

  constructor( 
    private title: Title,
    private router: Router,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private sharedService: SharedService,
    private clientService: ClientService,
    private translateService: TranslateService,
  ) { };

  public signinForm: FormGroup;
  private translateServiceSubscribe: any;


  private initForm(): void {
    this.signinForm = new FormGroup({ 
      emailOrPhone : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.emailOrPhone_required , 
          this.sharedService.mainErrorEnum.emailOrPhone_invalid , 
          /^(.*){100}$/
        ),
      ]),
      password : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.password_required , 
          this.sharedService.mainErrorEnum.password_invalid , 
          this.sharedService.mainRegExp.RegExpClientModel.password 
        ),
      ]),
    });
  };

  public onSubmit(): void {
    if(this.signinForm.invalid) {

      return this.alertService.showFormError( this.signinForm );

    } else {
      
      this.loadingService.show();
    
      let emailOrPhone: string = this.signinForm.controls['emailOrPhone'].value;
      let password: string = this.signinForm.controls['password'].value;

      this.clientService.signinClient(emailOrPhone , password)
      .then( (res: any) => {
        this.sharedService.clearLocalStorage();
        localStorage.setItem('token' , res.token);
        localStorage.setItem('client' , JSON.stringify(res.client) );
        this.router.navigate(['/view-client']);
      })
      .catch( e => this.alertService.httpError(e));

    };
  };

  private setTitle(): void {
    this.translateService.get('signin')
    .toPromise()
    .then( title => {
      this.title.setTitle(title);
    });
  };

  private onLangChange(): void {
    this.setTitle();
  };
  private init(): void {
    this.setTitle();
    this.initForm();
    if(isPlatformBrowser) {
      this.sharedService.interval(() => {
        this.initJquert();
      });
    };
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
  };

  private initJquert(): void {
    $('.js-tilt img').tilt();
  };


}

