import { InputErrorInFormModule } from '../common/input-error-in-form/input-error-in-form.module';
import { SigninRoutingModule } from './signin-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SigninComponent } from './signin.component';
import { NgModule } from '@angular/core';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    SigninRoutingModule,
    ReactiveFormsModule,
    TranslateModuleForChild,
    InputErrorInFormModule
  ],
  declarations: [
    SigninComponent,
  ]
})
export class SigninModule { }
