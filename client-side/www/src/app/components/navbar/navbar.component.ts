import { ClientService } from './../../service/client.service';
import { TranslateService } from '@ngx-translate/core';
import { Location, isPlatformBrowser } from '@angular/common';
import { Component, OnInit } from '@angular/core';
declare const $;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls : ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  
  constructor(
    private location: Location,
    private clientService: ClientService,
    private translateService: TranslateService,
  ){};

  public style: 'rtl'|'ltr';
  
  public changeLang(lang: 'en'|'ar'): void {
    (window as any).lang = lang;
    localStorage.setItem('lang' , lang);
    this.translateService.use(lang);
    this.style = lang == 'en' ? 'ltr' : 'rtl';
    if(isPlatformBrowser) {
      const antherStyle: string = lang === 'en' ? 'rtl' : 'ltr';
      $('body').addClass(`style-${this.style}`).removeClass(`style-${antherStyle}`);
    };
  };

  public ngOnInit(): void {
    this.style = (window as any).lang == 'en' ? 'ltr' : 'rtl';
  };

  public back(): void {
    this.location.back();
  };

  public signout(): void {
    this.clientService.signout();
  };

  public isClient(): boolean {
    return this.clientService.isClient();
  };

  


};
 