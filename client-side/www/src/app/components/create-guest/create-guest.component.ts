import { IconCarToBookingService } from './../common/icon-car-to-booking/icon-car-to-booking.service';
import { GuestService } from './../../service/guest.service';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../service/shared.service';
import { LoadingService } from '../../service/loading.service';
import { AlertService } from '../../service/alert.service';
import { FormGroup , FormControl } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
declare const moment;

@Component({
  selector: 'app-create-guest',
  templateUrl: './create-guest.component.html',
})
export class CreateGuestComponent implements OnInit , OnDestroy {

  constructor( 
    private title: Title,
    private iconCarToBookingService: IconCarToBookingService,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private sharedService: SharedService,
    private guestService: GuestService,
    private translateService: TranslateService,
  ) { };

  public createForm: FormGroup;
  private translateServiceSubscribe: any;

  public defaultHijriDate: Date|string = this.getCurentDateInHijri();
  public iqamaExpiryDateInHijriModel: Date|string;
  public drivingLicenceExpiryDateInHijriModel: Date|string;
  public birthdayInHijriModel: Date|string;

  public localeHijri = {
    firstDayOfWeek: 0,
    dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    dayNamesMin: ["Su","Mo","Tu","We","Th","Fr","Sa"],
    monthNames: [ "محرم","صفر","ربيع الأول","ربيع الثاني"," جمادي الأول","جمادي الثاني","رجب","شعبان","رمضان","شوال","ذو القعدة"," ذو الحجة" ],
    monthNamesShort: [ "محرم","صفر","ربيع الأول","ربيع الثاني"," جمادي الأول","جمادي الثاني","رجب","شعبان","رمضان","شوال","ذو القعدة"," ذو الحجة" ],
    today: 'Today',
    clear: 'Clear'
  };


  private initForm(): void {

    this.createForm = new FormGroup({ 
      name : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
        this.sharedService.mainErrorEnum.name_required , 
        this.sharedService.mainErrorEnum.name_invalid , 
        this.sharedService.mainRegExp.RegExpGuestModel.name
        ),
      ]),
      phone : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
        this.sharedService.mainErrorEnum.phone_required , 
        this.sharedService.mainErrorEnum.phone_invalid , 
        this.sharedService.mainRegExp.RegExpGuestModel.phone
        ),
      ]),
      iqama : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
        this.sharedService.mainErrorEnum.iqama_required , 
        this.sharedService.mainErrorEnum.iqama_invalid , 
        this.sharedService.mainRegExp.RegExpGuestModel.iqama 
        ),
      ]),
      iqamaExpiryDate : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
        this.sharedService.mainErrorEnum.iqamaExpiryDate_required , 
        this.sharedService.mainErrorEnum.iqamaExpiryDate_invalid , 
        /.*/
        ),
      ]),
      drivingLicence : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
        this.sharedService.mainErrorEnum.drivingLicence_required , 
        this.sharedService.mainErrorEnum.drivingLicence_invalid , 
        this.sharedService.mainRegExp.RegExpGuestModel.drivingLicence 
        ),
      ]),
      drivingLicenceExpiryDate : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
        this.sharedService.mainErrorEnum.drivingLicenceExpiryDate_required , 
        this.sharedService.mainErrorEnum.drivingLicenceExpiryDate_invalid ,
        /.*/
        ),
      ]),
      birthday : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
        this.sharedService.mainErrorEnum.birthday_required , 
        this.sharedService.mainErrorEnum.birthday_invalid , 
        /.*/
        ),
      ]),
    });
  };

  public onSubmit(): void {

    if(this.createForm.invalid) {

      return this.alertService.showFormError( this.createForm );

    } else {
      
      this.loadingService.show();
    
      let name: string = this.createForm.controls['name'].value;
      let phone: string = this.createForm.controls['phone'].value;
      let iqama: string = this.createForm.controls['iqama'].value;
      let iqamaExpiryDate: string = this.createForm.controls['iqamaExpiryDate'].value;
      let drivingLicence: string = this.createForm.controls['drivingLicence'].value;
      let drivingLicenceExpiryDate: string = this.createForm.controls['drivingLicenceExpiryDate'].value;
      let birthday: string = this.createForm.controls['birthday'].value;

      const guest = {
        name,
        phone,
        iqama,
        iqamaExpiryDate : new Date(iqamaExpiryDate).toISOString(),
        drivingLicence,
        drivingLicenceExpiryDate : new Date(drivingLicenceExpiryDate).toISOString(),
        birthday : new Date(birthday).toISOString(),
      };

      this.guestService.getOrCreateGuest(guest)
      .then( (res: any) => {
        this.sharedService.clearLocalStorage();
        localStorage.setItem('guestExpiredDate' , (new Date().getTime() + 1000 * 60 * 60 * 6).toString());
        localStorage.setItem('guest' , JSON.stringify(res.guest) );
        this.iconCarToBookingService.click_occured.emit();
        this.loadingService.hide();
      })
      .catch( e => this.alertService.httpError(e));

    };
  };
 
  private setTitle(): void {
    this.translateService.get('create_guest')
    .toPromise()
    .then( title => {
      this.title.setTitle(title);
    });
  };

  private onLangChange(): void {
    this.setTitle();
  };
  private init(): void {
    this.setTitle();
    this.initForm();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
  };

  public onSelectIqamaExpiryDateInHijri($event): void {
    this.createForm.controls['iqamaExpiryDate'].setValue(
      this.convertHijriToDate($event)
    );
  };
  public onSelectDrivingLicenceExpiryDateInHijri($event): void {
    this.createForm.controls['drivingLicenceExpiryDate'].setValue(
      this.convertHijriToDate($event)
    );
  };
  public onSelectBirthdayInHijri($event): void {
    this.createForm.controls['birthday'].setValue(
      this.convertHijriToDate($event)
    );
  };

  public onSelectIqamaExpiryDate($event): void {
    this.iqamaExpiryDateInHijriModel = this.convertDateToHijri($event);
  };
  public onSelectDrivingLicenceExpiryDate($event): void {
    this.drivingLicenceExpiryDateInHijriModel = this.convertDateToHijri($event);
  };
  public onSelectBirthday($event): void {
    this.birthdayInHijriModel = this.convertDateToHijri($event);
  };


  private convertHijriToDate(date: any): Date {
    const _date = new Date(date);
    const day = _date.getDate();
    const month = _date.getMonth() + 1;
    const year = _date.getFullYear();
    return  moment(`${year}-${month}-${day}`, 'iYYYY-iMM-iDD').toDate();
  };
  private convertDateToHijri(date: any): string {
    if(date){
      return this.sharedService.numberArToEn(
        moment(date).format('iYYYY-iMM-iDD')
      );
    } else {
      return ''
    }
  };
  private getCurentDateInHijri(): Date {
    const m = moment();
    return new Date(m.iYear() ,m.iMonth() , m.iDate());
  };


}

