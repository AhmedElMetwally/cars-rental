import { CalendarModule } from 'primeng/calendar';
import { GuestService } from './../../service/guest.service';
import { InputErrorInFormModule } from '../common/input-error-in-form/input-error-in-form.module';
import { CreateGuestRoutingModule } from './create-guest-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CreateGuestComponent } from './create-guest.component';
import { NgModule } from '@angular/core';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    CreateGuestRoutingModule,
    ReactiveFormsModule,
    TranslateModuleForChild,
    InputErrorInFormModule,
    CalendarModule,
    FormsModule
  ],
  declarations: [
    CreateGuestComponent,
  ],
  providers : [
  ]
})
export class CreateGuestModule { }
