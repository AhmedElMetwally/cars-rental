import { viewBookingByCodeAndIqamaComponent } from './view-booking-by-code-and-iqama.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path : '',
    component : viewBookingByCodeAndIqamaComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class viewBookingByCodeAndIqamaRoutingModule { }
