import { viewBookingByCodeAndIqamaRoutingModule } from './view-booking-by-code-and-iqama-routing.module';
import { viewBookingByCodeAndIqamaComponent } from './view-booking-by-code-and-iqama.component';
import { BtnModule } from '../common/btn/btn.module';
import { ImageSliderModule } from '../common/image-slider/image-slider.module';
import { InputTextInProfileModule } from '../common/input-text-in-profile/input-text-in-profile.module';
import { TooltipModule } from 'primeng/tooltip';
import { PanelHeadingInProfileModule } from '../common/panel-heading-in-profile/panel-heading-in-profile.module';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../../modules/PipesModule';
import { ButtonModule } from 'primeng/button';
import { ReactiveFormsModule } from '@angular/forms';
import { InputErrorInFormModule } from '../common/input-error-in-form/input-error-in-form.module';

@NgModule({
  imports: [
    CommonModule,
    viewBookingByCodeAndIqamaRoutingModule,
    TranslateModuleForChild,
    PipesModule,
    
    TooltipModule,
    ButtonModule,
    
    PanelHeadingInProfileModule,
    InputTextInProfileModule,
    ImageSliderModule,
    BtnModule,
    ReactiveFormsModule,
    InputErrorInFormModule
  ],
  declarations: [
    viewBookingByCodeAndIqamaComponent,
  ],
  providers : [
  ]
})
export class viewBookingByCodeAndIqamaModule { }
