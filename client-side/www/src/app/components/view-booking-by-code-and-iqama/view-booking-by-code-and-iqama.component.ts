import { FormGroup, FormControl } from '@angular/forms';
import { SharedService } from './../../service/shared.service';
import { ClientService } from './../../service/client.service';
import { environment } from '../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { LoadingService } from '../../service/loading.service';
import { AlertService } from '../../service/alert.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
declare const $ , moment;


@Component({
  selector: 'app-view-booking-by-code-and-iqama',
  templateUrl: './view-booking-by-code-and-iqama.component.html',
  styleUrls : ['./view-booking-by-code-and-iqama.component.scss']
})
export class viewBookingByCodeAndIqamaComponent implements OnInit , OnDestroy {
 
  constructor(
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translateService: TranslateService,
    private sharedService: SharedService,
    private clientService: ClientService,
  ) { };
  
  private translateServiceSubscribe: any;

  public carGroupTitle: string;

  public booking: any;

  public searchForm: FormGroup;
  
  public setOptions(): void {
  };
  private getBooking(bookingCode , iqama): void {
    this.clientService.getBookingsByCodeAndIqama(bookingCode , iqama)
    .then(({booking}) => {
      booking.carGroup.images = booking.carGroup.images.map( image => environment.url + '/images/' + image);
      this.booking = booking;
      this.loadingService.hide();
      this.afterGetBooking();
    })
    .catch( e => this.alertService.httpError(e) );
  };
  private setCarGroupTitle(): void {
    const fieldName = (window as any).lang === 'ar' ? 'nameAr': 'nameEn'
    this.carGroupTitle = `
      ${(this.booking.carGroup as any).branch[fieldName]} - 
      ${(this.booking.carGroup as any).carType[fieldName]} - 
      ${(this.booking.carGroup as any).carModel[fieldName]} -
      ${(this.booking.carGroup as any).year}
    `;
    $('title').text(this.carGroupTitle);
  };
  private init(){
    this.setOptions();
    this.setTitle();
    this.initForm();
  };
  private onLangChange(): void {
    if(this.booking){
      this.setCarGroupTitle();
    } else {
      this.setTitle();
    }
    this.setOptions();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };
  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
  };
  private afterGetBooking(): void {
    this.setCarGroupTitle();
  };

  private initForm(): void {

    this.searchForm = new FormGroup({ 
      bookingCode : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
        this.sharedService.mainErrorEnum.bookingCode_required , 
        this.sharedService.mainErrorEnum.bookingCode_invalid , 
        this.sharedService.mainRegExp.RegExpBookingModel.bookingCode
        ),
      ]),
      iqama : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
        this.sharedService.mainErrorEnum.iqama_required , 
        this.sharedService.mainErrorEnum.iqama_invalid , 
        this.sharedService.mainRegExp.RegExpBookingModel.iqama 
        ),
      ]),
    });
  };

  public onSubmit(): void {

    if(this.searchForm.invalid) {

      return this.alertService.showFormError( this.searchForm );

    } else {
      
      this.loadingService.show();
    
      let bookingCode: string = this.searchForm.controls['bookingCode'].value;
      let iqama: string = this.searchForm.controls['iqama'].value;

      this.getBooking(bookingCode ,iqama);

    };
  };
 
  private setTitle(): void {
    this.translateService.get('bookings_search')
    .toPromise()
    .then( title => {
      $('title').text(title);
    });
  };

  public convertDateToHijri(date: any): string {
    if(date){
      return this.sharedService.numberArToEn(
        moment(date).format('iYYYY-iMM-iDD')
      );
    } else {
      return ''
    }
  };
  
  public getBookingType(bookingType: number): string {
    return (window as any).lang === 'ar' ? 
      this.sharedService.EbookingTypeAr[bookingType]
    :
    this.sharedService.EbookingTypeEn[bookingType];
  };

};
 


