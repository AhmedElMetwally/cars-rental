import { SharedService } from './../../../service/shared.service';
import { FormControl } from '@angular/forms';
import { Component, Input } from '@angular/core';


@Component({
  selector: 'input-error-in-form',
  template: `
    <div 
      class="input-error" 
      [ngStyle]="{'visibility': 
      control?.invalid && 
      control?.touched ? 'visible' : 'hidden'}" 
      >
      <i class="fa fa-exclamation-circle"></i>
      <span>
        {{ errorMsg }}
      </span>
    </div>
  `,
})
export class InputErrorInFormComponent {

  constructor(
    private sharedService: SharedService,
  ){}
  
  @Input('control') public control: FormControl;
  get errorMsg(): string {
    if(this.control){
      if(this.control.errors) {
        return this.sharedService.getErrorMsg(this.control.errors.errorEnum);
      };
    };
  };

};
