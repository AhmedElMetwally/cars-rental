import { SelectOneModule } from '../select-one/select-one.module';
import { TranslateModuleForChild } from '../../../modules/TranslateModule';
import { InputSelectOneInProfileComponent } from './input-select-one-in-profile.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { TooltipModule } from 'primeng/tooltip';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [ 
    CommonModule,
    ButtonModule,
    TooltipModule,
    SelectOneModule,
    TranslateModuleForChild,
    FormsModule,
    RouterModule,
  ],
  declarations: [
    InputSelectOneInProfileComponent
  ],
  exports : [
    InputSelectOneInProfileComponent,
  ]
})
export class InputSelectOneInProfileModule { }
