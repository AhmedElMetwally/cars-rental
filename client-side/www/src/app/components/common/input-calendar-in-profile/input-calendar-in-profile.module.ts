import { InputCalendarInProfileComponent } from './input-calendar-in-profile.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { TooltipModule } from 'primeng/tooltip';
import { InputTextModule } from 'primeng/inputtext';
import { CalendarModule } from 'primeng/calendar';
import { TranslateModuleForChild } from '../../../modules/TranslateModule';

@NgModule({
  imports: [ 
    CommonModule,
    ButtonModule,
    TooltipModule,
    InputTextModule,
    CalendarModule,
    TranslateModuleForChild
  ],
  declarations: [
    InputCalendarInProfileComponent
  ],
  exports : [
    InputCalendarInProfileComponent,
  ]
})
export class InputCalendarInProfileModule { }
