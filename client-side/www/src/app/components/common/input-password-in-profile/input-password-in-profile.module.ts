import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { InputPasswordInProfileComponent } from './input-password-in-profile.component';
import { TooltipModule } from 'primeng/tooltip';
import { TranslateModuleForChild } from '../../../modules/TranslateModule';
import { InputTextModule } from 'primeng/inputtext';

@NgModule({
  imports: [ 
    CommonModule,
    ButtonModule,
    TooltipModule,
    InputTextModule,
    TranslateModuleForChild
  ],
  declarations: [
    InputPasswordInProfileComponent
  ],
  exports : [
    InputPasswordInProfileComponent,
  ]
})
export class InputPasswordInProfileModule { }
