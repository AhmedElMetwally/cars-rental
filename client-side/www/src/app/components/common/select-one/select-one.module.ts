import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectOneComponent } from './select-one.component';
import { DropdownModule } from 'primeng/dropdown';
import { TranslateModuleForChild } from '../../../modules/TranslateModule';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [ 
    CommonModule,
    DropdownModule,
    TranslateModuleForChild,
    FormsModule,
  ],
  declarations: [
    SelectOneComponent
  ],
  exports : [
    SelectOneComponent,
  ]
})
export class SelectOneModule { }
