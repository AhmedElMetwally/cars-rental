import { SelectItem } from 'primeng/components/common/selectitem';
import { Component, EventEmitter, Input, Output, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

export const noop = () => {
};

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => SelectOneComponent),
  multi: true
};

@Component({
  selector: 'select-one',
  template: `
    <div>
      <div style='position: relative;'>
        <i class='select-one fa {{icon}}'></i>
        <p-dropdown 
          [(ngModel)]="value"  
          class='select-one'
          [style]="{'min-width':'100%'}"
          [options]="options" 
          [placeholder]='placeholder'
          filter='true'
          (onChange)='change($event)'
          scrollHeight='150px'
          [disabled]='disabled'
          [emptyFilterMessage]="'no_items_found' | translate"
        >
        
          <ng-template let-item pTemplate="selectedItem"> 
            <span [innerHTML]='item.label'></span>
          </ng-template> 
          <ng-template let-item pTemplate="item"> 
              <div class="ui-helper-clearfix">
                <div [innerHTML]='item.label'></div> 
              </div>
          </ng-template>
        
        </p-dropdown>
      </div>
    </div>
  `,
  styleUrls : ['select-one.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class SelectOneComponent implements ControlValueAccessor {

  constructor(){};
  
  @Input('disabled') disabled: boolean = false;
  @Input('icon') icon: string;
  @Input('options') options: SelectItem[] = [];
  @Input('placeholder') placeholder: string; 
  @Output() onChange: EventEmitter<any> = new EventEmitter();

  public change($event) {
    this.onChange.emit($event);
  };



  // ---------------------------------------
  //  copy past code D;
  // ---------------------------------------
  private innerValue: any = '';

  //Placeholders for the callbacks which are later provided
  //by the Control Value Accessor
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;

  //get accessor
  get value(): any {
      return this.innerValue;
  };

  //set accessor including call the onchange callback
  set value(v: any) {
    if (v !== this.innerValue) {
        this.innerValue = v;
        this.onChangeCallback(v);
    }
  }

  //From ControlValueAccessor interface
  writeValue(value: any) {
      if (value !== this.innerValue) {
          this.innerValue = value;
      }
  }

  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
      this.onChangeCallback = fn;
  }

  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
      this.onTouchedCallback = fn;
  }

};
