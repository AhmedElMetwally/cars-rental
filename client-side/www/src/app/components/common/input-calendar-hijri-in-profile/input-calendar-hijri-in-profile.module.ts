import { InputCalendarHijriInProfileComponent } from './input-calendar-hijri-in-profile.component';
import { TranslateModuleForChild } from '../../../modules/TranslateModule';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { TooltipModule } from 'primeng/tooltip';
import { InputTextModule } from 'primeng/inputtext';
import { CalendarModule } from 'primeng/calendar';

@NgModule({
  imports: [ 
    CommonModule,
    ButtonModule,
    TooltipModule,
    InputTextModule,
    TranslateModuleForChild,
    CalendarModule,
  ],
  declarations: [
    InputCalendarHijriInProfileComponent
  ],
  exports : [
    InputCalendarHijriInProfileComponent,
  ]
})
export class InputCalendarHijriInProfileModule { }