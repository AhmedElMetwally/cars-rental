import { SharedService } from '../../../service/shared.service';
import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
declare const moment;

interface IOnSaveEvent {
  fieldName: string;
  value: string;
};

interface IOnValidationError {
  validationErrorMsg: string;
};

@Component({
  selector: 'input-calendar-hijri-in-profile',
  template: `

  <li class="list-group-item">
    <div>
      <i [class]="icon"></i>  
      <strong>{{displayName}} : </strong> 

      <div *ngIf="edit" class="ui-inputgroup">
        <p-calendar [yearNavigator]="true" yearRange="1400:1460" [defaultDate]='defaultHijriDate' [locale]='locale' [showTime]="showTime" hourFormat="12" (keyup.enter)='save()' [placeholder]="'select_date' | translate" #input dateFormat="yy-mm-dd" ></p-calendar>
        <button (click)='save()' pButton  tooltipPosition="top" [pTooltip]="'save' | translate" type="button" icon="fa fa-edit" class="ui-button-primary"></button>      
      </div>

      <span *ngIf="!edit">
        {{ fieldValue + ' ' + extraFieldValue}}
      </span>
       
    </div>
    <i *ngIf="displayControl && !edit" class="fa fa-edit icon-control" tooltipPosition="top" [pTooltip]="'edit' | translate" (click)='openEdit()'></i>
    <i *ngIf="displayControl && edit" class="fa fa-times icon-control" tooltipPosition="top" [pTooltip]="'close' | translate" (click)='closeEdit()'></i>
  </li>
  `,
  styleUrls : ['./input-calendar-hijri-in-profile.component.scss']
})
export class InputCalendarHijriInProfileComponent {
  
  constructor(
    private sharedService: SharedService,
  ){}


  @Input('showTime') showTime: boolean;

  @Input('displayName') displayName: string;
  @Input('icon') icon: string;
  
  // edit
  @Input('edit') edit: boolean;
  @Input('displayControl') displayControl: boolean;

  // onSave
  @Input('fieldName') fieldName: string;
  @Input('extraFieldValue') extraFieldValue: string = '';
  @Input('fieldValue') set fieldValue(fieldValue) {
    this._fieldValue = this.convertDateToHijri(fieldValue);
  };
  get fieldValue() {
    return this._fieldValue
  }
  private _fieldValue: any;

  @Input('value') value: string;
  @Output() onSave: EventEmitter<IOnSaveEvent> = new EventEmitter();

  // onValidationError
  @Input('requiredErrorEnum') requiredErrorEnum: number;
  @Input('invalidErrorEnum') invalidErrorEnum: number;
  @Input('RegExp') RegExp: RegExp;
  @Output() onValidationError: EventEmitter<IOnValidationError> = new EventEmitter();
  
  @Input('editNameInEvent') editNameInEvent: string;
  @Output() onOpenEdit: EventEmitter<any> = new EventEmitter();
  @Output() onCloseEdit: EventEmitter<any> = new EventEmitter();

  @ViewChild('input') input;

  public defaultHijriDate: Date|string = this.getCurentDateInHijri();
  public locale = {
    firstDayOfWeek: 0,
    dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    dayNamesMin: ["Su","Mo","Tu","We","Th","Fr","Sa"],
    monthNames: [ "محرم","صفر","ربيع الأول","ربيع الثاني"," جمادي الأول","جمادي الثاني","رجب","شعبان","رمضان","شوال","ذو القعدة"," ذو الحجة" ],
    monthNamesShort: [ "محرم","صفر","ربيع الأول","ربيع الثاني"," جمادي الأول","جمادي الثاني","رجب","شعبان","رمضان","شوال","ذو القعدة"," ذو الحجة" ],
    today: 'Today',
    clear: 'Clear'
  };

  // if displayControl true
  public save(): void  {

    if(!this.displayControl || ! this.input) return;

    let value = this.input.value;
    
    if(value === '' || value === undefined) {

      const $event = {
        validationErrorMsg : this.sharedService.getErrorMsg(this.requiredErrorEnum) , 
      };
      this.onValidationError.emit($event);

    } else {

      value = this.convertHijriToDate(value).toISOString();
      
      if (this.RegExp.test(value)) {

        const $event = {
          fieldName : this.fieldName , 
          value : value
        };
        this.onSave.emit($event);

      } else {

        const $event = {
          validationErrorMsg : this.sharedService.getErrorMsg(this.invalidErrorEnum) , 
        };
        this.onValidationError.emit($event);

      };

    };

  };

  public openEdit(): void {
    if(!this.displayControl) return;
    const $event = {  
      editNameInEvent : this.editNameInEvent
    };
    this.onOpenEdit.emit($event);
  };
  public closeEdit(): void {
    if(!this.displayControl) return;
    const $event = {  
      editNameInEvent : this.editNameInEvent
    };
    this.onCloseEdit.emit($event);
  };

  private convertHijriToDate(date: any): Date {
    const _date = new Date(date);
    const day = _date.getDate();
    const month = _date.getMonth() + 1;
    const year = _date.getFullYear();
    return  moment(`${year}-${month}-${day}`, 'iYYYY-iMM-iDD').toDate();
  };
  private convertDateToHijri(date: any): string {
    if(date){
      return this.sharedService.numberArToEn(
        moment(date).format('iYYYY-iMM-iDD')
      );
    } else {
      return ''
    }
  };
  private getCurentDateInHijri(): Date {
    const m = moment();
    return new Date(m.iYear() , m.iMonth() , m.iDate());
  };

};
 