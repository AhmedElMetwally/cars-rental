import { SelectItem } from 'primeng/components/common/selectitem';
import { Component, EventEmitter, Input, Output, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'custom-data-view',
  templateUrl: './custom-data-view.component.html',
  
})
export class CustomDataViewComponent implements OnInit ,  OnDestroy , OnChanges {

  constructor(
    private translateService: TranslateService,
  ){};

  @Input('useSearch') useSearch: boolean = false;
  @Input('useFilter') useFilter: boolean = false;
  @Input('displayLink') displayLink: boolean = false;
  @Input('loading') loading: boolean = true;
  @Input('filterByTitle') filterByTitle: string; 
  @Input('filterByIcon') filterByIcon: string; 
  @Input('filterByOptions') set filterByOptions(arr: SelectItem[]) {
    const clear: SelectItem[] = [
      {
        label : this.filterByTitle,
        value : false
      }
    ]
    this._filterByOptions = [...clear , ...arr];
  };
  @Input('bottomLengthTitle') bottomLengthTitle: string; 
  @Input('itemsCount') itemsCount: number; 
  @Input('linkUrl') linkUrl: string; // if displayLink
  @Input('linkLabel') linkLabel: string; // if displayLink

  @Output('onLazyLoad') onLazyLoad: EventEmitter<any> = new EventEmitter<any>();
  @Output('onReset') onReset: EventEmitter<any> = new EventEmitter<any>();

  public searchMode: boolean = false;
  public filterByModel: any = false;
  public _filterByOptions: SelectItem[] = []; 
  public sortByOptions: SelectItem[] = [];
  public sortByModel: string;
  public searchModel: string;
  private translateServiceSubscribe: EventEmitter<any>;

  public clickSearch(): void {
    if(this.searchModel && this.searchModel.trim() !== '') {
      this.searchMode = true;
      this.reset();
    }
  }
  public closeSearch(): void{
    this.searchModel = '';
    this.searchMode = false;
    this.reset();
  }
  public scroll(): void {
    const $event = {
      sortBy : this.sortByModel,
      
      ...this.useFilter ? {
        filterBy : this.filterByModel || false
      } : {},

      ...this.useSearch ? {
        searchModel : this.searchModel,
        searchMode : this.searchMode
      } : {}

    };
    this.onLazyLoad.emit($event);
  };
  private setOptions(): void {
    this.sortByOptions = [];
    this.sortByModel = this.sortByModel || '-createdAt';
    this.translateService.get('sortByOptions')
    .toPromise()
    .then(
      v => this.sortByOptions = v
    );
  };
  private onLangChange(): void {
    this.setOptions();
  };
  public init(): void {
    this.setOptions();
    this.scroll();
  };
  public ngOnInit(): void  {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };
  public ngOnDestroy(): void  {
    this.translateServiceSubscribe.unsubscribe();
  };
  public ngOnChanges(): void {

  };
  public reset(): void {
    const $event = {
      sortBy : this.sortByModel,
      
      ...this.useFilter ? {
        filterBy : this.filterByModel || false
      } : {},

      ...this.useSearch ? {
        searchModel : this.searchModel,
        searchMode : this.searchMode
      } : {}

    };
    this.onReset.emit($event);
  };

};
 