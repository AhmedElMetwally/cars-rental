import { BtnModule } from '../btn/btn.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CustomDataViewComponent } from './custom-data-view.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModuleForChild } from '../../../modules/TranslateModule';
import { SelectOneModule } from '../select-one/select-one.module';
import { FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [ 
    CommonModule,
    InfiniteScrollModule,
    TranslateModuleForChild,
    SelectOneModule,
    FormsModule,
    BtnModule,
    InputTextModule,
    ButtonModule,
    RouterModule
  ],
  declarations: [
    CustomDataViewComponent
  ],
  exports : [
    CustomDataViewComponent,
  ]
})
export class CustomDataViewModule { }
