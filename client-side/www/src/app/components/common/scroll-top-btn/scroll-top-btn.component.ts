import { Component, OnInit } from '@angular/core';
declare const $;

@Component({
  selector: 'scroll-top-btn',
  template: `
    <button class="scroll-top-btn" (click)='scrollToTop()' pButton type="button" icon="fa fa-chevron-up" ></button>
  `,
  styleUrls : ['scroll-top-btn.component.scss']
})
export class ScrollTopBtnComponent implements OnInit {

  constructor(){};

  public ngOnInit(): void {
    if(! (window as any).scroll_top_btn_event) {
      $(window).scroll(this.onScroll);
      this.onScroll();
      (window as any).scroll_top_btn_event = true;
    }; 
  };

  private onScroll(){
    if( $(window).scrollTop() === 0 ) {
      $('.scroll-top-btn').removeClass('scroll-top-btn-show').addClass('scroll-top-btn-hide');
    } else {
      $('.scroll-top-btn').removeClass('scroll-top-btn-hide').addClass('scroll-top-btn-show');
    };
  };

  public scrollToTop(): void {
    $("html, body").animate({ scrollTop: 0 }, 500);
  };

};
