import { Router } from '@angular/router';
import { IconCarToBookingService } from './icon-car-to-booking.service';
import { Component } from '@angular/core';
declare const $;

@Component({
  selector: 'icon-car-to-booking',
  template: `
    <button 
      class="icon-car-to-booking" 
      (click)='click()' 
      pButton type="button" 
      icon="fa fa-car"
      [tooltipPosition]="lang =='ar'? 'left' : 'right'" 
      [pTooltip]="'go_to_car' | translate" 
    ></button>
  `,
  styleUrls : ['icon-car-to-booking.component.scss']
})
export class IconCarToBookingComponent {

  constructor(
    private iconCarToBookingService: IconCarToBookingService,
    private router: Router,
  ){};

  private url: string[] = ['/'];
  public get lang() { 
    return (window as any).lang;
  };

  public ngOnInit(): void {
    this.iconCarToBookingService.icon_occured.subscribe( $event => {
      const { display , url } = $event;
      this.url = url;
      if(display) {
        $('.icon-car-to-booking').removeClass('icon-car-to-booking-hide').addClass('icon-car-to-booking-show');
      } else {
        $('.icon-car-to-booking').removeClass('icon-car-to-booking-show').addClass('icon-car-to-booking-hide');
      };
    });
    this.iconCarToBookingService.click_occured.subscribe( () => {
      this.click();
    });
  };

  public click(): void {
    if(this.url) {
      this.router.navigate(this.url);
    };
  };

};
