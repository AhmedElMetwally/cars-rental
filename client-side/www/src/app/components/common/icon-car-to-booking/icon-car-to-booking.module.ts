import { IconCarToBookingComponent } from './icon-car-to-booking.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { TooltipModule } from 'primeng/tooltip';
import { TranslateModuleForChild } from '../../../modules/TranslateModule';

@NgModule({
  imports: [ 
    CommonModule,
    ButtonModule,
    TooltipModule,
    TranslateModuleForChild
  ],
  declarations: [
    IconCarToBookingComponent
  ],
  exports : [
    IconCarToBookingComponent,
  ]
})
export class IconCarToBookingModule { }
