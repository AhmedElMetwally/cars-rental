import { Injectable , EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IconCarToBookingService {
  public icon_occured: EventEmitter<{display:boolean,url:string[]}> = new EventEmitter<{display:boolean,url:string[]}>();
  public click_occured: EventEmitter<void> = new EventEmitter<void>();
};
