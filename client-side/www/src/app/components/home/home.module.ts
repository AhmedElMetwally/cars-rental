import { GMapModule } from 'primeng/gmap';
import { HomeService } from './../../service/home.service';
import { TranslateModuleForChild } from './../../modules/TranslateModule';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    TranslateModuleForChild,
    ReactiveFormsModule,
    RouterModule,
    GMapModule,
  ],
  declarations: [
    HomeComponent,
  ],
  exports: [
    HomeComponent,
  ],
  providers : [
    HomeService
  ]
})
export class HomeModule { }
