import { ClientService } from './../../service/client.service';
import { Router } from '@angular/router';
import { AlertService } from './../../service/alert.service';
import { HomeService } from './../../service/home.service';
import { LoadingService } from './../../service/loading.service';
import { FormGroup, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from './../../service/shared.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { isPlatformBrowser } from '@angular/common';
declare const $,google;


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit , OnDestroy {
  
  constructor(
    private router: Router,
    private title: Title,
    private sharedService: SharedService,
    private homeService: HomeService,
    private translateService: TranslateService,
    private loadingService: LoadingService,
    private clientService: ClientService,
    private alertService: AlertService,
  ){}

  public _features: {title: string;text: string;icon: string}[] = [];
  public services: {title: string;text: string;icon: string}[] = [];
  private translateServiceSubscribe: any;

  public signupForm: FormGroup;
  public messageForm: FormGroup;
  
  public allBranches: any[];
  public mapOptions: any;
  public mapOverlays: any[] = [];


  private displayBranchesInMap(): void {

    const nameFieldByLang = (window as any).lang === 'ar' ? 'nameAr' : "nameEn";
      
    for(let branch of this.allBranches) {
      
      const label = `${branch[nameFieldByLang]} - ${branch.city[nameFieldByLang]}`;
     
      this.mapOverlays.push(
        new google.maps.Marker({
          position: {
            lat: branch.location[1], 
            lng: branch.location[0]
          }, 
          
          title : label
          
        }),
      );

    };


    if(this.allBranches.length) {
      this.mapOptions = {
        center: {
          lat: this.allBranches[0].location[1], 
          lng: this.allBranches[0].location[0]
        },
        zoom: 5
      };
    } else {
      this.mapOptions = {
        center: {
          lat  : 24.774265  ,
          lng : 46.738586  ,  
        },
        zoom: 5
      };
    };

  }
  private initMap(): void {

    if(this.allBranches) {

      this.displayBranchesInMap();

    } else {

      this.clientService.getAllBranchesLocations()
      .then( res => {
  
        this.allBranches = res.branches;
  
        this.displayBranchesInMap();
  
      })
      .catch( e => this.alertService.httpError(e));

    };

  };

  private initSignupForm(): void {
    this.signupForm = new FormGroup({ 
      name : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.name_required , 
          this.sharedService.mainErrorEnum.name_invalid , 
          this.sharedService.mainRegExp.RegExpClientModel.name
        ),
      ]),
      email : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.email_required , 
          this.sharedService.mainErrorEnum.email_invalid , 
          this.sharedService.mainRegExp.RegExpClientModel.email
        ),
      ]),
      phone : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.phone_required , 
          this.sharedService.mainErrorEnum.phone_invalid , 
          this.sharedService.mainRegExp.RegExpClientModel.phone
        ),
      ]),
      password : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.password_required , 
          this.sharedService.mainErrorEnum.password_invalid , 
          this.sharedService.mainRegExp.RegExpClientModel.password 
        ),
      ]),
    });
  };

  private initMessageForm(): void {
    this.messageForm = new FormGroup({ 
      name : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.name_required , 
          this.sharedService.mainErrorEnum.name_invalid , 
          this.sharedService.mainRegExp.RegExpMessageModel.name
        ),
      ]),
      email : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.email_required , 
          this.sharedService.mainErrorEnum.email_invalid , 
          this.sharedService.mainRegExp.RegExpMessageModel.email
        ),
      ]),
      message : new FormControl('' , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.message_required , 
          this.sharedService.mainErrorEnum.message_invalid , 
          this.sharedService.mainRegExp.RegExpMessageModel.message
        ),
      ]),
    });
  };

  public onSignupFormSubmit(): void {
    if(this.signupForm.invalid) {

      return this.alertService.showFormError( this.signupForm );

    } else {
      
      this.loadingService.show();
    
      let name: string = this.signupForm.controls['name'].value;
      let phone: string = this.signupForm.controls['phone'].value;
      let email: string = this.signupForm.controls['email'].value;
      let password: string = this.signupForm.controls['password'].value;

      const client = {
        name,phone,email,password
      };

      this.clientService.signupClient(client)
      .then( (res: any) => {
        this.sharedService.clearLocalStorage();
        localStorage.setItem('token' , res.token);
        localStorage.setItem('client' , JSON.stringify(res.client) );
        this.router.navigate(['/view-client']);
        this.loadingService.hide();
      })
      .catch( e => this.alertService.httpError(e));

    };
  };

  public onMessageFormSubmit(): void {
    if(this.messageForm.invalid) {

      return this.alertService.showFormError( this.messageForm );

    } else {
      
      this.loadingService.show();
    
      let name: string = this.messageForm.controls['name'].value;
      let email: string = this.messageForm.controls['email'].value;
      let message: string = this.messageForm.controls['message'].value;
      
      const msg = {
        name,email,message
      };

      this.homeService.sentMessage(msg)
      .then( () => {
        this.loadingService.hide();
        this.alertService.done();
      })
      .catch( e => this.alertService.httpError(e));

    };
  };

  private setTitle(): void {
    this.translateService.get('home_page')
    .toPromise()
    .then( title => {
      this.title.setTitle(title);
    });
  };
  
  private initJquert(): void {

    // btn hover
    $('.btn-6').on('mouseenter', function(e) {
      const parentOffset = $(this).offset(),
      relX = e.pageX - parentOffset.left,
      relY = e.pageY - parentOffset.top;
      $(this).find('span').css({
        top: relY,
        left: relX
      })
    })
    .on('mouseout', function(e) {
      const parentOffset = $(this).offset(),
      relX = e.pageX - parentOffset.left,
      relY = e.pageY - parentOffset.top;
      $(this).find('.btn-6 span').css({
        top: relY,
        left: relX
      })
    }); 

    // services
    $('.jarallax').jarallax({
      speed: 0.5,
      imgWidth: 1366,
      imgHeight: 768
    });    

  };

  private getHtmlDate(): void {
    
    this.services = [];
    this._features = [];

    this.translateService.get(['_features' , 'services'])
    .toPromise()
    .then( res => {
      this.services = res.services;
      this._features = res._features;
      if(isPlatformBrowser) {
        this.sharedService.interval(() => {
          this.initJquert();
        });
      };
    })
  };

  private init(){
    if(isPlatformBrowser) {
      this.sharedService.interval(() => {
        this.initJquert();
      });
      this.initMap();
    };
    this.setTitle();
    this.getHtmlDate();
    this.initSignupForm();
    this.initMessageForm();
  };
  private onLangChange(): void {
    if(isPlatformBrowser) {
      this.sharedService.interval(() => {
        this.initJquert();
      });
      this.initMap();
    };
    this.setTitle();
    this.getHtmlDate();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };
  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
  };

  public isClinet(): boolean {
    return this.clientService.isClient();
  };



};
