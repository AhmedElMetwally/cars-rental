import { ClientService } from './../../service/client.service';
import { AlertService } from './../../service/alert.service';
import { SharedService } from './../../service/shared.service';
import { LoadingService } from './../../service/loading.service';
import { Router, NavigationStart, Event, NavigationCancel, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
declare const $;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls : ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(
    private translateService: TranslateService,
    private router: Router,
    private loadingService: LoadingService,
    private clientService: ClientService,
    private sharedService: SharedService,
    private alertService: AlertService,
  ) {
    this.initRouterEvents();
    this.initLang();
  };

  private closeNavbar(): void {
    if( $('.navbar-collapse.navbar-ex1-collapse.collapse.in').length ) {
      $('.navbar-toggle').click();
    };
  };

  private initRouterEvents(): void {
    this.router.events.subscribe( (event: Event) => {
    
      if( event instanceof NavigationStart ) {
        this.loadingService.show();
      };

      if( event instanceof NavigationCancel ) {
        this.loadingService.hide();
        this.closeNavbar();
      };

      if( event instanceof NavigationEnd ) {
        this.loadingService.hide();
        this.closeNavbar();
      };
      
    });
  };
  
  private initLang(): void {
    this.translateService.setDefaultLang('en');
    (window as any).lang = 'en';
    if(localStorage.getItem('lang') === 'ar'){
      this.changeLang('ar');
    } else {
      this.changeLang('en');
    };
    
  };
  private changeLang(lang: 'en'|'ar'): void {
    (window as any).lang = lang;
    localStorage.setItem('lang' , lang);
    this.translateService.use(lang);
    const style = lang == 'en' ? 'ltr' : 'rtl';
    if(isPlatformBrowser) {
      const antherStyle: string = lang === 'en' ? 'rtl' : 'ltr';
      $('body').addClass(`style-${style}`).removeClass(`style-${antherStyle}`);
    };
  };

  private refreshClientToken(): void {
    this.clientService.refreshClientToken()
    .then( res => {
      this.sharedService.clearLocalStorage();
      localStorage.setItem('token' , res.token);
      localStorage.setItem('client' , JSON.stringify(res.client) );
    })
    .catch( e => {
      if(e.status === 0 ) return;
      this.clientService.signout();

      this.alertService.httpError(e);
    });

  };

  public ngOnInit(): any {
    const isTokenHere = localStorage.getItem('token') !== null;
    if(isTokenHere && this.clientService.isClient()) return this.refreshClientToken();
  };

  
};
