import { IconCarToBookingModule } from './../common/icon-car-to-booking/icon-car-to-booking.module';
import { HomeModule } from './../home/home.module';
import { LoadingComponent } from './../loading/loading.component';
import { MainRoutingModule } from './main.routing.module';
import { FooterComponent } from './../footer/footer.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MainComponent } from './main.component';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { TranslateModuleForRoot } from '../../modules/TranslateModule';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ScrollTopBtnModule } from '../common/scroll-top-btn/scroll-top-btn.module';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    MainRoutingModule,
    TranslateModuleForRoot,
    BrowserAnimationsModule,
    ScrollTopBtnModule,
    HomeModule,
    IconCarToBookingModule
  ],
  declarations: [
    MainComponent,
    NavbarComponent,
    FooterComponent,
    LoadingComponent,
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
  ],
  bootstrap: [MainComponent]
})
export class MainModule { }
