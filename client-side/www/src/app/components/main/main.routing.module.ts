import { isNotGuestGuard } from './../../guards/guest.guard';
import { isNotClientGuard, isClientGuard } from './../../guards/client.guard';
import { HomeComponent } from '../home/home.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forRoot([
            {
                path : '',
                pathMatch: 'full',
                redirectTo : 'home',
            },
            {
                path: 'home',
                component : HomeComponent
            },
            {
                path : 'signin',
                loadChildren: './../signin/signin.module#SigninModule' ,
                canActivate : [ isNotClientGuard]
            },
            {
                path : 'signup',
                loadChildren: './../signup/signup.module#SignupModule' ,
                canActivate : [ isNotClientGuard]
            },
            {
                path: 'view-car-groups',
                loadChildren: './../view-car-groups/view-car-groups.module#ViewCarGroupsModule' ,
            },
            {
                path: 'view-client',
                loadChildren: './../view-client/view-client.module#ViewClientModule' ,
                canActivate : [ isClientGuard]
            },

            {
                path : 'create-guest',
                loadChildren: './../create-guest/create-guest.module#CreateGuestModule' ,
                canActivate : [ isNotGuestGuard]
            },
            
            {
                path : 'view-booking-by-code-and-iqama',
                loadChildren: './../view-booking-by-code-and-iqama/view-booking-by-code-and-iqama.module#viewBookingByCodeAndIqamaModule' ,
            },

            {
                path : 'view-client-bookings',
                loadChildren: './../view-client-bookings/view-client-bookings.module#ViewClientBookingsModule' ,
                canActivate : [ isClientGuard]
            },
            
            {
                path : '**',
                redirectTo : 'home',
            },
        ])
    ],
    exports: [ RouterModule ]
})
export class MainRoutingModule {}
