import { BtnModule } from './../common/btn/btn.module';
import { InputCalendarInProfileModule } from './../common/input-calendar-in-profile/input-calendar-in-profile.module';
import { ImageSliderModule } from './../common/image-slider/image-slider.module';
import { InputSelectOneInProfileModule } from '../common/input-select-one-in-profile/input-select-one-in-profile.module'
import { InputPasswordInProfileModule } from '../common/input-password-in-profile/input-password-in-profile.module'
import { ViewClientComponent } from './view-client.component';
import { ViewClientRoutingModule } from './view-client-routing.module';
import { InputTextInProfileModule } from '../common/input-text-in-profile/input-text-in-profile.module';
import { TooltipModule } from 'primeng/tooltip';
import { PanelHeadingInProfileModule } from '../common/panel-heading-in-profile/panel-heading-in-profile.module';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../../modules/PipesModule';
import { ButtonModule } from 'primeng/button';
import { InputCalendarHijriInProfileModule } from '../common/input-calendar-hijri-in-profile/input-calendar-hijri-in-profile.module';

@NgModule({
  imports: [
    CommonModule,
    ViewClientRoutingModule,
    TranslateModuleForChild,
    PipesModule,
    
    TooltipModule,
    ButtonModule,
    
    PanelHeadingInProfileModule,
    InputTextInProfileModule,
    InputPasswordInProfileModule,
    InputSelectOneInProfileModule,
    ImageSliderModule,
    InputCalendarInProfileModule,
    InputCalendarHijriInProfileModule,
    BtnModule
  ],
  declarations: [
    ViewClientComponent,
  ],
  providers : [
  ]
})
export class ViewClientModule { }
