import { ActivatedRoute } from '@angular/router';
import { SelectItem } from 'primeng/components/common/selectitem';
import { INationalityModel } from '../../models/nationalityModel'
import { environment } from '../../../environments/environment'
import { IClientModel } from '../../models/clientModel';
import { ClientService } from '../../service/client.service';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../service/shared.service';
import { LoadingService } from '../../service/loading.service';
import { AlertService } from '../../service/alert.service';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
declare const $;


@Component({
  selector: 'app-view-client',
  templateUrl: './view-client.component.html',
  styleUrls : ['view-client.component.scss']
})
export class ViewClientComponent implements OnInit , OnDestroy {
 
  constructor(
    private alertService: AlertService,
    private activatedRoute: ActivatedRoute,
    private loadingService: LoadingService,
    private translateService: TranslateService,
    private clientService: ClientService,
    private sharedService: SharedService,
  ) { };

  private translateServiceSubscribe: any;
  private activatedRouteParamsSubscribe: any;
  
  public clientImage: [string];

  public client: IClientModel;
  public edit = {
    name: false, password: false, email: false, phone: false,
    iqama: false, iqamaExpiryDate: false, 
    drivingLicence: false, drivingLicenceExpiryDate: false,
    birthday: false, address: false,
    job: false, nationality_id: false, 
    birthday_hijri: false , iqamaExpiryDate_hijri: false , drivingLicenceExpiryDate_hijri: false,
  };
  public client_id: string;

  private allNationalities: INationalityModel[];
  public nationalitiesOptions: SelectItem[] = [];

  public RegExpClientModel = this.sharedService.mainRegExp.RegExpClientModel; 
  public RegExpNationalityModel = this.sharedService.mainRegExp.RegExpNationalityModel; 
  
  @ViewChild('name') name;
  @ViewChild('password') password;
  @ViewChild('email') email;
  @ViewChild('phone') phone;

  @ViewChild('iqama') iqama;
  @ViewChild('drivingLicence') drivingLicence;

  @ViewChild('iqamaExpiryDate') iqamaExpiryDate;
  @ViewChild('drivingLicenceExpiryDate') drivingLicenceExpiryDate;

  @ViewChild('birthday') birthday;
  @ViewChild('address') address;

  @ViewChild('job') job;
  @ViewChild('nationality_id') nationality_id;

  @ViewChild('birthday_hijri') birthday_hijri;
  @ViewChild('iqamaExpiryDate_hijri') iqamaExpiryDate_hijri;
  @ViewChild('drivingLicenceExpiryDate_hijri') drivingLicenceExpiryDate_hijri;

  // validationErrorMsg Enum
  public name_required = this.sharedService.mainErrorEnum.name_required; 
  public name_invalid = this.sharedService.mainErrorEnum.name_invalid; 

  public password_required = this.sharedService.mainErrorEnum.password_required; 
  public password_invalid = this.sharedService.mainErrorEnum.password_invalid; 
  
  public email_required = this.sharedService.mainErrorEnum.email_required; 
  public email_invalid = this.sharedService.mainErrorEnum.email_invalid; 

  public phone_required = this.sharedService.mainErrorEnum.phone_required; 
  public phone_invalid = this.sharedService.mainErrorEnum.phone_invalid; 

  public iqama_required = this.sharedService.mainErrorEnum.iqama_required; 
  public iqama_invalid = this.sharedService.mainErrorEnum.iqama_invalid; 

  public iqamaExpiryDate_required = this.sharedService.mainErrorEnum.iqamaExpiryDate_required; 
  public iqamaExpiryDate_invalid = this.sharedService.mainErrorEnum.iqamaExpiryDate_invalid; 

  public drivingLicence_required = this.sharedService.mainErrorEnum.drivingLicence_required; 
  public drivingLicence_invalid = this.sharedService.mainErrorEnum.drivingLicence_invalid; 

  public drivingLicenceExpiryDate_required = this.sharedService.mainErrorEnum.drivingLicenceExpiryDate_required; 
  public drivingLicenceExpiryDate_invalid = this.sharedService.mainErrorEnum.drivingLicenceExpiryDate_invalid; 

  public birthday_required = this.sharedService.mainErrorEnum.birthday_required; 
  public birthday_invalid = this.sharedService.mainErrorEnum.birthday_invalid; 

  public address_required = this.sharedService.mainErrorEnum.address_required; 
  public address_invalid = this.sharedService.mainErrorEnum.address_invalid; 

  public job_required = this.sharedService.mainErrorEnum.job_required; 
  public job_invalid = this.sharedService.mainErrorEnum.job_invalid; 

  public nationality_id_required = this.sharedService.mainErrorEnum.nationality_id_required; 
  public nationality_id_invalid = this.sharedService.mainErrorEnum.nationality_id_invalid; 
  // validationErrorMsg Enum
  
  private isWantToCompleteData: boolean = false;

  public openEdit($event): void  {
    this.edit[$event.editNameInEvent] = true;
  };
  public closeEdit($event): void  {
    this.edit[$event.editNameInEvent] = false;
  };

  public setOptions(): void {
    if(this.allNationalities) {
      this.setNationalities(this.allNationalities);
    } else {
      this.clientService.getAllNationalities()
      .then( res => {
        this.setNationalities(res.nationalities);
        this.allNationalities = res.nationalities;
      })
      .catch( e => this.alertService.httpError(e) );
    }
  };

  public onEditPassword($event): void {
    const {fieldName , value} = $event;

    this.edit[fieldName] = true;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['client_id'] = this.client_id;

    this.clientService.editClientPassword(obj)
    .then(() => {
      this.loadingService.hide(); 
      this.edit[fieldName] = false;
      this.client.updatedAt = new Date();
    })
    .catch( e => this.alertService.httpError(e) );
  };
  public onSaveEdit($event): void {
    const {fieldName , value} = $event;

    this.edit[fieldName] = true;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['client_id'] = this.client_id;
  
    this.clientService.editClient(obj)
    .then(() => {
      this.loadingService.hide(); 
      this.edit[fieldName] = false;
      this.client.updatedAt = new Date();
      this.client[fieldName] = value;
      if(fieldName == 'name') {
        this.setTitle();
      };
      this.clientService.saveClientInLocalStorage(this.client);
    })
    .catch( e => this.alertService.httpError(e) );

  };
  public onEditNationality($event): void {
    const {fieldName , value} = $event;

    this.edit[fieldName] = true;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['client_id'] = this.client_id;
  
    this.clientService.editClient(obj)
    .then(() => {
      this.loadingService.hide(); 
      this.edit[fieldName] = false;
      this.client.updatedAt = new Date();
      this.client.nationality = this.allNationalities.filter( v => v._id === value)[0];
      this.clientService.saveClientInLocalStorage(this.client);
    })
    .catch( e => this.alertService.httpError(e) );


  };
  public onSaveEditDate($event): void {
    let {fieldName , value} = $event;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['client_id'] = this.client_id;
  
    this.clientService.editClient(obj)
    .then(() => {
      this.loadingService.hide(); 
      this.edit[fieldName] = false;
      this.edit[fieldName + '_hijri'] = false;
      this.client.updatedAt = new Date();
      this.client[fieldName] = value;
      this.clientService.saveClientInLocalStorage(this.client);
    })
    .catch( e => this.alertService.httpError(e) );

  };


  public onValidationError($event): void {
    const {validationErrorMsg} = $event;
    this.alertService.alertError(validationErrorMsg);
  };

  private getClient(): void {
    this.clientService.getThisClient()
    .then( res => {
      this.client = JSON.parse(localStorage.getItem('client'));
      this.clientImage  = res.client.image ? [ environment.url + '/images/' + res.client.image ] : ['/assets/images/no-image.png']  
      this.afterGetClient();
    })
    .catch( e => this.alertService.httpError(e) );

  };

  private init(){
    this.setOptions();
    this.getClient();
  };
  private onLangChange(): void {
    this.setOptions();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
    this.activatedRouteParamsSubscribe = this.activatedRoute.queryParams.subscribe(
      (params) => {
        this.isWantToCompleteData = !!params['isWantToCompleteData'];
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
    this.activatedRouteParamsSubscribe.unsubscribe();
  };

  private afterGetClient(): void {
    this.setTitle();
    if(this.isWantToCompleteData) {
      this.edit.iqama = !this.client.iqama;
      this.edit.iqamaExpiryDate = !this.client.iqamaExpiryDate;
      this.edit.drivingLicence = !this.client.drivingLicence;
      this.edit.drivingLicenceExpiryDate = !this.client.drivingLicenceExpiryDate;
      this.edit.birthday = !this.client.birthday;
    }
  };

  private setTitle(): void {
    const title = this.client.name;
    $('title').text(title);
  };

  public onAddImage($event): void {
    this.loadingService.show();
    const formData: FormData = new FormData();
    formData.append('image', $event.files[0]);
    formData.append('client_id', this.client_id);
    this.clientService.addClientImage(formData)
    .then( res => {
      this.loadingService.hide();
      const image = environment.url + '/images/' + res.image ;
      this.clientImage = [ image ];
    })
    .catch( e => this.alertService.httpError(e))
  };

  private setNationalities(allNationalities: INationalityModel[]): void {
    if((window as any).lang === 'ar') {
      this.nationalitiesOptions = allNationalities.map( v => {
        return {
          label : v.nameAr,
          value : v._id
        }
      })
    } else {
      this.nationalitiesOptions = allNationalities.map( v => {
        return {
          label : v.nameEn,
          value : v._id
        }
      });
    };
  };
  
  public saveAll(): void {
    for(const i in this.edit) {
      if(this.edit[ i]) {
        this[i].save();
      };
    };
  };

};
 

