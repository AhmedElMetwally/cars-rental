import { IBookingModel } from './../../models/bookingModel';
import { ClientService } from '../../service/client.service';
import { environment } from '../../../environments/environment';
import { AlertService } from '../../service/alert.service';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
declare const $;

@Component({
  selector: 'app-view-client-bookings',
  templateUrl: './view-client-bookings.component.html',
})
export class ViewClientBookingsComponent implements OnInit {

  constructor(
    private clientService: ClientService,
    private translateService: TranslateService,
    private alertService: AlertService,
  ){};

  public imageUrl: string =  environment.url + '/images/';

  // loading
  public loading: boolean = true;
  
  // page & limit
  public limit: number = 30;
  public page: number = 1;

  // stop LazyLoad
  public scrollIsLock: boolean = false;
  
  // my data
  public bookings: IBookingModel[] = []; 

  private translateServiceSubscribe: EventEmitter<any>;

  // title
  private setTitle(): void {
    this.translateService.get('client_bookings')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };

  // show data
  private showBookings(bookings: IBookingModel[]): void {

    // if length < limit (stop new loading)
    this.scrollIsLock = bookings.length < this.limit;

    // add 1 to page
    this.page += 1 ;

    // add new data
    this.bookings.push(...bookings);
    
    // stop loading
    this.loading = false;
  };

  // on lazyload 
  public onLazyLoad($event): void {
    // if lock stop here
    if( ! this.scrollIsLock ) {
      // lock before get data
      this.scrollIsLock = true;

      // show loading before get data
      this.loading = true;

      if($event.searchMode) {
        this.getBookingsSearch($event.searchModel);
      } else {
        this.getBookings($event.sortBy);
      };
      
    };
  };
  public onReset($event): void {
    this.scrollIsLock = false;
    this.bookings = [];
    this.page = 1;
    this.
    onLazyLoad($event)
  };

  private getBookings(sortBy: string): void {
    this.clientService.getThisClientBookings(this.page , this.limit , sortBy)
    .then( res => {
      this.showBookings(res.bookings);
    })
    .catch( e => this.alertService.httpError(e) );
  };
  private getBookingsSearch(searchQuery: string): void {
    this.clientService.getThisClientBookingsSearch(this.page, this.limit , searchQuery)
    .then( res => {
      this.showBookings(res.bookings);
    })
    .catch( e => this.alertService.httpError(e) );
  };


  private setOptions(): void {
  };
  private onLangChange(): void {
    this.setOptions();
    this.setTitle();
  };
  private init(): void {
    this.setTitle();
  };
  public ngOnInit(): void  {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void  {
    this.translateServiceSubscribe.unsubscribe();
  };


};
