import { ViewClientBookingComponent } from './view-client-booking/view-client-booking.component';
import { InputCalendarHijriInProfileModule } from '../common/input-calendar-hijri-in-profile/input-calendar-hijri-in-profile.module';
import { InputSelectOneInProfileModule } from '../common/input-select-one-in-profile/input-select-one-in-profile.module'
import { InputPasswordInProfileModule } from '../common/input-password-in-profile/input-password-in-profile.module'
import { CustomDataViewModule } from '../common/custom-data-view/custom-data-view.module';
import { ViewClientBookingsComponent } from './view-client-bookings.component';
import { ViewClientBookingsRoutingModule } from './view-client-bookings-routing.module';
import { InputTextInProfileModule } from '../common/input-text-in-profile/input-text-in-profile.module';
import { TooltipModule } from 'primeng/tooltip';
import { PanelHeadingInProfileModule } from '../common/panel-heading-in-profile/panel-heading-in-profile.module';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../../modules/PipesModule';
import { ButtonModule } from 'primeng/button';
import { InputCalendarInProfileModule } from '../common/input-calendar-in-profile/input-calendar-in-profile.module';
import { ImageSliderModule } from '../common/image-slider/image-slider.module';

@NgModule({
  imports: [
    CommonModule,
    ViewClientBookingsRoutingModule,
    TranslateModuleForChild,
    PipesModule,
    
    TooltipModule,
    ButtonModule,
    
    CustomDataViewModule,
    PanelHeadingInProfileModule,
    InputTextInProfileModule,
    ImageSliderModule,
  ],
  declarations: [
    ViewClientBookingComponent,
    ViewClientBookingsComponent,
  ],
})
export class ViewClientBookingsModule { }
