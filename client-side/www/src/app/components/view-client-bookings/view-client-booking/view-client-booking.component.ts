import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { SharedService } from './../../../service/shared.service';
import { ClientService } from './../../../service/client.service';
import { environment } from './../../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { LoadingService } from './../../../service/loading.service';
import { AlertService } from './../../../service/alert.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
declare const $ , moment;


@Component({
  selector: 'app-view-client-booking',
  templateUrl: './view-client-booking.component.html',
  styleUrls : ['view-client-booking.component.scss']
})
export class ViewClientBookingComponent implements OnInit , OnDestroy {
 
  constructor(
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translateService: TranslateService,
    private sharedService: SharedService,
    private clientService: ClientService,
  ) { };
  
  private translateServiceSubscribe: any;
  private activatedRouteSubscribe: any;

  public carGroupTitle: string;

  public booking: any;
  public booking_id: string;

  public searchForm: FormGroup;
  
  public setOptions(): void {
  };
  private getBooking_id(): void {
    this.activatedRouteSubscribe = this.activatedRoute.params.subscribe( params => {
      this.booking_id = params['booking_id'];
      this.afterGetBooking_id();
    });
  };
  private afterGetBooking_id(): void {
    this.getBooking();
  };
  private afterGetBooking(): void {
    this.setCarGroupTitle();
  };
  private getBooking(): void {
    this.clientService.getThisClientBooking(this.booking_id)
    .then(({booking}) => {
      booking.carGroup.images = booking.carGroup.images.map( image => environment.url + '/images/' + image);
      this.booking = booking;
      this.loadingService.hide();
      this.afterGetBooking();
    })
    .catch( e => this.alertService.httpError(e) );
  };
  private setCarGroupTitle(): void {
    const fieldName = (window as any).lang === 'ar' ? 'nameAr': 'nameEn'
    this.carGroupTitle = `
      ${(this.booking.carGroup as any).carType[fieldName]} - 
      ${(this.booking.carGroup as any).carModel[fieldName]} -
      ${(this.booking.carGroup as any).year}
    `;
    $('title').text(this.carGroupTitle);
  };
  private init(){
    this.setOptions();
    this.getBooking_id()
  };
  private onLangChange(): void {
    if(this.booking){
      this.setCarGroupTitle();
    }
    this.setOptions();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };
  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
    this.activatedRouteSubscribe.unsubscribe();
  };

  public convertDateToHijri(date: any): string {
    if(date){
      return this.sharedService.numberArToEn(
        moment(date).format('iYYYY-iMM-iDD')
      );
    } else {
      return ''
    }
  };
  
  public getBookingType(bookingType: number): string {
    return (window as any).lang === 'ar' ? 
      this.sharedService.EbookingTypeAr[bookingType]
    :
    this.sharedService.EbookingTypeEn[bookingType];
  };

};
 


