import { ViewClientBookingComponent } from './view-client-booking/view-client-booking.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ViewClientBookingsComponent } from './view-client-bookings.component';

const routes: Routes = [
  {
    path : '',
    component : ViewClientBookingsComponent
  },
  {
    path: ':booking_id',
    component : ViewClientBookingComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewClientBookingsRoutingModule { }
