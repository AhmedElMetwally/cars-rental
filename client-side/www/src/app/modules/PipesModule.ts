import { SafeHtmlPipe } from '../pipes/safe-html.pipe';
import { carLengthPipe } from '../pipes/car-length.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeAgoPipe } from '../pipes/time-ago.pipe'
import { getValueByLangPipe } from '../pipes/get-value-by-lang.pipe';
import { ShowBranchesPipe } from '../pipes/show-branches.pipe';
import { numEnToArPipe } from '../pipes/num-en-to-ar.pipe';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    TimeAgoPipe,
    getValueByLangPipe,
    ShowBranchesPipe,
    numEnToArPipe,
    carLengthPipe,
    SafeHtmlPipe,
  ],
  exports: [
    TimeAgoPipe,
    getValueByLangPipe,
    ShowBranchesPipe,
    numEnToArPipe,
    carLengthPipe,
    SafeHtmlPipe,
  ],
})
export class PipesModule { }
