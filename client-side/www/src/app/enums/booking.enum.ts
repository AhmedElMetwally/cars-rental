export enum EbookingType {
    'request',
    'cancelled',
    'waiting_to_confirm', // then expired or active
    'active',
    'expired',
    'done',
    // if returnDate Done and type is active
    // this booking Done
};

export enum EbookingTypeAr {
    'طلب',
    'ملغي',
    'منتظر التاكيد', // then expired or active
    'مفعل',
    'منتهي الصلاحية',
    'منتهي',
    // if returnDate Done and type is active
    // this booking Done
};

export enum EbookingTypeEn {
    'Request',
    'Cancelled',
    'Waiting To Confirm', // then expired or active
    'Active',
    'Expired',
    'Done',
    // if returnDate Done and type is active
    // this booking Done
};