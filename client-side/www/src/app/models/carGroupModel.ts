import { IBranchModel } from './branchModel';
import { ICarGroupModel } from './carGroupModel';
import { ICarTypeModel } from './carTypeModel';
import { ICarModelModel } from './carModelModel';

export interface ICarGroupModel {
    
    _id: string;
    
    carType: ICarTypeModel;
    carModel: ICarModelModel;
    branch: IBranchModel;

    year: number;
    allowedKm: number;
    plusKmPrice: number;

    pricePerDay: number;
    discountPerDay: number;

    views: number;

    carsCount?: number; // set in [GET] http request

    images: string[];
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export const RegExpCarGroupModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    year : /^[\d]{4}$/,
    allowedKm : /^[1-9]([0-9]{0,5})$/, // 999999 ,
    plusKmPrice : /^([0-9]([0-9]{0,5})|[0-9]([0-9]{0,5}).[1-9]([0-9]{0,5}))$/, // 2999 2999.2999

    pricePerDay : /^[1-9]([0-9]{0,3})$/, // 9999
    discountPerDay : /^[1-9]([0-9]{0,3})$/, // 9999

    views : /^\d+$/, // any Number

    deleted : /^[0-1]$/, 
};


