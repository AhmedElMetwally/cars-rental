export interface ICarTypeModel {
    _id: string;
    
    nameAr: string;
    nameEn: string;
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export const RegExpCarTypeModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    nameEn : /^[\w-\s]+$/,
    nameAr : /^[\u0600-\u06FF\w-\s]+$/,

    deleted : /^[0-1]$/, 
};



