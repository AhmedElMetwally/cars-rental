import { ICityModel } from './cityModel';
export interface IBranchModel {
    _id: string;
    
    nameEn: string;
    nameAr: string;

    addressEn: string;
    addressAr: string;

    overviewEn: string;
    overviewAr: string;

    phone: string;
    location: [number , number]; // [ longitude, latitude ]
    bookingStartCode: number;
    waitingHoursForConfirm: number;
    
    city: string|ICityModel;

    images: string[];
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export const RegExpBranchModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    nameEn : /^[\w-\s]+$/,
    nameAr : /^[\u0600-\u06FF\w-\s]+$/,

    addressEn : /^[\w-\s]+$/,
    addressAr : /^[\u0600-\u06FF\w-\s]+$/,

    overviewEn: /.*/,
    overviewAr: /.*/,

    phone : /^[0-9\-\+]{9,15}$/,
    location : /^((([1-9])|([1-9]([0-9]+)))|-?\d*(\.\d+)?)$/, // [ item , item ]
    bookingStartCode : /^[0-9]{3}$/, // 999 
    waitingHoursForConfirm : /^[0-9]([0-9]{0,2})$/, 

    deleted : /^[0-1]$/, 
};
