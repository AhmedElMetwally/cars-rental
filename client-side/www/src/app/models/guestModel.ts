export interface IGuestModel {
    
    name: string;
    phone: string;
    iqama: string;
    iqamaExpiryDate: Date;
    drivingLicence: string;
    drivingLicenceExpiryDate: Date;
    birthday: Date;
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export const RegExpGuestModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    name : /^[\u0600-\u06FF\w-\s]+$/,

    phone : /^[0-9\-\+]{9,15}$/,

    iqama : /^[0-9]{1,30}$/, // 30 length of Number
    iqamaExpiryDate : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,

    drivingLicence : /^[0-9]{1,30}$/, // 30 length of Number
    drivingLicenceExpiryDate : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,

    birthday : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,

    deleted : /^[0-1]$/, 
};

