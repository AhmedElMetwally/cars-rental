
export interface IMessageModel {

    _id: string;
    
    name: string;
    email: string;
    message: string;
    
    isView: number;
    
    deleted: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export const RegExpMessageModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    name : /^[\u0600-\u06FF\w-\s]+$/,

    email : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    phone : /^[0-9\-\+]{9,15}$/,

    message : /^(.*){1,500}$/,
    
};

