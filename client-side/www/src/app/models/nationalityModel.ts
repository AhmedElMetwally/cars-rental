export interface INationalityModel {

    _id: string;

    nameEn: string;
    nameAr: string;

    createdAt?: Date;
    updatedAt?: Date;

};

export const RegExpNationalityModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    nameEn : /^[\w-\s]+$/,
    nameAr : /^[\u0600-\u06FF\w-\s]+$/,

    deleted : /^[0-1]$/, 
};
