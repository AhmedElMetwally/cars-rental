import { AuthService } from '../service/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate , Router } from '@angular/router';

@Injectable({
  providedIn : 'root'
})
export class isEmployeeGuard implements CanActivate {
  constructor(
    private authService: AuthService ,
    private router:Router
  ){ }

  canActivate():  boolean {
    if( this.authService.isEmployee() ) {
      return true
    } else {
      this.router.navigate(['/employee-signin'])
      return false
    }
  }
}



@Injectable({
  providedIn : 'root'
})
export class isNotEmployeeGuard implements CanActivate {
  constructor(
    private authService: AuthService ,
    private router:Router
  ){ }

  canActivate():  boolean {
    if( this.authService.isEmployee() ) {
      this.router.navigate(['/employee-profile'])
      return false
    } else {
      return true 
    }
  }
}

