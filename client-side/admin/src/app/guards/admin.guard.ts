import { AuthService } from '../service/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate , Router } from '@angular/router';

@Injectable({
  providedIn : 'root'
})
export class isAdminGuard implements CanActivate {
  constructor(
    private authService: AuthService ,
    private router:Router
  ){ }

  canActivate():  boolean {
    if( this.authService.isAdmin() ) {
      return true
    } else {
      this.router.navigate(['/admin-signin'])
      return false
    };
  };

};

@Injectable({
  providedIn : 'root'
})
export class isNotAdminGuard implements CanActivate {
  constructor(
    private authService: AuthService ,
    private router:Router
  ){ }

  canActivate():  boolean {
    if( this.authService.isAdmin() ) {
      this.router.navigate(['/view-employees'])
      return false
    } else {
      return true 
    }
  };
};