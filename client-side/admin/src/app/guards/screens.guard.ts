import { Escreens } from '../enums/screens.enum';
import { AlertService } from '../service/alert.service';
import { AuthService } from '../service/auth.service';
import { Injectable  } from '@angular/core';

import { CanActivate , ActivatedRouteSnapshot } from '@angular/router';


@Injectable({
  providedIn : 'root'
})
export class screensGuard implements CanActivate {

  constructor(
    private authService:AuthService ,
    private alertService: AlertService,
  ){ };

  canActivate( route: ActivatedRouteSnapshot ): boolean {
    const screens: Escreens[] = route.data.screens;
    for(let screen of screens) {
      if(!this.authService.checkScreen(screen)) {
        this.alertService.alertError(
          (window as any).lang === 'ar' ? 'لا تملك الصلاحية' : 'You Don`t Have access'
        )
        return false;
      };
    };
    return true
  };

}
