import { ViewClientComponent } from './view-client/view-client.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ViewClientsComponent } from './view-clients.component';

const routes: Routes = [
  {
    path : '',
    component : ViewClientsComponent
  },
  {
    path: ':client_id',
    component : ViewClientComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewClientsRoutingModule { }
