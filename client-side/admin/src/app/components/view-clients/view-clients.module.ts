import { InputCalendarHijriInProfileModule } from './../common/input-calendar-hijri-in-profile/input-calendar-hijri-in-profile.module';
import { InputSelectOneInProfileModule } from '../common/input-select-one-in-profile/input-select-one-in-profile.module'
import { InputPasswordInProfileModule } from '../common/input-password-in-profile/input-password-in-profile.module'
import { CustomDataViewModule } from '../common/custom-data-view/custom-data-view.module';
import { ViewClientsComponent } from './view-clients.component';
import { ViewClientComponent } from './view-client/view-client.component';
import { ViewClientsRoutingModule } from './view-clients-routing.module';
import { InputTextInProfileModule } from '../common/input-text-in-profile/input-text-in-profile.module';
import { TooltipModule } from 'primeng/tooltip';
import { PanelHeadingInProfileModule } from '../common/panel-heading-in-profile/panel-heading-in-profile.module';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../../modules/PipesModule';
import { ButtonModule } from 'primeng/button';
import { ClientService } from '../../service/client.service';
import { InputCalendarInProfileModule } from '../common/input-calendar-in-profile/input-calendar-in-profile.module';
import { ImageSliderModule } from '../common/image-slider/image-slider.module';

@NgModule({
  imports: [
    CommonModule,
    ViewClientsRoutingModule,
    TranslateModuleForChild,
    PipesModule,
    
    TooltipModule,
    ButtonModule,
    
    CustomDataViewModule,
    PanelHeadingInProfileModule,
    InputTextInProfileModule,
    InputCalendarInProfileModule,
    InputCalendarHijriInProfileModule,
    InputPasswordInProfileModule,
    ImageSliderModule,
    InputSelectOneInProfileModule
  ],
  declarations: [
    ViewClientComponent,
    ViewClientsComponent,
  ],
  providers : [
    ClientService
  ]
})
export class ViewClientsModule { }
