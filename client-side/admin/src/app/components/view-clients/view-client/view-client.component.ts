import { SelectItem } from 'primeng/components/common/selectitem';
import { INationalityModel } from '../../../models/nationalityModel'
import { environment } from '../../../../environments/environment'
import { IClientModel } from '../../../models/clientModel';
import { ClientService } from '../../../service/client.service';
import { AuthService } from '../../../service/auth.service';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../service/shared.service';
import { LoadingService } from '../../../service/loading.service';
import { AlertService } from '../../../service/alert.service';
import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { LogService } from '../../../service/log.service';
declare const $;


@Component({
  selector: 'app-view-client',
  templateUrl: './view-client.component.html',
  styleUrls : ['view-client.component.scss']
})
export class ViewClientComponent implements OnInit , OnDestroy {
 
  constructor(
    private authService: AuthService,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translateService: TranslateService,
    private clientService: ClientService,
    private logService: LogService,
    private sharedService: SharedService,
    private activatedRoute: ActivatedRoute
  ) { };

  private translateServiceSubscribe: EventEmitter<any>;
  private activatedRouteSubscribe: any;

  public clientImage: [string];

  public client: IClientModel;
  public edit = {
    name: false, password: false, email: false, phone: false,
    iqama: false, iqamaExpiryDate: false, 
    drivingLicence: false, drivingLicenceExpiryDate: false,
    birthday: false, address: false,
    job: false, nationality_id: false, 
    birthday_hijri: false , iqamaExpiryDate_hijri: false , drivingLicenceExpiryDate_hijri: false,
  };
  public client_id: string;

  private allNationalities: INationalityModel[];
  public nationalitiesOptions: SelectItem[] = [];

  public RegExpClientModel = this.sharedService.mainRegExp.RegExpClientModel; 
  public RegExpNationalityModel = this.sharedService.mainRegExp.RegExpNationalityModel; 
  
  
  // validationErrorMsg Enum
  public name_required = this.sharedService.mainErrorEnum.name_required; 
  public name_invalid = this.sharedService.mainErrorEnum.name_invalid; 

  public password_required = this.sharedService.mainErrorEnum.password_required; 
  public password_invalid = this.sharedService.mainErrorEnum.password_invalid; 
  
  public email_required = this.sharedService.mainErrorEnum.email_required; 
  public email_invalid = this.sharedService.mainErrorEnum.email_invalid; 

  public phone_required = this.sharedService.mainErrorEnum.phone_required; 
  public phone_invalid = this.sharedService.mainErrorEnum.phone_invalid; 

  public iqama_required = this.sharedService.mainErrorEnum.iqama_required; 
  public iqama_invalid = this.sharedService.mainErrorEnum.iqama_invalid; 

  public iqamaExpiryDate_required = this.sharedService.mainErrorEnum.iqamaExpiryDate_required; 
  public iqamaExpiryDate_invalid = this.sharedService.mainErrorEnum.iqamaExpiryDate_invalid; 

  public drivingLicence_required = this.sharedService.mainErrorEnum.drivingLicence_required; 
  public drivingLicence_invalid = this.sharedService.mainErrorEnum.drivingLicence_invalid; 

  public drivingLicenceExpiryDate_required = this.sharedService.mainErrorEnum.drivingLicenceExpiryDate_required; 
  public drivingLicenceExpiryDate_invalid = this.sharedService.mainErrorEnum.drivingLicenceExpiryDate_invalid; 

  public birthday_required = this.sharedService.mainErrorEnum.birthday_required; 
  public birthday_invalid = this.sharedService.mainErrorEnum.birthday_invalid; 

  public address_required = this.sharedService.mainErrorEnum.address_required; 
  public address_invalid = this.sharedService.mainErrorEnum.address_invalid; 

  public job_required = this.sharedService.mainErrorEnum.job_required; 
  public job_invalid = this.sharedService.mainErrorEnum.job_invalid; 

  public nationality_id_required = this.sharedService.mainErrorEnum.nationality_id_required; 
  public nationality_id_invalid = this.sharedService.mainErrorEnum.nationality_id_invalid; 
  // validationErrorMsg Enum
  

  // screens
  public canEditClient: boolean;
  public canDeleteClient: boolean;
  public canRestoreClient: boolean;
  public canEditClientImage: boolean;
  public canEditClientPassword: boolean;

  private checkScreens(): void {
    this.canEditClient =  this.authService.checkScreen(this.sharedService.Escreens.edit_client);
    this.canDeleteClient = this.authService.checkScreen(this.sharedService.Escreens.delete_client);
    this.canRestoreClient = this.authService.checkScreen(this.sharedService.Escreens.restore_client);
    this.canEditClientImage = this.authService.checkScreen(this.sharedService.Escreens.edit_client_image);
    this.canEditClientPassword = this.authService.checkScreen(this.sharedService.Escreens.edit_client_password);
  };
  public openEdit($event): void  {
    this.edit[$event.editNameInEvent] = true;
  };
  public closeEdit($event): void  {
    this.edit[$event.editNameInEvent] = false;
  };

  public setOptions(): void {
    if(this.allNationalities) {
      this.setNationalities(this.allNationalities);
    } else {
      this.clientService.getAllNationalities()
      .then( res => {
        this.setNationalities(res.nationalities);
        this.allNationalities = res.nationalities;
      })
      .catch( e => this.alertService.httpError(e) );
    }
  };

  public onEditPassword($event): void {
    const {fieldName , value} = $event;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['client_id'] = this.client_id;

    this.clientService.editClientPassword(obj)
    .then(() => {
      this.loadingService.hide(); 
      this.edit[fieldName] = false;
      this.client.updatedAt = new Date();
      this.client[fieldName] = value;
    })
    .catch( e => this.alertService.httpError(e) );
  };

  public onSaveEdit($event): void {
    const {fieldName , value} = $event;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['client_id'] = this.client_id;
  
    this.clientService.editClient(obj)
    .then(() => {
      this.loadingService.hide(); 
      this.edit[fieldName] = false;
      this.client.updatedAt = new Date();
      this.client[fieldName] = value;
      if(fieldName == 'name') {
        this.setTitle();
      };
    })
    .catch( e => this.alertService.httpError(e) );


  };

  public onSaveEditDate($event): void {
    let {fieldName , value} = $event;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['client_id'] = this.client_id;
  
    this.clientService.editClient(obj)
    .then(() => {
      this.loadingService.hide(); 
      this.edit[fieldName] = false;
      this.edit[fieldName + '_hijri'] = false;
      this.client.updatedAt = new Date();
      this.client[fieldName] = value;
    })
    .catch( e => this.alertService.httpError(e) );

  };
  
  public onEditNationality($event): void {
    const {fieldName , value} = $event;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['client_id'] = this.client_id;
  
    this.clientService.editClient(obj)
    .then(() => {
      this.loadingService.hide(); 
      this.edit[fieldName] = false;
      this.client.updatedAt = new Date();
      this.client.nationality = this.allNationalities.filter( v => v._id === value)[0];
    })
    .catch( e => this.alertService.httpError(e) );


  };

  public onValidationError($event): void {
    const {validationErrorMsg} = $event;
    this.alertService.alertError(validationErrorMsg);
  };

  private getClient(): void {
    this.loadingService.show();
    this.clientService.getClient(this.client_id)
    .then(({ client }) => {
      this.clientImage  = client.image ? [ environment.url + '/images/' + client.image ] : ['/assets/images/no-image.png']  
      this.client = client;
      this.loadingService.hide();
      this.afterGetClient();
    })
    .catch( e => this.alertService.httpError(e) );
  };

  private log(text): void {
    this.logService.log('view-client' , text);
  };

  private init(){
    this.setOptions();
    this.getClient_id();
  };
  private onLangChange(): void {
    this.setOptions();

  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
    this.activatedRouteSubscribe.unsubscribe();
  };

  private getClient_id(): void {
    this.activatedRouteSubscribe = this.activatedRoute.params.subscribe( params => {
      this.client_id = params['client_id'];
      this.afterGetClient_id();
    });
  };
  private afterGetClient_id(): void {
    this.getClient();
  };
  private afterGetClient(): void {
    this.checkScreens();
    this.setTitle();
  };

  public delete(): void {
    this.clientService.deleteClient(this.client_id)
    .then(() => {
      this.client.deleted = 1;
      this.alertService.done();
    })
    .catch(e => this.alertService.httpError(e))
  };
  public restore(): void {
    const obj = {
      client_id : this.client_id
    };
    this.clientService.restoreClient(obj)
    .then(() => {
      this.client.deleted = 0;
      this.alertService.done();
    })
    .catch(e => this.alertService.httpError(e))
  };

  public getSignupFrom(num): string {
    return (window as any).lang === 'ar' ? this.sharedService.EsignupFromAr[num] : this.sharedService.EsignupFromEn[num];
  };

  private setTitle(): void {
    const title = this.client.name;
    $('title').text(title);
  };

  public onAddImage($event): void {
    this.loadingService.show();
    const formData: FormData = new FormData();
    formData.append('image', $event.files[0]);
    formData.append('client_id', this.client_id);
    this.clientService.addClientImage(formData)
    .then( res => {
      this.loadingService.hide();
      const image = environment.url + '/images/' + res.image ;
      this.clientImage = [ image ];
    })
    .catch( e => this.alertService.httpError(e))
  };

  private setNationalities(allNationalities: INationalityModel[]): void {
    if((window as any).lang === 'ar') {
      this.nationalitiesOptions = allNationalities.map( v => {
        return {
          label : v.nameAr,
          value : v._id
        }
      })
    } else {
      this.nationalitiesOptions = allNationalities.map( v => {
        return {
          label : v.nameEn,
          value : v._id
        }
      });
    };
  };

};
 

