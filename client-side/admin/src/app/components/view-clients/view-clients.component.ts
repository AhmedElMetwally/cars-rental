import { ClientService } from '../../service/client.service';
import { environment } from '../../../environments/environment';
import { AlertService } from '../../service/alert.service';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IClientModel } from '../../models/clientModel';
declare const $;

@Component({
  selector: 'app-view-clients',
  templateUrl: './view-clients.component.html',
})
export class ViewClientsComponent implements OnInit {

  constructor(
    private clientService: ClientService,
    private translateService: TranslateService,
    private alertService: AlertService,
  ){};

  public imageUrl: string =  environment.url + '/images/';

  // loading
  public loading: boolean = true;
  
  // page & limit
  public limit: number = 30;
  public page: number = 1;

  // stop LazyLoad
  public scrollIsLock: boolean = false;
  
  // my data
  public clients: IClientModel[] = []; 

  private translateServiceSubscribe: EventEmitter<any>;

  // title
  private setTitle(): void {
    this.translateService.get('view_clients')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };

  // show data
  private showClients(clients: IClientModel[]): void {

    // if length < limit (stop new loading)
    this.scrollIsLock = clients.length < this.limit;

    // add 1 to page
    this.page += 1 ;

    // add new data
    this.clients.push(...clients);
    
    // stop loading
    this.loading = false;
  };

  // on lazyload 
  public onLazyLoad($event): void {
    // if lock stop here
    if( ! this.scrollIsLock ) {
      // lock before get data
      this.scrollIsLock = true;

      // show loading before get data
      this.loading = true;

      if($event.searchMode) {
        this.getClientsSearch($event.searchModel);
      } else {
        this.getClients($event.sortBy);
      };
      
    };
  };
  public onReset($event): void {
    this.scrollIsLock = false;
    this.clients = [];
    this.page = 1;
    this.
    onLazyLoad($event)
  };

  private getClients(sortBy: string): void {
    this.clientService.getClients(this.page , this.limit , sortBy)
    .then( res => {
      this.showClients(res.clients);
    })
    .catch( e => this.alertService.httpError(e) );
  };
  private getClientsSearch(searchQuery: string): void {
    this.clientService.getClientsSearch(this.page, this.limit , searchQuery)
    .then( res => {
      this.showClients(res.clients);
    })
    .catch( e => this.alertService.httpError(e) );
  };


  private setOptions(): void {
  };
  private onLangChange(): void {
    this.setOptions();
    this.setTitle();
  };
  private init(): void {
    this.setTitle();
  };
  public ngOnInit(): void  {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void  {
    this.translateServiceSubscribe.unsubscribe();
  };


};
