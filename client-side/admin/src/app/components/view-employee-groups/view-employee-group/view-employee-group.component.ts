import { IEmployeeGroupModel } from '../../../models/employeeGroupModel';
import { EmployeeGroupService } from '../../../service/employee-group.service';
import { IBranchModel } from '../../../models/branchModel';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../service/shared.service';
import { LoadingService } from '../../../service/loading.service';
import { AlertService } from '../../../service/alert.service';
import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { LogService } from '../../../service/log.service';
import { SelectItem } from 'primeng/components/common/selectitem';
import { EnumValues } from 'enum-values';
declare const $;


@Component({
  selector: 'app-view-employee-group',
  templateUrl: './view-employee-group.component.html',
})
export class ViewEmployeeGroupComponent implements OnInit , OnDestroy {
 
  constructor(
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translateService: TranslateService,
    private employeeGroupService: EmployeeGroupService,
    private logService: LogService,
    private sharedService: SharedService,
    private activatedRoute: ActivatedRoute
  ) { };

  private translateServiceSubscribe: EventEmitter<any>;
  private activatedRouteSubscribe: any;

  public allBranches: IBranchModel[];
  public branchesOptions: SelectItem[] = [];
  public screensOptions: SelectItem[] = [];
   
  public employeeGroup: IEmployeeGroupModel;
  public edit = {
    nameEn: false, nameAr: false,branches: false,screens: false
  };
  public employeeGroup_id: string;

  public RegExpEmployeeGroupModel = this.sharedService.mainRegExp.RegExpEmployeeGroupModel; 
  public RegExpBranchModel = this.sharedService.mainRegExp.RegExpBranchModel; 
  
  public branches_ids: string[];
  
  // validationErrorMsg Enum
  public nameEn_required = this.sharedService.mainErrorEnum.nameEn_required; 
  public nameEn_invalid = this.sharedService.mainErrorEnum.nameEn_invalid; 
  public nameAr_required = this.sharedService.mainErrorEnum.nameAr_required; 
  public nameAr_invalid = this.sharedService.mainErrorEnum.nameAr_invalid; 
  public branches_required = this.sharedService.mainErrorEnum.branches_required; 
  public branches_invalid = this.sharedService.mainErrorEnum.branches_invalid; 
  public screens_required = this.sharedService.mainErrorEnum.screens_required; 
  public screens_invalid = this.sharedService.mainErrorEnum.screens_invalid; 
  

  public openEdit($event): void  {
    this.edit[$event.editNameInEvent] = true;
  };

  public closeEdit($event): void  {
    this.edit[$event.editNameInEvent] = false;
  };

  private setBranchesOptions(allBranches: IBranchModel[]): void {
    if((window as any).lang === 'ar') {
     
      this.branchesOptions = allBranches.map( v => {
        return {
          value : v._id,
          label : v.nameAr
        }
      });

    } else {

      this.branchesOptions = allBranches.map( v => {
        return {
          value : v._id,
          label : v.nameEn
        }
      });

    };
  };
  public setOptions(): void {
  
    if(this.allBranches) {
      this.setBranchesOptions(this.allBranches);
    } else{
      this.employeeGroupService.getAllBranches()
      .then( res => {
        this.allBranches = res.branches;
        this.setBranchesOptions(res.branches);
      })
      .catch( e => this.alertService.httpError(e) );
    };

    this.screensOptions = EnumValues.getNamesAndValues(
      (window as any).lang === 'ar' ? this.sharedService.EscreensAr : this.sharedService.EscreensEn
    )
    .map( obj => {
      return {
        label : obj.name,
        value : obj.value
      }
    });

  };


  public onSaveEdit($event): void {
    const {fieldName , value} = $event;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['employeeGroup_id'] = this.employeeGroup_id;

    this.employeeGroupService.editEmployeeGroup(obj)
    .then(() => {
      this.loadingService.hide(); 
      this.edit[fieldName] = false;
      this.employeeGroup.updatedAt = new Date();
      if(fieldName === 'branches') {
        this.employeeGroup['branches'] = this.allBranches.filter( v => value.indexOf(v._id) !== -1);
        this.branches_ids = value;
      } else {
        this.employeeGroup[fieldName] = value;
      }
    })
    .catch( e => this.alertService.httpError(e) );
  };

  public onValidationError($event): void {
    const {validationErrorMsg} = $event;
    this.alertService.alertError(validationErrorMsg);
  };

  private getEmployeeGroup(): void {
    this.loadingService.show();
    this.employeeGroupService.getEmployeeGroup(this.employeeGroup_id)
    .then(({ employeeGroup }) => {
      this.employeeGroup = employeeGroup;
      this.loadingService.hide();
      this.afterGetEmployeeGroup();
    })
    .catch( e => this.alertService.httpError(e) );
  };

  private log(text): void {
    this.logService.log('employee-profile' , text);
  };

  private setTitle(): void {
    const title = this.employeeGroup[ (window as any).lang === 'ar' ? 'nameAr' : 'nameEn'];
    $('title').text(title);
  };

  private init(){
    this.setOptions();
    this.getEmployeeGroup_id()
  };
  private onLangChange(): void {
    this.setTitle();
    this.setOptions();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
    this.activatedRouteSubscribe.unsubscribe();
  };

  private getEmployeeGroup_id(): void {
    this.activatedRouteSubscribe = this.activatedRoute.params.subscribe( params => {
      this.employeeGroup_id = params['employeeGroup_id'];
      this.afterGetEmployeeGroup_id();
    });
  }
  private afterGetEmployeeGroup_id(): void {
    this.getEmployeeGroup();
  }
  private afterGetEmployeeGroup(): void {
    this.setTitle();
    this.branches_ids = (this.employeeGroup as any ).branches.map( v => v._id);
  }

  public delete(): void {
    this.employeeGroupService.deleteEmployeeGroup(this.employeeGroup_id)
    .then(() => {
      this.employeeGroup.deleted = 1;
      this.alertService.done();
    })
    .catch(e => this.alertService.httpError(e))
  }
  public restore(): void {
    const obj = {
      employeeGroup_id : this.employeeGroup_id
    };
    this.employeeGroupService.restoreEmployeeGroup(obj)
    .then(() => {
      this.employeeGroup.deleted = 0;
      this.alertService.done();
    })
    .catch(e => this.alertService.httpError(e))
  }
  

  
};
 

