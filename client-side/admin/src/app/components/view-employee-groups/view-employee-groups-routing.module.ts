import { ViewEmployeeGroupsComponent } from './view-employee-groups.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ViewEmployeeGroupComponent } from './view-employee-group/view-employee-group.component';
import { AddEmployeeGroupComponent } from './add-employee-group/add-employee-group.component';

const routes: Routes = [
  {
    path : '',
    component : ViewEmployeeGroupsComponent
  },
  {
    path: 'add-employee-group',
    component : AddEmployeeGroupComponent
  },
  {
    path: ':employeeGroup_id',
    component : ViewEmployeeGroupComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewEmployeeGroupsRoutingModule { }
