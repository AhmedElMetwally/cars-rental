import { InputErrorInFormComponent } from '../common/input-error-in-form/input-error-in-form.component';
import { SelectManyModule } from '../common/select-many/select-many.module';
import { InputSelectManyInProfileModule } from '../common/input-select-many-in-profile/input-select-many-in-profile.module';
import { InputTextInProfileModule } from '../common/input-text-in-profile/input-text-in-profile.module';
import { TooltipModule } from 'primeng/tooltip';
import { PanelHeadingInProfileModule } from '../common/panel-heading-in-profile/panel-heading-in-profile.module';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { CustomDataViewModule } from '../common/custom-data-view/custom-data-view.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../../modules/PipesModule';
import { ButtonModule } from 'primeng/button';
import { ViewEmployeeGroupsComponent } from './view-employee-groups.component';
import { ViewEmployeeGroupComponent } from './view-employee-group/view-employee-group.component';
import { ViewEmployeeGroupsRoutingModule } from './view-employee-groups-routing.module';
import { EmployeeGroupService } from '../../service/employee-group.service';
import { AddEmployeeGroupComponent } from './add-employee-group/add-employee-group.component';
import { InputErrorInFormModule } from '../common/input-error-in-form/input-error-in-form.module';

@NgModule({
  imports: [
    CommonModule,
    ViewEmployeeGroupsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModuleForChild,
    PipesModule,
    
    TooltipModule,
    ButtonModule,
    
    CustomDataViewModule,
    PanelHeadingInProfileModule,
    InputTextInProfileModule,
    InputSelectManyInProfileModule,
    SelectManyModule,
    InputErrorInFormModule,
  ],
  declarations: [
    ViewEmployeeGroupsComponent,
    ViewEmployeeGroupComponent,
    AddEmployeeGroupComponent
  ],
  providers : [
    EmployeeGroupService
  ]
})
export class ViewEmployeeGroupsModule { }
