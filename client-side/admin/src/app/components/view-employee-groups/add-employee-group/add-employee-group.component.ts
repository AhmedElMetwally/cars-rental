import { EnumValues } from 'enum-values';
import { IBranchModel } from '../../../models/branchModel';
import { EmployeeGroupService } from '../../../service/employee-group.service';
import { OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AlertService } from '../../../service/alert.service';
import { LoadingService } from '../../../service/loading.service';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/components/common/selectitem';
import { Component, OnInit , EventEmitter} from '@angular/core';
import { SharedService } from '../../../service/shared.service';
declare const $;


@Component({
  selector: 'app-add-employee-group',
  templateUrl: './add-employee-group.component.html',
})
export class AddEmployeeGroupComponent implements OnInit , OnDestroy {
  
  constructor(
    private loadingService: LoadingService,
    private employeeGroupService: EmployeeGroupService,
    private router: Router,
    private alertService: AlertService,
    private sharedService: SharedService,
    private translateService: TranslateService,
  ) { };

  private translateServiceSubscribe: EventEmitter<any>;
  public formGroup: FormGroup;

  public allBranches: IBranchModel[];
  public branchesOptions: SelectItem[] = [];
  public screensOptions: SelectItem[] = [];

  private initForm(): void {
    this.formGroup = new FormGroup({ 
      nameEn : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.nameEn_required , 
          this.sharedService.mainErrorEnum.nameEn_invalid , 
          this.sharedService.mainRegExp.RegExpEmployeeGroupModel.nameEn
        ),
      ]),
      nameAr : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.nameAr_required , 
          this.sharedService.mainErrorEnum.nameAr_invalid , 
          this.sharedService.mainRegExp.RegExpEmployeeGroupModel.nameAr 
        ),
      ]),
      screens : new FormControl([] , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.screens_required , 
          this.sharedService.mainErrorEnum.screens_invalid , 
          this.sharedService.mainRegExp.RegExpEmployeeGroupModel.screens 
        ),
      ]),
      branches : new FormControl([] , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.branches_required , 
          this.sharedService.mainErrorEnum.branches_invalid , 
          this.sharedService.mainRegExp.RegExpBranchModel._id 
        ),
      ]),
    });
  };

  public onSubmit(): void {

    if(this.formGroup.invalid) {
      
      this.alertService.showFormError(this.formGroup);

    } else {

      this.loadingService.show();
      const employeeGroup = {
        nameEn : this.formGroup.controls['nameEn'].value,
        nameAr : this.formGroup.controls['nameAr'].value,
        screens : this.formGroup.controls['screens'].value,
        branches : this.formGroup.controls['branches'].value,
      }
      this.employeeGroupService.addEmployeeGroup(employeeGroup)
      .then( (res: any) => {
        this.router.navigate(['view-employee-groups' , res._id]);
      })
      .catch( e => this.alertService.httpError(e));

    };

  };

  private setTitle(): void {
    this.translateService.get('add_employee_group')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };

  private setBranchesOptions(allBranches: IBranchModel[]): void {
    if((window as any).lang === 'ar') {
     
      this.branchesOptions = allBranches.map( v => {
        return {
          value : v._id,
          label : v.nameAr
        }
      });

    } else {

      this.branchesOptions = allBranches.map( v => {
        return {
          value : v._id,
          label : v.nameEn
        }
      });

    };
  };
  public setOptions(): void {
  
    if(this.allBranches) {
      this.setBranchesOptions(this.allBranches);
    } else{
      this.employeeGroupService.getAllBranches()
      .then( res => {
        this.allBranches = res.branches;
        this.setBranchesOptions(res.branches);
      })
      .catch( e => this.alertService.httpError(e) );
    };

    this.screensOptions = EnumValues.getNamesAndValues(
      (window as any).lang === 'ar' ? this.sharedService.EscreensAr : this.sharedService.EscreensEn
    )
    .map( obj => {
      return {
        label : obj.name,
        value : obj.value
      }
    });

  };

  private init(){
    this.initForm();
    this.setTitle();
    this.setOptions();
  };
  private onLangChange(): void {
    this.setTitle();
    this.setOptions();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };


  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
  }

 
}