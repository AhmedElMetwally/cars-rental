import { EmployeeGroupService } from '../../service/employee-group.service';
import { AlertService } from '../../service/alert.service';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IEmployeeGroupModel } from '../../models/employeeGroupModel';
declare const $;

@Component({
  selector: 'app-view-employee-groups',
  templateUrl: './view-employee-groups.component.html',
})
export class ViewEmployeeGroupsComponent implements OnInit {

  constructor(
    private employeeGroupService: EmployeeGroupService,
    private translateService: TranslateService,
    private alertService: AlertService,
  ){};


  // loading
  public loading: boolean = true;
  
  // page & limit
  public limit: number = 30;
  public page: number = 1;

  // stop LazyLoad
  public scrollIsLock: boolean = false;
  
  // my data
  public employeeGroups: IEmployeeGroupModel[] = []; 

  private translateServiceSubscribe: EventEmitter<any>;


  // title
  private setTitle(): void {
    this.translateService.get('view_employee_groups')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };

  // show data
  private showEmployeeGroups(employeeGroups: IEmployeeGroupModel[]): void {

    // if length < limit (stop new loading)
    this.scrollIsLock = employeeGroups.length < this.limit;

    // add 1 to page
    this.page += 1 ;

    // add new data
    this.employeeGroups.push(...employeeGroups);
    
    // stop loading
    this.loading = false;
  };

  // on lazyload 
  public onLazyLoad($event): void {
    // if lock stop here
    if( ! this.scrollIsLock ) {
      // lock before get data
      this.scrollIsLock = true;

      // show loading before get data
      this.loading = true;
      if($event.searchMode) {
        this.getEmployeeGroupsSearch($event.searchModel)
      } else if(!$event.filterBy || $event.filterBy === 'all') {
        this.getEmployeeGroups($event.sortBy);
      };
    };
  };
  public onReset($event): void {
    this.scrollIsLock = false;
    this.employeeGroups = [];
    this.page = 1;
    this.
    onLazyLoad($event)
  };

  private getEmployeeGroups(sortBy: string): void {
    this.employeeGroupService.getEmployeeGroups(this.page , this.limit , sortBy)
    .then( res => {
      this.showEmployeeGroups(res.employeeGroups);
    })
    .catch( e => this.alertService.httpError(e) );
  };
  private getEmployeeGroupsSearch(searchQuery: string): void {
    this.employeeGroupService.getEmployeeGroupsSearch(this.page, this.limit , searchQuery)
    .then( res => {
      this.showEmployeeGroups(res.employeeGroups);
    })
    .catch( e => this.alertService.httpError(e) );
  };

  private setOptions(): void {
  
  };
  private onLangChange(): void {
    this.setOptions();
    this.setTitle();
  };
  private init(): void {
    this.setOptions();
    this.setTitle();
  };
  public ngOnInit(): void  {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void  {
    this.translateServiceSubscribe.unsubscribe();
  };

};

  