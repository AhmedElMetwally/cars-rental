import { InputCalendarHijriInProfileModule } from './../common/input-calendar-hijri-in-profile/input-calendar-hijri-in-profile.module';
import { InputTextInProfileModule } from '../common/input-text-in-profile/input-text-in-profile.module';
import { BookingService } from '../../service/booking.service';
import { CustomDataView3Module } from '../common/custom-data-view3/custom-data-view3.module';
import { ViewBookingsComponent } from './view-bookings.component';
import { ViewBookingComponent } from './view-booking/view-booking.component';
import { ViewBookingsRoutingModule } from './view-bookings-routing.module';
import { TooltipModule } from 'primeng/tooltip';
import { PanelHeadingInProfileModule } from '../common/panel-heading-in-profile/panel-heading-in-profile.module';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../../modules/PipesModule';
import { ButtonModule } from 'primeng/button';
import { InputCalendarInProfileModule } from '../common/input-calendar-in-profile/input-calendar-in-profile.module';
import { InputSelectOneInProfileModule } from '../common/input-select-one-in-profile/input-select-one-in-profile.module';
import { BtnModule } from '../common/btn/btn.module';

@NgModule({
  imports: [
    CommonModule,
    ViewBookingsRoutingModule,
    TranslateModuleForChild,
    PipesModule,
    
    TooltipModule,
    ButtonModule,
    
    CustomDataView3Module,
    PanelHeadingInProfileModule,
    InputTextInProfileModule,
    InputCalendarInProfileModule,
    InputCalendarHijriInProfileModule,
    InputSelectOneInProfileModule,
    BtnModule
    
  ],
  declarations: [
    ViewBookingsComponent,
    ViewBookingComponent,
  ],
  providers : [
    BookingService
  ]
})
export class ViewBookingsModule { }
