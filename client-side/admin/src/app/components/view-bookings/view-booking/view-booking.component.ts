import { SelectItem } from 'primeng/components/common/selectitem';
import { ICarModel } from '../../../models/carModel';
import { BookingService } from '../../../service/booking.service';
import { IBookingModel } from '../../../models/bookingModel';
import { AuthService } from '../../../service/auth.service';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../service/shared.service';
import { LoadingService } from '../../../service/loading.service';
import { AlertService } from '../../../service/alert.service';
import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { LogService } from '../../../service/log.service';
declare const $;



@Component({
  selector: 'app-view-booking',
  templateUrl: './view-booking.component.html',
  styleUrls : ['view-booking.component.scss']
})
export class ViewBookingComponent implements OnInit , OnDestroy {
 
  constructor(
    private authService: AuthService,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translateService: TranslateService,
    private bookingService: BookingService,
    private logService: LogService,
    private sharedService: SharedService,
    private activatedRoute: ActivatedRoute
  ) { };

  private translateServiceSubscribe: EventEmitter<any>;
  private activatedRouteSubscribe: any;

  public carsOptions: SelectItem[] = [];
  private allCars: ICarModel[];

  public booking: IBookingModel;
  public edit = {
    expiredAt: false, daysCount: false, discountPerDay: false, pricePerDay: false, 
    pickUpDate: false, returnDate: false, 
    car_id: false,

    expiredAt_hijri: false,pickUpDate_hijri: false, returnDate_hijri: false, 
  };
  public booking_id: string;

  public RegExpBookingModel = this.sharedService.mainRegExp.RegExpBookingModel; 
  public RegExpCarModel = this.sharedService.mainRegExp.RegExpCarModel; 
  
  public carGroupTitle: string;
  
  // validationErrorMsg Enum
  public pickUpDate_required = this.sharedService.mainErrorEnum.pickUpDate_required; 
  public pickUpDate_invalid = this.sharedService.mainErrorEnum.pickUpDate_invalid; 
  
  public returnDate_required = this.sharedService.mainErrorEnum.returnDate_required; 
  public returnDate_invalid = this.sharedService.mainErrorEnum.returnDate_invalid; 

  public expiredAt_required = this.sharedService.mainErrorEnum.expiredAt_required; 
  public expiredAt_invalid = this.sharedService.mainErrorEnum.expiredAt_invalid; 

  public daysCount_required = this.sharedService.mainErrorEnum.daysCount_required; 
  public daysCount_invalid = this.sharedService.mainErrorEnum.daysCount_invalid; 

  public pricePerDay_required = this.sharedService.mainErrorEnum.pricePerDay_required; 
  public pricePerDay_invalid = this.sharedService.mainErrorEnum.pricePerDay_invalid; 

  public discountPerDay_required = this.sharedService.mainErrorEnum.discountPerDay_required; 
  public discountPerDay_invalid = this.sharedService.mainErrorEnum.discountPerDay_invalid; 
  // validationErrorMsg Enum
  
  // screens
  public canEditBooking: boolean;
  public canCancelBooking: boolean;
  public canApproveBooking: boolean;
  public canActiveBooking: boolean;
  public canDeleteBooking: boolean;
  public canRestoreBooking: boolean;

  private checkScreens(): void {
    this.canEditBooking = this.authService.checkScreen(this.sharedService.Escreens.edit_booking);
    this.canCancelBooking = this.authService.checkScreen(this.sharedService.Escreens.cancel_booking);
    this.canApproveBooking = this.authService.checkScreen(this.sharedService.Escreens.approve_booking);
    this.canActiveBooking = this.authService.checkScreen(this.sharedService.Escreens.active_booking);
    this.canDeleteBooking = this.authService.checkScreen(this.sharedService.Escreens.delete_booking);
    this.canRestoreBooking = this.authService.checkScreen(this.sharedService.Escreens.restore_booking);
  };
  
  public openEdit($event): void  {
    this.edit[$event.editNameInEvent] = true;
  };
  public closeEdit($event): void  {
    this.edit[$event.editNameInEvent] = false;
  };

  public setOptions(): void {
  };

  public onEditCar($event): void {
    const {fieldName , value} = $event;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['booking_id'] = this.booking_id;

    this.bookingService.addCarToBooking(obj)
    .then(() => {
      this.loadingService.hide(); 
      this.edit[fieldName] = false;
      this.booking.updatedAt = new Date();
      this.booking.car = this.allCars.filter( v => v._id === value)[0];
    })
    .catch( e => this.alertService.httpError(e) );

  };
  public onEditDate($event): void {
    const {fieldName , value} = $event;

    const anotherField = fieldName === 'returnDate' ? 'pickUpDate' : 'returnDate';

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['booking_id'] = this.booking_id;
    obj[anotherField] = this.booking[anotherField];

    this.bookingService.editBookingDate(obj)
    .then((res) => {
      this.loadingService.hide(); 
      this.edit[fieldName] = false;
      this.edit[fieldName + '_hijri'] = false;
      this.booking.updatedAt = new Date();
      this.booking.returnDate = res.booking.returnDate;
      this.booking.pickUpDate = res.booking.pickUpDate;
      this.booking.total = res.booking.total;
      this.booking.daysCount = res.booking.daysCount;
    })
    .catch( e => this.alertService.httpError(e) );

  };
  public onEditExpiredAt($event): void {
    const {fieldName , value} = $event;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['booking_id'] = this.booking_id;

    this.bookingService.editBookingExpiredAt(obj)
    .then(() => {
      this.loadingService.hide(); 
      this.edit[fieldName + '_hijri'] = false;
      this.edit[fieldName] = false;
      this.booking[fieldName] = value;
      this.booking.updatedAt = new Date();
    })
    .catch( e => this.alertService.httpError(e) );

  };
  public onEditPricePerDay($event): void {
    const {fieldName , value} = $event;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['booking_id'] = this.booking_id;

    this.bookingService.editBookingPricePerDay(obj)
    .then((res) => {
      this.loadingService.hide(); 
      this.edit[fieldName] = false;
      this.booking[fieldName] = value;
      this.booking.total = res.booking.total;
      this.booking.updatedAt = new Date();
    })
    .catch( e => this.alertService.httpError(e) );

  };
  public onEditDiscountPerDay($event): void {
    const {fieldName , value} = $event;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['booking_id'] = this.booking_id;

    this.bookingService.editBookingDiscountPerDay(obj)
    .then((res) => {
      this.loadingService.hide(); 
      this.edit[fieldName] = false;
      this.booking[fieldName] = value;
      this.booking.total = res.booking.total;
      this.booking.updatedAt = new Date();
    })
    .catch( e => this.alertService.httpError(e) );

  };

  public onValidationError($event): void {
    const {validationErrorMsg} = $event;
    this.alertService.alertError(validationErrorMsg);
  };

  private getBooking(): void {
    this.loadingService.show();
    this.bookingService.getBooking(this.booking_id)
    .then(({ booking }) => {
      this.booking = booking;
      this.loadingService.hide();
      this.afterGetBooking();
    })
    .catch( e => this.alertService.httpError(e) );
  };

  private log(text): void {
    this.logService.log('view-booking' , text);
  };

  private init(){
    this.setOptions();
    this.getBooking_id();
  };
  private onLangChange(): void {
    this.setOptions();
    this.setCarGroupTitle();
    this.getAllCarsByGroupCanThisEmployeeAccess();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
    this.activatedRouteSubscribe.unsubscribe();
  };

  private getBooking_id(): void {
    this.activatedRouteSubscribe = this.activatedRoute.params.subscribe( params => {
      this.booking_id = params['booking_id'];
      this.afterGetBooking_id();
    });
  };
  private afterGetBooking_id(): void {
    this.getBooking();
  };
  private afterGetBooking(): void {
    this.checkScreens();
    this.setCarGroupTitle();
    this.getAllCarsByGroupCanThisEmployeeAccess();
  };

  public delete(): void {
    this.bookingService.deleteBooking(this.booking_id)
    .then(() => {
      this.booking.deleted = 1;
      this.alertService.done();
    })
    .catch(e => this.alertService.httpError(e))
  };
  public restore(): void {
    const obj = {
      booking_id : this.booking_id
    };
    this.bookingService.restoreBooking(obj)
    .then(() => {
      this.booking.deleted = 0;
      this.alertService.done();
    })
    .catch(e => this.alertService.httpError(e))
  };

  public active(): void {


    this.translateService.get('you_will_active_booking')
    .toPromise()
    .then( txt => {
      
      this.alertService.ask(txt)
      .then( ok => {
        
        if(!ok) return;

        const obj = {
          booking_id : this.booking_id
        };
        this.bookingService.activeBooking(obj)
        .then(() => {
          this.booking.bookingType = this.sharedService.EbookingType.active;
          this.alertService.done();
        })
        .catch(e => this.alertService.httpError(e));

      });

    });

  };
  public cancel(): void {

    this.translateService.get('you_will_cancel_booking')
    .toPromise()
    .then( txt => {

      this.alertService.ask(txt)
      .then( ok => {
        
        if(!ok) return;

        const obj = {
          booking_id : this.booking_id
        };
        this.bookingService.cancelBooking(obj)
        .then(() => {
          this.booking.bookingType = this.sharedService.EbookingType.cancelled;
          this.alertService.done();
        })
        .catch(e => this.alertService.httpError(e));

      });

    });

  };
  public approve(): void {

    this.translateService.get('you_will_approve_booking')
    .toPromise()
    .then( txt => {

      this.alertService.ask(txt)
      .then( ok => {
        
        if(!ok) return;

        const obj = {
          booking_id : this.booking_id
        };
        this.bookingService.approveBooking(obj)
        .then(() => {
          this.booking.bookingType = this.sharedService.EbookingType.waiting_to_confirm;
          this.alertService.done();
        })
        .catch(e => this.alertService.httpError(e));
      
      });

    });

  };

  private setCarGroupTitle(): void {
    const fieldName = (window as any).lang === 'ar' ? 'nameAr': 'nameEn'
    this.carGroupTitle = `
      ${(this.booking.carGroup as any).branch[fieldName]} - 
      ${(this.booking.carGroup as any).carType[fieldName]} - 
      ${(this.booking.carGroup as any).carModel[fieldName]} -
      ${(this.booking.carGroup as any).year}
    `;
    $('title').text(this.carGroupTitle);
  };
 
  public getBookingType(bookingType: number): string {
    return (window as any).lang === 'ar' ? 
      this.sharedService.EbookingTypeAr[bookingType]
    :
    this.sharedService.EbookingTypeEn[bookingType];
  };
 
  private setAllCarsByGroupCanThisEmployeeAccess(allCars: ICarModel[]): void {
    this.carsOptions = allCars.map( car => {
      return {
        label : car.plateNumber,
        value : car._id,
      };
    });
  };

  private getAllCarsByGroupCanThisEmployeeAccess(): void {
    if(this.allCars) {
      this.setAllCarsByGroupCanThisEmployeeAccess(this.allCars);
    } else {
      this.bookingService.getAllCarsWhoCanAddToBooking(this.booking_id)
      .then( res => {
        this.allCars = res.cars;
        this.setAllCarsByGroupCanThisEmployeeAccess(res.cars);
      })
      .catch( e => this.alertService.httpError(e) );
    };
  };

};
 

