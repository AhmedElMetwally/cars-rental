import { ViewBookingComponent } from './view-booking/view-booking.component';
import { Escreens } from '../../enums/screens.enum';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ViewBookingsComponent } from './view-bookings.component';
import { screensGuard } from '../../guards/screens.guard';

const routes: Routes = [
  {
    path : '',
    component : ViewBookingsComponent
  },
  {
    path: ':booking_id',
    component : ViewBookingComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewBookingsRoutingModule { }
