import { SharedService } from '../../service/shared.service';
import { IBranchModel } from '../../models/branchModel';
import { SelectItem } from 'primeng/components/common/selectitem';
import { BookingService } from '../../service/booking.service';
import { IBookingModel } from '../../models/bookingModel';
import { environment } from '../../../environments/environment';
import { AlertService } from '../../service/alert.service';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { EnumValues } from 'enum-values';
declare const $;

@Component({
  selector: 'app-view-bookings',
  templateUrl: './view-bookings.component.html',
})
export class ViewBookingsComponent implements OnInit {

  constructor(
    private bookingService: BookingService,
    private translateService: TranslateService,
    private alertService: AlertService,
    private sharedService: SharedService,
  ){};

  public imageUrl: string =  environment.url + '/images/';

  public bookingsTypesFilterOptions: SelectItem[] = [];

  private allBranchesCanThisEmployeeAccess: IBranchModel[];
  public branchesCanThisEmployeeAccessFilterOptions: SelectItem[] = [];

  // loading
  public loading: boolean = true;
  
  // page & limit
  public limit: number = 30;
  public page: number = 1;

  // stop LazyLoad
  public scrollIsLock: boolean = false;
  
  // my data
  public bookings: IBookingModel[] = []; 

  private translateServiceSubscribe: EventEmitter<any>;

  // title
  private setTitle(): void {
    this.translateService.get('view_bookings')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };

  // show data
  private showBookings(bookings: IBookingModel[]): void {

    // if length < limit (stop new loading)
    this.scrollIsLock = bookings.length < this.limit;

    // add 1 to page
    this.page += 1 ;

    // add new data
    this.bookings.push(...bookings);
    
    // stop loading
    this.loading = false;
  };

  // on lazyload 
  public onLazyLoad($event): void {
    // if lock stop here
    if( ! this.scrollIsLock ) {
      // lock before get data
      this.scrollIsLock = true;

      // show loading before get data
      this.loading = true;

      // filterBy1 -> bookingType
      // filterBy2 -> branch_id
      if($event.searchMode) {
        this.getBookingsSearch($event.searchModel);
      } else if( $event.filterBy1 !== false|| $event.filterBy2 !== false) {
        this.getBookingsByFilter($event.sortBy ,$event.filterBy1 ,$event.filterBy2);
      } else {
        this.getBookings($event.sortBy);
      };
      
    };
  };
  public onReset($event): void {
    this.scrollIsLock = false;
    this.bookings = [];
    this.page = 1;
    this.onLazyLoad($event)
  };

  private getBookings(sortBy: string): void {
    this.bookingService.getBookings(this.page , this.limit , sortBy)
    .then( res => {
      this.showBookings(res.bookings);
    })
    .catch( e => this.alertService.httpError(e) );
  };
  private getBookingsByFilter(sortBy: string , bookingType: string , branch_id: string): void {
    this.bookingService.getBookingsByFilter(this.page , this.limit , sortBy , bookingType , branch_id)
    .then( res => {
      this.showBookings(res.bookings);
    })
    .catch( e => this.alertService.httpError(e) );
  };
  private getBookingsSearch(searchQuery: string): void {
    this.bookingService.getBookingsSearch(this.page, this.limit , searchQuery)
    .then( res => {
      this.showBookings(res.bookings);
    })
    .catch( e => this.alertService.httpError(e) );
  };

  private setBookingsTypes(): void {
    this.bookingsTypesFilterOptions = EnumValues.getNamesAndValues(
      (window as any).lang === 'ar' ? this.sharedService.EbookingTypeAr : this.sharedService.EbookingTypeEn
    )
    .map( obj => {
      return {
        label : obj.name,
        value : obj.value
      }
    });
  };
  private setBranchesFilterOptions(allBranches: IBranchModel[]): void {
    if((window as any).lang === 'ar') {
      this.branchesCanThisEmployeeAccessFilterOptions = allBranches.map( v => {
        return {
          label : v.nameAr,
          value : v._id
        }
      });
    } else {
      this.branchesCanThisEmployeeAccessFilterOptions = allBranches.map( v => {
        return {
          label : v.nameEn,
          value : v._id
        }
      });
    };
  };
  private setOptions(): void {
    this.setBookingsTypes();
    if(this.allBranchesCanThisEmployeeAccess) {
      this.setBranchesFilterOptions(this.allBranchesCanThisEmployeeAccess);
    } else {
      this.bookingService.getAllBranchesCanThisEmployeeAccess()
      .then( res => {
        this.allBranchesCanThisEmployeeAccess = res.branches;
        this.setBranchesFilterOptions(res.branches);
      })
      .catch( e => this.alertService.httpError(e) );
    };
  };
  private onLangChange(): void {
    this.setOptions();
    this.setTitle();
  };
  private init(): void {
    this.setOptions();
    this.setTitle();
  };
  public ngOnInit(): void  {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void  {
    this.translateServiceSubscribe.unsubscribe();
  };

  public getBookingType(bookingType: number): string {
    return (window as any).lang === 'ar' ? 
      this.sharedService.EbookingTypeAr[bookingType]
    :
    this.sharedService.EbookingTypeEn[bookingType];
  }

};
