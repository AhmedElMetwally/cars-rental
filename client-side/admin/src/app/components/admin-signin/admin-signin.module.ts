import { AdminSigninRoutingModule } from './admin-signin-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminSigninComponent } from './admin-signin.component';
import { NgModule } from '@angular/core';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { CommonModule } from '@angular/common';
import { InputErrorInFormModule } from '../common/input-error-in-form/input-error-in-form.module';

@NgModule({
  imports: [
    CommonModule,
    AdminSigninRoutingModule,
    ReactiveFormsModule,
    TranslateModuleForChild,
    InputErrorInFormModule
  ],
  declarations: [
    AdminSigninComponent,
  ]
})
export class AdminSigninModule { }
