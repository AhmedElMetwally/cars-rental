import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../service/shared.service';
import { LoadingService } from '../../service/loading.service';
import { AlertService } from '../../service/alert.service';
import { AuthService } from '../../service/auth.service';
import { Router } from '@angular/router';
import { FormGroup , FormControl } from '@angular/forms';
import { Component, OnInit, OnDestroy, EventEmitter, AfterContentInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { LogService } from '../../service/log.service';
declare const $;

@Component({
  selector: 'admin-signin',
  templateUrl: './admin-signin.component.html',
})
export class AdminSigninComponent implements OnInit , OnDestroy , AfterContentInit {

  constructor( 
    private router: Router, 
    private authService: AuthService,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private sharedService: SharedService,
    private translateService: TranslateService,
    private logService: LogService,

  ) { };

  public signinForm: FormGroup;
  private translateServiceSubscribe: EventEmitter<any>;

  private initJqueryTilt(): void {
    $('.js-tilt img').tilt();
  };

  private initForm(): void {
    this.signinForm = new FormGroup({ 
      email : new FormControl('ahmed@gmail.com' , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.email_required , 
          this.sharedService.mainErrorEnum.email_invalid , 
          this.sharedService.mainRegExp.RegExpAdminModel.email
        ),
      ]),
      password : new FormControl('123456789' , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.password_required , 
          this.sharedService.mainErrorEnum.password_invalid , 
          this.sharedService.mainRegExp.RegExpAdminModel.password 
        ),
      ]),
    });
  };

  public onSubmit(): void {
    if(this.signinForm.invalid) {

      return this.alertService.showFormError( this.signinForm );

    } else {
      
      this.loadingService.show();
    
      let email: string = this.signinForm.controls['email'].value;
      let password: string = this.signinForm.controls['password'].value;

      this.authService.adminSignin(email , password)
      .then( (res: any) => {
        this.sharedService.clearLocalStorage();
        localStorage.setItem('token' , res.token);
        localStorage.setItem('admin' , JSON.stringify(res.admin) );
        this.router.navigate(['/view-employees'])
      })
      .catch( (e: HttpErrorResponse) => this.alertService.httpError(e));

    };
  };

  private log(text): void {
    this.logService.log('admin-signin' , text);
  };

  private setTitle(): void {
    this.translateService.get('admin_signin')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };

  private onLangChange(): void {
    this.setTitle();
  };
  private init(): void {
    this.setTitle();
    this.initForm();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
  };

  public ngAfterContentInit(): void {
    this.initJqueryTilt();
  };


}

