import { BrowserModule } from '@angular/platform-browser';
import { SelectOneModule } from '../common/select-one/select-one.module';
import { InputErrorInFormModule } from '../common/input-error-in-form/input-error-in-form.module';
import { EmployeeService } from '../../service/employee.service';
import { EmployeeOnlineStatisticsModule } from '../common/employee-online-statistics/employee-online-statistics.module';
import { EmployeeActionsModule } from '../common/employee-actions/employee-actions.module';
import { InputTextInProfileModule } from '../common/input-text-in-profile/input-text-in-profile.module';
import { TooltipModule } from 'primeng/tooltip';
import { PanelHeadingInProfileModule } from '../common/panel-heading-in-profile/panel-heading-in-profile.module';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { CustomDataViewModule } from '../common/custom-data-view/custom-data-view.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewEmployeeComponent } from './view-employee/view-employee.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { ViewEmployeesComponent } from './view-employees.component';
import { ViewEmployeesRoutingModule } from './view-employees-routing.module';
import { PipesModule } from '../../modules/PipesModule';
import { ButtonModule } from 'primeng/button';
import { InputPasswordInProfileModule } from '../common/input-password-in-profile/input-password-in-profile.module';
import { InputSelectOneInProfileModule } from '../common/input-select-one-in-profile/input-select-one-in-profile.module';

@NgModule({
  imports: [
    CommonModule,
    ViewEmployeesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModuleForChild,
    PipesModule,
    
    TooltipModule,
    ButtonModule,
    
    CustomDataViewModule,
    PanelHeadingInProfileModule,
    InputTextInProfileModule,
    InputPasswordInProfileModule,
    InputSelectOneInProfileModule,
    EmployeeActionsModule,
    EmployeeOnlineStatisticsModule,

    InputErrorInFormModule,
    SelectOneModule,
    
  ],
  declarations: [
    ViewEmployeesComponent,
    AddEmployeeComponent,
    ViewEmployeeComponent
  ],
  providers : [
    EmployeeService
  ]
})
export class ViewEmployeesModule { }
