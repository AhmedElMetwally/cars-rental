import { IEmployeeModel } from '../../models/employeeModel';
import { AlertService } from '../../service/alert.service';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SelectItem } from 'primeng/components/common/selectitem';
import { IEmployeeGroupModel } from '../../models/employeeGroupModel';
import { EmployeeService } from '../../service/employee.service';
declare const $;

@Component({
  selector: 'app-view-employees',
  templateUrl: './view-employees.component.html',
})
export class ViewEmployeesComponent implements OnInit {

  constructor(
    private employeeService: EmployeeService,
    private translateService: TranslateService,
    private alertService: AlertService,
  ){};


  // filter options
  public employeeGroupsOptions: SelectItem[] = [];
  
  // loading
  public loading: boolean = true;
  
  // page & limit
  public limit: number = 30;
  public page: number = 1;

  // stop LazyLoad
  public scrollIsLock: boolean = false;
  
  // my data
  public employees: IEmployeeModel[] = []; 

  // filter data
  private allEmployeeGroups: IEmployeeGroupModel[];

  private translateServiceSubscribe: EventEmitter<any>;


  // title
  private setTitle(): void {
    this.translateService.get('view_employees')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };

  // show data
  private showEmployees(employees: IEmployeeModel[]): void {
    // if length < limit (stop new loading)
    this.scrollIsLock = employees.length < this.limit;

    // add 1 to page
    this.page += 1 ;

    // add new data
    this.employees.push(...employees);
    
    // stop loading
    this.loading = false;
  };

  // on lazyload 
  public onLazyLoad($event): void {
    // if lock stop here
    if( ! this.scrollIsLock ) {
      // lock before get data
      this.scrollIsLock = true;

      // show loading before get data
      this.loading = true;
      if($event.searchMode) {
        this.getEmployeesSearch($event.searchModel);
      } else if( ! $event.filterBy ) {
        this.getEmployees($event.sortBy);
      } else {
        this.getEmployeesByEmployeeGroupFilter($event.sortBy , $event.filterBy);
      };
    };
  };
  public onReset($event): void {
    this.scrollIsLock = false;
    this.employees = [];
    this.page = 1;
    this.
    onLazyLoad($event)
  };

  private getEmployees(sortBy: string): void {
    this.employeeService.getEmployees(this.page , this.limit , sortBy)
    .then( res => {
      this.showEmployees(res.employees);
    })
    .catch( e => this.alertService.httpError(e) );
  };
  private getEmployeesSearch(searchQuery: string): void {
    this.employeeService.getEmployeesSearch(this.page, this.limit , searchQuery)
    .then( res => {
      this.showEmployees(res.employees);
    })
    .catch( e => this.alertService.httpError(e) );
  };
  private getEmployeesByEmployeeGroupFilter(sortBy: string , filterBy: string): void {
    this.employeeService.getEmployeesByEmployeeGroupFilter(this.page, this.limit , sortBy , filterBy)
    .then( res => {
      this.showEmployees(res.employees);
    })
    .catch( e => this.alertService.httpError(e) );
  };

  private setEmployeeGroupsFilterOptions(allEmployeeGroups: IEmployeeGroupModel[]) {
    if((window as any).lang === 'ar') {
      this.employeeGroupsOptions = allEmployeeGroups.map( v => {
        return {
          value : v._id,
          label : v.nameAr
        }
      });
    } else {
      this.employeeGroupsOptions = allEmployeeGroups.map( v => {
        return {
          value : v._id,
          label : v.nameEn
        }
      });
    };
  }
  private setOptions(): void {
    if(this.allEmployeeGroups) {
      this.setEmployeeGroupsFilterOptions(this.allEmployeeGroups);
    } else{
      this.employeeService.getAllEmployeeGroups()
      .then( res => {
        this.setEmployeeGroupsFilterOptions(res.employeeGroups);
        this.allEmployeeGroups = res.employeeGroups;
      })
      .catch( e => this.alertService.httpError(e) );
    };

  };
  
  private onLangChange(): void {
    this.setOptions();
    this.setTitle();
  };
  private init(): void {
    this.setOptions();
    this.setTitle();
  };
  public ngOnInit(): void  {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void  {
    this.translateServiceSubscribe.unsubscribe();
  };

};

  