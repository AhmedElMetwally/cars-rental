import { ActivatedRoute } from '@angular/router';
import { IEmployeeModel } from '../../../models/employeeModel';
import { EmployeeService } from '../../../service/employee.service';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../service/shared.service';
import { LoadingService } from '../../../service/loading.service';
import { AlertService } from '../../../service/alert.service';
import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { LogService } from '../../../service/log.service';
import { IEmployeeGroupModel } from '../../../models/employeeGroupModel';
import { SelectItem } from 'primeng/components/common/selectitem';
declare const $;

@Component({
  selector: 'app-view-employee',
  templateUrl: './view-employee.component.html',
})
export class ViewEmployeeComponent implements OnInit , OnDestroy {
 
  constructor(
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translateService: TranslateService,
    private employeeService: EmployeeService,
    private logService: LogService,
    private sharedService: SharedService,
    private activatedRoute: ActivatedRoute
  ) { };

  private translateServiceSubscribe: EventEmitter<any>;
  private activatedRouteSubscribe: any;

  public allEmployeeGroups: IEmployeeGroupModel[];
  public employeeGroupsOptions: SelectItem[] = [];

  public employee: IEmployeeModel;
  public edit = {
    name: false, email: false,password: false,employeeGroup_id: false
  };
  public employee_id: string;

  public RegExpEmployeeModel = this.sharedService.mainRegExp.RegExpEmployeeModel; 
  public RegExpEmployeeGroupModel = this.sharedService.mainRegExp.RegExpEmployeeGroupModel; 
  
  
  // validationErrorMsg Enum
  public name_required = this.sharedService.mainErrorEnum.name_required; 
  public name_invalid = this.sharedService.mainErrorEnum.name_invalid; 
  public email_required = this.sharedService.mainErrorEnum.email_required; 
  public email_invalid = this.sharedService.mainErrorEnum.email_invalid; 
  public password_required = this.sharedService.mainErrorEnum.password_required; 
  public password_invalid = this.sharedService.mainErrorEnum.password_invalid; 
  public employee_group_required = this.sharedService.mainErrorEnum.employeeGroup_id_required; 
  public employee_group_invalid = this.sharedService.mainErrorEnum.employeeGroup_id_invalid; 
  

  public openEdit($event): void  {
    this.edit[$event.editNameInEvent] = true;
  };

  public closeEdit($event): void  {
    this.edit[$event.editNameInEvent] = false;
  };


  private setEmployeeGroupsFilterOptions(allEmployeeGroups: IEmployeeGroupModel[]) {
    if((window as any).lang === 'ar') {
      this.employeeGroupsOptions = allEmployeeGroups.map( v => {
        return {
          value : v._id,
          label : v.nameAr
        }
      });
    } else {
      this.employeeGroupsOptions = allEmployeeGroups.map( v => {
        return {
          value : v._id,
          label : v.nameEn
        }
      });
    };
  }
  private setOptions(): void {
    this.employeeGroupsOptions = [];

    if(this.allEmployeeGroups) {
      this.setEmployeeGroupsFilterOptions(this.allEmployeeGroups);
    } else{
      this.employeeService.getAllEmployeeGroups()
      .then( res => {
        this.setEmployeeGroupsFilterOptions(res.employeeGroups);
        this.allEmployeeGroups = res.employeeGroups;
      })
      .catch( e => this.alertService.httpError(e) );
    };

  };


  public onSaveEditPassword($event): void {
    const {fieldName , value} = $event;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['employee_id'] = this.employee_id;

    this.employeeService.editEmployeePassword(obj)
    .then(() => {
      this.loadingService.hide();
      this.employee[fieldName] = value;
      this.edit[fieldName] = false;
      this.employee.updatedAt = new Date();
    })
    .catch( e => this.alertService.httpError(e) );
  };

  public onSaveEdit($event): void {
    const {fieldName , value} = $event;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['employee_id'] = this.employee_id;

    this.employeeService.editEmployee(obj)
    .then(() => {
      this.loadingService.hide();
      this.employee[fieldName] = value;
      this.edit[fieldName] = false;
      this.employee.updatedAt = new Date();
      if(fieldName === 'employeeGroup_id') {
        this.employee.employeeGroup = this.allEmployeeGroups.filter( v => v._id == value)[0];
      };
    })
    .catch( e => this.alertService.httpError(e) );
  };

  public onValidationError($event): void {
    const {validationErrorMsg} = $event;
    this.alertService.alertError(validationErrorMsg);
  };

  private getEmployee(): void {
    this.loadingService.show();
    this.employeeService.getEmployee(this.employee_id)
    .then(({ employee }) => {
      this.employee = employee;
      this.loadingService.hide();
      this.afterGetEmployee();
    })
    .catch( e => this.alertService.httpError(e) );
  };

  private log(text): void {
    this.logService.log('employee-profile' , text);
  };

  private setTitle(): void {
    const title = this.employee.employeeGroup[ (window as any).lang === 'ar' ? 'nameAr' : 'nameEn'];
    $('title').text(title);
  };

  private init(){
    this.setOptions();
    this.getEmployee_id()
  };
  private onLangChange(): void {
    this.setTitle();
    this.setOptions();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
    this.activatedRouteSubscribe.unsubscribe();
  };

  private getEmployee_id(): void {
    this.activatedRouteSubscribe = this.activatedRoute.params.subscribe( params => {
      this.employee_id = params['employee_id'];
      this.afterGetEmployee_id();
    });
  }
  private afterGetEmployee_id(): void {
    this.getEmployee();
  }
  private afterGetEmployee(): void {
    this.setTitle();
  }

  public delete(): void {
    this.employeeService.deleteEmployee(this.employee_id)
    .then(() => {
      this.employee.deleted = 1;
      this.alertService.done();
    })
    .catch(e => this.alertService.httpError(e))
  }
  public restore(): void {
    const obj = {
      employee_id : this.employee_id
    };
    this.employeeService.restoreEmployee(obj)
    .then(() => {
      this.employee.deleted = 0;
      this.alertService.done();
    })
    .catch(e => this.alertService.httpError(e))
  }
  

  
};

 

