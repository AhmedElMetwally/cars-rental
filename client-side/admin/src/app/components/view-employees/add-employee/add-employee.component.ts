import { OnDestroy } from '@angular/core';
import { IEmployeeGroupModel } from '../../../models/employeeGroupModel';
import { FormGroup, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { EmployeeService } from '../../../service/employee.service';
import { AlertService } from '../../../service/alert.service';
import { LoadingService } from '../../../service/loading.service';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/components/common/selectitem';
import { Component, OnInit , EventEmitter} from '@angular/core';
import { SharedService } from '../../../service/shared.service';
declare const $;


@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
})
export class AddEmployeeComponent implements OnInit , OnDestroy {
  
  constructor(
    private loadingService: LoadingService,
    private employeeService: EmployeeService,
    private router: Router,
    private alertService: AlertService,
    private sharedService: SharedService,
    private translateService: TranslateService,
  ) { };

  private translateServiceSubscribe: EventEmitter<any>;
  public formGroup: FormGroup;
  private allEmployeeGroups: IEmployeeGroupModel[];
  public employeeGroupsOptions: SelectItem[] = [];

  private initForm(): void {
    this.formGroup = new FormGroup({ 
      email : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.email_required , 
          this.sharedService.mainErrorEnum.email_invalid , 
          this.sharedService.mainRegExp.RegExpEmployeeModel.email
        ),
      ]),
      password : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.password_required , 
          this.sharedService.mainErrorEnum.password_invalid , 
          this.sharedService.mainRegExp.RegExpEmployeeModel.password 
        ),
      ]),
      name : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.name_required , 
          this.sharedService.mainErrorEnum.name_invalid , 
          this.sharedService.mainRegExp.RegExpEmployeeModel.name 
        ),
      ]),
      employeeGroup_id : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.employeeGroup_id_required , 
          this.sharedService.mainErrorEnum.employeeGroup_id_invalid , 
          this.sharedService.mainRegExp.RegExpEmployeeGroupModel._id 
        ),
      ]),
    });
  };

  public onSubmit(): void {

    if(this.formGroup.invalid) {
      
      this.alertService.showFormError(this.formGroup);

    } else {

      this.loadingService.show();
      const employee = {
        name : this.formGroup.controls['name'].value,
        email : this.formGroup.controls['email'].value,
        password : this.formGroup.controls['password'].value,
        employeeGroup_id : this.formGroup.controls['employeeGroup_id'].value,
      }
      this.employeeService.addEmployee(employee)
      .then( (res: any) => {
        this.router.navigate(['view-employees' , res._id]);
      })
      .catch( e => this.alertService.httpError(e));

    };

  };

  private setTitle(): void {
    this.translateService.get('add_employee')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };

  private setEmployeeGroupsOptions(allEmployeeGroups: IEmployeeGroupModel[]): void {
    if((window as any).lang === 'ar') {
      this.employeeGroupsOptions = allEmployeeGroups.map( v => {
        return {
          label : v.nameAr,
          value: v._id,
        };
      });
    } else {
      this.employeeGroupsOptions = allEmployeeGroups.map( v => {
        return {
          label : v.nameEn,
          value: v._id,
        };
      });
    };
  }

  private setOptions(): void {
    if(this.allEmployeeGroups) { 
      this.setEmployeeGroupsOptions(this.allEmployeeGroups);
    } else {
      this.employeeService.getAllEmployeeGroups()
      .then( res => {
        this.allEmployeeGroups = res.employeeGroups;
        this.setEmployeeGroupsOptions(res.employeeGroups)
      })
      .catch( e => this.alertService.httpError(e));
    };
  }

  private init(){
    this.initForm();
    this.setTitle();
    this.setOptions();
  };
  private onLangChange(): void {
    this.setTitle();
    this.setOptions();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };


  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
  }

 
}