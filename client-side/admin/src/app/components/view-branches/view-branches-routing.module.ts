import { screensGuard } from '../../guards/screens.guard';
import { Escreens } from '../../enums/screens.enum';
import { AddBranchComponent } from './add-branch/add-branch.component';
import { ViewBranchesComponent } from './view-branches.component';
import { ViewBranchComponent } from './view-branch/view-branch.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path : '',
    component : ViewBranchesComponent
  },
  {
    path: 'add-branch',
    component : AddBranchComponent,
    data : {
      screens : [Escreens.add_car]
    },
    canActivate : [screensGuard],
  },
  {
    path: ':branch_id',
    component : ViewBranchComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewBranchesRoutingModule { }
