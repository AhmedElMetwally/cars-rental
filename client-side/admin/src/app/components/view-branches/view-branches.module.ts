import { SelectOneModule } from '../common/select-one/select-one.module';
import { InputErrorInFormModule } from '../common/input-error-in-form/input-error-in-form.module';
import { AddBranchComponent } from './add-branch/add-branch.component';
import { MapModule } from '../common/map/map.module';
import { OverviewEditorModule } from '../common/overview-editor/overview-editor.module';
import { BranchService } from '../../service/branch.service';
import { ViewBranchesComponent } from './view-branches.component';
import { ViewBranchesRoutingModule } from './view-branches-routing.module';
import { InputTextInProfileModule } from '../common/input-text-in-profile/input-text-in-profile.module';
import { TooltipModule } from 'primeng/tooltip';
import { PanelHeadingInProfileModule } from '../common/panel-heading-in-profile/panel-heading-in-profile.module';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { CustomDataViewModule } from '../common/custom-data-view/custom-data-view.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../../modules/PipesModule';
import { ButtonModule } from 'primeng/button';
import { ViewBranchComponent } from './view-branch/view-branch.component';
import { ImageSliderModule } from '../common/image-slider/image-slider.module';
import { InputSelectOneInProfileModule } from '../common/input-select-one-in-profile/input-select-one-in-profile.module';

@NgModule({
  imports: [
    CommonModule,
    ViewBranchesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModuleForChild,
    PipesModule,
    
    TooltipModule,
    ButtonModule,
    
    CustomDataViewModule,
    PanelHeadingInProfileModule,
    InputTextInProfileModule,
    ImageSliderModule,
    OverviewEditorModule,
    MapModule,
    InputErrorInFormModule,
    SelectOneModule,
    InputSelectOneInProfileModule

  ],
  declarations: [
    ViewBranchComponent,
    ViewBranchesComponent,
    AddBranchComponent
  ],
  providers : [
    BranchService
  ]
})
export class ViewBranchesModule { }
