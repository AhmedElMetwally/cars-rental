import { SelectItem } from 'primeng/components/common/selectitem';
import { ICityModel } from '../../../models/cityModel';
import { BranchService } from '../../../service/branch.service';
import { OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AlertService } from '../../../service/alert.service';
import { LoadingService } from '../../../service/loading.service';
import { Router } from '@angular/router';
import { Component, OnInit , EventEmitter} from '@angular/core';
import { SharedService } from '../../../service/shared.service';
declare const $;

@Component({
  selector: 'app-add-branch',
  templateUrl: './add-branch.component.html',
})
export class AddBranchComponent implements OnInit , OnDestroy {
  
  constructor(
    private loadingService: LoadingService,
    private branchService: BranchService,
    private router: Router,
    private alertService: AlertService,
    private sharedService: SharedService,
    private translateService: TranslateService,
  ) { };

  private translateServiceSubscribe: EventEmitter<any>;
  public formGroup: FormGroup;

  private allCities: ICityModel[];
  public citiesOptions: SelectItem[] = [];


  private initForm(): void {
    this.formGroup = new FormGroup({ 
      nameEn : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.nameEn_required , 
          this.sharedService.mainErrorEnum.nameEn_invalid , 
          this.sharedService.mainRegExp.RegExpBranchModel.nameEn
        ),
      ]),
      nameAr : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.nameAr_required , 
          this.sharedService.mainErrorEnum.nameAr_invalid , 
          this.sharedService.mainRegExp.RegExpBranchModel.nameAr 
        ),
      ]),
      addressAr : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.addressAr_required , 
          this.sharedService.mainErrorEnum.addressAr_invalid , 
          this.sharedService.mainRegExp.RegExpBranchModel.addressAr 
        ),
      ]),
      addressEn : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.addressEn_required , 
          this.sharedService.mainErrorEnum.addressEn_invalid , 
          this.sharedService.mainRegExp.RegExpBranchModel.addressEn 
        ),
      ]),

      phone : new FormControl(null , 
      [ 
          this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.phone_required , 
          this.sharedService.mainErrorEnum.phone_invalid , 
          this.sharedService.mainRegExp.RegExpBranchModel.phone 
        ),
      ]),
      bookingStartCode : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.bookingStartCode_required , 
          this.sharedService.mainErrorEnum.bookingStartCode_invalid , 
          this.sharedService.mainRegExp.RegExpBranchModel.bookingStartCode 
        ),
      ]),
      waitingHoursForConfirm : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.waitingHoursForConfirm_required , 
          this.sharedService.mainErrorEnum.waitingHoursForConfirm_invalid , 
          this.sharedService.mainRegExp.RegExpBranchModel.waitingHoursForConfirm 
        ),
      ]),
      city_id : new FormControl(null , 
        [ 
          this.sharedService.customValidator(
            this.sharedService.mainErrorEnum.city_id_required , 
            this.sharedService.mainErrorEnum.city_id_invalid , 
            this.sharedService.mainRegExp.RegExpCityModel._id 
          ),
        ]),
    });
  };

  public onSubmit(): void {

    if(this.formGroup.invalid) {
      
      this.alertService.showFormError(this.formGroup);

    } else {

      this.loadingService.show();
      const branch = {
        nameEn : this.formGroup.controls['nameEn'].value,
        nameAr : this.formGroup.controls['nameAr'].value,
        addressAr : this.formGroup.controls['addressAr'].value,
        addressEn : this.formGroup.controls['addressEn'].value,
        phone : this.formGroup.controls['phone'].value,
        bookingStartCode : this.formGroup.controls['bookingStartCode'].value,
        waitingHoursForConfirm : this.formGroup.controls['waitingHoursForConfirm'].value,
        city_id : this.formGroup.controls['city_id'].value,

        overviewEn : '',
        overviewAr : ''
      }
      this.branchService.addBranch(branch)
      .then( () => {
        this.router.navigate(['view-branches']);
        this.alertService.done();
      })
      .catch( e => this.alertService.httpError(e));

    };

  };

  private setTitle(): void {
    this.translateService.get('add_branch')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };

  private setCarTypesOptions(allCities: ICityModel[]): void {
    if((window as any).lang === 'ar') {
     
      this.citiesOptions = allCities.map( v => {
        return {
          value : v._id,
          label : v.nameAr
        }
      });

    } else {

      this.citiesOptions = allCities.map( v => {
        return {
          value : v._id,
          label : v.nameEn
        }
      });

    };
  };

  public setOptions(): void {
    if(this.allCities) {
      this.setCarTypesOptions(this.allCities);
    } else{
      this.branchService.getAllCities()
      .then( res => {
        this.allCities = res.cities;
        this.setCarTypesOptions(res.cities);
      })
      .catch( e => this.alertService.httpError(e) );
    };

  };

  private init(){
    this.initForm();
    this.setTitle();
    this.setOptions();
  };
  private onLangChange(): void {
    this.setTitle();
    this.setOptions();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
  };
 
};