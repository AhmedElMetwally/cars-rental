import { SelectItem } from 'primeng/components/common/selectitem';
import { ICityModel, RegExpCityModel } from '../../../models/cityModel';
import { AuthService } from '../../../service/auth.service';
import { BranchService } from '../../../service/branch.service';
import { IBranchModel } from '../../../models/branchModel';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../service/shared.service';
import { LoadingService } from '../../../service/loading.service';
import { AlertService } from '../../../service/alert.service';
import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { LogService } from '../../../service/log.service';
import { environment } from '../../../../environments/environment';
declare const $;


@Component({
  selector: 'app-view-branch',
  templateUrl: './view-branch.component.html',
})
export class ViewBranchComponent implements OnInit , OnDestroy {
 
  constructor(
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translateService: TranslateService,
    private branchService: BranchService,
    private logService: LogService,
    private authService: AuthService,
    private sharedService: SharedService,
    private activatedRoute: ActivatedRoute
  ) { };

  private translateServiceSubscribe: EventEmitter<any>;
  private activatedRouteSubscribe: any;

  private allCities: ICityModel[];
  public citiesOptions: SelectItem[] = [];

  public branch: IBranchModel;
  public edit = {
    nameEn: false, nameAr: false,addressEn: false,addressAr: false,
    phone: false , bookingStartCode: false , waitingHoursForConfirm: false , 
    city_id: false
  };
  public branch_id: string;

  public RegExpBranchModel = this.sharedService.mainRegExp.RegExpBranchModel; 
  public RegExpCityModel = this.sharedService.mainRegExp.RegExpCityModel; 
  
  
  // validationErrorMsg Enum
  public nameEn_required = this.sharedService.mainErrorEnum.nameEn_required; 
  public nameEn_invalid = this.sharedService.mainErrorEnum.nameEn_invalid; 

  public nameAr_required = this.sharedService.mainErrorEnum.nameAr_required; 
  public nameAr_invalid = this.sharedService.mainErrorEnum.nameAr_invalid; 
 
  public addressEn_required = this.sharedService.mainErrorEnum.addressEn_required; 
  public addressEn_invalid = this.sharedService.mainErrorEnum.addressEn_invalid; 

  public addressAr_required = this.sharedService.mainErrorEnum.addressAr_required; 
  public addressAr_invalid = this.sharedService.mainErrorEnum.addressAr_invalid; 

  public phone_required = this.sharedService.mainErrorEnum.phone_required; 
  public phone_invalid = this.sharedService.mainErrorEnum.phone_invalid; 

  public bookingStartCode_required = this.sharedService.mainErrorEnum.bookingStartCode_required; 
  public bookingStartCode_invalid = this.sharedService.mainErrorEnum.bookingStartCode_invalid; 

  public waitingHoursForConfirm_required = this.sharedService.mainErrorEnum.waitingHoursForConfirm_required; 
  public waitingHoursForConfirm_invalid = this.sharedService.mainErrorEnum.waitingHoursForConfirm_invalid; 

  public city_id_required = this.sharedService.mainErrorEnum.city_id_required; 
  public city_id_invalid = this.sharedService.mainErrorEnum.city_id_invalid; 

  public canEditBranch: boolean;
  public canEditBranchImage: boolean;
  public canDeleteBranch: boolean;
  public canRestoreBranch: boolean;

  private checkScreens(): void {
    const isHaveAccessToThisBranch = this.authService.checkBranch(this.branch_id);
    this.canEditBranch = isHaveAccessToThisBranch &&  this.authService.checkScreen(this.sharedService.Escreens.edit_branch);
    this.canEditBranchImage = isHaveAccessToThisBranch && this.authService.checkScreen(this.sharedService.Escreens.edit_branch_image);
    this.canDeleteBranch = isHaveAccessToThisBranch && this.authService.checkScreen(this.sharedService.Escreens.delete_branch);
    this.canRestoreBranch = isHaveAccessToThisBranch && this.authService.checkScreen(this.sharedService.Escreens.restore_branch);
  };

  public openEdit($event): void  {
    this.edit[$event.editNameInEvent] = true;
  };

  public closeEdit($event): void  {
    this.edit[$event.editNameInEvent] = false;
  };

  private setCarTypesOptions(allCities: ICityModel[]): void {
    if((window as any).lang === 'ar') {
     
      this.citiesOptions = allCities.map( v => {
        return {
          value : v._id,
          label : v.nameAr
        }
      });

    } else {

      this.citiesOptions = allCities.map( v => {
        return {
          value : v._id,
          label : v.nameEn
        }
      });

    };
  };

  public setOptions(): void {
    if(this.allCities) {
      this.setCarTypesOptions(this.allCities);
    } else{
      this.branchService.getAllCities()
      .then( res => {
        this.allCities = res.cities;
        this.setCarTypesOptions(res.cities);
      })
      .catch( e => this.alertService.httpError(e) );
    };

  };

  public onSaveEdit($event): void {
    const {fieldName , value} = $event;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['branch_id'] = this.branch_id;

    this.branchService.editBranch(obj)
    .then(() => {

      this.loadingService.hide(); 
      
      this.edit[fieldName] = false;
      this.branch.updatedAt = new Date();
      this.branch[fieldName] = value;

      if(fieldName === 'nameEn' || fieldName === 'nameAr') {
        this.setTitle();
      };

    })
    .catch( e => this.alertService.httpError(e) );
  };

  public onEditCity($event): void {
    const {fieldName , value} = $event;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['branch_id'] = this.branch_id;

    this.branchService.editBranch(obj)
    .then(() => {
      this.loadingService.hide(); 
      this.edit[fieldName] = false;
      this.branch.updatedAt = new Date();
      this.branch.city = this.allCities.filter( v => v._id === value)[0];
    })
    .catch( e => this.alertService.httpError(e) );
  };

  public onValidationError($event): void {
    const {validationErrorMsg} = $event;
    this.alertService.alertError(validationErrorMsg);
  };

  private getBranch(): void {
    this.loadingService.show();
    this.branchService.getBranch(this.branch_id)
    .then(({ branch }) => {
      branch.images = branch.images.map( image => environment.url + '/images/' + image);
      this.branch = branch;
      this.loadingService.hide();
      this.afterGetBranch();
    })
    .catch( e => this.alertService.httpError(e) );
  };

  private log(text): void {
    this.logService.log('branch' , text);
  };

  private setTitle(): void {
    const title = this.branch[ (window as any).lang === 'ar' ? 'nameAr' : 'nameEn'];
    $('title').text(title);
  };

  private init(){
    this.setOptions();
    this.getBranch_id();
  };
  private onLangChange(): void {
    this.setTitle();
    this.setOptions();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
    this.activatedRouteSubscribe.unsubscribe();
  };

  private getBranch_id(): void {
    this.activatedRouteSubscribe = this.activatedRoute.params.subscribe( params => {
      this.branch_id = params['branch_id'];
      this.afterGetBranch_id();
    });
  };
  private afterGetBranch_id(): void {
    this.getBranch();
    this.checkScreens();
  };
  private afterGetBranch(): void {
    this.setTitle();
  };

  public delete(): void {
    this.branchService.deleteBranch(this.branch_id)
    .then(() => {
      this.branch.deleted = 1;
      this.alertService.done();
    })
    .catch(e => this.alertService.httpError(e))
  };
  public restore(): void {
    const obj = {
      branch_id : this.branch_id
    };
    this.branchService.restoreBranch(obj)
    .then(() => {
      this.branch.deleted = 0;
      this.alertService.done();
    })
    .catch(e => this.alertService.httpError(e))
  };

  public onSaveOverViewEn($event): void {
    const { html } = $event;
    this.onSaveEdit({
      fieldName : 'overviewEn', 
      value : html
    });
  };
  
  public onSaveOverViewAr($event): void {
    const { html } = $event;
    this.onSaveEdit({
      fieldName : 'overviewAr', 
      value : html
    });
  };
  
  public onDeleteImage($event): void {
    this.loadingService.show();
    const image = $event.image.replace(environment.url + '/images/' , '');
    this.branchService.deleteBranchImage(this.branch_id , image)
    .then( () => {
      this.loadingService.hide();
      this.branch.images = this.branch.images.filter( v => v !== $event.image);
      this.alertService.done();
    })
    .catch( e => this.alertService.httpError(e))
  };

  public onAddImage($event): void {
    this.loadingService.show();
    const formData: FormData = new FormData();
    formData.append('image', $event.files[0]);
    formData.append('branch_id', this.branch_id);
    this.branchService.addBranchImage(formData)
    .then( res => {
      this.loadingService.hide();
      const image = environment.url + '/images/' + res.image ;
      this.branch.images.push(image);
    })
    .catch( e => this.alertService.httpError(e))
  };

  public onSaveLocation($event): void {
    const { location } = $event;
    this.onSaveEdit({
      fieldName : 'location', 
      value : location
    });
  };


  

};
 

