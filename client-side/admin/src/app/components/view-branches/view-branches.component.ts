import { environment } from '../../../environments/environment';
import { SharedService } from '../../service/shared.service';
import { AuthService } from '../../service/auth.service';
import { IBranchModel } from '../../models/branchModel';
import { BranchService } from '../../service/branch.service';
import { AlertService } from '../../service/alert.service';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
declare const $;

@Component({
  selector: 'app-view-branches',
  templateUrl: './view-branches.component.html',
})
export class ViewBranchesComponent implements OnInit {

  constructor(
    private branchService: BranchService,
    private translateService: TranslateService,
    private alertService: AlertService,
    private authService: AuthService,
    private sharedService: SharedService,
  ){};


  public imageUrl: string =  environment.url + '/images/';

  // loading
  public loading: boolean = true;
  
  // page & limit
  public limit: number = 30;
  public page: number = 1;

  // stop LazyLoad
  public scrollIsLock: boolean = false;
  
  // my data
  public branches: IBranchModel[] = []; 

  private translateServiceSubscribe: EventEmitter<any>;

  public canAddBranch: boolean;

  private setScreens(): void {
    this.canAddBranch = this.authService.checkScreen(this.sharedService.Escreens.add_branch);
  }

  // title
  private setTitle(): void {
    this.translateService.get('view_branches')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };

  // show data
  private showBranches(branches: IBranchModel[]): void {

    // if length < limit (stop new loading)
    this.scrollIsLock = branches.length < this.limit;

    // add 1 to page
    this.page += 1 ;

    // add new data
    this.branches.push(...branches);
    
    // stop loading
    this.loading = false;
  };

  // on lazyload 
  public onLazyLoad($event): void {
    // if lock stop here
    if( ! this.scrollIsLock ) {
      // lock before get data
      this.scrollIsLock = true;

      // show loading before get data
      this.loading = true;
      if($event.searchMode) {
        this.getBranchesSearch($event.searchModel)
      } else if(!$event.filterBy || $event.filterBy === 'all') {
        this.getBranches($event.sortBy);
      };
    };
  };
  public onReset($event): void {
    this.scrollIsLock = false;
    this.branches = [];
    this.page = 1;
    this.
    onLazyLoad($event)
  };

  private getBranches(sortBy: string): void {
    this.branchService.getBranches(this.page , this.limit , sortBy)
    .then( res => {
      this.showBranches(res.branches);
    })
    .catch( e => this.alertService.httpError(e) );
  };
  private getBranchesSearch(searchQuery: string): void {
    this.branchService.getBranchesSearch(this.page, this.limit , searchQuery)
    .then( res => {
      this.showBranches(res.branches);
    })
    .catch( e => this.alertService.httpError(e) );
  };

  private setOptions(): void {
  
  };
  private onLangChange(): void {
    this.setOptions();
    this.setTitle();
  };
  private init(): void {
    this.setOptions();
    this.setTitle();
    this.setScreens();
  };
  public ngOnInit(): void  {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void  {
    this.translateServiceSubscribe.unsubscribe();
  };

};

  