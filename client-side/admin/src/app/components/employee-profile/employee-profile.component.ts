import { IEmployeeModel } from '../../models/employeeModel';
import { EmployeeService } from '../../service/employee.service';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../service/shared.service';
import { LoadingService } from '../../service/loading.service';
import { AlertService } from '../../service/alert.service';
import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { LogService } from '../../service/log.service';
declare const $;


@Component({
  selector: 'employee-profile',
  templateUrl: './employee-profile.component.html',
  styleUrls: ['./employee-profile.component.scss']
})
export class EmployeeProfileComponent implements OnInit , OnDestroy {

  constructor(
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translateService: TranslateService,
    private employeeService: EmployeeService,
    private logService: LogService,
    private sharedService: SharedService
  ) { };

  private translateServiceSubscribe: EventEmitter<any>;


  public employee: IEmployeeModel;
  public edit = {
    name: false, email: false,password: false
  };

  public RegExpEmployeeModel = this.sharedService.mainRegExp.RegExpEmployeeModel; 
  
  // validationErrorMsg Enum
  public name_required = this.sharedService.mainErrorEnum.name_required; 
  public name_invalid = this.sharedService.mainErrorEnum.name_invalid; 
  public email_required = this.sharedService.mainErrorEnum.email_required; 
  public email_invalid = this.sharedService.mainErrorEnum.email_invalid; 
  public password_required = this.sharedService.mainErrorEnum.password_required; 
  public password_invalid = this.sharedService.mainErrorEnum.password_invalid; 
  
  
  public openEdit($event): void  {
    this.edit[$event.editNameInEvent] = true;
  };

  public closeEdit($event): void  {
    this.edit[$event.editNameInEvent] = false;
  };

  public setOptions(): void {
  };

  public onSaveEditPassword($event): void {
    const {fieldName , value} = $event;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;

    this.employeeService.editThisEmployeePassword(obj)
    .then(() => {
      this.loadingService.hide();
      this.employee[fieldName] = value;
      this.edit[fieldName] = false;
      this.employee.updatedAt = new Date();
    })
    .catch( e => this.alertService.httpError(e) );
  };

  public onSaveEdit($event): void {
    const {fieldName , value} = $event;

    this.edit[fieldName] = true;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;

    this.employeeService.editThisEmployee(obj)
    .then(() => {
      this.loadingService.hide();
      this.employee[fieldName] = value;
      this.edit[fieldName] = false;
      this.employee.updatedAt = new Date();
    })
    .catch( e => this.alertService.httpError(e) );
  };

  public onValidationError($event): void {
    const {validationErrorMsg} = $event;
    this.alertService.alertError(validationErrorMsg);
  };

  private getThisEmployee(): void {
    this.loadingService.show();
    this.employeeService.getThisEmployee()
    .then(({ employee }) => {
      this.employee = employee;
      this.loadingService.hide();
      this.afterGetThisEmployee();
    })
    .catch( e => this.alertService.httpError(e) );
  };

  private afterGetThisEmployee(): void {
  }
  
  private log(text): void {
    this.logService.log('employee-profile' , text);
  };

  private setTitle(): void {
    this.translateService.get('employee_profile')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };

  private init(){
    this.setTitle();
    this.setOptions();
    this.getThisEmployee();
  };
  private onLangChange(): void {
    this.setTitle();
    this.setOptions();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
  };


};

