import { InputSelectOneInProfileModule } from '../common/input-select-one-in-profile/input-select-one-in-profile.module';
import { InputPasswordInProfileModule } from '../common/input-password-in-profile/input-password-in-profile.module';
import { PanelHeadingInProfileModule } from '../common/panel-heading-in-profile/panel-heading-in-profile.module';
import { PipesModule } from '../../modules/PipesModule';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { EmployeeProfileRoutingModule } from './employee-profile-routing.module';
import { EmployeeProfileComponent } from './employee-profile.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeService } from '../../service/employee.service';
import { InputTextInProfileModule } from '../common/input-text-in-profile/input-text-in-profile.module';
import { ScrollTopBtnModule } from '../common/scroll-top-btn/scroll-top-btn.module';
import { EmployeeOnlineStatisticsModule } from '../common/employee-online-statistics/employee-online-statistics.module';
import { EmployeeActionsModule } from '../common/employee-actions/employee-actions.module';
 


@NgModule({
  imports: [ 
    CommonModule,
    EmployeeProfileRoutingModule,
    TranslateModuleForChild,

    PanelHeadingInProfileModule,
    InputTextInProfileModule,
    InputPasswordInProfileModule,
    EmployeeOnlineStatisticsModule,
    EmployeeActionsModule,

    PipesModule,

    InputSelectOneInProfileModule
  ],
  declarations: [
    EmployeeProfileComponent,
  ],
  providers : [
    EmployeeService,
  ]
})
export class EmployeeProfileModule { }
