import { CarGroupService } from '../../../service/car-group.service';
import { IBranchModel } from '../../../models/branchModel';
import { ICarModelModel } from '../../../models/carModelModel';
import { ICarTypeModel } from '../../../models/carTypeModel';
import { OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AlertService } from '../../../service/alert.service';
import { LoadingService } from '../../../service/loading.service';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/components/common/selectitem';
import { Component, OnInit , EventEmitter} from '@angular/core';
import { SharedService } from '../../../service/shared.service';
declare const $;


@Component({
  selector: 'app-add-car-group',
  templateUrl: './add-car-group.component.html',
})
export class AddCarGroupComponent implements OnInit , OnDestroy {
  
  constructor(
    private loadingService: LoadingService,
    private carGroupService: CarGroupService,
    private router: Router,
    private alertService: AlertService,
    private sharedService: SharedService,
    private translateService: TranslateService,
  ) { };

  private translateServiceSubscribe: EventEmitter<any>;
  public formGroup: FormGroup;
  
  private allCarTypes: ICarTypeModel[];
  public carTypesOptions: SelectItem[] = [];

  private allCarModels: ICarModelModel[];
  public carModelsOptions: SelectItem[] = [];
   
  private allBranchesCanThisEmployeeAccess: IBranchModel[];
  public branchesCanThisEmployeeAccessOptions: SelectItem[] = [];

  public yearsOptions: SelectItem[] = [];
  
  private initForm(): void {
    this.formGroup = new FormGroup({ 
      carType_id : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.carType_id_required , 
          this.sharedService.mainErrorEnum.carType_id_invalid , 
          this.sharedService.mainRegExp.RegExpCarTypeModel._id
        ),
      ]),
      carModel_id : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.carModel_id_required , 
          this.sharedService.mainErrorEnum.carModel_id_invalid , 
          this.sharedService.mainRegExp.RegExpCarModelModel._id
        ),
      ]),
      branch_id : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.branch_id_required , 
          this.sharedService.mainErrorEnum.branch_id_invalid , 
          this.sharedService.mainRegExp.RegExpBranchModel._id
        ),
      ]),
      year : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.year_required , 
          this.sharedService.mainErrorEnum.year_invalid , 
          this.sharedService.mainRegExp.RegExpCarGroupModel.year
        ),
      ]),

      allowedKm : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.allowedKm_required , 
          this.sharedService.mainErrorEnum.allowedKm_invalid , 
          this.sharedService.mainRegExp.RegExpCarGroupModel.allowedKm 
        ),
      ]),
      plusKmPrice : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.plusKmPrice_required , 
          this.sharedService.mainErrorEnum.plusKmPrice_invalid , 
          this.sharedService.mainRegExp.RegExpCarGroupModel.plusKmPrice 
        ),
      ]),
      pricePerDay : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.pricePerDay_required , 
          this.sharedService.mainErrorEnum.pricePerDay_invalid , 
          this.sharedService.mainRegExp.RegExpCarGroupModel.pricePerDay 
        ),
      ]),
      discountPerDay : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.discountPerDay_required , 
          this.sharedService.mainErrorEnum.discountPerDay_invalid , 
          this.sharedService.mainRegExp.RegExpCarGroupModel.discountPerDay 
        ),
      ]),
    });
  };

  public onSubmit(): void {

    if(this.formGroup.invalid) {
      
      this.alertService.showFormError(this.formGroup);

    } else {

      this.loadingService.show();
      const carGroup = {
        carType_id : this.formGroup.controls['carType_id'].value,
        carModel_id : this.formGroup.controls['carModel_id'].value,
        branch_id : this.formGroup.controls['branch_id'].value,
        year : this.formGroup.controls['year'].value,

        allowedKm : this.formGroup.controls['allowedKm'].value,
        plusKmPrice : this.formGroup.controls['plusKmPrice'].value,
        pricePerDay : this.formGroup.controls['pricePerDay'].value,
        discountPerDay : this.formGroup.controls['discountPerDay'].value,
      }
      this.carGroupService.addCarGroup(carGroup)
      .then( (res: any) => {
        this.router.navigate(['view-car-groups' , res._id]);
      })
      .catch( e => this.alertService.httpError(e));

    };

  };

  private setTitle(): void {
    this.translateService.get('add_car_group')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };

  private setYearsOptions(): void {
    if((window as any).lang === 'ar') {
      const yearsOptions = []
      for(let i = new Date().getFullYear() , len = 1960; i >= len ;--i ) {
        yearsOptions.push({
          label : this.sharedService.numberEnToAr(i.toString()),
          value : i.toString(),
        });
      };
      this.yearsOptions = yearsOptions;

    } else {
      const yearsOptions = []
      for(let i = new Date().getFullYear() , len = 1960; i >= len ;--i ) {
        yearsOptions.push({
          label : i.toString(),
          value : i.toString()
        });
      };
      this.yearsOptions = yearsOptions;
    };
  };

  private setBranchesOptions(allBranches: IBranchModel[]): void {
    if((window as any).lang === 'ar') {
     
      this.branchesCanThisEmployeeAccessOptions = allBranches.map( v => {
        return {
          value : v._id,
          label : v.nameAr
        }
      });

    } else {

      this.branchesCanThisEmployeeAccessOptions = allBranches.map( v => {
        return {
          value : v._id,
          label : v.nameEn
        }
      });

    };
  };

  private setCarTypesOptions(allCarTypes: ICarTypeModel[]): void {
    if((window as any).lang === 'ar') {
     
      this.carTypesOptions = allCarTypes.map( v => {
        return {
          value : v._id,
          label : v.nameAr
        }
      });

    } else {

      this.carTypesOptions = allCarTypes.map( v => {
        return {
          value : v._id,
          label : v.nameEn
        }
      });

    };
  };

  private getCarModelsOptions(carType_id: string): void {
    this.carGroupService.getAllCarModelsByCarType(carType_id)
    .then( res => {
      this.allCarModels = res.carModels;
      this.setCarModelsOptions(res.carModels);
    })
    .catch( e => this.alertService.httpError(e) );
  };

  public onChangeCarType($event): void {
    this.getCarModelsOptions($event.value);
  };

  private setCarModelsOptions(allCarModels: ICarModelModel[]): void {
    if((window as any).lang === 'ar') {
     
      this.carModelsOptions = allCarModels.map( v => {
        return {
          value : v._id,
          label : v.nameAr
        }
      });

    } else {

      this.carModelsOptions = allCarModels.map( v => {
        return {
          value : v._id,
          label : v.nameEn
        }
      });

    };
  };

  public setOptions(): void {
  
    if(this.allCarModels) {
      this.setCarModelsOptions(this.allCarModels);
    };

    if(this.allBranchesCanThisEmployeeAccess) {
      this.setBranchesOptions(this.allBranchesCanThisEmployeeAccess);
    } else{
      this.carGroupService.getAllBranchesCanThisEmployeeAccess()
      .then( res => {
        this.allBranchesCanThisEmployeeAccess = res.branches;
        this.setBranchesOptions(res.branches);
      })
      .catch( e => this.alertService.httpError(e) );
    };

    if(this.allCarTypes) {
      this.setCarTypesOptions(this.allCarTypes);
    } else{
      this.carGroupService.getAllCarTypes()
      .then( res => {
        this.allCarTypes = res.carTypes;
        this.setCarTypesOptions(res.carTypes);
      })
      .catch( e => this.alertService.httpError(e) );
    };

    this.setYearsOptions();

  };

  private init(){
    this.initForm();
    this.setTitle();
    this.setOptions();
  };
  private onLangChange(): void {
    this.setTitle();
    this.setOptions();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
  }

 
}