import { environment } from '../../../../environments/environment';
import { AuthService } from '../../../service/auth.service';
import { ICarModelModel } from '../../../models/carModelModel';
import { ICarTypeModel } from '../../../models/carTypeModel';
import { ICarGroupModel } from '../../../models/carGroupModel';
import { CarGroupService } from '../../../service/car-group.service';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../service/shared.service';
import { LoadingService } from '../../../service/loading.service';
import { AlertService } from '../../../service/alert.service';
import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { LogService } from '../../../service/log.service';
import { SelectItem } from 'primeng/components/common/selectitem';
import { IBranchModel } from '../../../models/branchModel';
declare const $;


@Component({
  selector: 'app-view-car-group',
  templateUrl: './view-car-group.component.html',
})
export class ViewCarGroupComponent implements OnInit , OnDestroy {
 
  constructor(
    private authService: AuthService,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translateService: TranslateService,
    private carGroupService: CarGroupService,
    private logService: LogService,
    private sharedService: SharedService,
    private activatedRoute: ActivatedRoute
  ) { };

  private translateServiceSubscribe: EventEmitter<any>;
  private activatedRouteSubscribe: any;

  private allCarTypes: ICarTypeModel[];
  public carTypesOptions: SelectItem[] = [];

  private allCarModels: ICarModelModel[];
  public carModelsOptions: SelectItem[] = [];
   
  private allBranchesCanThisEmployeeAccess: IBranchModel[];
  public branchesCanThisEmployeeAccessOptions: SelectItem[] = [];
  public branches_ids: string[];

  public yearsOptions: SelectItem[] = [];

  public carGroup: ICarGroupModel;
  public edit = {
    carType_id: false, carModel_id: false, branch_id: false, cars: false,
    year: false, allowedKm: false, plusKmPrice: false, pricePerDay: false,
    discountPerDay: false, views: false,
  };
  public carGroup_id: string;

  public RegExpCarGroupModel = this.sharedService.mainRegExp.RegExpCarGroupModel; 
  public RegExpBranchModel = this.sharedService.mainRegExp.RegExpBranchModel; 
  public RegExpCarModelModel = this.sharedService.mainRegExp.RegExpCarModelModel; 
  public RegExpCarTypeModel = this.sharedService.mainRegExp.RegExpCarTypeModel; 
  
  public carGroupTitle: string;

  // validationErrorMsg Enum
  public carType_id_required = this.sharedService.mainErrorEnum.carType_id_required; 
  public carType_id_invalid = this.sharedService.mainErrorEnum.carType_id_invalid; 
  
  public carModel_id_required = this.sharedService.mainErrorEnum.carModel_id_required; 
  public carModel_id_invalid = this.sharedService.mainErrorEnum.carModel_id_invalid; 
  
  public branch_id_required = this.sharedService.mainErrorEnum.branch_id_required; 
  public branch_id_invalid = this.sharedService.mainErrorEnum.branch_id_invalid; 
  
  public year_required = this.sharedService.mainErrorEnum.year_required; 
  public year_invalid = this.sharedService.mainErrorEnum.year_invalid; 
  
  public allowedKm_required = this.sharedService.mainErrorEnum.allowedKm_required; 
  public allowedKm_invalid = this.sharedService.mainErrorEnum.allowedKm_invalid; 
  
  public plusKmPrice_required = this.sharedService.mainErrorEnum.plusKmPrice_required; 
  public plusKmPrice_invalid = this.sharedService.mainErrorEnum.plusKmPrice_invalid; 

  public pricePerDay_required = this.sharedService.mainErrorEnum.pricePerDay_required; 
  public pricePerDay_invalid = this.sharedService.mainErrorEnum.pricePerDay_invalid; 
  
  public discountPerDay_required = this.sharedService.mainErrorEnum.discountPerDay_required; 
  public discountPerDay_invalid = this.sharedService.mainErrorEnum.discountPerDay_invalid; 
  
  public views_required = this.sharedService.mainErrorEnum.views_required; 
  public views_invalid = this.sharedService.mainErrorEnum.views_invalid; 
  // validationErrorMsg Enum
  
  // screens
  public canEditCarGroup: boolean;
  public canEditCarGroupImage: boolean;
  public canDeleteCarGroup: boolean;
  public canRestoreCarGroup: boolean;


  private checkScreens(): void {
    const isHaveAccessToThisBranch = this.authService.checkBranch((this.carGroup.branch as any)._id);
    this.canEditCarGroup = isHaveAccessToThisBranch &&  this.authService.checkScreen(this.sharedService.Escreens.edit_car_group);
    this.canEditCarGroupImage = isHaveAccessToThisBranch && this.authService.checkScreen(this.sharedService.Escreens.edit_car_group_image);
    this.canDeleteCarGroup = isHaveAccessToThisBranch && this.authService.checkScreen(this.sharedService.Escreens.delete_car_group);
    this.canRestoreCarGroup = isHaveAccessToThisBranch && this.authService.checkScreen(this.sharedService.Escreens.restore_car_group);
  };
  public openEdit($event): void  {
    this.edit[$event.editNameInEvent] = true;
  };
  public closeEdit($event): void  {
    this.edit[$event.editNameInEvent] = false;
  };

  private setYearsOptions(): void {
    if((window as any).lang === 'ar') {
      const yearsOptions = []
      for(let i = new Date().getFullYear() , len = 1960; i >= len ;--i ) {
        yearsOptions.push({
          label : this.sharedService.numberEnToAr(i.toString()),
          value : i.toString(),
        });
      };
      this.yearsOptions = yearsOptions;

    } else {
      const yearsOptions = []
      for(let i = new Date().getFullYear() , len = 1960; i >= len ;--i ) {
        yearsOptions.push({
          label : i.toString(),
          value : i.toString()
        });
      };
      this.yearsOptions = yearsOptions;
    };
  };

  private setBranchesOptions(allBranches: IBranchModel[]): void {
    if((window as any).lang === 'ar') {
     
      this.branchesCanThisEmployeeAccessOptions = allBranches.map( v => {
        return {
          value : v._id,
          label : v.nameAr
        }
      });

    } else {

      this.branchesCanThisEmployeeAccessOptions = allBranches.map( v => {
        return {
          value : v._id,
          label : v.nameEn
        }
      });

    };
  };

  private setCarTypesOptions(allCarTypes: ICarTypeModel[]): void {
    if((window as any).lang === 'ar') {
     
      this.carTypesOptions = allCarTypes.map( v => {
        return {
          value : v._id,
          label : v.nameAr
        }
      });

    } else {

      this.carTypesOptions = allCarTypes.map( v => {
        return {
          value : v._id,
          label : v.nameEn
        }
      });

    };
  };

  private getCarModelsOptions(carType_id: string): void {
    this.carGroupService.getAllCarModelsByCarType(carType_id)
    .then( res => {
      this.allCarModels = res.carModels;
      this.setCarModelsOptions(res.carModels);
    })
    .catch( e => this.alertService.httpError(e) );
  };

  private setCarModelsOptions(allCarModels: ICarModelModel[]): void {
    if((window as any).lang === 'ar') {
     
      this.carModelsOptions = allCarModels.map( v => {
        return {
          value : v._id,
          label : v.nameAr
        }
      });

    } else {

      this.carModelsOptions = allCarModels.map( v => {
        return {
          value : v._id,
          label : v.nameEn
        }
      });

    };
  };

  public setOptions(): void {

    if(this.allCarModels) {
      this.setCarModelsOptions(this.allCarModels);
    };

    if(this.allBranchesCanThisEmployeeAccess) {
      this.branches_ids = this.allBranchesCanThisEmployeeAccess.map( v => v._id);
      this.setBranchesOptions(this.allBranchesCanThisEmployeeAccess);
    } else{
      this.carGroupService.getAllBranchesCanThisEmployeeAccess()
      .then( res => {
        this.allBranchesCanThisEmployeeAccess = res.branches;
        this.branches_ids = res.branches.map( v => v._id);
        this.setBranchesOptions(res.branches);
      })
      .catch( e => this.alertService.httpError(e) );
    };

    if(this.allCarTypes) {
      this.setCarTypesOptions(this.allCarTypes);
    } else{
      this.carGroupService.getAllCarTypes()
      .then( res => {
        this.allCarTypes = res.carTypes;
        this.setCarTypesOptions(res.carTypes);
      })
      .catch( e => this.alertService.httpError(e) );
    };

    this.setYearsOptions();

  };

  public onSaveEdit($event): void {
    const {fieldName , value} = $event;


    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['carGroup_id'] = this.carGroup_id;

    this.carGroupService.editCarGroup(obj)
    .then(() => {
      this.loadingService.hide(); 
      this.edit[fieldName] = false;
      this.carGroup.updatedAt = new Date();

      if(fieldName === 'carType_id') {
        this.getCarModelsOptions(value);
        this.edit.carModel_id = true;
        this.carGroup['carType'] = this.allCarTypes.filter( v => v._id === value)[0];
      } else if(fieldName === 'carModel_id') {
        this.carGroup['carModel'] = this.allCarModels.filter( v => v._id === value)[0];
      } else if(fieldName === 'branch_id') {
        this.carGroup['branch'] = this.allBranchesCanThisEmployeeAccess.filter( v => v._id === value)[0];
      } else {
        this.carGroup[fieldName] = value;
      };

      this.setCarGroupTitle()

    })
    .catch( e => this.alertService.httpError(e) );
  };

  public onValidationError($event): void {
    const {validationErrorMsg} = $event;
    this.alertService.alertError(validationErrorMsg);
  };

  private getCarGroup(): void {
    this.loadingService.show();
    this.carGroupService.getCarGroup(this.carGroup_id)
    .then(({ carGroup }) => {
      carGroup.images = carGroup.images.map( image => environment.url + '/images/' + image);
      this.carGroup = carGroup;
      this.loadingService.hide();
      this.afterGetCarGroup();
    })
    .catch( e => this.alertService.httpError(e) );
  };

  private log(text): void {
    this.logService.log('employee-profile' , text);
  };

  private setCarGroupTitle(): void {
    const fieldName = (window as any).lang === 'ar' ? 'nameAr': 'nameEn'
    this.carGroupTitle = `
      ${(this.carGroup as any).branch[fieldName]} - 
      ${(this.carGroup as any).carType[fieldName]} - 
      ${(this.carGroup as any).carModel[fieldName]} -
      ${(this.carGroup as any).year}
    `;
    $('title').text(this.carGroupTitle);
  }

  private init(){
    this.setOptions();
    this.getCarGroup_id();
  };
  private onLangChange(): void {
    this.setCarGroupTitle();
    this.setOptions();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
    this.activatedRouteSubscribe.unsubscribe();
  };

  private getCarGroup_id(): void {
    this.activatedRouteSubscribe = this.activatedRoute.params.subscribe( params => {
      this.carGroup_id = params['carGroup_id'];
      this.afterGetCarGroup_id();
    });
  };
  private afterGetCarGroup_id(): void {
    this.getCarGroup();
  };
  private afterGetCarGroup(): void {
    this.setCarGroupTitle();
    this.checkScreens();
    this.getCarModelsOptions((this.carGroup.carType as any)._id);
  };

  public delete(): void {
    this.carGroupService.deleteCarGroup(this.carGroup_id)
    .then(() => {
      this.carGroup.deleted = 1;
      this.alertService.done();
    })
    .catch(e => this.alertService.httpError(e))
  };
  public restore(): void {
    const obj = {
      carGroup_id : this.carGroup_id
    };
    this.carGroupService.restoreCarGroup(obj)
    .then(() => {
      this.carGroup.deleted = 0;
      this.alertService.done();
    })
    .catch(e => this.alertService.httpError(e))
  };
  
  public onDeleteImage($event): void {
    this.loadingService.show();
    const image = $event.image.replace(environment.url + '/images/' , '');
    this.carGroupService.deleteCarGroupImage(this.carGroup_id , image)
    .then( () => {
      this.loadingService.hide();
      this.carGroup.images = this.carGroup.images.filter( v => v !== $event.image);
      this.alertService.done();
    })
    .catch( e => this.alertService.httpError(e))
  };

  public onAddImage($event): void {
    this.loadingService.show();
    const formData: FormData = new FormData();
    formData.append('image', $event.files[0]);
    formData.append('carGroup_id', this.carGroup_id);
    this.carGroupService.addCarGroupImage(formData)
    .then( res => {
      this.loadingService.hide();
      const image = environment.url + '/images/' + res.image ;
      this.carGroup.images.push(image);
    })
    .catch( e => this.alertService.httpError(e))
  };


};
 

