import { AddCarGroupComponent } from './add-car-group/add-car-group.component';
import { Escreens } from '../../enums/screens.enum';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ViewCarGroupsComponent } from './view-car-groups.component';
import { ViewCarGroupComponent } from './view-car-group/view-car-group.component';
import { screensGuard } from '../../guards/screens.guard';

const routes: Routes = [
  {
    path : '',
    component : ViewCarGroupsComponent
  },
  {
    path: 'add-car-group',
    component : AddCarGroupComponent,
    data : {
      screens : [Escreens.add_car_group]
    },
    canActivate : [screensGuard],
  },
  {
    path: ':carGroup_id',
    component : ViewCarGroupComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewCarGroupsRoutingModule { }
