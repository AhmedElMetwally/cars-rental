import { environment } from '../../../environments/environment';
import { SharedService } from '../../service/shared.service';
import { AuthService } from '../../service/auth.service';
import { SelectItem } from 'primeng/components/common/selectitem';
import { CarGroupService } from '../../service/car-group.service';
import { ICarTypeModel } from '../../models/carTypeModel';
import { IBranchModel } from '../../models/branchModel';
import { ICarGroupModel } from '../../models/carGroupModel';
import { AlertService } from '../../service/alert.service';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
declare const $;

@Component({
  selector: 'app-view-car-groups',
  templateUrl: './view-car-groups.component.html',
})
export class ViewCarGroupsComponent implements OnInit {

  constructor(
    private carGroupService: CarGroupService,
    private translateService: TranslateService,
    private alertService: AlertService,
    private authService: AuthService,
    private sharedService: SharedService,
  ){};

  public imageUrl: string =  environment.url + '/images/';

  // loading
  public loading: boolean = true;
  
  // page & limit
  public limit: number = 30;
  public page: number = 1;

  // stop LazyLoad
  public scrollIsLock: boolean = false;
  
  // my data
  public carGroups: ICarGroupModel[] = []; 

  private translateServiceSubscribe: EventEmitter<any>;

  private allBranchesCanThisEmployeeAccess: IBranchModel[];
  private allCarTypes: ICarTypeModel[];

  public branchesCanThisEmployeeAccessFilterOptions: SelectItem[] = [];
  public carTypesFilterOptions: SelectItem[] = [];


  public canAddCarGroup: boolean;
  private checkScreens(): void {
    this.canAddCarGroup = this.authService.checkScreen(this.sharedService.Escreens.add_car_group);
  }

  // title
  private setTitle(): void {
    this.translateService.get('view_car_groups')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };

  // show data
  private showCarGroups(carGroups: ICarGroupModel[]): void {

    // if length < limit (stop new loading)
    this.scrollIsLock = carGroups.length < this.limit;

    // add 1 to page
    this.page += 1 ;

    // add new data
    this.carGroups.push(...carGroups);
    
    // stop loading
    this.loading = false;
  };

  // on lazyload 
  public onLazyLoad($event): void {
    // if lock stop here
    if( ! this.scrollIsLock ) {
      // lock before get data
      this.scrollIsLock = true;

      // show loading before get data
      this.loading = true;

      // filterBy1 -> branch_id
      // filterBy2 -> carType_id
      if($event.filterBy1 || $event.filterBy2) {
        $event.filterBy1 = $event.filterBy1 === 'all' ? false : $event.filterBy1;
        $event.filterBy2 = $event.filterBy2 === 'all' ? false : $event.filterBy2;
        this.getCarGroupsByFilter($event.sortBy , $event.filterBy1 , $event.filterBy2)
      } else {
        this.getCarGroups($event.sortBy);
      };
      
    };
  };
  public onReset($event): void {
    this.scrollIsLock = false;
    this.carGroups = [];
    this.page = 1;
    this.
    onLazyLoad($event)
  };

  private getCarGroups(sortBy: string): void {
    this.carGroupService.getCarGroups(this.page , this.limit , sortBy)
    .then( res => {
      this.showCarGroups(res.carGroups);
    })
    .catch( e => this.alertService.httpError(e) );
  };
  private getCarGroupsByFilter(sortBy: string , branch_id: string , carType_id: string): void {
    this.carGroupService.getCarGroupsByFilter(this.page, this.limit , sortBy ,branch_id , carType_id)
    .then( res => {
      this.showCarGroups(res.carGroups);
    })
    .catch( e => this.alertService.httpError(e) );
  };


  private setBranchesFilterOptions(allBranches: IBranchModel[]): void {
    if((window as any).lang === 'ar') {
      this.branchesCanThisEmployeeAccessFilterOptions = allBranches.map( v => {
        return {
          label : v.nameAr,
          value : v._id
        }
      });
    } else {
      this.branchesCanThisEmployeeAccessFilterOptions = allBranches.map( v => {
        return {
          label : v.nameEn,
          value : v._id
        }
      });
    }
  }

  private setCarTypesFilterOptions(allCarTypes: ICarTypeModel[]): void {
    if((window as any).lang === 'ar') {
      this.carTypesFilterOptions = allCarTypes.map( v => {
        return {
          label : v.nameAr,
          value : v._id
        }
      });
    } else {
      this.carTypesFilterOptions = allCarTypes.map( v => {
        return {
          label : v.nameEn,
          value : v._id
        }
      });
    }
  }

  private setOptions(): void {
    if(this.allBranchesCanThisEmployeeAccess) {
      this.setBranchesFilterOptions(this.allBranchesCanThisEmployeeAccess);
    } else {
      this.carGroupService.getAllBranchesCanThisEmployeeAccess()
      .then( res => {
        this.allBranchesCanThisEmployeeAccess = res.branches;
        this.setBranchesFilterOptions(res.branches);
      })
      .catch( e => this.alertService.httpError(e) );
    };

    if(this.allCarTypes) {
      this.setCarTypesFilterOptions(this.allCarTypes);
    } else {
      this.carGroupService.getAllCarTypes()
      .then( res => {
        this.allCarTypes = res.carTypes;
        this.setCarTypesFilterOptions(res.carTypes);
      })
      .catch( e => this.alertService.httpError(e) );
    };

  };
  private onLangChange(): void {
    this.setOptions();
    this.setTitle();
  };
  private init(): void {
    this.setOptions();
    this.setTitle();
    this.checkScreens();
  };
  public ngOnInit(): void  {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void  {
    this.translateServiceSubscribe.unsubscribe();
  };

};

  