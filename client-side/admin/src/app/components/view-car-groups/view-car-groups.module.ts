import { SelectOneModule } from '../common/select-one/select-one.module';
import { InputErrorInFormModule } from '../common/input-error-in-form/input-error-in-form.module';
import { CarGroupService } from '../../service/car-group.service';
import { InputTextInProfileModule } from '../common/input-text-in-profile/input-text-in-profile.module';
import { TooltipModule } from 'primeng/tooltip';
import { PanelHeadingInProfileModule } from '../common/panel-heading-in-profile/panel-heading-in-profile.module';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../../modules/PipesModule';
import { ButtonModule } from 'primeng/button';
import { ViewCarGroupsComponent } from './view-car-groups.component';
import { ViewCarGroupComponent } from './view-car-group/view-car-group.component';
import { ViewCarGroupsRoutingModule } from './view-car-groups-routing.module';
import { CustomDataView2Module } from '../common/custom-data-view2/custom-data-view2.module';
import { InputSelectOneInProfileModule } from '../common/input-select-one-in-profile/input-select-one-in-profile.module';
import { ImageSliderModule } from '../common/image-slider/image-slider.module';
import { AddCarGroupComponent } from './add-car-group/add-car-group.component';

@NgModule({
  imports: [
    CommonModule,
    ViewCarGroupsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModuleForChild,
    PipesModule,
    
    TooltipModule,
    ButtonModule,
    
    CustomDataView2Module,
    PanelHeadingInProfileModule,
    InputTextInProfileModule,
    InputSelectOneInProfileModule,
    ImageSliderModule,
    InputErrorInFormModule,
    SelectOneModule

  ],
  declarations: [
    ViewCarGroupsComponent,
    ViewCarGroupComponent,
    AddCarGroupComponent
  ],
  providers : [
    CarGroupService
  ]
})
export class ViewCarGroupsModule { }
