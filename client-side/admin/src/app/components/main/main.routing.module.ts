import { Error404Component } from '../error404/error404.component';
import { isNotAdminGuard , isAdminGuard} from '../../guards/admin.guard';
import { isNotEmployeeGuard , isEmployeeGuard } from '../../guards/employee.guard';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        RouterModule.forRoot([
            {
                path : '',
                pathMatch: 'full',
                redirectTo : 'employee-signin',
            },
            {
                path : 'employee-signin',
                canActivate : [isNotEmployeeGuard , isNotAdminGuard ],
                loadChildren: './../employee-signin/employee-signin.module#EmployeeSigninModule' ,
            },
            {
                path : 'admin-signin',
                canActivate : [isNotAdminGuard , isNotEmployeeGuard],
                loadChildren: './../admin-signin/admin-signin.module#AdminSigninModule' ,
            },
            {
                path : 'employee-profile',
                canActivate : [isEmployeeGuard],
                loadChildren: './../employee-profile/employee-profile.module#EmployeeProfileModule' ,
            },  
            {
                path : 'view-employees',
                canActivate : [isAdminGuard],
                loadChildren: './../view-employees/view-employees.module#ViewEmployeesModule' ,
            },
            {
                path : 'view-employee-groups',
                canActivate : [isAdminGuard],
                loadChildren: './../view-employee-groups/view-employee-groups.module#ViewEmployeeGroupsModule' ,
            },

            {
                path : 'view-car-groups',
                canActivate : [isEmployeeGuard],
                loadChildren: './../view-car-groups/view-car-groups.module#ViewCarGroupsModule' ,
            },
            {
                path : 'view-cars',
                canActivate : [isEmployeeGuard],
                loadChildren: './../view-cars/view-cars.module#ViewCarsModule' ,
            },
            {
                path : 'view-branches',
                canActivate : [isEmployeeGuard],
                loadChildren: './../view-branches/view-branches.module#ViewBranchesModule' ,
            },

            {
                path : 'view-clients',
                canActivate : [isEmployeeGuard],
                loadChildren: './../view-clients/view-clients.module#ViewClientsModule' ,
            },

            {
                path : 'view-bookings',
                canActivate : [isEmployeeGuard],
                loadChildren: './../view-bookings/view-bookings.module#ViewBookingsModule' ,
            },
            {
                path : 'car-types',
                canActivate : [isEmployeeGuard],
                loadChildren: './../car-types/car-types.module#CarTypesModule' ,
            }, 
            {
                path : 'view-guests',
                canActivate : [isEmployeeGuard],
                loadChildren: './../view-guests/view-guests.module#ViewGuestsModule' ,
            }, 
            {
                path : '**',
                component : Error404Component
            },
        ])
    ],
    exports: [ RouterModule ]
})
export class MainRoutingModule {}
