import { ScrollTopBtnModule } from '../common/scroll-top-btn/scroll-top-btn.module';
import { Error404Component } from '../error404/error404.component';
import { LoadingComponent } from '../loading/loading.component';
import { MainComponent } from './main.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MainRoutingModule} from './main.routing.module';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { TranslateModuleForRoot } from '../../modules/TranslateModule';


@NgModule({

  imports: [
    BrowserModule,
    MainRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModuleForRoot,
    ScrollTopBtnModule
  ],

  declarations: [
    MainComponent,
    LoadingComponent,
    Error404Component,
  ],

  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
  ],

  bootstrap: [
    MainComponent
  ]

})
export class MainModule { }
