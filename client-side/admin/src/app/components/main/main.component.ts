import { SharedService } from './../../service/shared.service';
import { AlertService } from './../../service/alert.service';
import { LoadingService } from '../../service/loading.service';
import { AuthService } from '../../service/auth.service';
import { Router, Event, NavigationStart, NavigationCancel, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { Component , OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
declare const $;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(
    private translateService: TranslateService,
    private location: Location,
    private router: Router,
    private authService: AuthService,
    private sharedService: SharedService,
    private alertService: AlertService,
    private loadingService: LoadingService
  ) {
    this.initRouterEvents();
    this.initLang();
  };

  public style: 'rtl'|'ltr';

  private initRouterEvents(): void {
    this.router.events.subscribe( (event: Event) => {
    
      if( event instanceof NavigationStart ) {
        this.loadingService.show();
      };

      if( event instanceof NavigationCancel ) {
        this.loadingService.hide();
      };

      if( event instanceof NavigationEnd ) {
        this.loadingService.hide();
      };
      
    });
  };

  private initLang(): void {
    // default
    this.translateService.setDefaultLang('en');
    (window as any).lang = 'en';

    if(localStorage.getItem('lang') === 'ar'){
      this.changeLang('ar');
    } else {
      this.changeLang('en');
    };
  };

  public isEmployee(): boolean {
    return this.authService.isEmployee();
  };

  public isAdmin(): boolean {
    return this.authService.isAdmin();
  };

  public logout(): void {
    this.authService.logout();
  };

  public back(): void {
    this.location.back();
  };

  public changeLang(lang: 'en'|'ar'): void {
    (window as any).lang = lang;
    localStorage.setItem('lang' , lang);
    
    this.style = lang == 'en' ? 'ltr' : 'rtl';
    const antherStyle: string = lang === 'en' ? 'rtl' : 'ltr';

    this.translateService.use(lang);
    
    $(`#adminlte-${this.style}`).prop('disabled', false);
    $(`#adminlte-${antherStyle}`).prop('disabled', true);

    $('body').addClass(`style-${this.style}`).removeClass(`style-${antherStyle}`);

  };

  private refreshAdminToken(): void {
    this.authService.refreshAdminToken()
    .then( res => {
      
      this.sharedService.clearLocalStorage();

      localStorage.setItem('token' , res.token);
      localStorage.setItem('admin' , JSON.stringify(res.admin) );
    })
    .catch( e => {
      this.authService.logout();
      this.alertService.httpError(e)
    });
  };

  private refreshEmployeeToken(): void {
    this.authService.refreshEmployeeToken()
    .then( res => {
      
      this.authService.employeeSigninInSocketIO(res.token);

      this.sharedService.clearLocalStorage();
      localStorage.setItem('token' , res.token);
      localStorage.setItem('employee' , JSON.stringify(res.employee) );
    })
    .catch( e => {
      this.authService.logout();
      this.alertService.httpError(e);
    });
  };

  public ngOnInit(): any {
    const isTokenHere = localStorage.getItem('token') !== null;
    if(isTokenHere && this.authService.isAdmin()) return this.refreshAdminToken();
    if(isTokenHere && this.authService.isEmployee()) return this.refreshEmployeeToken();
  };

};
