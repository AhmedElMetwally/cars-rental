import { SharedService } from '../../service/shared.service';
import { LogService } from '../../service/log.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../service/auth.service';
import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
declare const $;

@Component({
  selector: 'error404',
  templateUrl: './error404.component.html',
  styleUrls: ['./error404.component.scss']
})
export class Error404Component implements OnInit , OnDestroy {


  constructor(
    private router: Router,
    private authService: AuthService,
    private translateService: TranslateService,
    private logService: LogService,
    private sharedService: SharedService,
  ) {};

  private translateServiceSubscribe: EventEmitter<any>;
  public number404: string;

  public return(): void {
    if( this.authService.isAdmin() ) {
      this.router.navigate(['view-employees']);
      return 
    } else if ( this.authService.isEmployee() ) {
      this.router.navigate(['employee-profile']);
      return 
    } else {
      this.router.navigate(['employee-signin']);
    };
  }

  private log(text): void {
    this.logService.log('error404' , text);
  };

  private setTitle(): void {
    this.translateService.get('error404_title')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };

  private onLangChange(): void {
    this.setTitle();
    this.number404 = (window as any).lang == 'ar' ? this.sharedService.numberEnToAr('404') : '404';
  };

  private init(): void {
    this.setTitle();
    this.number404 = (window as any).lang == 'ar' ? this.sharedService.numberEnToAr('404') : '404';
  };

  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange()
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
  };

};

