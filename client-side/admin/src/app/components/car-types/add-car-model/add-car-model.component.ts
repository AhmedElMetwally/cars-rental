import { ICarTypeModel } from '../../../models/carTypeModel';
import { CarTypeService } from '../../../service/car-type.service';
import { OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AlertService } from '../../../service/alert.service';
import { LoadingService } from '../../../service/loading.service';
import { SelectItem } from 'primeng/components/common/selectitem';
import { Component, OnInit , EventEmitter} from '@angular/core';
import { SharedService } from '../../../service/shared.service';
declare const $;


@Component({
  selector: 'app-add-car-model',
  templateUrl: './add-car-model.component.html',
})
export class AddCarModelComponent implements OnInit , OnDestroy {
  
  constructor(
    private loadingService: LoadingService,
    private carTypeService: CarTypeService,
    private alertService: AlertService,
    private sharedService: SharedService,
    private translateService: TranslateService,
  ) { };

  private translateServiceSubscribe: EventEmitter<any>;
  public formGroup: FormGroup;

  private allCarTypes: ICarTypeModel[];
  public carTypesOptions: SelectItem[] = [];

  private initForm(): void {
    this.formGroup = new FormGroup({ 
      carType_id : new FormControl(null , 
        [ 
          this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.carType_id_required , 
          this.sharedService.mainErrorEnum.carType_id_invalid , 
          this.sharedService.mainRegExp.RegExpCarTypeModel._id
        ),
      ]),
      nameEn : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.nameEn_required , 
          this.sharedService.mainErrorEnum.nameEn_invalid , 
          this.sharedService.mainRegExp.RegExpCarModelModel.nameEn
        ),
      ]),
      nameAr : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.nameAr_required , 
          this.sharedService.mainErrorEnum.nameAr_invalid , 
          this.sharedService.mainRegExp.RegExpCarModelModel.nameAr
        ),
      ]),
    });
  };

  public onSubmit(): void {

    if(this.formGroup.invalid) {
      
      this.alertService.showFormError(this.formGroup);

    } else {

      this.loadingService.show();
      const carModel = {
        carType_id : this.formGroup.controls['carType_id'].value,
        nameEn : this.formGroup.controls['nameEn'].value,
        nameAr : this.formGroup.controls['nameAr'].value,
      }
      this.carTypeService.addCarModel(carModel)
      .then( () => {
        this.loadingService.hide();
        this.formGroup.reset();
      })
      .catch( e => this.alertService.httpError(e));

    };

  };

  private setTitle(): void {
    this.translateService.get('add_car_model')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };

  private setCarTypesOptions(allCarTypes: ICarTypeModel[]): void {
    if((window as any).lang === 'ar') {
     
      this.carTypesOptions = allCarTypes.map( v => {
        return {
          value : v._id,
          label : v.nameAr
        }
      });

    } else {

      this.carTypesOptions = allCarTypes.map( v => {
        return {
          value : v._id,
          label : v.nameEn
        }
      });

    };
  };

  public setOptions(): void {
    if(this.allCarTypes) {
      this.setCarTypesOptions(this.allCarTypes);
    } else{
      this.carTypeService.getAllCarTypes()
      .then( res => {
        this.allCarTypes = res.carTypes;
        this.setCarTypesOptions(res.carTypes);
      })
      .catch( e => this.alertService.httpError(e) );
    };

  };

  private init(){
    this.initForm();
    this.setTitle();
    this.setOptions();
  };
  private onLangChange(): void {
    this.setTitle();
    this.setOptions();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
  };

};