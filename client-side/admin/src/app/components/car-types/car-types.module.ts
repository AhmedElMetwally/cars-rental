import { AddCarModelComponent } from './add-car-model/add-car-model.component';
import { CarTypeService } from '../../service/car-type.service';
import { AddCarTypeComponent } from './add-car-type/add-car-type.component';
import { SelectOneModule } from '../common/select-one/select-one.module';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../../modules/PipesModule';
import { InputErrorInFormModule } from '../common/input-error-in-form/input-error-in-form.module';
import { CarTypesRoutingModule } from './car-types-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CarTypesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModuleForChild,
    PipesModule,
    InputErrorInFormModule,
    SelectOneModule
  ],
  declarations: [
    AddCarTypeComponent,
    AddCarModelComponent,
  ],
  providers : [
    CarTypeService
  ]
})
export class CarTypesModule { }
