import { AddCarModelComponent } from './add-car-model/add-car-model.component';
import { AddCarTypeComponent } from './add-car-type/add-car-type.component';
import { Escreens } from '../../enums/screens.enum';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { screensGuard } from '../../guards/screens.guard';

const routes: Routes = [
  {
    path : 'add-car-type',
    component : AddCarTypeComponent,
    data : {
      screens : [Escreens.add_car_type]
    },
    canActivate : [screensGuard],
  },
  {
    path : 'add-car-model',
    component : AddCarModelComponent,
    data : {
      screens : [Escreens.add_car_model]
    },
    canActivate : [screensGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarTypesRoutingModule { }
