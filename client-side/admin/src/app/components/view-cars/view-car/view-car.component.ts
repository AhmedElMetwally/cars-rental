import { ICarModel } from '../../../models/carModel';
import { CarService } from '../../../service/car.service';
import { AuthService } from '../../../service/auth.service';
import { ICarGroupModel } from '../../../models/carGroupModel';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../service/shared.service';
import { LoadingService } from '../../../service/loading.service';
import { AlertService } from '../../../service/alert.service';
import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { LogService } from '../../../service/log.service';
import { SelectItem } from 'primeng/components/common/selectitem';
import { EnumValues } from 'enum-values';
declare const $;


@Component({
  selector: 'app-view-car',
  templateUrl: './view-car.component.html',
  styleUrls : ['view-car.component.scss']
})
export class ViewCarComponent implements OnInit , OnDestroy {
 
  constructor(
    private authService: AuthService,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translateService: TranslateService,
    private carService: CarService,
    private logService: LogService,
    private sharedService: SharedService,
    private activatedRoute: ActivatedRoute
  ) { };

  private translateServiceSubscribe: EventEmitter<any>;
  private activatedRouteSubscribe: any;

  private allCarGroupsCanThisEmployeeAccess: ICarGroupModel[];
  public carGroupsCanThisEmployeeAccessOptions: SelectItem[] = [];

  public gearTypesOptions: SelectItem[] = [];
  public statusOptions: SelectItem[] = [];

    
  public carGroupTitle: string;

  public car: ICarModel;
  public edit = {
    carGroup_id: false, plateNumber: false, gearType: false, status: false,
  };
  public car_id: string;

  public RegExpCarGroupModel = this.sharedService.mainRegExp.RegExpCarGroupModel; 
  public RegExpCarModel = this.sharedService.mainRegExp.RegExpCarModel; 
  
  
  // validationErrorMsg Enum
  public carGroup_id_required = this.sharedService.mainErrorEnum.carGroup_id_required; 
  public carGroup_id_invalid = this.sharedService.mainErrorEnum.carGroup_id_invalid; 
  
  public plateNumber_required = this.sharedService.mainErrorEnum.plateNumber_required; 
  public plateNumber_invalid = this.sharedService.mainErrorEnum.plateNumber_invalid; 
  
  public gearType_required = this.sharedService.mainErrorEnum.gearType_required; 
  public gearType_invalid = this.sharedService.mainErrorEnum.gearType_invalid; 
  
  public status_required = this.sharedService.mainErrorEnum.status_required; 
  public status_invalid = this.sharedService.mainErrorEnum.status_invalid; 
  // validationErrorMsg Enum
  
  // screens
  public canEditCar: boolean;
  public canDeleteCar: boolean;
  public canRestoreCar: boolean;


  private checkScreens(): void {
    const isHaveAccessToThisBranch = this.authService.checkBranch((this.car.carGroup as any).branch._id);
    this.canEditCar = isHaveAccessToThisBranch &&  this.authService.checkScreen(this.sharedService.Escreens.edit_car);
    this.canDeleteCar = isHaveAccessToThisBranch && this.authService.checkScreen(this.sharedService.Escreens.delete_car);
    this.canRestoreCar = isHaveAccessToThisBranch && this.authService.checkScreen(this.sharedService.Escreens.restore_car);
  };
  public openEdit($event): void  {
    this.edit[$event.editNameInEvent] = true;
  };
  public closeEdit($event): void  {
    this.edit[$event.editNameInEvent] = false;
  };

  private setCarGroupTitle(): void {
    const fieldName = (window as any).lang === 'ar' ? 'nameAr': 'nameEn'
    this.carGroupTitle = `
      ${(this.car.carGroup as any).branch[fieldName]} - 
      ${(this.car.carGroup as any).carType[fieldName]} - 
      ${(this.car.carGroup as any).carModel[fieldName]} -
      ${(this.car.carGroup as any).year}
    `;
    $('title').text(this.carGroupTitle);
  }

  private setStatusOptions(): void {
    this.statusOptions = EnumValues.getNamesAndValues(
      (window as any).lang === 'ar' ? this.sharedService.EstatusAr : this.sharedService.EstatusEn
    )
    .map( obj => {
      return {
        label : obj.name,
        value : obj.value
      }
    });
  };
  private setGearTypeOptions(): void {
    this.gearTypesOptions = EnumValues.getNamesAndValues(
      (window as any).lang === 'ar' ? this.sharedService.EgearTypeAr : this.sharedService.EgearTypeEn
    )
    .map( obj => {
      return {
        label : obj.name,
        value : obj.value
      }
    });
  };
  private setCarGroupsOptions(allCarGroups: ICarGroupModel[]): void {
    if((window as any).lang === 'ar') {
     
      this.carGroupsCanThisEmployeeAccessOptions = allCarGroups.map( carGroup => {
        const name = `${carGroup.branch.nameAr} - ${carGroup.carType.nameAr} <br> ${carGroup.carModel.nameAr} - ${carGroup.year}`
        return {
          label : name,
          value : carGroup._id,
        }
      });

    } else {
      this.carGroupsCanThisEmployeeAccessOptions = allCarGroups.map( carGroup => {
        const name = `${carGroup.branch.nameEn} - ${carGroup.carType.nameEn} <br> ${carGroup.carModel.nameEn} - ${carGroup.year}`
        return {
          label : name,
          value : carGroup._id,
        }
      });

    };
  };

  public setOptions(): void {
    this.setGearTypeOptions();
    this.setStatusOptions();
    
    if(this.allCarGroupsCanThisEmployeeAccess) {
      this.setCarGroupsOptions(this.allCarGroupsCanThisEmployeeAccess);
    } else{
      this.carService.getAllCarGroupsCanThisEmployeeAccess()
      .then( res => {
        this.allCarGroupsCanThisEmployeeAccess = res.carGroups;
        this.setCarGroupsOptions(res.carGroups);
      })
      .catch( e => this.alertService.httpError(e) );
    };

  };

  public onSaveEdit($event): void {
    const {fieldName , value} = $event;

    this.loadingService.show();

    const obj = {};
    obj[fieldName] = value;
    obj['car_id'] = this.car_id;

    this.carService.editCar(obj)
    .then(() => {
      this.loadingService.hide(); 
      this.edit[fieldName] = false;
      this.car.updatedAt = new Date();

      if(fieldName === 'carGroup_id') {
        this.car.carGroup  = this.allCarGroupsCanThisEmployeeAccess.filter( v => v._id === value)[0];
        this.setCarGroupTitle();
      } else {
        this.car[fieldName] = value;
      };

    })
    .catch( e => this.alertService.httpError(e) );
  };

  public onValidationError($event): void {
    const {validationErrorMsg} = $event;
    this.alertService.alertError(validationErrorMsg);
  };

  private getCar(): void {
    this.loadingService.show();
    this.carService.getCar(this.car_id)
    .then(({ car }) => {
      this.car = car;
      this.loadingService.hide();
      this.afterGetCar();
    })
    .catch( e => this.alertService.httpError(e) );
  };

  private log(text): void {
    this.logService.log('employee-profile' , text);
  };


  private init(){
    this.setOptions();
    this.getCar_id();
  };
  private onLangChange(): void {
    this.setOptions();
    this.setCarGroupTitle();

  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
    this.activatedRouteSubscribe.unsubscribe();
  };

  private getCar_id(): void {
    this.activatedRouteSubscribe = this.activatedRoute.params.subscribe( params => {
      this.car_id = params['car_id'];
      this.afterGetCar_id();
    });
  };
  private afterGetCar_id(): void {
    this.getCar();
  };
  private afterGetCar(): void {
    this.checkScreens();
    this.setCarGroupTitle();
  };

  public delete(): void {
    this.carService.deleteCar(this.car_id)
    .then(() => {
      this.car.deleted = 1;
      this.alertService.done();
    })
    .catch(e => this.alertService.httpError(e))
  };
  public restore(): void {
    const obj = {
      car_id : this.car_id
    };
    this.carService.restoreCar(obj)
    .then(() => {
      this.car.deleted = 0;
      this.alertService.done();
    })
    .catch(e => this.alertService.httpError(e))
  };

  public getStatus(num): string {
    return (window as any).lang === 'ar' ? this.sharedService.EstatusAr[num] : this.sharedService.EstatusEn[num];
  }

  public getGearType(num): string {
    return (window as any).lang === 'ar' ? this.sharedService.EgearTypeAr[num] : this.sharedService.EgearTypeEn[num];
  }
  


};
 

