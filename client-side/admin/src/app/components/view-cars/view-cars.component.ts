import { ICarModel } from '../../models/carModel';
import { CarService } from '../../service/car.service';
import { environment } from '../../../environments/environment';
import { SharedService } from '../../service/shared.service';
import { AuthService } from '../../service/auth.service';
import { SelectItem } from 'primeng/components/common/selectitem';
import { IBranchModel } from '../../models/branchModel';
import { ICarGroupModel } from '../../models/carGroupModel';
import { AlertService } from '../../service/alert.service';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
declare const $;

@Component({
  selector: 'app-view-cars',
  templateUrl: './view-cars.component.html',
})
export class ViewCarsComponent implements OnInit {

  constructor(
    private carService: CarService,
    private translateService: TranslateService,
    private alertService: AlertService,
    private authService: AuthService,
    private sharedService: SharedService,
  ){};

  public imageUrl: string =  environment.url + '/images/';

  // loading
  public loading: boolean = true;
  
  // page & limit
  public limit: number = 30;
  public page: number = 1;

  // stop LazyLoad
  public scrollIsLock: boolean = false;
  
  // my data
  public cars: ICarModel[] = []; 

  private translateServiceSubscribe: EventEmitter<any>;

  private allCarGroupsCanThisEmployeeAccess: ICarGroupModel[];

  public carGroupsCanThisEmployeeAccessFilterOptions: SelectItem[] = [];


  public canAddCar: boolean;
  private checkScreens(): void {
    this.canAddCar = this.authService.checkScreen(this.sharedService.Escreens.add_car);
  }

  // title
  private setTitle(): void {
    this.translateService.get('view_cars')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };

  // show data
  private showCars(cars: ICarModel[]): void {

    // if length < limit (stop new loading)
    this.scrollIsLock = cars.length < this.limit;

    // add 1 to page
    this.page += 1 ;

    // add new data
    this.cars.push(...cars);
    
    // stop loading
    this.loading = false;
  };

  // on lazyload 
  public onLazyLoad($event): void {
    // if lock stop here
    if( ! this.scrollIsLock ) {
      // lock before get data
      this.scrollIsLock = true;

      // show loading before get data
      this.loading = true;

      if($event.searchMode) {
        this.getCarsSearch($event.searchModel);
      } else if ($event.filterBy) {
        this.getCarsByFilter($event.sortBy , $event.filterBy)
      } else {
        this.getCars($event.sortBy);
      }
      
    };
  };
  public onReset($event): void {
    this.scrollIsLock = false;
    this.cars = [];
    this.page = 1;
    this.
    onLazyLoad($event)
  };

  private getCars(sortBy: string): void {
    this.carService.getCars(this.page , this.limit , sortBy)
    .then( res => {
      this.showCars(res.cars);
    })
    .catch( e => this.alertService.httpError(e) );
  };
  private getCarsByFilter(sortBy: string , carGroup_id: string): void {
    this.carService.getCarsByFilter(this.page, this.limit , sortBy ,carGroup_id)
    .then( res => {
      this.showCars(res.cars);
    })
    .catch( e => this.alertService.httpError(e) );
  };
  private getCarsSearch(searchQuery: string): void {
    this.carService.getCarsSearch(this.page, this.limit , searchQuery)
    .then( res => {
      this.showCars(res.cars);
    })
    .catch( e => this.alertService.httpError(e) );
  };



  private setCarGroupsFilterOptions(allCarGroups: ICarGroupModel[]): void {
    if((window as any).lang === 'ar') {
      this.carGroupsCanThisEmployeeAccessFilterOptions = allCarGroups.map( carGroup => {
        const name = `${carGroup.carType.nameAr} - ${carGroup.carModel.nameAr} <br> ${carGroup.branch.nameAr} - ${carGroup.year}`
        return {
          label : name,
          value : carGroup._id
        }
      });
    } else {
      this.carGroupsCanThisEmployeeAccessFilterOptions = allCarGroups.map( carGroup => {
        const name = `${carGroup.carType.nameEn} - ${carGroup.carModel.nameEn} <br> ${carGroup.branch.nameEn} - ${carGroup.year}`
        return {
          label : name,
          value : carGroup._id
        }
      });
    }
  };

  private setOptions(): void {

    if(this.allCarGroupsCanThisEmployeeAccess) {
      this.setCarGroupsFilterOptions(this.allCarGroupsCanThisEmployeeAccess);
    } else {
      this.carService.getAllCarGroupsCanThisEmployeeAccess()
      .then( res => {
        this.allCarGroupsCanThisEmployeeAccess = res.carGroups;
        this.setCarGroupsFilterOptions(res.carGroups);
      })
      .catch( e => this.alertService.httpError(e) );
    };

  };
  private onLangChange(): void {
    this.setOptions();
    this.setTitle();
  };
  private init(): void {
    this.setOptions();
    this.setTitle();
    this.checkScreens();
  };
  public ngOnInit(): void  {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void  {
    this.translateServiceSubscribe.unsubscribe();
  };

  public getCarStatus(status: number): string {
    return (window as any).lang === 'ar' ? 
      this.sharedService.EstatusAr[status] 
    : 
      this.sharedService.EstatusEn[status] ;

  }

};

  