import { EnumValues } from 'enum-values';
import { CarService } from '../../../service/car.service';
import { ICarGroupModel, RegExpCarGroupModel } from '../../../models/carGroupModel';
import { OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AlertService } from '../../../service/alert.service';
import { LoadingService } from '../../../service/loading.service';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/components/common/selectitem';
import { Component, OnInit , EventEmitter} from '@angular/core';
import { SharedService } from '../../../service/shared.service';
declare const $;


@Component({
  selector: 'app-add-car',
  templateUrl: './add-car.component.html',
})
export class AddCarComponent implements OnInit , OnDestroy {
  
  constructor(
    private loadingService: LoadingService,
    private carService: CarService,
    private router: Router,
    private alertService: AlertService,
    private sharedService: SharedService,
    private translateService: TranslateService,
  ) { };

  private translateServiceSubscribe: EventEmitter<any>;
  public formGroup: FormGroup;

  private allCarGroupsCanThisEmployeeAccess: ICarGroupModel[];
  public carGroupsCanThisEmployeeAccessOptions: SelectItem[] = [];

  public gearTypesOptions: SelectItem[] = [];
  public statusOptions: SelectItem[] = [];


  private initForm(): void {
    this.formGroup = new FormGroup({ 
      carGroup_id : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.carGroup_id_required , 
          this.sharedService.mainErrorEnum.carGroup_id_invalid , 
          this.sharedService.mainRegExp.RegExpCarGroupModel._id
        ),
      ]),
      plateNumber : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.plateNumber_required , 
          this.sharedService.mainErrorEnum.plateNumber_invalid , 
          this.sharedService.mainRegExp.RegExpCarModel.plateNumber
        ),
      ]),
      gearType : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.gearType_required , 
          this.sharedService.mainErrorEnum.gearType_invalid , 
          this.sharedService.mainRegExp.RegExpCarModel.gearType
        ),
      ]),
      status : new FormControl(null , 
      [ 
        this.sharedService.customValidator(
          this.sharedService.mainErrorEnum.status_required , 
          this.sharedService.mainErrorEnum.status_invalid , 
          this.sharedService.mainRegExp.RegExpCarModel.status
        ),
      ]),
    });
  };

  public onSubmit(): void {

    if(this.formGroup.invalid) {
      
      this.alertService.showFormError(this.formGroup);

    } else {

      this.loadingService.show();
      const car = {
        carGroup_id : this.formGroup.controls['carGroup_id'].value,
        plateNumber : this.formGroup.controls['plateNumber'].value,
        gearType : this.formGroup.controls['gearType'].value,
        status : this.formGroup.controls['status'].value,
      }
      this.carService.addCar(car)
      .then( (res: any) => {
        this.router.navigate(['view-cars' , res._id]);
      })
      .catch( e => this.alertService.httpError(e));

    };

  };

  private setTitle(): void {
    this.translateService.get('add_car')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };


  private setStatusOptions(): void {
    this.statusOptions = EnumValues.getNamesAndValues(
      (window as any).lang === 'ar' ? this.sharedService.EstatusAr : this.sharedService.EstatusEn
    )
    .map( obj => {
      return {
        label : obj.name,
        value : obj.value
      }
    });
  };
  private setGearTypeOptions(): void {
    this.gearTypesOptions = EnumValues.getNamesAndValues(
      (window as any).lang === 'ar' ? this.sharedService.EgearTypeAr : this.sharedService.EgearTypeEn
    )
    .map( obj => {
      return {
        label : obj.name,
        value : obj.value
      }
    });
  };
  private setCarGroupsOptions(allCarGroups: ICarGroupModel[]): void {
    if((window as any).lang === 'ar') {
     
      this.carGroupsCanThisEmployeeAccessOptions = allCarGroups.map( carGroup => {
        const name = `${carGroup.branch.nameAr} - ${carGroup.carType.nameAr} <br> ${carGroup.carModel.nameAr} - ${carGroup.year}`
        return {
          label : name,
          value : carGroup._id,
        }
      });

    } else {
      this.carGroupsCanThisEmployeeAccessOptions = allCarGroups.map( carGroup => {
        const name = `${carGroup.branch.nameEn} - ${carGroup.carType.nameEn} <br> ${carGroup.carModel.nameEn} - ${carGroup.year}`
        return {
          label : name,
          value : carGroup._id,
        }
      });

    };
  };

  public setOptions(): void {
    this.setGearTypeOptions();
    this.setStatusOptions();
    
    if(this.allCarGroupsCanThisEmployeeAccess) {
      this.setCarGroupsOptions(this.allCarGroupsCanThisEmployeeAccess);
    } else{
      this.carService.getAllCarGroupsCanThisEmployeeAccess()
      .then( res => {
        this.allCarGroupsCanThisEmployeeAccess = res.carGroups;
        this.setCarGroupsOptions(res.carGroups);
      })
      .catch( e => this.alertService.httpError(e) );
    };

  };


  private init(){
    this.initForm();
    this.setTitle();
    this.setOptions();
  };
  private onLangChange(): void {
    this.setTitle();
    this.setOptions();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
  }

 
}