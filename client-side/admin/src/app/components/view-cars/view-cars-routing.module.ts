import { AddCarComponent } from './add-car/add-car.component';
import { ViewCarComponent } from './view-car/view-car.component';
import { Escreens } from '../../enums/screens.enum';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ViewCarsComponent } from './view-cars.component';
import { screensGuard } from '../../guards/screens.guard';

const routes: Routes = [
  {
    path : '',
    component : ViewCarsComponent
  },
  {
    path: 'add-car',
    component : AddCarComponent,
    data : {
      screens : [Escreens.add_car]
    },
    canActivate : [screensGuard],
  },
  {
    path: ':car_id',
    component : ViewCarComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewCarsRoutingModule { }
