import { SelectOneModule } from '../common/select-one/select-one.module';
import { AddCarComponent } from './add-car/add-car.component';
import { CustomDataViewModule } from '../common/custom-data-view/custom-data-view.module';
import { CarService } from '../../service/car.service';
import { ViewCarComponent } from './view-car/view-car.component';
import { InputTextInProfileModule } from '../common/input-text-in-profile/input-text-in-profile.module';
import { TooltipModule } from 'primeng/tooltip';
import { PanelHeadingInProfileModule } from '../common/panel-heading-in-profile/panel-heading-in-profile.module';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../../modules/PipesModule';
import { ButtonModule } from 'primeng/button';
import { ViewCarsComponent } from './view-cars.component';
import { ViewCarsRoutingModule } from './view-cars-routing.module';
import { InputSelectOneInProfileModule } from '../common/input-select-one-in-profile/input-select-one-in-profile.module';
import { InputErrorInFormModule } from '../common/input-error-in-form/input-error-in-form.module';

@NgModule({
  imports: [
    CommonModule,
    ViewCarsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModuleForChild,
    PipesModule,
    
    TooltipModule,
    ButtonModule,
    
    CustomDataViewModule,
    PanelHeadingInProfileModule,
    InputTextInProfileModule,
    InputSelectOneInProfileModule,
    InputErrorInFormModule,
    SelectOneModule
  ],
  declarations: [
    ViewCarsComponent,
    ViewCarComponent,
    AddCarComponent
  ],
  providers : [
    CarService
  ]
})
export class ViewCarsModule { }
