import { InputErrorInFormModule } from '../common/input-error-in-form/input-error-in-form.module';
import { EmployeeSigninRoutingModule } from './employee-signin-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { EmployeeSigninComponent } from './employee-signin.component';
import { NgModule } from '@angular/core';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { CommonModule } from '@angular/common';


@NgModule({
  imports: [
    CommonModule,
    EmployeeSigninRoutingModule,
    ReactiveFormsModule,
    TranslateModuleForChild,
    InputErrorInFormModule
  ],
  declarations: [
    EmployeeSigninComponent,
  ]
})
export class EmployeeSigninModule { }
