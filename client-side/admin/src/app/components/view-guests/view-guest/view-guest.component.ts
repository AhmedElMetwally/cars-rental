import { GuestService } from './../../../service/guest.service';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../service/shared.service';
import { LoadingService } from '../../../service/loading.service';
import { AlertService } from '../../../service/alert.service';
import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { LogService } from '../../../service/log.service';
import { IGuestModel } from '../../../models/guestModel';
declare const $;


@Component({
  selector: 'app-view-guest',
  templateUrl: './view-guest.component.html',
  styleUrls : ['view-guest.component.scss']
})
export class ViewGuestComponent implements OnInit , OnDestroy {
 
  constructor(
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translateService: TranslateService,
    private guestService: GuestService,
    private logService: LogService,
    private sharedService: SharedService,
    private activatedRoute: ActivatedRoute
  ) { };

  private translateServiceSubscribe: EventEmitter<any>;
  private activatedRouteSubscribe: any;

  public guest: IGuestModel;
  public guest_id: string;


  public setOptions(): void {
  };

  private getGuest(): void {
    this.loadingService.show();
    this.guestService.getGuest(this.guest_id)
    .then(({ guest }) => {
      this.guest = guest;
      this.loadingService.hide();
      this.afterGetGuest();
    })
    .catch( e => this.alertService.httpError(e) );
  };

  private log(text): void {
    this.logService.log('view-guest' , text);
  };

  private init(){
    this.setOptions();
    this.getGuest_id();
  };
  private onLangChange(): void {
    this.setOptions();

  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
    this.activatedRouteSubscribe.unsubscribe();
  };

  private getGuest_id(): void {
    this.activatedRouteSubscribe = this.activatedRoute.params.subscribe( params => {
      this.guest_id = params['guest_id'];
      this.afterGetGuest_id();
    });
  };
  private afterGetGuest_id(): void {
    this.getGuest();
  };
  private afterGetGuest(): void {
    this.setTitle();
  };

  public getSignupFrom(num): string {
    return (window as any).lang === 'ar' ? this.sharedService.EsignupFromAr[num] : this.sharedService.EsignupFromEn[num];
  };

  private setTitle(): void {
    const title = this.guest.name;
    $('title').text(title);
  };

};
 

