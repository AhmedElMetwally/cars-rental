import { ViewGuestComponent } from './view-guest/view-guest.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ViewGuestsComponent } from './view-guests.component';

const routes: Routes = [
  {
    path : '',
    component : ViewGuestsComponent
  },
  {
    path: ':guest_id',
    component : ViewGuestComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewGuestsRoutingModule { }
