import { GuestService } from './../../service/guest.service';
import { ViewGuestsComponent } from './view-guests.component';
import { ViewGuestComponent } from './view-guest/view-guest.component';
import { CustomDataViewModule } from '../common/custom-data-view/custom-data-view.module';
import { ViewGuestsRoutingModule } from './view-guests-routing.module';
import { InputTextInProfileModule } from '../common/input-text-in-profile/input-text-in-profile.module';
import { TooltipModule } from 'primeng/tooltip';
import { PanelHeadingInProfileModule } from '../common/panel-heading-in-profile/panel-heading-in-profile.module';
import { TranslateModuleForChild } from '../../modules/TranslateModule';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../../modules/PipesModule';
import { ButtonModule } from 'primeng/button';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ViewGuestsRoutingModule,
    ReactiveFormsModule,
    TranslateModuleForChild,
    PipesModule,
    
    TooltipModule,
    ButtonModule,
    
    CustomDataViewModule,
    PanelHeadingInProfileModule,
    InputTextInProfileModule,
  ],
  declarations: [
    ViewGuestComponent,
    ViewGuestsComponent,
  ],
  providers : [
    GuestService
  ]
})
export class ViewGuestsModule { }
