import { IGuestModel } from './../../models/guestModel';
import { GuestService } from './../../service/guest.service';
import { AlertService } from '../../service/alert.service';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
declare const $;

@Component({
  selector: 'app-view-guests',
  templateUrl: './view-guests.component.html',
})
export class ViewGuestsComponent implements OnInit {

  constructor(
    private guestService: GuestService,
    private translateService: TranslateService,
    private alertService: AlertService,
  ){};


  // loading
  public loading: boolean = true;
  
  // page & limit
  public limit: number = 30;
  public page: number = 1;

  // stop LazyLoad
  public scrollIsLock: boolean = false;
  
  // my data
  public guests: IGuestModel[] = []; 

  private translateServiceSubscribe: EventEmitter<any>;

  // title
  private setTitle(): void {
    this.translateService.get('view_guests')
    .toPromise()
    .then(
      title => $('title').text(title)
    );
  };

  // show data
  private showGuests(guests: IGuestModel[]): void {

    // if length < limit (stop new loading)
    this.scrollIsLock = guests.length < this.limit;

    // add 1 to page
    this.page += 1 ;

    // add new data
    this.guests.push(...guests);
    
    // stop loading
    this.loading = false;
  };

  // on lazyload 
  public onLazyLoad($event): void {
    // if lock stop here
    if( ! this.scrollIsLock ) {
      // lock before get data
      this.scrollIsLock = true;

      // show loading before get data
      this.loading = true;

      if($event.searchMode) {
        this.getGuestsSearch($event.searchModel);
      } else {
        this.getGuests($event.sortBy);
      };
      
    };
  };
  public onReset($event): void {
    this.scrollIsLock = false;
    this.guests = [];
    this.page = 1;
    this.
    onLazyLoad($event)
  };

  private getGuests(sortBy: string): void {
    this.guestService.getGuests(this.page , this.limit , sortBy)
    .then( res => {
      this.showGuests(res.guests);
    })
    .catch( e => this.alertService.httpError(e) );
  };
  private getGuestsSearch(searchQuery: string): void {
    this.guestService.getGuestsSearch(this.page, this.limit , searchQuery)
    .then( res => {
      this.showGuests(res.guests);
    })
    .catch( e => this.alertService.httpError(e) );
  };


  private setOptions(): void {
  };
  private onLangChange(): void {
    this.setOptions();
    this.setTitle();
  };
  private init(): void {
    this.setTitle();
  };
  public ngOnInit(): void  {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void  {
    this.translateServiceSubscribe.unsubscribe();
  };


};
