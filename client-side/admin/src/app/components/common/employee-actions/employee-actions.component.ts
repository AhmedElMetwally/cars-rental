import { AlertService } from './../../../service/alert.service';
import { SharedService } from '../../../service/shared.service';
import { SelectItem } from 'primeng/components/common/selectitem';
import { Component, EventEmitter, Input, OnInit, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { EmployeeService } from '../../../service/employee.service';

@Component({
  selector: 'employee-actions',
  templateUrl: './employee-actions.component.html'
})
export class EmployeeActionsComponent implements OnInit , OnDestroy {

  constructor(
    private employeeService: EmployeeService,
    private translateService: TranslateService,
    private sharedService: SharedService,
    private alertService: AlertService,

  ){};

  @Input('employee_id') employee_id: string;
  @Input('isAdmin') isAdmin: boolean;

  // filter options
  public filterByOptions: SelectItem[] = [];
  
  // loading
  public loading: boolean = true;
  
  // page & limit
  public limit: number = 30;
  public page: number = 1;

  // stop LazyLoad
  public scrollIsLock: boolean = false;
  
  // my data
  public employeeActions: any[] = []; 

  public onLazyLoad;

  private translateServiceSubscribe: EventEmitter<any>;


  // show data
  private showEmployeeActions(employeeActions: any[]): void {
    
    // if length < limit (stop new loading)
    this.scrollIsLock = employeeActions.length < this.limit;

    // add 1 to page
    this.page += 1 ;

    // add new data
    this.employeeActions.push(...employeeActions);
    
    // stop loading
    this.loading = false;
  };

  // on lazyload for employee
  public employeeActionsOnLazyLoadForEmployee($event): void {

    // if lock stop here
    if( ! this.scrollIsLock ) {

      // lock before get data
      this.scrollIsLock = true;

      // show loading before get data
      this.loading = true;

      if( ! $event.filterBy ) {
        this.getThisEmployeeActions($event.sortBy);
      } else {
        this.getThisEmployeeActionsByItemNameFilter($event.sortBy , $event.filterBy);
      };


    };
  };
  private getThisEmployeeActions(sortBy: string): void {
    this.employeeService.getThisEmployeeActions(this.page , this.limit , sortBy)
    .then( res => {
      this.showEmployeeActions(res.employeeActions);
    })
    .catch(e => this.alertService.httpError(e));
  };
  private getThisEmployeeActionsByItemNameFilter(sortBy: string , filterBy: string): void {
    this.employeeService.getThisEmployeeActionsByItemNameFilter(this.page , this.limit , sortBy, filterBy)
    .then( res => {
      this.showEmployeeActions(res.employeeActions);
    })
    .catch(e => this.alertService.httpError(e));
  };

  // on lazyload for admin
  public employeeActionsOnLazyLoadForAdmin($event): void {
    // if lock stop here
    if( ! this.scrollIsLock ) {
      // lock before get data
      this.scrollIsLock = true;

      // show loading before get data
      this.loading = true;

      if( ! $event.filterBy ) {
        this.getEmployeeActions($event.sortBy);
      } else {
        this.getEmployeeActionsByItemNameFilter($event.sortBy , $event.filterBy);
      };


    };
  };
  private getEmployeeActions(sortBy: string): void {
    this.employeeService.getEmployeeActions(this.page , this.limit , sortBy , this.employee_id)
    .then( res => {
      this.showEmployeeActions(res.employeeActions);
    })
    .catch(e => this.alertService.httpError(e));
  };
  private getEmployeeActionsByItemNameFilter(sortBy: string , filterBy: string): void {
    this.employeeService.getEmployeeActionsByItemNameFilter(this.page , this.limit , sortBy, filterBy, this.employee_id)
    .then( res => {
      this.showEmployeeActions(res.employeeActions);
    })
    .catch(e => this.alertService.httpError(e));
  };

  public onReset($event): void {
    this.scrollIsLock = false;
    this.employeeActions = [];
    this.page = 1;
    this.onLazyLoad($event)
  };

  private setOptions(): void {
    this.filterByOptions = [];
    this.translateService.get('employeeActionsFilterByItemNameOptions')
    .toPromise()
    .then(
      v => this.filterByOptions = v
    );
  };

  private onLangChange(): void {
    this.setOptions();
  };
  private init(): void {
    this.setOptions();
    if(this.isAdmin && this.employee_id){
      this.onLazyLoad = this.employeeActionsOnLazyLoadForAdmin;
    } else {
      this.onLazyLoad = this.employeeActionsOnLazyLoadForEmployee;
    };
  };
  public ngOnInit(): void  {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void  {
    this.translateServiceSubscribe.unsubscribe();
  };

  public getScreen(screen: number): string {
    return (window as any).lang === 'ar' ? this.sharedService.EscreensAr[screen] : this.sharedService.EscreensEn[screen];
  };
 

};
 