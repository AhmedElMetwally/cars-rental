import { RouterModule } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { PipesModule } from '../../../modules/PipesModule';
import { EmployeeActionsComponent } from './employee-actions.component';
import { TranslateModuleForChild } from '../../../modules/TranslateModule';
import { PanelHeadingInProfileModule } from '../panel-heading-in-profile/panel-heading-in-profile.module';
import { CustomDataViewModule } from '../custom-data-view/custom-data-view.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TooltipModule } from 'primeng/tooltip';

@NgModule({
  imports: [ 
    CommonModule,
    TranslateModuleForChild,

    PanelHeadingInProfileModule,
    CustomDataViewModule,

    TooltipModule,
    PipesModule,
    ButtonModule,

    RouterModule,
  ],
  declarations: [
    EmployeeActionsComponent
  ],
  exports : [
    EmployeeActionsComponent
  ]
})
export class EmployeeActionsModule { }
