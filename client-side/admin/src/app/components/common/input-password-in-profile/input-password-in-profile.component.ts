import { SharedService } from '../../../service/shared.service';
import { Component, EventEmitter, Input,Output } from '@angular/core';

interface IOnSaveEvent {
  fieldName: string;
  value: string;
};

interface IOnValidationError {
  validationErrorMsg: string;
};

@Component({
  selector: 'input-password-in-profile',
  template: `

  <li class="list-group-item">
    <div>
      <i class="fa fa-lock"></i>  
      <strong>{{displayName}} : </strong> 

      <div *ngIf="edit" class="ui-inputgroup password">
        <input (keyup.enter)='save(field.value)' #field type="password" [placeholder]="'enter_password' | translate" pInputText >
        <i tooltipPosition="top" [pTooltip]="'show' | translate" class="fa fa-eye" (mouseenter)='field.type="text"' (mouseleave)='field.type="password"' ></i>
        <button (click)='save(field.value)' pButton  tooltipPosition="top" [pTooltip]="'save' | translate" type="button" icon="fa fa-edit" class="ui-button-primary"></button>      
      </div>

      <span *ngIf="!edit">
        *******
      </span>
    </div>
    <i *ngIf="displayControl && !edit" class="fa fa-edit icon-control" tooltipPosition="top" [pTooltip]="'edit' | translate" (click)='openEdit()'></i>
    <i *ngIf="displayControl && edit" class="fa fa-times icon-control" tooltipPosition="top" [pTooltip]="'close' | translate" (click)='closeEdit()'></i>
  </li>
 
  `,
  // styleUrls : ['input-password-in-profile.component.scss']
})
export class InputPasswordInProfileComponent {
  
  constructor(
    private sharedService: SharedService,
  ){};

  @Input('displayName') displayName: string;

  // edit
  @Input('edit') edit: boolean;
  @Input('displayControl') displayControl: boolean;

  // onSave
  @Input('fieldName') fieldName: string;
  @Output() onSave: EventEmitter<IOnSaveEvent> = new EventEmitter();

  // onValidationError
  @Input('requiredErrorEnum') requiredErrorEnum: number;
  @Input('invalidErrorEnum') invalidErrorEnum: number;
  @Input('RegExp') RegExp: RegExp;
  @Output() onValidationError: EventEmitter<IOnValidationError> = new EventEmitter();

  @Input('editNameInEvent') editNameInEvent: string;
  @Output() onOpenEdit: EventEmitter<any> = new EventEmitter();
  @Output() onCloseEdit: EventEmitter<any> = new EventEmitter();

  // if displayControl true
  public save(value: string): void {
    
    if(!this.displayControl) return;

    if(value === '' || value === null){

      const $event = {
        validationErrorMsg : this.sharedService.getErrorMsg(this.requiredErrorEnum) , 
      };
      this.onValidationError.emit($event);

    } else if (this.RegExp.test(value)) {

      const $event = {
        fieldName : this.fieldName , 
        value : value
      };
      this.onSave.emit($event);

    } else {

      const $event = {
        validationErrorMsg : this.sharedService.getErrorMsg(this.invalidErrorEnum) , 
      };
      this.onValidationError.emit($event);

    };

  };

  public openEdit(): void {
    if(!this.displayControl) return;

    const $event = {  
      editNameInEvent : 'password'
    };
    this.onOpenEdit.emit($event);
  };

  public closeEdit(): void {
    if(!this.displayControl) return;

    const $event = {  
      editNameInEvent : 'password'
    };
    this.onCloseEdit.emit($event);
  };

  
};

