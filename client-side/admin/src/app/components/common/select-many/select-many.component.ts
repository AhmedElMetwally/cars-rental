import { SelectItem } from 'primeng/components/common/selectitem';
import { Component, EventEmitter, Input, Output, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

export const noop = () => {
};

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => SelectManyComponent),
  multi: true
};

@Component({
  selector: 'select-many',
  template: `
    <div>
      <div style='position: relative;'>
        <i class='select-many fa {{icon}}'></i>
        <p-multiSelect 
          [(ngModel)]="value"  
          class='select-many'
          [style]="{'min-width':'100%'}"
          [options]="options" 
          filter='true'
          [defaultLabel]='placeholder'
          (onChange)='change($event)'
          maxSelectedLabels='1'
          [selectedItemsLabel]="'{} ' + ('selected_items' | translate)"
          scrollHeight='150px'
          [disabled]='disabled'
        >
        
          <ng-template let-item pTemplate="selectedItem"> 
            <span [innerHTML]='item.label'></span>
          </ng-template> 
          <ng-template let-item pTemplate="item"> 
              <div class="ui-helper-clearfix select-item-label">
                <div [innerHTML]='item.label'></div> 
              </div>
          </ng-template>
        
        </p-multiSelect>
      </div>
    </div>
  `,
  styleUrls : ['select-many.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class SelectManyComponent implements ControlValueAccessor {

  constructor(){};
  
  @Input('disabled') disabled: boolean;
  @Input('icon') icon: string;
  @Input('options') options: SelectItem[] = [];
  @Input('placeholder') placeholder: string; 

  @Output() onChange: EventEmitter<any> = new EventEmitter();

  public change($event) {
    this.onChange.emit($event);
  };



  // ---------------------------------------
  //  copy past code D;
  // ---------------------------------------
  private innerValue: any = '';

  //Placeholders for the callbacks which are later provided
  //by the Control Value Accessor
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;

  //get accessor
  get value(): any {
      return this.innerValue;
  };

  //set accessor including call the onchange callback
  set value(v: any) {
    if (v !== this.innerValue) {
        this.innerValue = v;
        this.onChangeCallback(v);
    }
  }

  //From ControlValueAccessor interface
  writeValue(value: any) {
      if (value !== this.innerValue) {
          this.innerValue = value;
      }
  }

  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
      this.onChangeCallback = fn;
  }

  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
      this.onTouchedCallback = fn;
  }

};
