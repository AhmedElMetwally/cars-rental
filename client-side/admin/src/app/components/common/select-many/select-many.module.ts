import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectManyComponent } from './select-many.component';
import { MultiSelectModule } from 'primeng/multiselect';
import { TranslateModuleForChild } from '../../../modules/TranslateModule';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [ 
    CommonModule,
    MultiSelectModule,
    TranslateModuleForChild,
    FormsModule,
  ],
  declarations: [
    SelectManyComponent
  ],
  exports : [
    SelectManyComponent,
  ]
})
export class SelectManyModule { }
