import { environment } from '../../../../environments/environment';
import { LogService } from '../../../service/log.service';
import { Component, EventEmitter, Output, Input, OnInit } from '@angular/core';



@Component({
  selector: 'overview-editor',
  templateUrl : 'overview-editor.component.html',
  styleUrls : ['overview-editor.component.scss']
})
export class OverviewEditorComponent implements OnInit {

  constructor(
    private logService: LogService,
  ){};
  
  @Input('direction') direction: string;
  @Input('title') title: string;
  @Input('canEdit') canEdit: boolean;
  @Input('html') set html(html: string){
    this.overviewModel = html;
    this.viewer = html;
  };
  @Output('onSave') onSave: EventEmitter<any> = new EventEmitter();

  public edit: boolean = false;
  public overviewModel: string = '';
  public viewer: string = '';

  private log(text): void {
    this.logService.log('chart-statistics' , text);
  };

  public save(): void {
    this.hideEdit();
    const $event = {
      html : this.overviewModel
    };
    this.onSave.emit($event);
    this.viewer = this.overviewModel;
  };

  public showEdit(){
    this.edit = true;
  };
  public hideEdit(){
    this.edit = false;
  };

  // lazy load files
  public isFullLoad: boolean = false;
  private script: boolean = false;
  private style: boolean = false;
  private onScriptLoad(): void {
    this.script =  true;
    this.onFullLoad();
  };
  private onStyleLoad(): void {
    this.style =  true;
    this.onFullLoad();
  };
  private loadQuillScript(): void {
    if(document.getElementById('quill-script') === null){
      this.log('load new quill-script');
      const body = <HTMLDivElement> document.body;
      const script = document.createElement('script');
      script.innerHTML = '';
      script.src = environment.lazyload.quill.script;
      script.async = true;
      script.defer = true;
      script.id = 'quill-script';
      body.appendChild(script);
      script.onload = () => {
        this.onScriptLoad();
      };
    } else {
      this.log('quill-script used before');
      this.onScriptLoad();
    }
  };
  private loadQuillStyle(): void {
    if(document.getElementById('quill-style') === null){
      this.log('load new quill-style');
      const body = <HTMLDivElement> document.body;
      const style = document.createElement('link');
      style.innerHTML = '';
      style.href = environment.lazyload.quill.style;
      style.type = 'text/css';
      style.rel = 'stylesheet';
      style.id = 'quill-style';
      body.appendChild(style);
      style.onload = () => {
        this.onStyleLoad();
      };
    } else {
      this.log('quill-style used before');
      this.onStyleLoad();
    }
  };
  private onFullLoad(): void {
    if(this.script && this.style) {
      this.log('Full Load')
      this.isFullLoad = true;
    };
  }
  // lazy load files

  public ngOnInit(): void {
    this.loadQuillScript();
    this.loadQuillStyle();
  };

};
