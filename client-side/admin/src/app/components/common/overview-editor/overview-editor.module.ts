import { PipesModule } from '../../../modules/PipesModule';
import { TranslateModuleForChild } from '../../../modules/TranslateModule';
import { FormsModule } from '@angular/forms';
import { BtnModule } from '../btn/btn.module';
import { OverviewEditorComponent } from './overview-editor.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { EditorModule } from 'primeng/editor';
import { PanelHeadingInProfileModule } from '../panel-heading-in-profile/panel-heading-in-profile.module';
import { TooltipModule } from 'primeng/tooltip';

@NgModule({
  imports: [ 
    CommonModule,
    ButtonModule,
    EditorModule,
    PanelHeadingInProfileModule,
    BtnModule,
    FormsModule,
    TranslateModuleForChild,
    TooltipModule,
    PipesModule,
  ],
  declarations: [
    OverviewEditorComponent
  ],
  exports : [
    OverviewEditorComponent,
  ]
})
export class OverviewEditorModule { }
