import { SharedService } from '../../../service/shared.service';
import { Component, EventEmitter, Input, Output } from '@angular/core';

interface IOnSaveEvent {
  fieldName: string;
  value: string;
};

interface IOnValidationError {
  validationErrorMsg: string;
};

@Component({
  selector: 'input-text-in-profile',
  template: `

  <li class="list-group-item">
    <div>
      <i [class]="icon"></i>  
      <strong>{{displayName}} : </strong> 

      <div *ngIf="edit" class="ui-inputgroup">
        <input [placeholder]='placeholder' (keyup.enter)='save(field.value)' #field type="text" value="{{value}}" pInputText >
        <button (click)='save(field.value)' pButton  tooltipPosition="top" [pTooltip]="'save' | translate" type="button" icon="fa fa-edit" class="ui-button-primary"></button>      
      </div>

      <span *ngIf="!edit" [innerHTML]='fieldValue || value'></span>
      
    </div>

    <i *ngIf="displayControl && !edit" class="fa fa-edit icon-control" tooltipPosition="top" [pTooltip]="'edit' | translate" (click)='openEdit()'></i>
    <i *ngIf="displayControl && edit" class="fa fa-times icon-control" tooltipPosition="top" [pTooltip]="'close' | translate" (click)='closeEdit()'></i>

    <i *ngIf="displayLink && !edit" class="fa fa-search-plus extra-link  {{displayControl ? 'extra-link-2' : 'extra-link-1'}}" tooltipPosition="top" [pTooltip]="linkLabel"  [routerLink]="linkUrl" ></i>
 
  </li>

  `,
})
export class InputTextInProfileComponent {
  
  constructor(
    private sharedService: SharedService,
  ){}


  @Input('linkUrl') linkUrl: string; 
  @Input('linkLabel') linkLabel: string; 
  @Input('displayLink') displayLink: boolean = false;

  
  @Input('placeholder') placeholder: string = '';

  @Input('displayName') displayName: string;
  @Input('icon') icon: string;

  // edit
  @Input('edit') edit: boolean;
  @Input('displayControl') displayControl: boolean;

  // onSave
  @Input('fieldName') fieldName: string;
  @Input('fieldValue') fieldValue: string;
  @Input('value') value: string;
  @Output() onSave: EventEmitter<IOnSaveEvent> = new EventEmitter();

  // onValidationError
  @Input('requiredErrorEnum') requiredErrorEnum: number;
  @Input('invalidErrorEnum') invalidErrorEnum: number;
  @Input('RegExp') RegExp: RegExp;
  @Output() onValidationError: EventEmitter<IOnValidationError> = new EventEmitter();
  
  @Input('editNameInEvent') editNameInEvent: string;
  @Output() onOpenEdit: EventEmitter<any> = new EventEmitter();
  @Output() onCloseEdit: EventEmitter<any> = new EventEmitter();


  // if displayControl true
  public save(value: string): void  {
    
    if(!this.displayControl) return;

    if(value === '' || value === null) {

      const $event = {
        validationErrorMsg : this.sharedService.getErrorMsg(this.requiredErrorEnum) , 
      };
      this.onValidationError.emit($event);

    } else if (this.RegExp.test(value)) {

      const $event = {
        fieldName : this.fieldName , 
        value : value
      };
      this.onSave.emit($event);

    } else {

      const $event = {
        validationErrorMsg : this.sharedService.getErrorMsg(this.invalidErrorEnum) , 
      };
      this.onValidationError.emit($event);

    };

  };

  public openEdit(): void {
    if(!this.displayControl) return;
    const $event = {  
      editNameInEvent : this.editNameInEvent
    };
    this.onOpenEdit.emit($event);
  };

  public closeEdit(): void {
    if(!this.displayControl) return;
    const $event = {  
      editNameInEvent : this.editNameInEvent
    };
    this.onCloseEdit.emit($event);
  };


};
