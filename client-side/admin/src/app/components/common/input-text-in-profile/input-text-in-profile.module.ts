import { TranslateModuleForChild } from '../../../modules/TranslateModule';
import { InputTextInProfileComponent } from './input-text-in-profile.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { TooltipModule } from 'primeng/tooltip';
import { InputTextModule } from 'primeng/inputtext';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [ 
    CommonModule,
    ButtonModule,
    TooltipModule,
    InputTextModule,
    TranslateModuleForChild,
    RouterModule,
  ],
  declarations: [
    InputTextInProfileComponent
  ],
  exports : [
    InputTextInProfileComponent,
  ]
})
export class InputTextInProfileModule { }
