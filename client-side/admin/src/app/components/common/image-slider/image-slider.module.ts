import { PanelHeadingInProfileModule } from '../panel-heading-in-profile/panel-heading-in-profile.module';
import { TranslateModuleForChild } from '../../../modules/TranslateModule';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageSliderComponent } from './image-slider.component';
import { TooltipModule } from 'primeng/tooltip';
import { ButtonModule } from 'primeng/button';
import { SlideshowModule } from 'ng-simple-slideshow';
import { FileUploadModule } from 'primeng/fileupload';

@NgModule({
  imports: [ 
    CommonModule,
    TranslateModuleForChild,
    TooltipModule,
    ButtonModule,
    SlideshowModule,
    FileUploadModule,
    PanelHeadingInProfileModule
  ],
  declarations: [
    ImageSliderComponent
  ],
  exports : [
    ImageSliderComponent,
  ]
})
export class ImageSliderModule { }
