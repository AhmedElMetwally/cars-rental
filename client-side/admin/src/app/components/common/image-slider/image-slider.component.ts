import { TranslateService } from '@ngx-translate/core';
import { AlertService } from '../../../service/alert.service';
import { Component, EventEmitter, Input, Output, ViewChild, OnInit, OnChanges } from '@angular/core';
declare const $;

@Component({
  selector: 'image-slider',
  template: `

    <div class="panel panel-default">
      <div class="panel-heading">
        <panel-heading-in-profile
          [name]='title'
          [deleted]='false'
            
          [displayIsOnline]='false'
          [isOnline]='false'

          [displayControl]='false'
          [canDelete]='false'
          [canRestore]='false'
        >
        </panel-heading-in-profile>
      </div>

      <div class="panel-body slideshow-parent">
        <div class='row'>
          <div class='col-xs-12 col-xs-offset-0 col-md-8 col-md-offset-2'>
              
            <slideshow 
                #slideshow
                height="400px"
                [imageUrls]="images.length ? images : defaultImage"
                [lazyLoad]="true"
                [showDots]="true"
                [autoPlay]="false"
                [showArrows]="images.length > 1"
              > </slideshow>

            <button *ngIf='canDelete && images.length' (click)='delete()' tooltipPosition="top" [pTooltip]="'delete_image' | translate"  class='delete-image' pButton type="button" icon="fa fa-trash" ></button>
            <button *ngIf='images.length && canDelete'  class='open-image open-image-position-2' (click)='zoom()' tooltipPosition="top" [pTooltip]="'view_image' | translate"  pButton type="button" icon="fa fa-search-plus" ></button>
            <button *ngIf='images.length && ! canDelete'  class='open-image open-image-position-1' (click)='zoom()' tooltipPosition="top" [pTooltip]="'view_image' | translate"  pButton type="button" icon="fa fa-search-plus" ></button>
          </div>
        </div>


        <div *ngIf='canAdd' class='row upload-image-parent'>
          <div class='center-child-in-laptop col-xs-12 col-xs-offset-0 col-md-8 col-md-offset-2'>
              <p-fileUpload 
                #form
                customUpload="true" 
                (uploadHandler)="add($event)"
                mode="basic" 
                accept="image/*" 
                maxFileSize="2000000" 
                auto="true" 
                [chooseLabel]="'add_image' | translate"
              >
              </p-fileUpload>
          </div>
        </div>

      </div>

    </div>
  `,
  styleUrls : ['image-slider.component.scss']
})
export class ImageSliderComponent implements OnInit , OnChanges {
  
  constructor(
    private alertService: AlertService,
    private translateService: TranslateService,
  ){};
  
  public defaultImage: string[] = ['/assets/images/no-image.png'];

  @Input('images') images: string[] = [];
  @Input('canAdd') canAdd: boolean;
  @Input('canDelete') canDelete: boolean;
  @Input('title') title: string;
  
  @Output() onDelete: EventEmitter<any> = new EventEmitter();
  @Output() onAdd: EventEmitter<any> = new EventEmitter();

  @ViewChild('slideshow') slideshow: any;
  @ViewChild('form') form: any;

  public delete() {
    if( ! this.images[this.slideshow.slideIndex] ) return;

    const $event = {
      image : this.images[this.slideshow.slideIndex] 
    };

    this.translateService.get('you_will_delete_image')
    .toPromise()
    .then( v => {

      this.alertService.ask(v)
      .then( ok => {

        if(ok) {
          this.slideshow.slideIndex = 0;
          this.onDelete.emit($event);
        };
        
      });

    });

  };

  public add($event) {
    this.form.clear();
    this.onAdd.emit($event);
  };

  public ngOnInit(): void {
  };

  public ngOnChanges(): void {
  };

  public zoom(): void {
    const image = this.images[this.slideshow.slideIndex];

    $('body').append(`
      <div class='center-child-in-laptop image-slider-zoom'>
        <div>
          <img src="${image}" />
          <i class='fa fa-times'></i>
        </div>
      </div>
    `);

    $('.image-slider-zoom .fa-times').click(() => {
      $('.image-slider-zoom').remove();
    })

  }

};
 