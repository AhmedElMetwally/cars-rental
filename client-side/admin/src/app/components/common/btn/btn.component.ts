import { Component , EventEmitter , Output , Input } from '@angular/core';

@Component({
  selector: 'app-btn',
  template: `
    <div class='center-child-in-laptop parent-of-btn-component'>
      <button (click)='click($event)' pButton type="button" [label]='label' class="ui-button-rounded ui-button-success"></button>
    </div>
  `,
  styleUrls : ['btn.component.scss']
})
export class BtnComponent {

  constructor(){};

  @Input('label') label: string ;
  @Output() onClick: EventEmitter<any> = new EventEmitter();

  public click($event): void {
    this.onClick.emit($event);
  };

};
