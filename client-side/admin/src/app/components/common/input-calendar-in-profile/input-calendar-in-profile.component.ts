import { SharedService } from '../../../service/shared.service';
import { Component, EventEmitter, Input, Output } from '@angular/core';

interface IOnSaveEvent {
  fieldName: string;
  value: string;
};

interface IOnValidationError {
  validationErrorMsg: string;
};

@Component({
  selector: 'input-calendar-in-profile',
  template: `

  <li class="list-group-item">
    <div>
      <i [class]="icon"></i>  
      <strong>{{displayName}} : </strong> 

      <div *ngIf="edit" class="ui-inputgroup">
        <p-calendar [yearNavigator]="true" yearRange="1940:2030" [showTime]="showTime" hourFormat="12" (keyup.enter)='save(field.value)' [placeholder]="'select_date' | translate" #field dateFormat="yy-mm-dd" ></p-calendar>
        <button (click)='save(field.value)' pButton  tooltipPosition="top" [pTooltip]="'save' | translate" type="button" icon="fa fa-edit" class="ui-button-primary"></button>      
      </div>

      <span *ngIf="!edit" [innerHTML]='fieldValue'></span>
       
    </div>
    <i *ngIf="displayControl && !edit" class="fa fa-edit icon-control" tooltipPosition="top" [pTooltip]="'edit' | translate" (click)='openEdit()'></i>
    <i *ngIf="displayControl && edit" class="fa fa-times icon-control" tooltipPosition="top" [pTooltip]="'close' | translate" (click)='closeEdit()'></i>
  </li>
  `,
})
export class InputCalendarInProfileComponent {
  
  constructor(
    private sharedService: SharedService,
  ){}


  @Input('showTime') showTime: boolean;

  @Input('displayName') displayName: string;
  @Input('icon') icon: string;
  
  // edit
  @Input('edit') edit: boolean;
  @Input('displayControl') displayControl: boolean;

  // onSave
  @Input('fieldName') fieldName: string;
  @Input('fieldValue') fieldValue: string;
  @Input('value') value: string;
  @Output() onSave: EventEmitter<IOnSaveEvent> = new EventEmitter();

  // onValidationError
  @Input('requiredErrorEnum') requiredErrorEnum: number;
  @Input('invalidErrorEnum') invalidErrorEnum: number;
  @Input('RegExp') RegExp: RegExp;
  @Output() onValidationError: EventEmitter<IOnValidationError> = new EventEmitter();
  
  @Input('editNameInEvent') editNameInEvent: string;
  @Output() onOpenEdit: EventEmitter<any> = new EventEmitter();
  @Output() onCloseEdit: EventEmitter<any> = new EventEmitter();


  // if displayControl true
  public save(value: string): void  {

    if(!this.displayControl) return;
    
    if(value === '' || value === undefined) {

      const $event = {
        validationErrorMsg : this.sharedService.getErrorMsg(this.requiredErrorEnum) , 
      };
      this.onValidationError.emit($event);

    } else {

      value = new Date(value).toISOString();
      
      if (this.RegExp.test(value)) {

        const $event = {
          fieldName : this.fieldName , 
          value : value
        };
        this.onSave.emit($event);

      } else {

        const $event = {
          validationErrorMsg : this.sharedService.getErrorMsg(this.invalidErrorEnum) , 
        };
        this.onValidationError.emit($event);

      };

    };

  };

  public openEdit(): void {
    if(!this.displayControl) return;
    const $event = {  
      editNameInEvent : this.editNameInEvent
    };
    this.onOpenEdit.emit($event);
  };

  public closeEdit(): void {
    if(!this.displayControl) return;
    const $event = {  
      editNameInEvent : this.editNameInEvent
    };
    this.onCloseEdit.emit($event);
  };


};
