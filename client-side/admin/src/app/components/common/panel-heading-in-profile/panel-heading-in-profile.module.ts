import { PanelHeadingInProfileComponent } from './panel-heading-in-profile.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TooltipModule } from 'primeng/tooltip';
import { TranslateModuleForChild } from '../../../modules/TranslateModule';

@NgModule({
  imports: [ 
    CommonModule,
    TooltipModule,
    TranslateModuleForChild
  ],
  declarations: [
    PanelHeadingInProfileComponent
  ],
  exports : [
    PanelHeadingInProfileComponent,
  ]
})
export class PanelHeadingInProfileModule { }
