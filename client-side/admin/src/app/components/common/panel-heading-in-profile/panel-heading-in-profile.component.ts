import { TranslateService } from '@ngx-translate/core';
import { AlertService } from '../../../service/alert.service';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'panel-heading-in-profile',
  template: `
    <h2 class="panel-title text-center"> 

        <div class='parent-panel-heading-in-profile'>
          
          <i *ngIf="displayIsOnline && isOnline" 
            style="color:#5fc00c" 
            class="fa fa-circle panel-heading-in-profile-online-icon" tooltipPosition="top" 
            [pTooltip]="'online' | translate">
          </i>

          <i *ngIf="displayIsOnline && !isOnline" style="color:gray" 
            class="fa fa-circle panel-heading-in-profile-online-icon" tooltipPosition="top" 
            [pTooltip]="'offline' | translate">
          </i>

          {{ name }}

        </div>


        <i *ngIf="displayControl && canDelete && (!deleted)" 
          (click)='delete()' 
          class="fa fa-trash" 
          tooltipPosition="top" 
          tooltipZIndex='9999999'
          [pTooltip]="'delete' | translate"  
          ></i>

        <i *ngIf="displayControl && canRestore && (!!deleted)" 
          (click)='restore()' 
          class="fa restore fa-undo" 
          tooltipZIndex='9999999'
          tooltipPosition="top" 
          [pTooltip]="'restore' | translate" 
        ></i>

    </h2>
  `,
  styleUrls : ['panel-heading-in-profile.component.scss']
})
export class PanelHeadingInProfileComponent {

  constructor(
    private alertService: AlertService,
    private translateService: TranslateService,
  ){};

  @Input('name') name: string;
  @Input('deleted') deleted: number; 

  @Input('displayControl') displayControl: boolean;
  @Input('canDelete') canDelete: boolean;
  @Input('canRestore') canRestore: boolean;
  
  @Input('displayIsOnline') displayIsOnline: boolean;
  @Input('isOnline') isOnline: boolean;


  @Output() onDelete: EventEmitter<any> = new EventEmitter();
  @Output() onRestore: EventEmitter<any> = new EventEmitter();


  public delete(): void {
    this.translateService.get('are_you_sure_you_want_to_delete')
    .toPromise()
    .then( v => {
      this.alertService.ask(v)
      .then( ok => {
        if(ok) this.onDelete.emit();
      });
    });
  };
  public restore(): void {
    this.translateService.get('are_you_sure_you_want_to_restore')
    .toPromise()
    .then( v => {
      this.alertService.ask(v)
      .then( ok => {
        if(ok) this.onRestore.emit();
      });
    });
  };


};
