import { TranslateModuleForChild } from '../../../modules/TranslateModule';
import { InputSelectManyInProfileComponent } from './input-select-many-in-profile.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { TooltipModule } from 'primeng/tooltip';
import { FormsModule } from '@angular/forms';
import { SelectManyModule } from '../select-many/select-many.module';

@NgModule({
  imports: [ 
    CommonModule,
    ButtonModule,
    TooltipModule,
    SelectManyModule,
    TranslateModuleForChild,
    FormsModule,
  ],
  declarations: [
    InputSelectManyInProfileComponent
  ],
  exports : [
    InputSelectManyInProfileComponent,
  ]
})
export class InputSelectManyInProfileModule { }
