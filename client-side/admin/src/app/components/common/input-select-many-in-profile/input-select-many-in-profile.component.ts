import { SelectItem } from 'primeng/components/common/selectitem';
import { SharedService } from '../../../service/shared.service';
import { Component, EventEmitter, Input, Output } from '@angular/core';

interface IOnSaveEvent {
  fieldName: string;
  value: string[];
};

interface IOnValidationError {
  validationErrorMsg: string;
};

@Component({
  selector: 'input-select-many-in-profile',
  template: `

  <!-- import input-select-many-in-profile--open to fix css -->
  <li class="list-group-item {{ edit ? 'input-select-many-in-profile--open' : ''}}">
    <div>
      <i [class]="icon"></i>  
      <strong>{{displayName}} : </strong> 

      <div *ngIf="edit" class="ui-inputgroup">
        <select-many
          [disabled]='false'
          [(ngModel)]="value"
          [icon]='icon'     
          [options]='options'   
          (keyup.enter)='save(value)'
          [placeholder]='placeholder'
        >
        </select-many>
        <button (click)='save(value)' pButton  tooltipPosition="top" [pTooltip]="'save' | translate" type="button" icon="fa fa-edit" class="ui-button-primary"></button>      
      </div>

      <span *ngIf="!edit" [innerHTML]='fieldValue'></span>
      
    </div>
    <i *ngIf="displayControl && !edit" class="fa fa-edit icon-control" tooltipPosition="top" [pTooltip]="'edit' | translate" (click)='openEdit()'></i>
    <i *ngIf="displayControl && edit" class="fa fa-times icon-control" tooltipPosition="top" [pTooltip]="'close' | translate" (click)='closeEdit()'></i>
  </li>

  `,
  styleUrls : ['input-select-many-in-profile.component.scss']
})
export class InputSelectManyInProfileComponent {
  
  constructor(
    private sharedService: SharedService,
  ){}

  @Input('placeholder') placeholder: string;
  @Input('displayName') displayName: string;
  @Input('icon') icon: string;

  // edit
  @Input('edit') edit: boolean;
  @Input('displayControl') displayControl: boolean;

  // onSave
  @Input('fieldName') fieldName: string;
  @Input('fieldValue') fieldValue: string;
  @Input('value') value: string;
  @Input('options') options: SelectItem[] = [];
  @Output() onSave: EventEmitter<IOnSaveEvent> = new EventEmitter();

  // onValidationError
  @Input('requiredErrorEnum') requiredErrorEnum: number;
  @Input('invalidErrorEnum') invalidErrorEnum: number;
  @Input('RegExp') RegExp: RegExp;
  @Output() onValidationError: EventEmitter<IOnValidationError> = new EventEmitter();
  
  @Input('editNameInEvent') editNameInEvent: string;
  @Output() onOpenEdit: EventEmitter<any> = new EventEmitter();
  @Output() onCloseEdit: EventEmitter<any> = new EventEmitter();


  // if displayControl true
  public save(value: string[]): void  {
    if(value === null || value.length === 0) {

      const $event = {
        validationErrorMsg : this.sharedService.getErrorMsg(this.requiredErrorEnum) , 
      };
      this.onValidationError.emit($event);

    } else if (value.every( v => this.RegExp.test(v))) {

      const $event = {
        fieldName : this.fieldName , 
        value : value
      };
      this.onSave.emit($event);

    } else {

      const $event = {
        validationErrorMsg : this.sharedService.getErrorMsg(this.invalidErrorEnum) , 
      };
      this.onValidationError.emit($event);

    };

  };

  public openEdit(): void {
    if(!this.displayControl) return;
    const $event = {  
      editNameInEvent : this.editNameInEvent
    };
    this.onOpenEdit.emit($event);
  };

  public closeEdit(): void {
    if(!this.displayControl) return;
    const $event = {  
      editNameInEvent : this.editNameInEvent
    };
    this.onCloseEdit.emit($event);
  };


};
