import { SelectItem } from 'primeng/components/common/selectitem';
import { Component, EventEmitter, Input, OnInit, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { EmployeeService } from '../../../service/employee.service';
import { SharedService } from '../../../service/shared.service';
import { LogService } from '../../../service/log.service';
import { AlertService } from '../../../service/alert.service';
import { IEmployeeStatistics, IEmployeeTimerModel } from '../../../models/employeeTimer';
declare const moment;

interface IDataPoints {
  label: string;
  y: number;
  title: string;
};

@Component({
  selector: 'employee-online-statistics',
  templateUrl: './employee-online-statistics.component.html',
  styleUrls: ['./employee-online-statistics.component.scss'],
})
export class EmployeeOnlineStatisticsComponent implements OnInit , OnDestroy {

  constructor(
    private employeeService: EmployeeService,
    private translateService: TranslateService,
    private alertService: AlertService,
    private logService: LogService,
    private sharedService: SharedService
  ){};

  @Input('employee_id') employee_id: string;
  @Input('isAdmin') isAdmin: boolean;

  private translateServiceSubscribe: EventEmitter<any>;

  public dataPoints: IDataPoints[];
  
  public yearOptions: SelectItem[]; 
  public monthOptions: SelectItem[]; 

  public yearModel: any; 
  public monthModel: any; 

  public chartStatisticsLoading: boolean;;

  public getThisEmployeeTimeStatistics;


  private renderChart(statistics): void {
    // create data
    // on change dataPoints
    // render chart <chart-statistics>
    this.dataPoints = this.createDataPoints(statistics);

    this.chartStatisticsLoading = false;
  };

  private createDataPoints(statistics): IDataPoints[] {
    return statistics.map( s => {
      return {
        label : s.date,
        y : parseFloat( ( s.timeMs / (1000 * 60 * 60)  ).toFixed(2) ),
        title : this.sharedService.msToTime(s.timeMs),
      }
    });
  };

  // for admin
  private getEmployeeTimeStatisticsInYearForAdmin(year: string): void {
    // loading
    this.chartStatisticsLoading = true;

    // http
    this.employeeService.getEmployeeTimeStatisticsInYear(this.employee_id, year)
    .then( res => {

      const completeStatisticsOfYear = this.createCompleteStatisticsOfYear(res.statistics , parseInt(year))

      this.renderChart(completeStatisticsOfYear);

    })
    .catch( e => this.alertService.httpError(e) );
      
  };
  private getEmployeeTimeStatisticsInMonthForAdmin(year: string , month: string): void {
    // loading
    this.chartStatisticsLoading = true;

    // http
    this.employeeService.getEmployeeTimeStatisticsInMonth(this.employee_id,year, month)
    .then( res => {

      const completeStatisticsOfMonth = this.createCompleteStatisticsOfMonth(res.statistics , parseInt(year) , parseInt(month)-1 )

      this.renderChart(completeStatisticsOfMonth);

    })
    .catch( e => this.alertService.httpError(e) );
      
  };
  public getThisEmployeeTimeStatisticsForAdmin(): void {
    if(this.monthModel == 'all'){
      this.getEmployeeTimeStatisticsInYearForAdmin(this.yearModel);
    } else {
      this.getEmployeeTimeStatisticsInMonthForAdmin(this.yearModel , this.monthModel);
    };
  }

  // for employee
  private getThisEmployeeTimeStatisticsInYearForEmployee(year: string): void {
    // loading
    this.chartStatisticsLoading = true;

    // http
    this.employeeService.getThisEmployeeTimeStatisticsInYear(year)
    .then( res => {

      const completeStatisticsOfYear = this.createCompleteStatisticsOfYear(res.statistics , parseInt(year))

      this.renderChart(completeStatisticsOfYear);

    })
    .catch( e => this.alertService.httpError(e) );
      
  };
  private getThisEmployeeTimeStatisticsInMonthForEmployee(year: string , month: string): void {
    // loading
    this.chartStatisticsLoading = true;

    // http
    this.employeeService.getThisEmployeeTimeStatisticsInMonth(year, month)
    .then( res => {

      const completeStatisticsOfMonth = this.createCompleteStatisticsOfMonth(res.statistics , parseInt(year) , parseInt(month) -1 )

      this.renderChart(completeStatisticsOfMonth);

    })
    .catch( e => this.alertService.httpError(e) );
      
  };
  public getThisEmployeeTimeStatisticsForEmployee(): void {
    if(this.monthModel == 'all'){
      this.getThisEmployeeTimeStatisticsInYearForEmployee(this.yearModel);
    } else {
      this.getThisEmployeeTimeStatisticsInMonthForEmployee(this.yearModel , this.monthModel);
    };
  };

  public setOptions(): void {

    const date: Date = new Date();
    
    this.yearOptions = [];
    this.monthOptions = [];

    this.yearModel = this.yearModel || date.getFullYear().toString();
    this.monthModel = this.monthModel || (date.getMonth() + 1).toString();

    this.translateService.get('allYearsAsSelectItem')
    .toPromise()
    .then( arr => {
      if((window as any).lang === 'ar') {
        this.yearOptions = arr.map(v => {
          return {
            ...v,
            label : this.sharedService.numberEnToAr(v.label)
          }
        });
      } else {
        this.yearOptions = arr;
      };
    })

    this.translateService.get(['allMonthsAsSelectItem' , 'all_months'])
    .toPromise()
    .then( obj => {
      this.monthOptions = [
        {
          label : obj.all_months,
          value : 'all'
        },
        ...obj.allMonthsAsSelectItem
      ];
    });

  };
  
  private log(text): void {
    this.logService.log('employee-online-statistics' , text);
  };

  private onLangChange(): void {
    this.setOptions();
  };
  private init(){
    this.setOptions();
    if(this.isAdmin && this.employee_id){
      this.getThisEmployeeTimeStatistics = this.getThisEmployeeTimeStatisticsForAdmin;
    } else {
      this.getThisEmployeeTimeStatistics = this.getThisEmployeeTimeStatisticsForEmployee;
    };
    this.getThisEmployeeTimeStatistics();
  };
  public ngOnInit(): void {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange()
      }
    );
  };

  public ngOnDestroy(): void {
    this.translateServiceSubscribe.unsubscribe();
  };


  // -----------------------------------
  // helper to make complate Statistics
  // -----------------------------------
  // ['2018-05-01' , '2018-05-02' , '2018-05-03' , '2018-05-05']
  // result -> ['2018-05-01' , '2018-05-02' , '2018-05-03' , '2018-05-04' , '2018-05-05']
  private createCompleteStatisticsOfYear(statistics: IEmployeeTimerModel[] , year: number): IEmployeeStatistics[] {
    
    const allDatesOfThisYear: string[] = this.getAllDatesOfYear( year );
    const completeStatistic: IEmployeeStatistics[] = [];

    for(let i = 0 , len = allDatesOfThisYear.length ; i < len ; ++i ) {
        
        let tmp: IEmployeeStatistics = {
            timeMs : 0,
            date : allDatesOfThisYear[i]
        }

        // check if this date exist
        for(let x = 0 , _len = statistics.length ; x < _len ; ++x ) {
            if( statistics[x].date === allDatesOfThisYear[i] ) {
                tmp.timeMs = statistics[x].timeMs;
                break;
            };
        };

        completeStatistic.push( tmp );

    };

    return completeStatistic;

  };
  private createCompleteStatisticsOfMonth(statistics: IEmployeeTimerModel[] , year: number , month: number): IEmployeeStatistics[] {
      
      const allDatesOfThisMonth: string[] = this.getAllDatesOfMonth(year , month);
      const completeStatistic: IEmployeeStatistics[] = [];

      for(let i = 0 , len = allDatesOfThisMonth.length ; i < len ; ++i ) {
          
          let tmp: IEmployeeStatistics = {
              timeMs : 0,
              date : allDatesOfThisMonth[i]
          }

          // check if this date exist
          for(let x = 0 , _len = statistics.length ; x < _len ; ++x ) {
              if( statistics[x].date === allDatesOfThisMonth[i] ) {
                  tmp.timeMs = statistics[x].timeMs;
                  break;
              };
          };

          completeStatistic.push( tmp );

      };

      return completeStatistic;

  };
  private getDaysCountOfMonth(year: number, month: number): number {
      var isLeap = ((year % 4) == 0 && ((year % 100) != 0 || (year % 400) == 0));
      return [31, (isLeap ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][ month];
  };
  // use here
  private getAllDatesOfMonth(year: number , month: number): string[] {

      let allDaysOfMonth = [];

      let mLen = this.getDaysCountOfMonth(year , month);

      for(let d = 1 ; d <= mLen ; d += 1) {

          allDaysOfMonth.push( 
            this.getDay(year , month  , d)
          );

      };

      return allDaysOfMonth
      
  };
  private getAllDatesOfYear(year: number): string[] {

      let allDaysOfYear = [];
      
      for(let m = 0 ; m < 12 ; m += 1) {
          allDaysOfYear.push(
              ...this.getAllDatesOfMonth(year , m)
          );
      };

      return allDaysOfYear
      
  };
  private getDay(y: number , m: number , d: number): string {
    const date: string = moment( new Date(y ,  m  , d)  ).format('YYYY-MM-DD');
    return date;
  };
  // -----------------------------------
  // helper to make complate Statistics
  // -----------------------------------

};
 




