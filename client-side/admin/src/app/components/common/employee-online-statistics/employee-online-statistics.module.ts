import { PanelHeadingInProfileModule } from '../panel-heading-in-profile/panel-heading-in-profile.module';
import { EmployeeOnlineStatisticsComponent } from './employee-online-statistics.component';
import { ChartStatisticsModule } from '../chart-statistics/chart-statistics.module';
import { SelectOneModule } from '../select-one/select-one.module';
import { TranslateModuleForChild } from '../../../modules/TranslateModule';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [ 
    CommonModule,
    FormsModule,
    TranslateModuleForChild,

    PanelHeadingInProfileModule,
    SelectOneModule,
    ChartStatisticsModule
  ],
  declarations: [
    EmployeeOnlineStatisticsComponent
  ],
  exports : [
    EmployeeOnlineStatisticsComponent
  ]
})
export class EmployeeOnlineStatisticsModule { }
