import { TranslateService } from '@ngx-translate/core';
import { LoadingService } from '../../../service/loading.service';
import { AlertService } from '../../../service/alert.service';
import { environment } from '../../../../environments/environment';
import { LogService } from '../../../service/log.service';
import { Component, EventEmitter, Output, Input, OnInit } from '@angular/core';
declare const google;

@Component({
  selector: 'map',
  templateUrl : 'map.component.html',
  styleUrls : ['map.component.scss']
})
export class MapComponent implements OnInit {

  constructor(
    private logService: LogService,
    private alertService: AlertService,
    private loadingService: LoadingService,
    private translateService: TranslateService,
  ){};

  @Input('location') location: number[];
  @Input('title') title: string;
  @Input('canEdit') canEdit: boolean;
  @Output('onSave') onSave: EventEmitter<any> = new EventEmitter();

  public edit: boolean = false;
  public gmap: any;
  public options: any;
  public overlays: any[];
  public tempLocation: number[];


  public useGPS(): void {
    if( 'geolocation' in navigator ) {
      this.loadingService.show();
      navigator.geolocation .getCurrentPosition( 
        position => {
          this.tempLocation = [position.coords.longitude , position.coords.latitude];
          this.addMarker();
          this.centerMap();
          this.loadingService.hide();
        },
        () => {
          this.loadingService.hide();
          this.translateService.get('you_cant_use_gps')
          .toPromise()
          .then( msg => {
            this.alertService.alertError( msg )
          });
        } ,
        {
          timeout: 5000,
        }
      );
    } else {
      this.translateService.get('you_cant_use_gps')
      .toPromise()
      .then( msg => {
        this.alertService.alertError( msg )
      });
    };
  };

  private centerMap() {
    this.gmap.setCenter(new google.maps.LatLng( 
      this.tempLocation[1],  
      this.tempLocation[0]
    ));
  }
  private addMarker() {
    const marker = new google.maps.Marker({
      position:{
        lat: this.tempLocation[1], 
        lng: this.tempLocation[0]
      }
    });
    this.overlays = [ marker ]
  }
  
  private log(text): void {
    this.logService.log('map' , text);
  };

  public click($event): void {
    if(this.edit) {
      this.tempLocation = [ $event.latLng.lng() , $event.latLng.lat() ];
      this.addMarker();
    };
  };

  public save(): void {
    this.hideEdit();
    const $event = {
      location : this.tempLocation
    };
    this.onSave.emit($event);
    this.location = this.tempLocation;
  };

  public showEdit(){
    this.edit = true;
  };
  public hideEdit(){
    this.edit = false;
    this.centerMap();
  };

  public setMap(event) {
    this.gmap = event.map;
  }

  // lazy load files
  public isFullLoad: boolean = false;
  private loadGMapScript(): void {
    if(document.getElementById('gmap-script') === null){
      this.log('load new gmap-script');
      const body = <HTMLDivElement> document.body;
      const script = document.createElement('script');
      script.innerHTML = '';
      script.src = environment.lazyload.gmap.script;
      script.async = true;
      script.defer = true;
      script.id = 'gmap-script';
      body.appendChild(script);
      script.onload = () => {
        this.onFullLoad();
      };
    } else {
      this.log('gmap-script used before');
      this.onFullLoad();
    }
  };
  private onFullLoad(): void {
    if(this.location) {
      
      this.tempLocation = this.location.slice();
      this.options = {
        center: { 
          lat: this.location[1] , 
          lng: this.location[0]
        },
        zoom: 7
      };
      this.addMarker();
      this.isFullLoad = true;

    } else {

      fetch(`${environment.url}/api/employee/this-location` , {
        headers: {
          "token": localStorage.getItem('token'),
        },
      })
      .then(function(response) {
        return response.json();
      })
      .then( res => {
        this.location = [res.longitude , res.latitude];
        this.tempLocation = [res.longitude , res.latitude];
        this.options = {
          center: { 
            lat: this.location[1] , 
            lng: this.location[0]
          },
          zoom: 7
        };
        this.addMarker();
        this.isFullLoad = true;
      });
      
    };
  };
  // lazy load files

  public ngOnInit(): void {
    this.loadGMapScript();
  };


};
