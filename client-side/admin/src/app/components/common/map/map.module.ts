import { TranslateModuleForChild } from '../../../modules/TranslateModule';
import { FormsModule } from '@angular/forms';
import { BtnModule } from '../btn/btn.module';
import { MapComponent } from './map.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { PanelHeadingInProfileModule } from '../panel-heading-in-profile/panel-heading-in-profile.module';
import { TooltipModule } from 'primeng/tooltip';
import { GMapModule } from 'primeng/gmap';

@NgModule({
  imports: [ 
    CommonModule,
    ButtonModule,
    PanelHeadingInProfileModule,
    FormsModule,
    TranslateModuleForChild,
    TooltipModule,
    BtnModule,
    GMapModule,
  ],
  declarations: [
    MapComponent
  ],
  exports : [
    MapComponent,
  ]
})
export class MapModule { }
