import { BtnModule } from '../btn/btn.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CustomDataView2Component } from './custom-data-view2.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModuleForChild } from '../../../modules/TranslateModule';
import { SelectOneModule } from '../select-one/select-one.module';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [ 
    CommonModule,
    InfiniteScrollModule,
    TranslateModuleForChild,
    SelectOneModule,
    FormsModule,
    BtnModule,
    ButtonModule,
    RouterModule
  ],
  declarations: [
    CustomDataView2Component
  ],
  exports : [
    CustomDataView2Component,
  ]
})
export class CustomDataView2Module { }
