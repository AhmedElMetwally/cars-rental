import { LogService } from '../../../service/log.service';
import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { environment } from '../../../../environments/environment';
declare const CanvasJS , $;

interface IDataPoints {
  label: string;
  y: number;
  title: string;
};


@Component({
  selector: 'chart-statistics',
  template: `
    <div class="row chart-statistics-parent">

      <!-- loading -->
      <div *ngIf='loading || (!isLoadCanvasJsScript)' class='chart-overlay center-child-in-laptop'>
        <div class="loading-two"></div>  
      </div>
      <!-- ./loading -->

      <div [id]="elementId" style="height: 400px; width: 100%;"></div>

    </div>
  `,
  styleUrls : ['chart-statistics.component.scss']
})
export class ChartStatisticsComponent implements OnInit {

  constructor(
    private logService: LogService,
  ){};

  @Input('elementId') elementId: string;
  @Input('loading') loading: boolean;

  public isLoadCanvasJsScript: boolean = false;
  private chart: any;
  private _dataPoints: IDataPoints[];

  
  private log(text): void {
    this.logService.log('chart-statistics' , text);
  };

  @Input('dataPoints') set dataPoints(_dataPoints: IDataPoints[]) {
    this._dataPoints = _dataPoints;
    this.renderChart();
  };

  public renderChart(): void {

    // check if CanvasJsScript isLoad
    // check if data is valid
    if(this.isLoadCanvasJsScript && Array.isArray(this._dataPoints)) {
      
      this.log('renderChart [OK]')
       
      if(this.chart) {

        this.chart.options.data[0].dataPoints = this._dataPoints;

      } else {

        const dataSeries = { 
          dataPoints : this._dataPoints,
          type: "line" , // line , spline , area
          toolTipContent: "{label} - ( {title} )",    
          color: "#6edd19",
        };
        const options = {
          zoomEnabled: false,
          animationEnabled: true,
          axisY: {
            includeZero: false,
            lineThickness: 1,
          },
          data: [dataSeries],
        };
        this.chart = new CanvasJS.Chart(this.elementId, options);
      };

      this.chart.render();
      $('chart-statistics .canvasjs-chart-credit').remove();
        
    };

  };

  // lazy load js file
  public loadCanvasJsScript(): void {
    //  tsconfig.app.json -> "module": "esNext", 
    // import('./../../../../assets/canvasjs/canvas.js').then( () => {
    //   this.log('load canvas-script-file');
    //   this.afterLoadCanvasJsScript();
    // });
    if(document.getElementById('canvasjs-script') === null){
      this.log('load new canvasjs-script v1.7.0');
      const body = <HTMLDivElement> document.body;
      const script = document.createElement('script');
      script.innerHTML = '';
      script.src = environment.lazyload.canvasjs.script;
      script.async = true;
      script.defer = true;
      script.id = 'canvasjs-script';
      body.appendChild(script);
      script.onload = () => {
        this.onFullLoad();
      };
    } else {
      this.log('canvasjs-script v1.7.0 used before');
      this.onFullLoad();
    }
  };
  public onFullLoad(): void {
    this.isLoadCanvasJsScript = true;
    this.renderChart();
  };
  // ./lazy load js file

  public ngOnInit(): void {
    this.loadCanvasJsScript();
    this.loading = true;
  };


};

