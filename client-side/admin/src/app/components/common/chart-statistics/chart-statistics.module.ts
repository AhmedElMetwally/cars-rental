import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartStatisticsComponent } from './chart-statistics.component';

@NgModule({
  imports: [ 
    CommonModule,
  ],
  declarations: [
    ChartStatisticsComponent
  ],
  exports : [
    ChartStatisticsComponent,
  ]
})
export class ChartStatisticsModule { }
