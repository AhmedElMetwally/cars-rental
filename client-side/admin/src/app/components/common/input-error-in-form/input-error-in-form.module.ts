import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputErrorInFormComponent } from './input-error-in-form.component';

@NgModule({
  imports: [ 
    CommonModule,
  ],
  declarations: [
    InputErrorInFormComponent
  ],
  exports : [
    InputErrorInFormComponent,
  ]
})
export class InputErrorInFormModule { }
