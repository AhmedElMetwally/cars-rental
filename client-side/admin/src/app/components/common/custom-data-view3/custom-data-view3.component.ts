import { SharedService } from '../../../service/shared.service';
import { LogService } from '../../../service/log.service';
import { SelectItem } from 'primeng/components/common/selectitem';
import { Component, EventEmitter, Input, Output, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'custom-data-view3',
  templateUrl: './custom-data-view3.component.html',
})
export class CustomDataView3Component implements OnInit ,  OnDestroy , OnChanges {

  constructor(
    private translateService: TranslateService,
    private logService: LogService,
    private sharedService: SharedService,
  ){};

  @Input('loading') loading: boolean = true;
  
  @Input('filterBy1Title') filterBy1Title: string;
  @Input('filterBy1Icon') filterBy1Icon: string;
  
  @Input('filterBy2Title') filterBy2Title: string;
  @Input('filterBy2Icon') filterBy2Icon: string;

  @Input('bottomLengthTitle') bottomLengthTitle: string; 
  @Input('itemsCount') itemsCount: number; 

  @Input('filterBy1Options') set filterBy1Options(arr: SelectItem[]) {
    const clear: SelectItem[] = [
      {
        label : this.filterBy1Title,
        value : false
      }
    ]
    this._filterBy1Options = [...clear , ...arr];
  };
  @Input('filterBy2Options') set filterBy2Options(arr: SelectItem[]) {
    const clear: SelectItem[] = [
      {
        label : this.filterBy2Title,
        value : false
      }
    ]
    this._filterBy2Options = [...clear , ...arr];
  };

  @Output('onLazyLoad') onLazyLoad: EventEmitter<any> = new EventEmitter<any>();
  @Output('onReset') onReset: EventEmitter<any> = new EventEmitter<any>();

  public filterBy1Model: any = false;
  public filterBy2Model: any = false;
  
  public _filterBy1Options: SelectItem[] = [];
  public _filterBy2Options: SelectItem[] = [];

  public sortByOptions: SelectItem[] = [];
  public sortByModel: string;


  public searchMode: boolean = false;
  public searchModel: string;

  private translateServiceSubscribe: EventEmitter<any>;

  public clickSearch(): void {
    if(this.searchModel && this.searchModel.trim() !== '') {
      this.searchMode = true;
      this.reset();
    }
  }
  public closeSearch(): void{
    this.searchModel = '';
    this.searchMode = false;
    this.reset();
  }

  public scroll(): void {
    const $event = {
      sortBy : this.sortByModel,
      filterBy1 : this.filterBy1Model === false ? false : this.filterBy1Model ,
      filterBy2 : this.filterBy2Model || false,
      searchModel : this.searchModel,
      searchMode : this.searchMode
    };
    this.onLazyLoad.emit($event);
  };

  private log(text): void {
    this.logService.log('custom-data-view3' , text);
  };
  private setOptions(): void {
    this.sortByOptions = [];
    this.sortByModel = this.sortByModel || '-createdAt';
    this.translateService.get('sortByOptions')
    .toPromise()
    .then(
      v => this.sortByOptions = v
    );
  };
  private onLangChange(): void {
    this.setOptions();
  };
  public init(): void {
    this.setOptions();
    this.scroll();
  };
  public ngOnInit(): void  {
    this.init();
    this.translateServiceSubscribe = this.translateService.onLangChange.subscribe(
      () => {
        this.onLangChange();
      }
    );
  };

  public ngOnDestroy(): void  {
    this.translateServiceSubscribe.unsubscribe();
  };

  public ngOnChanges(): void {

  };

  public reset(): void {
    const $event = {
      sortBy : this.sortByModel,
      filterBy1 : this.filterBy1Model === false ? false : this.filterBy1Model ,
      filterBy2 : this.filterBy2Model || false,
      searchModel : this.searchModel,
      searchMode : this.searchMode
    };
    this.onReset.emit($event);
  };


  public getItemsCount(): string {
    return (window as any).lang === 'ar' ? 
      this.sharedService.numberEnToAr(this.itemsCount.toString()) 
    : 
      this.itemsCount.toString();
  }

};
 