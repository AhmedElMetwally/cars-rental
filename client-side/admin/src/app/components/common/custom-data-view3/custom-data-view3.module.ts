import { BtnModule } from '../btn/btn.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CustomDataView3Component } from './custom-data-view3.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModuleForChild } from '../../../modules/TranslateModule';
import { SelectOneModule } from '../select-one/select-one.module';
import { FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';

@NgModule({
  imports: [ 
    CommonModule,
    InfiniteScrollModule,
    TranslateModuleForChild,
    SelectOneModule,
    FormsModule,
    BtnModule,
    ButtonModule,
    InputTextModule,
  ],
  declarations: [
    CustomDataView3Component
  ],
  exports : [
    CustomDataView3Component,
  ]
})
export class CustomDataView3Module { }
