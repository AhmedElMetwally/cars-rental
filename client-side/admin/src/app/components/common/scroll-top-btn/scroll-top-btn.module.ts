import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollTopBtnComponent } from './scroll-top-btn.component';
import { ButtonModule } from 'primeng/button';
@NgModule({
  imports: [ 
    CommonModule,
    ButtonModule,
  ],
  declarations: [
    ScrollTopBtnComponent
  ],
  exports : [
    ScrollTopBtnComponent,
  ]
})
export class ScrollTopBtnModule { }
