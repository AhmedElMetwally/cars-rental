import { IEmployeeGroupModel } from './employeeGroupModel';

export interface IEmployeeModel {
    _id: string;
    
    employeeGroup: string|IEmployeeGroupModel|any;

    name: string;
    email: string;

    password: string;

    roles: [string];
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

    isOnline?: Number; // set in [GET] http request

};


export const RegExpEmployeeModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    name : /^[\u0600-\u06FF\w-\s]{1,40}$/,
    email : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,

    password : /^.{8,30}$/ ,

    deleted : /^[0-1]$/, 
};
