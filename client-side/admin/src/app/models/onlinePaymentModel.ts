import { IClientModel } from "./clientModel";
import { IGuestModel } from "./guestModel";
import { IOnlinePaymentModel } from './onlinePaymentModel';
import { IBookingModel } from './bookingModel';

export interface IOnlinePaymentModel {
    
    client: string|null|IClientModel;
    guest: string|null|IGuestModel;
    booking: string|null|IBookingModel;

    bookingCode: string;
    iqama: string;

    daysCount: number;
    pricePerDay: number;
    discountPerDay: number;
    total: number;

    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export const RegExpOnlinePaymentModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    
    bookingCode : /^[0-9]{3}-[0-9]{1,9}$/, // 999-999999999
    iqama : /^[0-9]{1,30}$/, // 30 length of Number
    
    daysCount : /^[1-9]([0-9]{0,3})$/, // 9999
    pricePerDay : /^[1-9]([0-9]{0,3})$/, // 9999
    discountPerDay : /^[1-9]([0-9]{0,3})$/, // 9999
    total : /^[1-9]([0-9]{0,5})$/, // 999999

    deleted : /^[0-1]$/, 
};

