import { IClientModel } from "./clientModel";
import { INationalityModel } from './nationalityModel';

export interface IClientModel {
    
    nationality: string|INationalityModel;

    name: string;
    
    email: string;
    phone: string;
    
    password: string;
    
    iqama: string;
    iqamaExpiryDate: Date;
    
    drivingLicence: string;
    drivingLicenceExpiryDate: Date;
    
    birthday: Date;
    
    address: string;
    job: string;
    signupFrom: number;
    
    roles: [string];

    image?: string;
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};


export const RegExpClientModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    name : /^[\u0600-\u06FF\w-\s]+$/,

    email : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    phone : /^[0-9\-\+]{9,15}$/,

    password : /^.{8,30}$/ ,

    iqama : /^[0-9]{1,30}$/, // 30 length of Number
    iqamaExpiryDate : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,

    drivingLicence : /^[0-9]{1,30}$/, // 30 length of Number
    drivingLicenceExpiryDate : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,

    birthday : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,

    address : /^[\u0600-\u06FF\w-\s]+$/,
    job : /^[\u0600-\u06FF\w-\s]+$/,
    signupFrom : /^[0-1]$/, 


    deleted : /^[0-1]$/, 
};

