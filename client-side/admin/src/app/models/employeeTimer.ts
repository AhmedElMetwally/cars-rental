export interface IEmployeeTimerModel {
    
    employee_id: string;
    date: string;
    timeMs: number;

    createdAt?: Date;
    updatedAt?: Date;

};
export interface IEmployeeStatistics {
    date: string;
    timeMs: number;
};
export const RegExpEmployeeTimerModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    year : /^20[1-2][0-9]$/,
    month : /^([1-9]|1[0-2])$/,
};


