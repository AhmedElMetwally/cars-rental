import { IBranchModel } from './branchModel';
import { ICarModel } from './carModel';
import { IClientModel } from "./clientModel";
import { IGuestModel } from "./guestModel";
import { IOnlinePaymentModel } from './onlinePaymentModel';
import { ICarGroupModel } from './carGroupModel';

export interface IBookingModel {
    
    client: string|null|IClientModel;
    guest: string|null|IGuestModel;
    car: string|null|ICarModel;
    onlinePayment: string|null|IOnlinePaymentModel;

    carGroup: string|ICarGroupModel;
    pickUpBranch: string|IBranchModel;
    returnBranch: string|IBranchModel;
    
    bookingCode: string;
    iqama: string;
    
    pickUpDate: Date;
    returnDate: Date;
    expiredAt: Date;
    
    bookingType: number;

    daysCount: number;
    pricePerDay: number;
    discountPerDay: number;
    total: number;
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};


export const RegExpBookingModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    
    bookingCode : /^[0-9]{1,12}$/, // 999-999999999
    iqama : /^[0-9]{1,30}$/, // 30 length of Number
    
    pickUpDate : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,
    returnDate : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,
    expiredAt : /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,
    
    bookingType : /^[0-5]$/,
    
    daysCount : /^[1-9]([0-9]{0,3})$/, // 9999
    pricePerDay : /^[1-9]([0-9]{0,3})$/, // 9999
    discountPerDay : /^[0-9]([0-9]{0,3})$/, // 9999
    total : /^[1-9]([0-9]{0,5})$/, // 999999

    deleted : /^[0-1]$/, 
};
