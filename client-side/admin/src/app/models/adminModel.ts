export interface IAdminModel {
    _id: string;
    
    email: string;
    password: string;

    roles: [string];

    createdAt?: Date;
    updatedAt?: Date;

};

export const RegExpAdminModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,
    email : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    password : /^.{8,30}$/ ,
}