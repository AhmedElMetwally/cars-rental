export interface IEmployeeActionModel {
    
    client: string|null;
    car: string|null;
    carGroup: string|null;
    carType: string|null;
    carModel: string|null;
    booking: string|null;
    branch: string|null;

    employee_id: string;
    
    screen: number;

    body: JSON;
    query: JSON;

    itemName: EitemName;

    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export enum EitemName {
    client,
    car,
    carGroup,
    carType,
    carModel,
    booking,
    branch,
};


