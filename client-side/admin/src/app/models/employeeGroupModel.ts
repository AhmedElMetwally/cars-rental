import { IBranchModel } from "./branchModel";

export interface IEmployeeGroupModel {
    _id: string;
    
    branches: string[]|IBranchModel[];
    
    screens: number[];
   
    nameEn: string;
    nameAr: string;
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};

export const RegExpEmployeeGroupModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    nameEn : /^[\w-\s]+$/,
    nameAr : /^[\u0600-\u06FF\w-\s]+$/,

    screens : /^[0-9]{1,3}$/, // [num , ...etc]

    deleted : /^[0-1]$/, 
};
