import { ICarGroupModel } from './carGroupModel';

export interface ICarModel {

    _id: string;

    carGroup: string|ICarGroupModel;

    plateNumber: string;
    gearType: number;
    status: number;
    
    deleted?: number;

    createdAt?: Date;
    updatedAt?: Date;

};



export const RegExpCarModel = {
    _id : /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i,

    plateNumber : /^[0-9]{4}-[\u0600-\u06FF]{1}\s[\u0600-\u06FF]{1}\s[\u0600-\u06FF]{1}$/,
    gearType : /^[0-1]$/, 
    status : /^[0-9]$/, 

    deleted : /^[0-1]$/, 
};