import { HttpClient, HttpHeaders , HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';


@Injectable()
export class BranchService {
    
    constructor(
        private http: HttpClient
    ){ };

    private get headers(): HttpHeaders {
        return new HttpHeaders()
        .append('Content-Type', 'application/json')
        .append('token', localStorage.getItem('token'))
    };


    // in employee
    public getBranch(branch_id: string): Promise<any> {
        const params = new HttpParams()
            .set('branch_id', branch_id)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/branch`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public editBranch(obj): Promise<any> {
        return this.http.put(
            `${environment.url}/api/employee/branch/edit`,
            obj,
            { 
                headers : this.headers 
            },
        ).toPromise()
    };
    public getBranches(page: number , limit: number , sortBy: string ): Promise<any> {
        const params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/branches`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public getBranchesSearch(page: number  , limit: number , searchQuery: string): Promise<any> {
        const params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('searchQuery', searchQuery)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/branches-search`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public addBranch(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/employee/branch/add`,
            obj, 
            { 
                headers : this.headers 
            },
        ).toPromise();
    };
    public restoreBranch(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/employee/branch/restore`,
            obj,
            { 
                headers : this.headers 
            }
        ).toPromise()
    };
    public deleteBranch(branch_id: string): Promise<any> {
        const params = new HttpParams()
        .set('branch_id', branch_id);
        return this.http.delete(
            `${environment.url}/api/employee/branch/delete`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise()
    };
    public deleteBranchImage(branch_id: string , image: string): Promise<any> {
        const params = new HttpParams()
            .set('branch_id', branch_id)
            .set('image', image)
        ;
        return this.http.delete(
            `${environment.url}/api/employee/branch/delete-image`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise()
    };
    public addBranchImage(obj): Promise<any> {
        const headers = new HttpHeaders()
        .append('token', localStorage.getItem('token'))
        return this.http.post(
            `${environment.url}/api/employee/branch/add-image`,
            obj,
            { 
                headers 
            },
        ).toPromise()
    };

    // options
    public getAllCities(): Promise<any> {
        return this.http.get(
            `${environment.url}/api/employee/view/all-cities`,
            { 
                headers : this.headers 
            },
        ).toPromise()
    }







    
};
