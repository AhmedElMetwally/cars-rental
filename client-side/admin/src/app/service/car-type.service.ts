import { HttpClient, HttpHeaders , HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';


@Injectable()
export class CarTypeService {
    
    constructor(
        private http: HttpClient
    ){ };

    private get headers(): HttpHeaders {
        return new HttpHeaders()
        .append('Content-Type', 'application/json')
        .append('token', localStorage.getItem('token'))
    };


    // in employee
    public addCarType(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/employee/car-type/add`,
            obj, 
            { 
                headers : this.headers 
            },
        ).toPromise();
    };
    public addCarModel(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/employee/car-model/add`,
            obj, 
            { 
                headers : this.headers 
            },
        ).toPromise();
    };



    // options
    public getAllCarTypes(): Promise<any> {
        return this.http.get(
           `${environment.url}/api/employee/view/all-car-types`,
            { 
                headers : this.headers 
            },
        ).toPromise();
    };
    public getAllCarModelsByCarType(carType_id: string): Promise<any> {
        const params = new HttpParams()
        .set('carType_id', carType_id);
        return this.http.get(
           `${environment.url}/api/employee/view/all-car-models-by-car-type`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };




    
}
