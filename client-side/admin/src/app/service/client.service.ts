import { HttpClient, HttpHeaders , HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';


@Injectable()
export class ClientService {
    
    constructor(
        private http: HttpClient
    ){ };

    private get headers(): HttpHeaders {
        return new HttpHeaders()
        .append('Content-Type', 'application/json')
        .append('token', localStorage.getItem('token'))
    };


    // in employee
    public getClient(client_id: string): Promise<any> {
        const params = new HttpParams()
            .set('client_id', client_id)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/client`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public editClient(obj): Promise<any> {
        return this.http.put(
            `${environment.url}/api/employee/client/edit`,
            obj,
            { 
                headers : this.headers 
            },
        ).toPromise()
    };
    public editClientPassword(obj): Promise<any> {
        return this.http.put(
            `${environment.url}/api/employee/client/edit-password`,
            obj,
            { 
                headers : this.headers 
            },
        ).toPromise()
    };
    public getClients(page: number , limit: number , sortBy: string): Promise<any> {
        const params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/clients`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public getClientsSearch(page: number , limit: number , searchQuery: string): Promise<any> {
        const params = new HttpParams()
        .set('page', page.toString())
        .set('limit', limit.toString())
        .set('searchQuery', searchQuery)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/clients-search`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public addClient(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/employee/client/add`,
            obj, 
            { 
                headers : this.headers 
            },
        ).toPromise();
    };
    public restoreClient(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/employee/client/restore`,
            obj,
            { 
                headers : this.headers 
            }
        ).toPromise()
    };
    public deleteClient(client_id: string): Promise<any> {
        const params = new HttpParams()
        .set('client_id', client_id);
        return this.http.delete(
            `${environment.url}/api/employee/client/delete`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise()
    };
    public viewBookingsForClient(page: number , limit: number , sortBy: string , client_id: string): Promise<any> {
        const params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
            .set('client_id', client_id)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/bookings-for-client`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public viewOnlinePaymentsForClient(page: number , limit: number , sortBy: string , client_id: string): Promise<any> {
        const params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
            .set('client_id', client_id)
        ;
        return this.http.get(
           `${environment.url}/api/employee/client/online-payments-for-client`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public addClientImage(obj): Promise<any> {
        const headers = new HttpHeaders()
        .append('token', localStorage.getItem('token'))
        return this.http.post(
            `${environment.url}/api/employee/client/add-image`,
            obj,
            { 
                headers 
            },
        ).toPromise()
    };

    // options
    public getAllNationalities(): Promise<any> {
        return this.http.get(
           `${environment.url}/api/employee/view/all-nationalities`,
            { 
                headers : this.headers 
            },
        ).toPromise();
    };
    



    
}
