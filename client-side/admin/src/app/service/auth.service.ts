import { LogService } from './log.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { SharedService } from './shared.service';
import * as io from 'socket.io-client';
import { IEmployeeModel } from '../models/employeeModel';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
    
    constructor(
      private http: HttpClient,
      private router: Router,
      private sharedService: SharedService,
      private logService: LogService,
    ) { };

    private socket;

    private log(text): void {
      this.logService.log('AuthService' , text)
    };


    private get headers(): HttpHeaders {
      return new HttpHeaders()
      .append('Content-Type', 'application/json')
      .append('token', localStorage.getItem('token'))
    };

    public isEmployee(): boolean{
      return localStorage.getItem('employee') !== null;
    };

    public isAdmin(): boolean{
      return localStorage.getItem('admin') !== null;
    };

    public logout(): void {
      this.sharedService.clearLocalStorage();
      this.router.navigate(['employee-signin']);
      if( this.socket ) this.socket.disconnect();
    }

    public employeeSigninInSocketIO(token: string): void {
      this.socket = io.connect( `${environment.url}/employee` , {
        query : {
            token 
        },
      });

      this.socket
      .on('connect' , () => {
        this.log('employee socketIo connect');
      })
      .on('disconnect' , () => {
        this.log('employee socketIo disconnect');
      });
    };

    public employeeSignin(email: string , password: string): Promise<any> {
      return this.http.post(
        `${environment.url}/api/employee/signin`,
        { 
          email , 
          password 
        },
      )
      .toPromise()
    };

    public adminSignin( email: string , password: string ): Promise<any> {
      return this.http.post(
        environment.url + '/api/admin/signin',
        { 
          email , 
          password 
        },
      ).toPromise();
    };

    public refreshEmployeeToken(): Promise<any> {
      return this.http.get(
        `${environment.url}/api/employee/refresh-token`,
        {
          headers : this.headers
        }
      )
      .toPromise()
    };

    public refreshAdminToken(): Promise<any> {
      return this.http.get(
        `${environment.url}/api/admin/refresh-token`,
        {
          headers : this.headers
        }
      ).toPromise()
    };

    public checkScreen(screen: number): boolean {
      if((window as any).employeeScreens) {
        return (window as any).employeeScreens.indexOf( screen ) !== -1;
      } else {
        try {
          const employee: IEmployeeModel = JSON.parse(localStorage.getItem('employee'));
          const employeeScreens: number[] = employee.employeeGroup.screens;
          (window as any).employeeScreens = employeeScreens;
          return employeeScreens.indexOf(screen) !== -1;
        } catch ( err ) {
          this.logout();
          return false;
        };
      };
    };
    public checkBranch(branch: string): boolean {
      if((window as any).employeeBranches) {
        return (window as any).employeeBranches.indexOf(branch) !== -1;
      } else {
        try {
          const employee: IEmployeeModel = JSON.parse(localStorage.getItem('employee'))
          const employeeBranches: string[] = employee.employeeGroup.branches;
          (window as any).employeeBranches = employeeBranches;
          return employeeBranches.indexOf(branch) !== -1;
        } catch ( err ) {
          this.logout();
          return false;
        };
      };
    };





    

    
}
