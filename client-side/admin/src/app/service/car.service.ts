import { HttpClient, HttpHeaders , HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';


@Injectable()
export class CarService {
    
    constructor(
        private http: HttpClient
    ){ };

    private get headers(): HttpHeaders {
        return new HttpHeaders()
        .append('Content-Type', 'application/json')
        .append('token', localStorage.getItem('token'))
    };


    // in employee
    public getCar(car_id: string): Promise<any> {
        const params = new HttpParams()
            .set('car_id', car_id)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/car`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public editCar(obj): Promise<any> {
        return this.http.put(
            `${environment.url}/api/employee/car/edit`,
            obj,
            { 
                headers : this.headers 
            },
        ).toPromise()
    };
    public getCars(page: number , limit: number , sortBy: string): Promise<any> {
        const params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/cars`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public getCarsByFilter(page: number , limit: number , sortBy: string, carGroup_id: string): Promise<any> {
        const params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
            .set('carGroup_id', carGroup_id)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/cars-by-filter`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public getCarsSearch(page: number , limit: number , searchQuery: string): Promise<any> {
        const params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('searchQuery', searchQuery)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/cars-search`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public addCar(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/employee/car/add`,
            obj, 
            { 
                headers : this.headers 
            },
        ).toPromise();
    };
    public restoreCar(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/employee/car/restore`,
            obj,
            { 
                headers : this.headers 
            }
        ).toPromise()
    };
    public deleteCar(car_id: string): Promise<any> {
        const params = new HttpParams()
        .set('car_id', car_id);
        return this.http.delete(
            `${environment.url}/api/employee/car/delete`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise()
    };
 


    // options
    public getAllCarGroupsCanThisEmployeeAccess(): Promise<any> {
        return this.http.get(
           `${environment.url}/api/employee/view/all-car-groups-can-this-employee-access`,
            { 
                headers : this.headers 
            },
        ).toPromise();
    };




    
}
