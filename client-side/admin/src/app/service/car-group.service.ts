import { HttpClient, HttpHeaders , HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';


@Injectable()
export class CarGroupService {
    
    constructor(
        private http: HttpClient
    ){ };

    private get headers(): HttpHeaders {
        return new HttpHeaders()
        .append('Content-Type', 'application/json')
        .append('token', localStorage.getItem('token'))
    };


    // in employee
    public getCarGroup(carGroup_id: string): Promise<any> {
        const params = new HttpParams()
            .set('carGroup_id', carGroup_id)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/car-group`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public editCarGroup(obj): Promise<any> {
        return this.http.put(
            `${environment.url}/api/employee/car-group/edit`,
            obj,
            { 
                headers : this.headers 
            },
        ).toPromise()
    };
    public getCarGroups(page: number , limit: number , sortBy: string): Promise<any> {
        const params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/car-groups`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public getCarGroupsByFilter(page: number , limit: number , sortBy: string, branch_id: string, carType_id: string): Promise<any> {
        let params;
        if(carType_id && !branch_id){
            params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
            .set('carType_id', carType_id)
        } else if(branch_id && !carType_id){
            params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
            .set('branch_id', branch_id)
        } else if (carType_id && branch_id) {
            params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
            .set('branch_id', branch_id)
            .set('carType_id', carType_id)
        } else {
            params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
        };
        
        return this.http.get(
           `${environment.url}/api/employee/view/car-groups-by-filter`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public addCarGroup(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/employee/car-group/add`,
            obj, 
            { 
                headers : this.headers 
            },
        ).toPromise();
    };
    public restoreCarGroup(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/employee/car-group/restore`,
            obj,
            { 
                headers : this.headers 
            }
        ).toPromise()
    };
    public deleteCarGroup(carGroup_id: string): Promise<any> {
        const params = new HttpParams()
        .set('carGroup_id', carGroup_id);
        return this.http.delete(
            `${environment.url}/api/employee/car-group/delete`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise()
    };
    public deleteCarGroupImage(carGroup_id: string , image: string): Promise<any> {
        const params = new HttpParams()
            .set('carGroup_id', carGroup_id)
            .set('image', image)
        ;
        return this.http.delete(
            `${environment.url}/api/employee/car-group/delete-image`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise()
    };
    public addCarGroupImage(obj): Promise<any> {
        const headers = new HttpHeaders()
        .append('token', localStorage.getItem('token'))
        return this.http.post(
            `${environment.url}/api/employee/car-group/add-image`,
            obj,
            { 
                headers 
            },
        ).toPromise()
    };


    // options
    public getAllBranchesCanThisEmployeeAccess(): Promise<any> {
        return this.http.get(
           `${environment.url}/api/employee/view/all-branches-can-this-employee-access`,
            { 
                headers : this.headers 
            },
        ).toPromise();
    };
    public getAllCarTypes(): Promise<any> {
        return this.http.get(
           `${environment.url}/api/employee/view/all-car-types`,
            { 
                headers : this.headers 
            },
        ).toPromise();
    };
    public getAllCarModelsByCarType(carType_id: string): Promise<any> {
        const params = new HttpParams()
        .set('carType_id', carType_id);
        return this.http.get(
           `${environment.url}/api/employee/view/all-car-models-by-car-type`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };




    
};
