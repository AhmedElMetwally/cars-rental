import { HttpClient, HttpHeaders , HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';


@Injectable()
export class GuestService {
    
    constructor(
        private http: HttpClient
    ){ };

    private get headers(): HttpHeaders {
        return new HttpHeaders()
        .append('Content-Type', 'application/json')
        .append('token', localStorage.getItem('token'))
    };


    // in employee
    public getGuest(guest_id: string): Promise<any> {
        const params = new HttpParams()
            .set('guest_id', guest_id)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/guest`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public getGuests(page: number , limit: number , sortBy: string): Promise<any> {
        const params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/guests`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public getGuestsSearch(page: number , limit: number , searchQuery: string): Promise<any> {
        const params = new HttpParams()
        .set('page', page.toString())
        .set('limit', limit.toString())
        .set('searchQuery', searchQuery)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/guests-search`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
   



    
}
