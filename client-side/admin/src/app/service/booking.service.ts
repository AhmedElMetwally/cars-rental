import { HttpClient, HttpHeaders , HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';


@Injectable()
export class BookingService {
    
    constructor(
        private http: HttpClient
    ){ };

    private get headers(): HttpHeaders {
        return new HttpHeaders()
        .append('Content-Type', 'application/json')
        .append('token', localStorage.getItem('token'))
    };


    // in employee
    public getBooking(booking_id: string): Promise<any> {
        const params = new HttpParams()
            .set('booking_id', booking_id)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/booking`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public getBookings(page: number , limit: number , sortBy: string): Promise<any> {
        const params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/bookings`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public getBookingsByFilter(page: number , limit: number , sortBy: string , bookingType: any , branch_id: any): Promise<any> {
        let params;
        if(bookingType !== false && branch_id === false){
            params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
            .set('bookingType', bookingType)
        } else if(branch_id !== false  && bookingType === false){
            params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
            .set('branch_id', branch_id)
        } else {
            params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
            .set('branch_id', branch_id)
            .set('bookingType', bookingType)
        } 
        return this.http.get(
           `${environment.url}/api/employee/view/bookings-by-filter`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public getBookingsSearch(page: number , limit: number , searchQuery: string): Promise<any> {
        const params = new HttpParams()
        .set('page', page.toString())
        .set('limit', limit.toString())
        .set('searchQuery', searchQuery)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/bookings-search`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public addCarToBooking(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/employee/booking/add-car`,
            obj,
            { 
                headers : this.headers 
            }
        ).toPromise()
    };
    public editBookingDate(obj: any): Promise<any> {
        return this.http.put(
            `${environment.url}/api/employee/booking/edit-date`,
            obj,
            { 
                headers : this.headers 
            }
        ).toPromise()
    };
    public editBookingExpiredAt(obj: any): Promise<any> {
        return this.http.put(
            `${environment.url}/api/employee/booking/edit-expiredAt`,
            obj,
            { 
                headers : this.headers 
            }
        ).toPromise()
    };
    public editBookingPricePerDay(obj: any): Promise<any> {
        return this.http.put(
            `${environment.url}/api/employee/booking/edit-pricePerDay`,
            obj,
            { 
                headers : this.headers 
            }
        ).toPromise()
    };
    public editBookingDiscountPerDay(obj: any): Promise<any> {
        return this.http.put(
            `${environment.url}/api/employee/booking/edit-discountPerDay`,
            obj,
            { 
                headers : this.headers 
            }
        ).toPromise()
    };
    public cancelBooking(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/employee/booking/cancel`,
            obj,
            { 
                headers : this.headers 
            }
        ).toPromise()
    };
    public approveBooking(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/employee/booking/approve`,
            obj,
            { 
                headers : this.headers 
            }
        ).toPromise()
    };
    public activeBooking(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/employee/booking/active`,
            obj,
            { 
                headers : this.headers 
            }
        ).toPromise()
    };
    public restoreBooking(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/employee/booking/restore`,
            obj,
            { 
                headers : this.headers 
            }
        ).toPromise()
    };
    public deleteBooking(booking_id: string): Promise<any> {
        const params = new HttpParams()
        .set('booking_id', booking_id);
        return this.http.delete(
            `${environment.url}/api/employee/booking/delete`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise()
    };
 
    // options
    public getAllBranchesCanThisEmployeeAccess(): Promise<any> {
        return this.http.get(
           `${environment.url}/api/employee/view/all-branches-can-this-employee-access`,
            { 
                headers : this.headers 
            },
        ).toPromise();
    };
    public getAllCarsWhoCanAddToBooking(booking_id: string): Promise<any> {
        const params = new HttpParams()
        .set('booking_id', booking_id);

        return this.http.get(
            `${environment.url}/api/employee/view/all-cars-who-can-add-to-booking`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };


};
