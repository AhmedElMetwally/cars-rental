import { HttpClient, HttpHeaders , HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';


@Injectable()
export class EmployeeGroupService {
    
    constructor(
        private http: HttpClient
    ){ };

    private get headers(): HttpHeaders {
        return new HttpHeaders()
        .append('Content-Type', 'application/json')
        .append('token', localStorage.getItem('token'))
    };


    // in admin
    public getEmployeeGroup(employeeGroup_id: string): Promise<any> {
        const params = new HttpParams()
            .set('employeeGroup_id', employeeGroup_id)
        ;
        return this.http.get(
           `${environment.url}/api/admin/view/employee-group`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public editEmployeeGroup(obj): Promise<any> {
        return this.http.put(
            `${environment.url}/api/admin/employee-group/edit`,
            obj,
            { 
                headers : this.headers 
            },
        ).toPromise()
    };
    public getEmployeeGroups(page: number , limit: number , sortBy: string ): Promise<any> {
        const params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
        ;
        return this.http.get(
           `${environment.url}/api/admin/view/employee-groups`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public getEmployeeGroupsSearch(page: number  , limit: number , searchQuery: string): Promise<any> {
        const params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('searchQuery', searchQuery)
        ;
        return this.http.get(
           `${environment.url}/api/admin/view/employee-groups-search`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public addEmployeeGroup(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/admin/employee-group/add`,
            obj, 
            { 
                headers : this.headers 
            },
        ).toPromise();
    };
    public restoreEmployeeGroup(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/admin/employee-group/restore`,
            obj,
            { 
                headers : this.headers 
            }
        ).toPromise()
    }
    public deleteEmployeeGroup(employeeGroup_id: string): Promise<any> {
        const params = new HttpParams()
        .set('employeeGroup_id', employeeGroup_id);
        return this.http.delete(
            `${environment.url}/api/admin/employee-group/delete`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise()
    }


    // options
    public getAllBranches(): Promise<any> {
        return this.http.get(
           `${environment.url}/api/admin/view/all-branches`,
            { 
                headers : this.headers 
            },
        ).toPromise();
    };
    public getAllEmployeeGroups(): Promise<any> {
        return this.http.get(
           `${environment.url}/api/admin/view/all-employee-groups`,
            { 
                headers : this.headers 
            },
        ).toPromise();
    };




    
}
