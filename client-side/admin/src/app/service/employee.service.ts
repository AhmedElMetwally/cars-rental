import { HttpClient, HttpHeaders , HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';


@Injectable()
export class EmployeeService {
    
    constructor(
        private http: HttpClient
    ){ };

    private get headers(): HttpHeaders {
        return new HttpHeaders()
        .append('Content-Type', 'application/json')
        .append('token', localStorage.getItem('token'))
    };

    
    // this
    public getThisEmployee(): Promise<any> {
        return this.http.get(
           `${environment.url}/api/employee/view-this-employee`,
            { 
                headers : this.headers 
            },
        ).toPromise();
    };
    public editThisEmployee(obj): Promise<any> {
        return this.http.put(
            `${environment.url}/api/employee/edit-this-employee`,
            obj,
            { 
                headers : this.headers 
            },
        ).toPromise()
    };
    public editThisEmployeePassword(obj): Promise<any> {
        return this.http.put(
           `${environment.url}/api/employee/edit-this-employee-password`,
            obj,
            { 
                headers : this.headers 
            },
        ).toPromise()
    };
    public getThisEmployeeTimeStatisticsInYear(year: string): Promise<any> {
        const params = new HttpParams()
            .set('year', year)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/this-employee-time-statistics-in-year`,
            { 
                params ,
                headers : this.headers 
            },
        ).toPromise()
    };
    public getThisEmployeeTimeStatisticsInMonth(year: string , month: string): Promise<any> {
        const params = new HttpParams()
            .set('year', year)
            .set('month', month)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/this-employee-time-statistics-in-month`,
            { 
                params ,
                headers : this.headers 
            },
        ).toPromise()
    };
    public getThisEmployeeActions(page: number , limit: number , sortBy: string): Promise<any> {
        const params = new HttpParams()
            .set('limit', limit.toString())
            .set('page', page.toString())
            .set('sortBy', sortBy)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/this-employee-actions`,
            { 
                params ,
                headers : this.headers 
            },
        ).toPromise()
    };
    public getThisEmployeeActionsByItemNameFilter(page: number , limit: number , sortBy: string, itemName: string): Promise<any> {
        const params = new HttpParams()
            .set('itemName', itemName)
            .set('limit', limit.toString())
            .set('page', page.toString())
            .set('sortBy', sortBy)
        ;
        return this.http.get(
           `${environment.url}/api/employee/view/this-employee-actions-by-item-name-filter`,
            { 
                params ,
                headers : this.headers 
            },
        ).toPromise()
    };


    // in admin
    public getEmployee(employee_id: string): Promise<any> {
        const params = new HttpParams()
            .set('employee_id', employee_id)
        ;
        return this.http.get(
           `${environment.url}/api/admin/view/employee`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public editEmployee(obj): Promise<any> {
        return this.http.put(
            `${environment.url}/api/admin/employee/edit`,
            obj,
            { 
                headers : this.headers 
            },
        ).toPromise()
    };
    public editEmployeePassword(obj): Promise<any> {
        return this.http.put(
           `${environment.url}/api/admin/employee/edit-password`,
            obj,
            { 
                headers : this.headers 
            },
        ).toPromise()
    };
    public getEmployees(page: number , limit: number , sortBy: string ): Promise<any> {
        const params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
        ;
        return this.http.get(
           `${environment.url}/api/admin/view/employees`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public getEmployeesSearch(page: number  , limit: number , searchQuery: string): Promise<any> {
        const params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('searchQuery', searchQuery)
        ;
        return this.http.get(
           `${environment.url}/api/admin/view/employees-search`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public getEmployeesByEmployeeGroupFilter(page: number  , limit: number , sortBy: string  , employeeGroup_id: string): Promise<any> {
        const params = new HttpParams()
            .set('page', page.toString())
            .set('limit', limit.toString())
            .set('sortBy', sortBy)
            .set('employeeGroup_id', employeeGroup_id)
        ;
        return this.http.get(
           `${environment.url}/api/admin/view/employees-by-employee-group-filter`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise();
    };
    public getEmployeeActions(page: number, limit: number , sortBy: string , employee_id: string): Promise<any> {
        const params = new HttpParams()
            .set('limit', limit.toString())
            .set('page', page.toString())
            .set('sortBy', sortBy)
            .set('employee_id', employee_id)
        ;
        return this.http.get(
           `${environment.url}/api/admin/view/employee-actions`,
            { 
                params ,
                headers : this.headers 
            },
        ).toPromise()
    };
    public getEmployeeActionsByItemNameFilter(page: number , limit: number , sortBy: string, itemName: string , employee_id: string): Promise<any> {
        const params = new HttpParams()
            .set('itemName', itemName)
            .set('limit', limit.toString())
            .set('page', page.toString())
            .set('sortBy', sortBy)
            .set('employee_id', employee_id)
        ;
        return this.http.get(
           `${environment.url}/api/admin/view/employee-actions-by-item-name-filter`,
            { 
                params ,
                headers : this.headers 
            },
        ).toPromise()
    };
    public getEmployeeTimeStatisticsInYear(employee_id: string , year: string): Promise<any> {
        const params = new HttpParams()
            .set('year', year)
            .set('employee_id', employee_id)
        ;
        return this.http.get(
           `${environment.url}/api/admin/view/employee-time-statistics-in-year`,
            { 
                params ,
                headers : this.headers 
            },
        ).toPromise()
    };
    public getEmployeeTimeStatisticsInMonth(employee_id: string , year: string , month: string): Promise<any> {
        const params = new HttpParams()
            .set('year', year)
            .set('month', month)
            .set('employee_id', employee_id)
        ;
        return this.http.get(
           `${environment.url}/api/admin/view/employee-time-statistics-in-month`,
            { 
                params ,
                headers : this.headers 
            },
        ).toPromise()
    };
    public addEmployee(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/admin/employee/add`,
            obj, 
            { 
                headers : this.headers 
            },
        ).toPromise();
    };
    public restoreEmployee(obj: any): Promise<any> {
        return this.http.post(
            `${environment.url}/api/admin/employee/restore`,
            obj,
            { 
                headers : this.headers 
            }
        ).toPromise()
    };
    public deleteEmployee(employee_id: string): Promise<any> {
        const params = new HttpParams()
        .set('employee_id', employee_id);
        return this.http.delete(
            `${environment.url}/api/admin/employee/delete`,
            { 
                params,
                headers : this.headers 
            },
        ).toPromise()
    };

    // options
    public getAllEmployeeGroups(): Promise<any> {
        return this.http.get(
           `${environment.url}/api/admin/view/all-employee-groups`,
            { 
                headers : this.headers 
            },
        ).toPromise();
    };




    
}
