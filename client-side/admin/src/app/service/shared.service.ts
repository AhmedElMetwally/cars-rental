import { mainErrorEnum , mainErrorEnumEn , mainErrorEnumAr } from './../../../../../shared/enums/error.enum';
import { RegExpCityModel } from '../models/cityModel';
import { RegExpBookingModel } from '../models/bookingModel';
import { EbookingType, EbookingTypeAr, EbookingTypeEn } from '../enums/booking.enum';
import { RegExpNationalityModel } from '../models/nationalityModel'
import {  EsignupFromAr , EsignupFromEn , EsignupFrom } from '../enums/client.enum';
import { RegExpClientModel } from '../models/clientModel';
import { EgearType, EgearTypeEn, EgearTypeAr } from '../enums/car.enum';
import { RegExpCarModel } from '../models/carModel';
import { Escreens , EscreensAr, EscreensEn } from '../enums/screens.enum';
import { RegExpCarModelModel } from '../models/carModelModel';
import { RegExpCarTypeModel } from '../models/carTypeModel';
import { RegExpCarGroupModel } from '../models/carGroupModel';
import { RegExpBranchModel } from '../models/branchModel';
import { RegExpEmployeeGroupModel } from '../models/employeeGroupModel';
import { RegExpEmployeeModel } from '../models/employeeModel';
import { RegExpAdminModel } from '../models/adminModel';
import { Injectable  } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { EstatusAr, Estatus , EstatusEn } from '../enums/car.enum';
declare let $;


@Injectable({
    providedIn: 'root'
})
export class SharedService {
    
    constructor(){ };
    
    public mainErrorEnum = mainErrorEnum;
    public mainErrorEnumEn = mainErrorEnumEn;
    public mainErrorEnumAr = mainErrorEnumAr;
    
    public Escreens = Escreens;
    public EscreensAr = EscreensAr;
    public EscreensEn = EscreensEn;
    
    public Estatus = Estatus;
    public EstatusAr = EstatusAr;
    public EstatusEn = EstatusEn;
    
    public EgearType = EgearType;
    public EgearTypeAr = EgearTypeAr;
    public EgearTypeEn = EgearTypeEn;

    public EsignupFrom = EsignupFrom;
    public EsignupFromAr = EsignupFromAr;
    public EsignupFromEn = EsignupFromEn;

    public EbookingType = EbookingType;
    public EbookingTypeAr = EbookingTypeAr;
    public EbookingTypeEn = EbookingTypeEn;
    
    public mainRegExp = {
        RegExpEmployeeModel,
        RegExpAdminModel,
        RegExpEmployeeGroupModel,
        RegExpBranchModel,
        RegExpCarGroupModel,        
        RegExpCarModelModel,
        RegExpCarTypeModel,
        RegExpCarModel,
        RegExpClientModel,
        RegExpNationalityModel,
        RegExpBookingModel,
        RegExpCityModel,
    };

    public clearLocalStorage(): void {
        localStorage.removeItem('token');
        localStorage.removeItem('admin');
        localStorage.removeItem('employee');
    };

    public setTitle(title: string): void {
        $('title').text(title);
    };

    public getErrorMsg(errorEnum: mainErrorEnum): string {
        if((window as any).lang == 'ar') {
            return mainErrorEnumAr[errorEnum];
        } else {
            return mainErrorEnumEn[errorEnum];
        };
    }

    public customValidator(requiredErrorEnum: mainErrorEnum , invalidErrorEnum: mainErrorEnum , regExp: RegExp): any {
        return (control: AbstractControl) => {
            if(Array.isArray(control.value) ) {
                if ( ! control.value.every( v => regExp.test(v)) ) {
                    return {
                        errorEnum : invalidErrorEnum
                    };
                } else {
                    return null
                }; 
            } else {
                if(control.value === '' || control.value === null){
                    return {
                        errorEnum : requiredErrorEnum
                    };
                } else if ( !regExp.test(control.value) ) {
                    return {
                        errorEnum : invalidErrorEnum
                    };
                } else {
                    return null
                }; 
            };
        }; 
    };

    public msToTime(duration) {
        let seconds = parseInt((duration / 1000) % 60 + '');
        let minutes = parseInt((duration / (1000 * 60)) % 60 + '');
        let hours = parseInt((duration / (1000 * 60 * 60)) % 24 + '');
      
        let h = (hours < 10) ? "0" + hours : hours;
        let m = (minutes < 10) ? "0" + minutes : minutes;
        let s = (seconds < 10) ? "0" + seconds : seconds;
      
        return h + ":" + m 
    };

    public numberEnToAr(str: string): string {
        const numberEnToArMap: string[] = [
            '٠', 
            '١',
            '٢',
            '٣', 
            '٤',
            '٥',
            '٦',
            '٧',
            '٨', 
            '٩'
        ];
        return str.replace(/\d/g, function($0) { return numberEnToArMap[$0]})
    };
    public numberArToEn(str: string): string {
        str = str.replace(/٠/g, '0');
        str = str.replace(/١/g, '1');
        str = str.replace(/٢/g, '2');
        str = str.replace(/٣/g, '3');
        str = str.replace(/٤/g, '4');
        str = str.replace(/٥/g, '5');
        str = str.replace(/٦/g, '6');
        str = str.replace(/٧/g, '7');
        str = str.replace(/٨/g, '8');
        str = str.replace(/٩/g, '9');
        return str;
    };

    

}