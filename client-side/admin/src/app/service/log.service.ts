import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  public log(component: string, text: any): void {
    if(typeof text === "object"){
      console.log(
        '%c %s : ', 
        'background: #222; color: #5fc00c;font-size:14px;padding:4px 3px;' , 
        component,
        text
      );
    } else {
      console.log(
        '%c %s : %s ', 
        'background: #222; color: #5fc00c;font-size:14px;padding:4px 3px;' , 
        component,
        text
      );
    }
   
  };

};
