export enum Escreens {

    edit_client,
    edit_client_password,
    delete_client,
    restore_client,
    edit_client_image,
  
    add_car,
    edit_car,
    delete_car,
    restore_car,
  
    add_car_type,
    edit_car_type,
    delete_car_type,
    restore_car_type,
  
    add_car_model,
    edit_car_model,
    delete_car_model,
    restore_car_model,
  
    add_car_group,
    edit_car_group,
    delete_car_group,
    restore_car_group,
    edit_car_group_image,
  
    add_branch,
    edit_branch,
    delete_branch,
    restore_branch,
    edit_branch_image,

    edit_booking,
    cancel_booking,
    approve_booking,
    active_booking,
    delete_booking,
    restore_booking,
  
};
export enum EscreensEn {

    'Edit Client',
    'Edit Client Password',
    'Delete Client',
    'Restore Client',
    'Edit Client Image',
  
    'Add Car',
    'Edit Car',
    'Delete Car',
    'Restore Car',
  
    'Add Car Type',
    'Edit Car Type',
    'Delete Car Type',
    'Restore Car Type',

    'Add Car Model',
    'Edit Car Model',
    'Delete Car Model',
    'Restore Car Model',
  
    'Add Car Group',
    'Edit Car Group',
    'Delete Car Group',
    'Restore Car Group',
    'Edit Car Group Image',
  
    'Add Branch',
    'Edit Branch',
    'Delete Branch',
    'Restore Branch',
    'Edit Branch Image',

    'Edit Booking',
    'Cancel Booking',
    'Approve Booking',
    'Active Booking',
    'Delete Booking',
    'Restore Booking',

};
export enum EscreensAr {

    'تعديل العميل',
    'تعديل كلمة مرور العميل',
    'حذف العميل',
    'استرجاع العميل',
    'تعديل صورة العميل',
  
    'اضافة سيارة',
    'تعديل سيارة',
    'حذف سيارة',
    'استرجاع سيارة',
  
    'اضافة نوع سيارة',
    'تعديل نوع سيارة',
    'حذف نوع سيارة',
    'استرجاع نوع سيارة',

    'اضافة موديل سيارة',
    'تعديل موديل سيارة',
    'حذف موديل سيارة',
    'استرجاع موديل سيارة',

    'اضافة جروب سيارة',
    'تعديل جروب سيارة',
    'حذف جروب سيارة',
    'استرجاع جروب سيارة',
    'تعديل صورة جروب سيارة',


    'اضافة فرع',
    'تعديل فرع',
    'حذف فرع',
    'استرجاع فرع',
    'تعديل صورة فرع',
  

    'تعديل حجز',
    'عدم موفقة علي حجز',
    'موافقة علي حجز',
    'تفعيل حجز',
    'حذف حجز',
    'استرجاع حجز',

};

