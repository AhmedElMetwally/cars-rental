export enum EgearType {
    'manual',
    'automatic'
};
export enum EgearTypeEn {
    'Manual',
    'Automatic'
};

export enum EgearTypeAr {
    'مانوال',
    'اوتوماتك'
};



export enum Estatus {
    'hiring',
    'accident',
    'sold',
    'steal',
    'available',
    'not_available'
};
export enum EstatusEn {
    'Hiring',
    'Accident',
    'Sold',
    'Steal',
    'Available',
    'Not Available'
};
export enum EstatusAr {
    'محجوزة',
    'حادثة',
    'تم البيع',
    'مسروقة',
    'متاحة',
    'غير متاحة'
};

