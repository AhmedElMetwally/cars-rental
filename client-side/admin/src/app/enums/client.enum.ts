export enum EsignupFrom {
    'normal_signup',
    'booking_before_and_signup',
};
export enum EsignupFromAr {
    'تسجيل عادي',
    'حجز من قبل ثم اكمل التسجيل',
};
export enum EsignupFromEn {
    'Normal Signup',
    'Booking Before And Signup',
};

