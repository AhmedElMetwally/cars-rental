import { SharedService } from '../service/shared.service';
import { IBranchModel } from '../models/branchModel';
import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
	name:'showBranches',
	pure: false
})
export class ShowBranchesPipe implements PipeTransform {
	constructor(
		private sharedService: SharedService
	) {}
	transform(arr: IBranchModel[] = []) {
		const len = arr.length;
		if((window as any).lang == 'ar'){
			if(len == 0) {
				return 'لا يوجد'
			} else if(len == 1) {
				return `${arr[0].nameAr} <strong>فقط</strong>`
			} else if(len == 2) {
				return `${arr[0].nameAr}<strong> و ${ this.sharedService.numberEnToAr((len - 1).toString()) } أخر</strong>`
			} else {
				return `${arr[0].nameAr}<strong> و ${ this.sharedService.numberEnToAr((len - 1).toString()) } اخرين</strong>`
			}
		} else {
			if(len == 0) {
				return 'Not Found'
			} else if(len == 1) {
				return `${arr[0].nameEn}<strong> Only </strong>`
			} else if(len == 2) {
				return `${arr[0].nameEn}<strong> And  ${len - 1} Other</strong>`
			} else {
				return `${arr[0].nameEn}<strong> And${len - 1} Others</strong>`
			}
		};
	}
}