import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
	name:'getValueByLang',
	pure:false
})
export class getValueByLangPipe implements PipeTransform {
	constructor() {}
	transform(obj: string ,arg: string) {
		if( ! obj ) return '';
		
		const [en , ar] = arg.split(',');
		return obj[ (window as any).lang === 'ar' ? ar : en] || '';
	}
}