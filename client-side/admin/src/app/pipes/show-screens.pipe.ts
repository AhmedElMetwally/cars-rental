import { SharedService } from '../service/shared.service';
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
	name:'showScreens',
	pure: false
})
export class ShowScreensPipe implements PipeTransform {
	constructor(
		private sharedService: SharedService
	) {}
	transform(arr: number[]) {
		const len = arr.length;
		if((window as any).lang == 'ar'){
			if(len == 0) {
				return 'لا يوجد'
			} else if(len == 1) {
				return `${this.sharedService.EscreensAr[arr[0]]}<strong> فقط </strong>`
			} else if(len == 2) {
				return `${this.sharedService.EscreensAr[arr[0]]}<strong> و ${ this.sharedService.numberEnToAr((len - 1).toString()) } أخر</strong>`
			} else {
				return `${this.sharedService.EscreensAr[arr[0]]}<strong> و ${ this.sharedService.numberEnToAr((len - 1).toString()) } اخرين</strong>`
			}
		} else {
			if(len == 0) {
				return 'Not Found'
			} else if(len == 1) {
				return `${this.sharedService.EscreensEn[arr[0]]}<strong> Only </strong>`
			} else if(len == 2) {
				return `${this.sharedService.EscreensEn[arr[0]]}<strong> And ${len - 1} Other</strong>`
			} else {
				return `${this.sharedService.EscreensEn[arr[0]]}<strong> And ${len - 1} Others</strong>`
			}
		};
	}
}