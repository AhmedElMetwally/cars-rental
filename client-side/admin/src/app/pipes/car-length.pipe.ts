import { SharedService } from '../service/shared.service';
import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
	name:'carLength',
	pure: false
})
export class carLengthPipe implements PipeTransform {
	constructor(
		private sharedService: SharedService
	) {}
	transform(len: number = 0) {
		let res ;
		if(len === 0) {
			res = (window as any).lang === 'ar' ? "لا يوجد سيارات" : 'No Cars' ;
		} else if(len === 1) {
			res = (window as any).lang === 'ar' ? "سيارة واحدة" : 'One Car' ;
		} else if(len === 2) {
			res = (window as any).lang === 'ar' ? "سيارتان" : 'Two Cars' ;
		} else {
			res = (window as any).lang === 'ar' 
			? this.sharedService.numberEnToAr(
				len.toString()
			) + ' سيارات' : len + ' Cars';
		};

		return res;
		
	}
}