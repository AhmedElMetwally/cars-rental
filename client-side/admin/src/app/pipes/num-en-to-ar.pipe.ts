import { SharedService } from '../service/shared.service';
import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
	name:'numEnToAr',
	pure: false
})
export class numEnToArPipe implements PipeTransform {
	constructor(
		private sharedService: SharedService
	) {}
	transform(num: string|number = '') {
		return (window as any).lang === 'ar' 
		? this.sharedService.numberEnToAr(
			num.toString()
		) : num;
	}
}