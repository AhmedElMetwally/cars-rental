export const environment = {
  production: false,
  url : 'http://localhost:4000',
  lazyload : {
    quill : {
      script : '/assets/quill/quill.js',
      style : '/assets/quill/quill.snow.css'
    },
    canvasjs : {
      script : '/assets/canvasjs/canvasjs-v1.7.0.js',
    },
    gmap : {
      script : 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBbR8Fqy8mi1U44Dk_2zGgvnlmJRZ0-YT4'
    }
  }
};
